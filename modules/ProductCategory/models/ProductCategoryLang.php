<?php

namespace app\modules\ProductCategory\models;

use Yii;

/**
 * This is the model class for table "ProductCategoryLang".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property string $Title
 * @property string $LangID
 * @property string $Text
 *
 * @property ProductCategory $category
 */
class ProductCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductCategoryLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CategoryID', 'Title', 'LangID', 'Text'], 'required'],
            [['CategoryID'], 'integer'],
            [['Text'], 'string'],
            [['Title'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['CategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['CategoryID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CategoryID' => 'Category ID',
            'Title' => 'Title',
            'LangID' => 'Lang ID',
            'Text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['ID' => 'CategoryID']);
    }
}
