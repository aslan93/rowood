<?php

namespace app\modules\ProductCategory;

use app\modules\Admin\Admin;

/**
 * ProductCategory module definition class
 */
class ProductCategory extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\ProductCategory\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
