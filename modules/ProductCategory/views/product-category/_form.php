<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\ProductCategory\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-category-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <?php
    $initialPreview = [];
    $initialPreviewConfig = [];
    if ($model->imagePath){
        $initialPreview[] = Html::img($model->imagePath,['width' => 200]);
    }
    $initialPreviewConfig[] = [
        'url' => \yii\helpers\Url::to(['/admin/ecommerce/product-category/product-category/image-delete']),
        'key' => $model->ID,
    ];

    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
            echo FileInput::widget([
                'name' => 'Image',
                'options'=>['accept'=>'image/*','multiple' => true],
                'pluginOptions' => [
                    'overwriteInitial'=>false,
                    'maxFileSize'=>2800,
                    'fileActionSettings' => [
                        'fileActionSettings' => [
                            'showZoom' => false,
                            'showDelete' => true,
                        ],
                    ],
                    'browseClass' => 'btn btn-success',
                    'uploadClass' => 'btn btn-info',
                    'removeClass' => 'btn btn-danger',
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                    'initialPreviewConfig' => $initialPreviewConfig,
                ],

            ]);

            ?>

        </div></div>
    <div>
    <div class="col-md-6">
    <?= $form->field($model, "Type")->dropDownList( $model->typeList ,['id'=>'category-type']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "ProductType")->dropDownList( $model->productTypeList ,['id'=>'product-type']) ?>
    </div>
    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langModel->LangID),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
                'model' => $model,
            ]),
        ];
    }
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
