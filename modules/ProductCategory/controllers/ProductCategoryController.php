<?php

namespace app\modules\ProductCategory\controllers;


use Yii;
use app\modules\ProductCategory\models\ProductCategory;
use app\modules\ProductCategory\models\ProductCategorySearch;
use app\modules\Ecommerce\controllers\EcommerceController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends EcommerceController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductCategory();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->Image = $this->saveImage($model);
            if($model->save()){
            return $this->redirect(['view', 'id' => $model->ID]);
            }
        }
            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing ProductCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->Image = $this->saveImage($model);
            if($model->save()){


            return $this->redirect(['view', 'id' => $model->ID]);
        } }
            return $this->render('update', [
                'model' => $model,
            ]);

    }

    /**
     * Deletes an existing ProductCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $image = UploadedFile::getInstanceByName('Image');
        if($image){


            $file = md5(microtime(true)) . '.' . $image->extension;


            if ($image->saveAs(Yii::getAlias("@webroot/uploads/productCategory/$file")))
            {
                return $file;
            }
        }
        return $model->Image;
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        $model = ProductCategory::findOne($key);
        $model->Image = '*';
        $model->save();
        return '{}';
    }
}
