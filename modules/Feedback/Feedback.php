<?php

namespace app\modules\Feedback;

use app\modules\Admin\Admin;
/**
 * Feedback module definition class
 */
class Feedback extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Feedback\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
