<?php

namespace app\modules\Feedback\models;

use Yii;

/**
 * This is the model class for table "Feedback".
 *
 * @property integer $ID
 * @property string $Nume
 * @property string $Prenume
 * @property string $Email
 * @property string $Telefon
 * @property string $Mesaj
 * @property string $CreatedAt
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nume', 'Email', 'Mesaj'], 'required'],
            [['Mesaj'], 'string'],
            [['CreatedAt'], 'safe'],
            [['Nume', 'Prenume'], 'string', 'max' => 255],
            [['Email'], 'string', 'max' => 25],
            [['Telefon'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nume' => 'Nume',
            'Prenume' => 'Prenume',
            'Email' => 'Email',
            'Telefon' => 'Telefon',
            'Mesaj' => 'Mesaj',
            'CreatedAt' => 'Created At',
        ];
    }
}
