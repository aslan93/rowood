<?php

namespace app\modules\Dashboard;

use app\modules\Admin\Admin;

/**
 * dashboard module definition class
 */
class Dashboard extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Dashboard\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
