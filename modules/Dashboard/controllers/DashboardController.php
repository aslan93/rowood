<?php

namespace app\modules\Dashboard\controllers;

use Yii;
use app\modules\Admin\controllers\AdminController;

class DashboardController extends AdminController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}
