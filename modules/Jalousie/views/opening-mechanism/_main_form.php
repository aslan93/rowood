<?php

    /* @var $form yii\widgets\ActiveForm */

    use app\modules\Jalousie\models\Jalousie;

?>

<br />

<div class="row">
    <div class="col-md-6">
        
        <?= $form->field($model, 'Type')->dropDownList(Jalousie::getTypesList('-')) ?>
        
        <?= $form->field($model, 'HasOptions')->checkbox() ?>
        
    </div>
</div>
<br />