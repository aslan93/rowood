<?php

    use app\modules\Jalousie\models\Model;

?>

<br />

<div class="row">
    <div class="col-md-6">
        
        <?= $form->field($model, 'ModelID')->dropdownList(Model::getList('-')) ?>
        
        <?= $form->field($model, 'Percent')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
        
    </div>
</div>
<br />