<?php

    /* @var $form yii\widgets\ActiveForm */

?>

<br />

<div class="row">
    <div class="col-md-6">
        
        <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'step' => 0.01, 'min' => 0]); ?>
        
    </div>
</div>
<br />