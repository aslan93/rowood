<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Jalousie\models\Jalousie */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Roll Material',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roll Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="connection-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
