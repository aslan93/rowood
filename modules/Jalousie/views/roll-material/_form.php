<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Windowsill\models\Screw */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="color-form">
    
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
        <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'step' => 0.01, 'min' => 0]); ?>
    
        <?= $this->render('_languages_form', [
            'form' => $form,
            'model' => $model,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>