<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Jalousie\models\Jalousie */

$this->title = Yii::t('app', 'Create Jalousie');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jalousies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connection-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
