<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Decoration\models\Decoration */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Color',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Model'), 'url' => ['model/update', 'id' => Yii::$app->request->get('modelID')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Model Colors'), 'url' => ['model/update', 'id' => Yii::$app->request->get('modelID'), 'tab' => 'colors']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="decoration-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
