<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Decoration\models\Decoration */

$this->title = Yii::t('app', 'Create Decoration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Model'), 'url' => ['model/update', 'id' => Yii::$app->request->get('modelID')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Model Colors'), 'url' => ['model/update', 'id' => Yii::$app->request->get('modelID'), 'tab' => 'colors']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="decoration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
