<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Roll\models\RollColor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Roll Color',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roll Colors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="windowsill-color-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
