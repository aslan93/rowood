<?php

    use yii\widgets\Pjax;
    use yii\bootstrap\Html;
    use app\components\GridView\GridView;

?>

<br />

<p>
    <?= Html::a(Yii::t('app', 'Create Model color'), ['model-color/create', 'modelID' => $model->ID], ['class' => 'btn btn-success']) ?>
</p>

<?php Pjax::begin(); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'lang.Title',
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function($model)
                {
                    return Html::img($model->imagePath, ['height' => 60]);
                },
                'filter' => false,
            ],
            'Price',

            [
                'class' => 'app\components\GridView\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['model-color/update', 'id' => $model->ID, 'modelID' => Yii::$app->request->get('id')], [
                            'title' => Yii::t('app', 'Edit'),
                            'data-pjax' => 0,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['model-color/delete', 'id' => $model->ID, 'modelID' => Yii::$app->request->get('id')], [
                            'title' => Yii::t('app', 'Delete'),
                            'data-pjax' => 0,
                            'onclick' => 'return confirm("Confirmati?");',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>