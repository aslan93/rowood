<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Jalousie\models\Model */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="color-form">
    
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
    <?php
    
        $items = [
            [
                'label' => Yii::t('app', 'Date generale'),
                'content' => $this->render('_main_form', [
                    'form' => $form,
                    'model' => $model,
                ]),
                'active' => true,
            ],
            [
                'label' => Yii::t('app', 'Descriere'),
                'content' => $this->render('_languages_form', [
                    'form' => $form,
                    'model' => $model,
                ]),
            ],
        ];
        
        if (!$model->isNewRecord)
        {
            $items[] = [
                'label' => Yii::t('app', 'Colors'),
                'content' => $this->render('_colors_form', [
                    'form' => $form,
                    'model' => $model,
                    'searchModel'  => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
            ];
        }
    
    ?>
    
        <?= Tabs::widget([
            'items' => $items,
        ]); ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<?php if (Yii::$app->request->get('tab', false) == 'colors') { ?>
<?php $this->registerJs("
    $('.color-form #w7 li:last a').click();
"); ?>
<?php } ?>
