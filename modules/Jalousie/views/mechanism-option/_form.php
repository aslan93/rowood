<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Jalousie\models\OpeningMechanism;

/* @var $this yii\web\View */
/* @var $model app\modules\Windowsill\models\Screw */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="color-form">
    
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'step' => 0.01]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'OpeningMechanismID')->dropDownList(OpeningMechanism::getList('-')) ?>
        </div>
    </div>
    
        <?= $this->render('_languages_form', [
            'form' => $form,
            'model' => $model,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>