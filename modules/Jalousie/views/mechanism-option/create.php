<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Jalousie\models\Jalousie */

$this->title = Yii::t('app', 'Create Mechanism option');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mechanism options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connection-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
