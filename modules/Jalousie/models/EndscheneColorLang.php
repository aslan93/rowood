<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "EndscheneColorLang".
 *
 * @property integer $ID
 * @property integer $EndscheneColorID
 * @property string $LangID
 * @property string $Title
 *
 * @property EndscheneColor $windowsillColor
 */
class EndscheneColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EndscheneColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EndscheneColorID', 'LangID', 'Title'], 'required'],
            [['EndscheneColorID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['EndscheneColorID'], 'exist', 'skipOnError' => true, 'targetClass' => EndscheneColor::className(), 'targetAttribute' => ['EndscheneColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'EndscheneColorID' => Yii::t('app', 'Roll Color ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndscheneColor()
    {
        return $this->hasOne(EndscheneColor::className(), ['ID' => 'EndscheneColorID']);
    }
}
