<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "MechanismOptionLang".
 *
 * @property integer $ID
 * @property integer $MechanismOptionID
 * @property string $LangID
 * @property string $Title
 *
 */
class MechanismOptionLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MechanismOptionLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MechanismOptionID', 'LangID', 'Title'], 'required'],
            [['MechanismOptionID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['MechanismOptionID'], 'exist', 'skipOnError' => true, 'targetClass' => MechanismOption::className(), 'targetAttribute' => ['MechanismOptionID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MechanismOptionID' => Yii::t('app', 'MechanismOption ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }
    
}