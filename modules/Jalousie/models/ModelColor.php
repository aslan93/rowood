<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model as YiiModel;
use yii\helpers\Url;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ModelColor".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property ModelColorLang[] $decorationLangs
 */
class ModelColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ModelColor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'Price', 'ModelID'], 'required'],
            [['ModelID'], 'integer'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/jalousie/' . $this->Image);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ModelColorLang::className(), ['ModelColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ModelColorLang::className(), ['ModelColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ModelColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        TagDependency::invalidate(Yii::$app->cache, Jalousie::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ModelColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (YiiModel::loadMultiple($langModels, Yii::$app->request->post()) && YiiModel::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }

    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->with('lang');
        $result = ArrayHelper::map($query->all(),'ID','lang.Title');
        return $return + $result;
    }

    public static function getImageList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image', 'lang.Title');
        return $return + $result;
    }
}
