<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "EndscheneColor".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property EndscheneColorLang[] $windowsillColorLangs
 */
class EndscheneColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EndscheneColor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(EndscheneColorLang::className(), ['EndscheneColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(EndscheneColorLang::className(), ['EndscheneColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new EndscheneColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/jalousie/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->EndscheneColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
