<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "ModelLang".
 *
 * @property integer $ID
 * @property integer $ModelID
 * @property string $LangID
 * @property string $Title
 *
 * @property Model $windowsillColor
 */
class ModelSizeLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ModelSizeLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ModelSizeID', 'LangID', 'Title'], 'required'],
            [['ModelSizeID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ModelSizeID'], 'exist', 'skipOnError' => true, 'targetClass' => ModelSize::className(), 'targetAttribute' => ['ModelSizeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ModelSizeID' => Yii::t('app', 'Model Size'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelSize()
    {
        return $this->hasOne(Model::className(), ['ID' => 'ModelSizeID']);
    }
}
