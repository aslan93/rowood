<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "OpeningMechanismLang".
 *
 * @property integer $ID
 * @property integer $OpeningMechanismID
 * @property string $LangID
 * @property string $Title
 *
 */
class OpeningMechanismLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OpeningMechanismLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OpeningMechanismID', 'LangID', 'Title', 'OptionsName'], 'required'],
            [['OpeningMechanismID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'OptionsName'], 'string', 'max' => 255],
            [['OpeningMechanismID'], 'exist', 'skipOnError' => true, 'targetClass' => OpeningMechanism::className(), 'targetAttribute' => ['OpeningMechanismID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OpeningMechanismID' => Yii::t('app', 'OpeningMechanism ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'OptionsName' => Yii::t('app', 'Options Name'),
        ];
    }
    
}