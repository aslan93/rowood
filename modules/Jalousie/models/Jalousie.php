<?php

namespace app\modules\Jalousie\models;

use Yii;
//use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Jalousie".
 *
 * @property integer $ID
 * @property integer $HasOptions
 * @property string $Type
 *
 * @property JalousieLang $lang
 * @property JalousieLang[] $langs
 */
class Jalousie extends \yii\db\ActiveRecord
{
    
    const TypeOnWindow = 'OnWindow';
    const TypeFrontal = 'Frontal';
    const TypeInternal = 'Internal';

    public $Price = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Jalousie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type'], 'required'],
            [['Type'], 'string', 'max' => 50],
            [['HasOptions'], 'integer'],
            [['HasOptions'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'HasOptions' => Yii::t('app', 'Has Options'),
            'Type' => Yii::t('app', 'Type'),
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(JalousieLang::className(), ['JalousieID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(JalousieLang::className(), ['JalousieID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new JalousieLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->JalousieID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getTypesList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        
        return $res + [
            self::TypeOnWindow => Yii::t('app', 'On Window'),
            self::TypeFrontal => Yii::t('app', 'Frontal'),
            self::TypeInternal => Yii::t('app', 'Internal'),
        ];
    }
    
    public static function getList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        $models = ArrayHelper::map(self::find()->with(['lang'])->all(), 'ID', 'lang.Title');
        return $res + $models;
    }
    
    public function getModels()
    {
        return $this->hasMany(Model::className(), ['JalousieID' => 'ID'])->with('lang');
    }
    
}
