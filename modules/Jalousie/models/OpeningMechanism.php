<?php

namespace app\modules\Jalousie\models;

use Yii;
//use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "OpeningMechanism".
 *
 * @property integer $ID
 * @property integer $HasOptions
 * @property string $Type
 *
 * @property OpeningMechanismLang $lang
 * @property OpeningMechanismLang[] $langs
 */
class OpeningMechanism extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OpeningMechanism';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Status'], 'default', 'value' => 'Active'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Status' => Yii::t('app', 'Type'),
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(OpeningMechanismLang::className(), ['OpeningMechanismID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(OpeningMechanismLang::className(), ['OpeningMechanismID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new OpeningMechanismLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->OpeningMechanismID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        $models = ArrayHelper::map(self::find()->with(['lang'])->all(), 'ID', 'lang.Title');
        return $res + $models;
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getMechanismOptions()
    {
        return $this->hasMany(MechanismOption::className(), ['OpeningMechanismID' => 'ID'])->with('lang');
    }
    
}
