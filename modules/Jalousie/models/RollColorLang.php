<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "RollColorLang".
 *
 * @property integer $ID
 * @property integer $RollColorID
 * @property string $LangID
 * @property string $Title
 *
 * @property RollColor $windowsillColor
 */
class RollColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RollColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RollColorID', 'LangID', 'Title'], 'required'],
            [['RollColorID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['RollColorID'], 'exist', 'skipOnError' => true, 'targetClass' => RollColor::className(), 'targetAttribute' => ['RollColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'RollColorID' => Yii::t('app', 'Roll Color ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRollColor()
    {
        return $this->hasOne(RollColor::className(), ['ID' => 'RollColorID']);
    }
}
