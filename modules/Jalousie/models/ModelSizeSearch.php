<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model as YiiModel;
use yii\data\ActiveDataProvider;
use app\modules\Jalousie\models\Model;

/**
 * ModelSearch represents the model behind the search form about `app\modules\Jalousie\models\Model`.
 */
class ModelSizeSearch extends Model
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Image'], 'safe'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ModelSize::find()->with(['model', 'lang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
        ]);

        return $dataProvider;
    }
}
