<?php

namespace app\modules\Jalousie\models;

use Yii;
//use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "MechanismOption".
 *
 * @property integer $ID
 * @property integer $HasOptions
 * @property string $Type
 *
 * @property MechanismOptionLang $lang
 * @property MechanismOptionLang[] $langs
 */
class MechanismOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MechanismOption';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'OpeningMechanismID'], 'required'],
            [['Price'], 'number', 'min' => 0],
            [['OpeningMechanismID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Price' => Yii::t('app', 'Price'),
            'OpeningMechanismID' => Yii::t('app', 'Opening Mechanism'),
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(MechanismOptionLang::className(), ['MechanismOptionID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpeningMechanism()
    {
        return $this->hasOne(OpeningMechanism::className(), ['ID' => 'OpeningMechanismID'])->joinWith('lang')->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(MechanismOptionLang::className(), ['MechanismOptionID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new MechanismOptionLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        TagDependency::invalidate(Yii::$app->cache, OpeningMechanism::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->MechanismOptionID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        $models = ArrayHelper::map(self::find()->with(['lang'])->all(), 'ID', 'lang.Title');
        return $res + $models;
    }
    
}
