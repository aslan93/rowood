<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "RollColor".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property RollColorLang[] $windowsillColorLangs
 */
class RollColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RollColor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RollMaterialID', 'Image', 'Price'], 'required'],
            [['RollMaterialID'], 'integer'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'RollMaterialID' => Yii::t('app', 'Roll Material'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(RollColorLang::className(), ['RollColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(RollColorLang::className(), ['RollColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new RollColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/jalousie/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->RollColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
