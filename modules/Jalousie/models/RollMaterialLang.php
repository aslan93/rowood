<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "RollMaterialLang".
 *
 * @property integer $ID
 * @property integer $RollMaterialID
 * @property string $LangID
 * @property string $Title
 *
 */
class RollMaterialLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RollMaterialLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RollMaterialID', 'LangID', 'Title'], 'required'],
            [['RollMaterialID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['RollMaterialID'], 'exist', 'skipOnError' => true, 'targetClass' => RollMaterial::className(), 'targetAttribute' => ['RollMaterialID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'RollMaterialID' => Yii::t('app', 'RollMaterial ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }
    
}