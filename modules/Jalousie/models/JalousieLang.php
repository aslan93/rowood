<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "JalousieLang".
 *
 * @property integer $ID
 * @property integer $JalousieID
 * @property string $LangID
 * @property string $Title
 *
 */
class JalousieLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'JalousieLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['JalousieID', 'LangID', 'Title'], 'required'],
            [['JalousieID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['JalousieID'], 'exist', 'skipOnError' => true, 'targetClass' => Jalousie::className(), 'targetAttribute' => ['JalousieID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'JalousieID' => Yii::t('app', 'Jalousie ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }
    
}