<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "ModelLang".
 *
 * @property integer $ID
 * @property integer $ModelID
 * @property string $LangID
 * @property string $Title
 *
 * @property Model $windowsillColor
 */
class ModelLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ModelLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ModelID', 'LangID', 'Title'], 'required'],
            [['ModelID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['Text'], 'string'],
            [['ModelID'], 'exist', 'skipOnError' => true, 'targetClass' => Model::className(), 'targetAttribute' => ['ModelID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ModelID' => Yii::t('app', 'Model ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['ID' => 'ModelID']);
    }
}
