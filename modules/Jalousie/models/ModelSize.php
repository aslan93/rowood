<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model as YiiModel;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "ModelSize".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property ModelLang[] $langs
 */
class ModelSize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ModelSize';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Percent', 'ModelID'], 'required'],
            [['Percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ModelID' => Yii::t('app', 'Model ID'),
            'Percent' => Yii::t('app', 'Percent'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ModelSizeLang::className(), ['ModelSizeID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['ID' => 'ModelID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ModelSizeLang::className(), ['ModelSizeID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ModelSizeLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        TagDependency::invalidate(Yii::$app->cache, Jalousie::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ModelSizeID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (YiiModel::loadMultiple($langModels, Yii::$app->request->post()) && YiiModel::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
