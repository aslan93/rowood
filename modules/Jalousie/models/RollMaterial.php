<?php

namespace app\modules\Jalousie\models;

use Yii;
//use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "RollMaterial".
 *
 * @property integer $ID
 * @property integer $Price
 *
 * @property RollMaterialLang $lang
 * @property RollMaterialLang[] $langs
 */
class RollMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RollMaterial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price'], 'required'],
            [['Status'], 'default', 'value' => 'Active'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Price' => Yii::t('app', 'Price'),
            'Status' => Yii::t('app', 'Type'),
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(RollMaterialLang::className(), ['RollMaterialID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(RollMaterialLang::className(), ['RollMaterialID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new RollMaterialLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->RollMaterialID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        $models = ArrayHelper::map(self::find()->with(['lang'])->all(), 'ID', 'lang.Title');
        return $res + $models;
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(RollMaterialColor::className(), ['RollMaterialID' => 'ID'])->with('lang');
    }
    
}
