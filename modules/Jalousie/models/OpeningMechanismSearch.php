<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Jalousie\models\OpeningMechanism;

/**
 * OpeningMechanismSearch represents the model behind the search form about `app\modules\Windowsill\models\OpeningMechanism`.
 */
class OpeningMechanismSearch extends OpeningMechanism
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OpeningMechanism::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
        ]);

        return $dataProvider;
    }
}
