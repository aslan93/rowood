<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model as YiiModel;
use yii\data\ActiveDataProvider;
use app\modules\Jalousie\models\ModelColor;

/**
 * ModelColorSearch represents the model behind the search form about `app\modules\ModelColor\models\ModelColor`.
 */
class ModelColorSearch extends ModelColor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return YiiModel::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ModelColor::find()->with('lang')->where(['ModelID' => Yii::$app->request->get('id')]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'ModelID' => $this->ModelID,
        ]);

        return $dataProvider;
    }
}
