<?php

namespace app\modules\Jalousie\models;

use Yii;
use yii\base\Model as YiiModel;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use app\modules\Jalousie\models\Jalousie;

/**
 * This is the model class for table "Model".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property ModelLang[] $langs
 */
class Model extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'JalousieID'], 'required'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'JalousieID' => Yii::t('app', 'Jalousie ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ModelLang::className(), ['ModelID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJalousie()
    {
        return $this->hasOne(Jalousie::className(), ['ID' => 'JalousieID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ModelLang::className(), ['ModelID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ModelLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/jalousie/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        TagDependency::invalidate(Yii::$app->cache, Jalousie::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ModelID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (YiiModel::loadMultiple($langModels, Yii::$app->request->post()) && YiiModel::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $res = $addEmpty ? ['' => $addEmpty] : [];
        $models = ArrayHelper::map(self::find()->with(['lang'])->all(), 'ID', 'lang.Title');
        return $res + $models;
    }
    
    public function getColors()
    {
        return $this->hasMany(ModelColor::className(), ['ModelID' => 'ID'])->with('lang');
    }
    
    public function getSizes()
    {
        return $this->hasMany(ModelSize::className(), ['ModelID' => 'ID'])->with('lang');
    }
    
}
