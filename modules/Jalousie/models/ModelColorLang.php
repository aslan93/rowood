<?php

namespace app\modules\Jalousie\models;

use Yii;

/**
 * This is the model class for table "ModelColorLang".
 *
 * @property integer $ID
 * @property integer $ModelColorID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property ModelColor $decoration
 */
class ModelColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ModelColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ModelColorID', 'LangID', 'Title'], 'required'],
            [['ModelColorID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ModelColorID'], 'exist', 'skipOnError' => true, 'targetClass' => ModelColor::className(), 'targetAttribute' => ['ModelColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ModelColorID' => Yii::t('app', 'ModelColor ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelColor()
    {
        return $this->hasOne(ModelColor::className(), ['ID' => 'ModelColorID']);
    }
}
