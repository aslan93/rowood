<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Product\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'ProductCategoryID') ?>

    <?= $form->field($model, 'MaterialID') ?>

    <?= $form->field($model, 'ProfileID') ?>

    <?= $form->field($model, 'WoodID') ?>

    <?php // echo $form->field($model, 'FrontColorID') ?>

    <?php // echo $form->field($model, 'BackColorID') ?>

    <?php // echo $form->field($model, 'TypeID') ?>

    <?php // echo $form->field($model, 'VariantID') ?>

    <?php // echo $form->field($model, 'Width') ?>

    <?php // echo $form->field($model, 'Height') ?>

    <?php // echo $form->field($model, 'TopDimentions') ?>

    <?php // echo $form->field($model, 'RightDimensions') ?>

    <?php // echo $form->field($model, 'BottomDimensions') ?>

    <?php // echo $form->field($model, 'LeftDimensions') ?>

    <?php // echo $form->field($model, 'GlassID') ?>

    <?php // echo $form->field($model, 'TermoEdge') ?>

    <?php // echo $form->field($model, 'TermoEdgeID') ?>

    <?php // echo $form->field($model, 'SoundProofing') ?>

    <?php // echo $form->field($model, 'SoundProofingID') ?>

    <?php // echo $form->field($model, 'Safety') ?>

    <?php // echo $form->field($model, 'SafetyID') ?>

    <?php // echo $form->field($model, 'Decoration') ?>

    <?php // echo $form->field($model, 'DecorationID') ?>

    <?php // echo $form->field($model, 'MontageHoles') ?>

    <?php // echo $form->field($model, 'Windowsill') ?>

    <?php // echo $form->field($model, 'WindowsillLength') ?>

    <?php // echo $form->field($model, 'WindowsillColorID') ?>

    <?php // echo $form->field($model, 'WindowsillWidth') ?>

    <?php // echo $form->field($model, 'EndCorner') ?>

    <?php // echo $form->field($model, 'EndCornerID') ?>

    <?php // echo $form->field($model, 'HConnector') ?>

    <?php // echo $form->field($model, 'Screw') ?>

    <?php // echo $form->field($model, 'ScrewID') ?>

    <?php // echo $form->field($model, 'FittingID') ?>

    <?php // echo $form->field($model, 'Price') ?>

    <?php // echo $form->field($model, 'Quantity') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
