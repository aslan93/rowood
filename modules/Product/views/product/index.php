<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',
            [
                'attribute' => 'Image',
                'format' => 'html',
                'value'=>function($data) { if($data->mainImage){return Html::img($data->mainImage->thumbPath, ['width' => 100, 'class' => 'image']);} },
            ],
            [
                'attribute' => 'Material',
                'label' => 'Material',
                'format' => 'html',
                'value' => function($data) { if($data->MaterialID){return Html::img($data->material->imagePath, ['width' => 100, 'class' => 'color-image']);} },

            ],
            [

                'label' => 'FrontColor',
                'format' => 'html',
                'value' => function($data) {
                            if($data->FrontColorID){
                                if(isset($data->frontColor)) {

                                    return Html::img($data->frontColor->imagePath, ['width' => 100, 'class' => 'color-image']);
                                }else{
                                    return Html::img($data->doorColor->imagePath, ['width' => 100, 'class' => 'color-image']);

                                }
                            }
                           },

            ],
            [
                'label' => 'BackColor',
                'format' => 'html',
                'value' => function($data) { if($data->BackColorID){return Html::img($data->backColor->imagePath, ['width' => 100, 'class' => 'color-image']);} },

            ],
            [
                'attribute' => 'Decor',
                'label' => 'Sticla',
                'format' => 'html',
                'value' => function($data) { if($data->DecorationID){return Html::img($data->decoration->imagePath, ['width' => 100, 'class' => 'color-image']);} },

            ],
            'lang.Title',
            [
                'attribute' => 'category.lang.Title',
                'label' => 'Category'
            ],
            //'MaterialID',
            //'ProfileID',
            //'WoodID',
            // 'FrontColorID',
            // 'BackColorID',
            // 'TypeID',
            // 'VariantID',
             //'Width',
             //'Height',
            // 'TopDimentions',
            // 'RightDimensions',
            // 'BottomDimensions',
            // 'LeftDimensions',
            // 'GlassID',
            // 'TermoEdge',
            // 'TermoEdgeID',
            // 'SoundProofing',
            // 'SoundProofingID',
            // 'Safety',
            // 'SafetyID',
            // 'Decoration',
            // 'DecorationID',
            // 'MontageHoles',
            // 'Windowsill',
            // 'WindowsillLength',
            // 'WindowsillColorID',
            // 'WindowsillWidth',
            // 'EndCorner',
            // 'EndCornerID',
            // 'HConnector',
            // 'Screw',
            // 'ScrewID',
            // 'FittingID',
             'Price',
            // 'Quantity',
             //'Status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
