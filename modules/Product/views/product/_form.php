<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>
    <?php
    $items[0] = [
    'label' => 'Descriere',
    'content' => $this->render('_lang_form',[
    'form' => $form,
    'model' => $model,

    ])];
    $items[1] = [
    'label' => 'Imagini',
    'content' => $this->render('_image_form',[
    'form' => $form,
    'model' => $model,


    ])];

    echo '<br>';
    echo Tabs::widget([
    'items' => $items,
    ]);

    ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
