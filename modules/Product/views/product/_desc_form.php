<?php
use app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;

?>


<?= $form->field($langModel, "[$langModel->LangID]Title")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]ShortDescription")->textArea()->widget(TinyMce::className(),[]) ?>
<?= $form->field($langModel, "[$langModel->LangID]Description")->textArea()->widget(TinyMce::className(),[]) ?>


