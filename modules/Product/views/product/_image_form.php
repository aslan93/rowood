<?php
use yii\helpers\Html;
use kartik\file\FileInput;
use app\modules\Location\models\Location;

$initialPreview = [];
$initialPreviewConfig = [];
foreach($model->images as  $image){

$initialPreview[] = Html::img($image->imagePath,['width' => 200]);
$initialPreviewConfig[] = [
'url' => \yii\helpers\Url::to(['/admin/ecommerce/product/product/image-delete']),
'key' => $image->ID,
];
}

?>
<br>
<div class="row">
    <div class="col-md-12">
        <?php
        echo FileInput::widget([
            'name' => 'Images[]',
            'attribute' => 'Images',
            'options'=>['accept'=>'image/*','multiple' => true],
            'pluginOptions' => [
                'otherActionButtons' => '<button class="set-main" type="button" {dataKey}><i class="glyphicon glyphicon-star"></i></button>',
                'overwriteInitial'=>false,
                'maxFileSize'=>2800,
                'fileActionSettings' => [
                    'fileActionSettings' => [
                        'showZoom' => false,
                        'showDelete' => true,
                    ],
                ],
                'browseClass' => 'btn btn-success',
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
                'initialPreviewConfig' => $initialPreviewConfig,

            ],

        ]);
        $this->registerJs("
    
            $('.set-main').on('click',function(){
                $.post('/admin/ecommerce/product/product/set-main-image/' , {id:$(this).attr('data-key')});
                
                });
    ");
        ?>

    </div></div>
    <div>
    <br>


