<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\Product\models\Product */

$this->title = $model->lang->Title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div style="text-align: center;">
<?php foreach ($model->images as $image){?>
    <span style="text-align: center; margin-right: 10px;">
        <?php
         echo  Html::img($image->imagePath, [ 'width'=>200,'class' => 'image']);
        ?>
    </span>
<?php }?>

    </div>
    <br>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'ID',
            'lang.Title',
            [
                    'attribute'=>'category.lang.Title',
                    'label' => 'Categorie',
            ],
//            'MaterialID',
//            'ProfileID',
//            'WoodID',
//            'FrontColorID',
//            'BackColorID',
//            'TypeID',
//            'VariantID',
            'Width',
            'Height',
//            'TopDimentions',
//            'RightDimensions',
//            'BottomDimensions',
//            'LeftDimensions',
//            'GlassID',
//            'TermoEdge',
//            'TermoEdgeID',
//            'SoundProofing',
//            'SoundProofingID',
//            'Safety',
//            'SafetyID',
//            'Decoration',
//            'DecorationID',
//            'MontageHoles',
//            'Windowsill',
//            'WindowsillLength',
//            'WindowsillColorID',
//            'WindowsillWidth',
//            'EndCorner',
//            'EndCornerID',
//            'HConnector',
//            'Screw',
//            'ScrewID',
//            'FittingID',
            'Price',
//            'Quantity',

            'Status',
            [
                'attribute' => 'lang.ShortDescription',
                'format' => 'raw',
            ],
            [
                'attribute' => 'lang.Description',
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
