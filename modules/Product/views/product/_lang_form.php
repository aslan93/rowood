<?php
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\modules\ProductCategory\models\ProductCategory;
use app\modules\Color\models\Color;
use app\modules\Material\models\Material;
use app\modules\Decoration\models\Decoration;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
use app\modules\Product\models\Product;
use kartik\depdrop\DepDrop;



$url =  Url::to('@web/uploads/doors-colors/');
$format = <<< SCRIPT
function format(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url' +  state.text
    return '<img class="image" src="' + src + '"/>';
    
}
SCRIPT;
$url4 =  Url::to('@web/uploads/doors-handles/');
$format4 = <<< SCRIPT
function format4(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url4' +  state.text
    return '<img class="image" src="' + src + '"/>';
    
}
SCRIPT;
$url2 =  Url::to('@web/uploads/decoration/');
$format2 = <<< SCRIPT
function format2(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url2' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$url3 =  Url::to('@web/uploads/material/');
$format3 = <<< SCRIPT
function format3(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url3' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$escape = new JsExpression("function(m) { return m; }");

$this->registerJs($format, View::POS_HEAD);
$this->registerJs($format2, View::POS_HEAD);
$this->registerJs($format3, View::POS_HEAD);
$this->registerJs($format4, View::POS_HEAD);



?>
<style>
    .image{
        width:50%;
        height:50%;
        position: relative;

    }
    .select2-container--krajee .select2-selection--single{
        height: auto;
    }
    .select2-container--krajee .select2-selection--single .select2-selection__arrow{
        border-left: none!important;
    }
</style>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Width')->textInput() ?>
            <?= $form->field($model, 'Visibility')->hiddenInput(['value'=> 1]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Height')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, "ProductCategoryID")->dropDownList( ProductCategory::getList() ,['id'=>'category']) ?>
        </div>
        <div class="col-sm-12">

            <?= $form->field($model, 'DoorsColorsIDs[]')->widget(Select2::className(), [
                'data' => \app\modules\DoorsColors\models\DoorsColors::getList(),
                'hideSearch' => true,
                'options' => [
                    'id' => 'colors-ids',
                    'multiple' => true,
                    'value' => $model->doorsColorsIDs,
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('format'),
                        'templateSelection' => new JsExpression('format'),
                        'escapeMarkup' => $escape,
                        'allowClear' => true
                    ],
                ]
            ])->label('Culorile disponibile pentru produs:'); ?>
        </div>
        <div class="col-sm-12">

            <?= $form->field($model, 'DoorsSizesIDs[]')->widget(Select2::className(), [
                'data' => \app\modules\DoorsSizes\models\DoorsSizes::getList(),
                'hideSearch' => true,
                'options' => [
                    'id' => 'sizes-ids',
                    'multiple' => true,
                    'value' => $model->doorsSizesIDs,
                ]
            ])->label('Dimensiunile disponibile pentru produs:');?>
        </div>
        <div class="col-sm-12">

            <?= $form->field($model, 'DoorsHandlesIDs[]')->widget(Select2::className(), [
                'data' => \app\modules\DoorsHandles\models\DoorsHandles::getList(),
                'hideSearch' => true,
                'options' => [
                    'id' => 'handles-ids',
                    'multiple' => true,
                    'value' => $model->doorsHandlesIDs,
                ]
            ])->label('Manerele disponibile pentru produs:');?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'SizeID')->widget(DepDrop::classname(), [
                'data' => \app\modules\DoorsSizes\models\DoorsSizes::getList(false,$model->SizeID),
                'pluginOptions'=>[
                    'depends'=>['sizes-ids'],
                    'placeholder'=>'Select...',
                    'value' => $model->SizeID,
                    'url'=>Url::to(['/admin/ecommerce/product/product/doors-sizes']),
                    'params' => ['model_id2'],
                ],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'hideSearch' => true,
                ]
            ]);
            ?>

        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'HandleID')->widget(DepDrop::classname(), [

                'data' => \app\modules\DoorsHandles\models\DoorsHandles::getListOfImages(false,$model->HandleID),
                'pluginOptions'=>[
                    'depends'=>['handles-ids'],
                    'placeholder'=>'Select...',
                    'value' => $model->HandleID,
                    'url'=>Url::to(['/admin/ecommerce/product/product/doors-handles']),
                    'params' => ['model_id3'],

                ],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('format4'),
                        'templateSelection' => new JsExpression('format4'),
                        'escapeMarkup' => $escape,
                        'allowClear' => true
                    ],
                ]

            ]);
            ?>

        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'FrontColorID')->widget(DepDrop::classname(), [

                'data' => \app\modules\DoorsColors\models\DoorsColors::getListOfImages(false,$model->FrontColorID),

                'pluginOptions'=>[
                    'depends'=>['colors-ids'],
                    'placeholder'=>'Select...',
                    'value' => $model->FrontColorID,
                    'url'=>Url::to(['/admin/ecommerce/product/product/doors-colors']),
                    'params' => ['model_id1'],

                ],
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => [
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'templateResult' => new JsExpression('format'),
                            'templateSelection' => new JsExpression('format'),
                            'escapeMarkup' => $escape,
                            'allowClear' => true
                        ],
                    ]
            ]);
            ?>

        </div>
        <div class="hidden">
            <?=Select2::widget([
                'name' => 'hidden',
                'data' => ['1','2'],
                'pluginOptions' => [
                    'initSelection' => new JsExpression("function() {
     
                            $('#colors-ids').trigger('change');
                            return 1;
                         }"),
                ]
            ])?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, "DecorationID")->widget(Select2::className(),[
                'name' => 'DecorationID',
                'data' => Decoration::getImageList(),
                'options' => ['placeholder' => 'Select a decoration ...'],
                'pluginOptions' => [
                    'templateResult' => new JsExpression('format2'),
                    'templateSelection' => new JsExpression('format2'),
                    'escapeMarkup' => $escape,
                    'allowClear' => true
                ],
            ])->label('Tipul sticlei');?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, "MaterialID")->widget(Select2::className(),[
                'name' => 'DMaterialID',
                'data' => Material::getImageList(),
                'options' => ['placeholder' => 'Select a decoration ...'],
                'pluginOptions' => [
                    'templateResult' => new JsExpression('format3'),
                    'templateSelection' => new JsExpression('format3'),
                    'escapeMarkup' => $escape,
                    'allowClear' => true
                ],
            ])->label('Material');?>
        </div>

    </div>
            <?php
            $items = [];
            foreach ($model->langs as $langID => $langModel)
            {
                $items[] = [
                    'label' => strtoupper($langID),
                    'content' => $this->render('_desc_form',[
                        'form' => $form,
                        'langModel' => $langModel,
                        'model' => $model,
                    ]),
                ];
            }
            echo '<br>';
            echo Tabs::widget([
                'items' => $items,
            ]);
            ?>

