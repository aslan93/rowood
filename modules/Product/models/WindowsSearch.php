<?php

namespace app\modules\Product\models;

use app\modules\ProductCategory\models\ProductCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Product\models\ProductSearch;

/**
 * ProductSearch represents the model behind the search form about `app\modules\Product\models\Product`.
 */
class WindowsSearch extends ProductSearch
{
    /**
     * @inheritdoc
     */
    public $sortParam = NULL;

    public function rules()
    {
        return [
            [['sortParam'],'safe'],
            [['ID', 'ProductCategoryID', 'MaterialID', 'ProfileID', 'WoodID', 'FrontColorID', 'BackColorID', 'TypeID', 'VariantID', 'Width', 'Height', 'GlassID', 'TermoEdge', 'TermoEdgeID', 'SoundProofing', 'SoundProofingID', 'Safety', 'SafetyID', 'Decoration', 'DecorationID', 'MontageHoles', 'Windowsill', 'WindowsillLength', 'WindowsillColorID', 'WindowsillWidth', 'EndCorner', 'EndCornerID', 'HConnector', 'Screw', 'ScrewID', 'FittingID', 'Quantity'], 'integer'],
            [['TopDimentions', 'RightDimensions', 'BottomDimensions', 'LeftDimensions', 'Status','Price','sortParam'], 'safe'],
            [['Price'], 'number'],
        ];
    }


    public function search($params,$cid = false)
    {
        $query = Product::find()->joinWith('category')->where(['ProductType' => ProductCategory::ProductTypeFereastra,'ProductCategory.ID'=>$cid]);//->orderBy(['Price' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if($this->sortParam == 3) {
            $query->orderBy(['Price' => 3]);
        }
        if($this->sortParam == 4) {
            $query->orderBy(['Price' => 4]);
        }

        if ($this->Height){
            $query->andWhere(['Height'=>$this->Height]);
        }
        if ($this->Width){
            $query->andWhere(['Width'=>$this->Width]);
        }
        if ($this->Price){

        }
        if ($this->FrontColorID){
            $query->andWhere(['FrontColorID' =>$this->FrontColorID]);
        }
        if ($this->BackColorID){
            $query->andWhere(['BackColorID' =>$this->BackColorID]);
        }
        if ($this->DecorationID){
            $query->andWhere(['DecorationID' =>$this->DecorationID]);
        }
        if ($this->MaterialID){
            $query->andWhere(['MaterialID' =>$this->MaterialID]);
        }

        return $dataProvider;
    }
}
