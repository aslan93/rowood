<?php

namespace app\modules\Product\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Product\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\modules\Product\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public $sortParam = NULL;
    public function rules()
    {
        return [
            [['sortParam'],'safe'],
            [['ID', 'ProductCategoryID', 'MaterialID', 'ProfileID', 'WoodID', 'FrontColorID', 'BackColorID', 'TypeID', 'VariantID', 'Width', 'Height', 'GlassID', 'TermoEdge', 'TermoEdgeID', 'SoundProofing', 'SoundProofingID', 'Safety', 'SafetyID', 'Decoration', 'DecorationID', 'MontageHoles', 'Windowsill', 'WindowsillLength', 'WindowsillColorID', 'WindowsillWidth', 'EndCorner', 'EndCornerID', 'HConnector', 'Screw', 'ScrewID', 'FittingID', 'Quantity'], 'integer'],
            [['TopDimentions', 'RightDimensions', 'BottomDimensions', 'LeftDimensions', 'Status'], 'safe'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$cid = false)
    {
        $query = Product::find()->with('material','frontColor','doorColor','backColor','mainImage','category.lang','category','decoration','lang')->where(['Visibility' => 1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($cid){
            $query->where(['ProductCategoryID'=>$cid]);
        }
        if($this->sortParam == 3) {
            $query->orderBy(['Price' => 3]);
        }
        if($this->sortParam == 4) {
            $query->orderBy(['Price' => 4]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ProductCategoryID' => $this->ProductCategoryID,
            'MaterialID' => $this->MaterialID,
            'ProfileID' => $this->ProfileID,
            'WoodID' => $this->WoodID,
            'FrontColorID' => $this->FrontColorID,
            'BackColorID' => $this->BackColorID,
            'TypeID' => $this->TypeID,
            'VariantID' => $this->VariantID,
            'Width' => $this->Width,
            'Height' => $this->Height,
            'GlassID' => $this->GlassID,
            'TermoEdge' => $this->TermoEdge,
            'TermoEdgeID' => $this->TermoEdgeID,
            'SoundProofing' => $this->SoundProofing,
            'SoundProofingID' => $this->SoundProofingID,
            'Safety' => $this->Safety,
            'SafetyID' => $this->SafetyID,
            'Decoration' => $this->Decoration,
            'DecorationID' => $this->DecorationID,
            'MontageHoles' => $this->MontageHoles,
            'Windowsill' => $this->Windowsill,
            'WindowsillLength' => $this->WindowsillLength,
            'WindowsillColorID' => $this->WindowsillColorID,
            'WindowsillWidth' => $this->WindowsillWidth,
            'EndCorner' => $this->EndCorner,
            'EndCornerID' => $this->EndCornerID,
            'HConnector' => $this->HConnector,
            'Screw' => $this->Screw,
            'ScrewID' => $this->ScrewID,
            'FittingID' => $this->FittingID,
            'Price' => $this->Price,
            'Quantity' => $this->Quantity,
        ]);

        $query->andFilterWhere(['like', 'TopDimentions', $this->TopDimentions])
            ->andFilterWhere(['like', 'RightDimensions', $this->RightDimensions])
            ->andFilterWhere(['like', 'BottomDimensions', $this->BottomDimensions])
            ->andFilterWhere(['like', 'LeftDimensions', $this->LeftDimensions])
            ->andFilterWhere(['like', 'Status', $this->Status]);

        return $dataProvider;
    }
}
