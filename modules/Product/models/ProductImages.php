<?php

namespace app\modules\Product\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "ProductImages".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property string $Image
 * @property string $Thumb
 * @property string $Type
 * @property integer $IsMain
 *
 * @property Product $product
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductImages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'Image'], 'required'],
            [['ProductID', 'IsMain'], 'integer'],
            [['Image', 'Thumb', 'Type'], 'string', 'max' => 255],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'Image' => 'Image',
            'Thumb' => 'Thumb',
            'Type' => 'Type',
            'IsMain' => 'Is Main',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }

    public function getImagePath(){

        return  empty($this->Image) ? '' : Url::to('/uploads/product/'.$this->Image);
    }

    public function getTypeImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/variant/' . $this->Image);
    }

    public function getThumbPath(){

        return  empty($this->Thumb) ? '' : Url::to('/uploads/product/'.$this->Thumb);
    }

    public function beforeDelete()
    {
        unlink(Yii::getAlias("@webroot".$this->imagePath));
        unlink(Yii::getAlias("@webroot".$this->thumbPath));
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
