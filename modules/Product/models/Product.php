<?php

namespace app\modules\Product\models;

use app\modules\Color\models\Color;
use app\modules\Decoration\models\Decoration;
use app\modules\DoorsColors\models\DoorsColors;
use app\modules\DoorsColors\models\DoorsColorsRel;
use app\modules\DoorsHandles\models\DoorsHandles;
use app\modules\DoorsHandles\models\DoorsHandlesRel;
use app\modules\DoorsSizes\models\DoorsSizeRel;
use app\modules\DoorsSizes\models\DoorsSizes;
use app\modules\Material\models\Material;
use app\modules\ProductCategory\models\ProductCategory;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Product".
 *
 * @property integer $ID
 * @property integer $MaterialID
 * @property integer $ProfileID
 * @property integer $FrontColorID
 * @property integer $BackColorID
 * @property integer $WoodID
 * @property integer $TypeID
 * @property integer $VariantID
 * @property integer $Width
 * @property integer $Height
 * @property string $TopDimentions
 * @property string $RightDimensions
 * @property string $BottomDimensions
 * @property string $LeftDimensions
 * @property integer $GlassID
 * @property integer $TermoEdge
 * @property integer $TermoEdgeID
 * @property integer $SoundProofing
 * @property integer $SoundProofingID
 * @property integer $Safety
 * @property integer $SafetyID
 * @property integer $Decoration
 * @property integer $DecorationID
 * @property integer $MontageHoles
 * @property integer $Windowsill
 * @property integer $WindowsillLength
 * @property integer $WindowsillWidth
 * @property integer $WindowsillColorID
 * @property integer $FittingID
 * @property string $Price
 * @property integer $Quantity
 * @property string $Status
 * * @property string $HandleID
 * * @property string $SizeID
 */
class Product extends \yii\db\ActiveRecord
{
    const ScenarioAddToCart = 'AddToCart';
    const StatusConfigurable = 'Configurable';
    const StatusInCart = 'InCart';
    const Sort_Desc_Price = 3;
    const Sort_Asc_Price = 4;
    const OpenDirLeft = 'Left';
    const OpenDirRight = 'Right';
    public $DoorsColorsIDs = [];
    public $DoorsSizesIDs = [];
    public $DoorsHandlesIDs = [];


    public $sortParam = NULL;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [[
                'MaterialID', 'ProfileID', 'FrontColorID', 'BackColorID', 'WoodID', 'TypeID', 
                'VariantID', 'Width', 'Height', 'Quantity', 'GlassID', 'TermoEdge', 'TermoEdgeID', 
                'Decoration', 'DecorationID', 'MontageHoles', 'Windowsill', 'WindowsillLength',
                'WindowsillColorID', 'WindowsillWidth', 'EndCorner', 'EndCornerID', 'HConnector',
                'Screw', 'ScrewID', 'FittingID','ProductCategoryID','ParentID','SizeID','HandleID','Visibility'
            ], 'integer'],
            [['TopDimentions', 'RightDimensions', 'BottomDimensions', 'LeftDimensions','OpenDirection'], 'string'],
            [['Price'], 'number'],
            [['Status','sortParam'], 'string', 'max' => 50],
            [['DoorsColorsIDs','DoorsSizesIDs','DoorsHandlesIDs'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MaterialID' => Yii::t('app', 'Material ID'),
            'ProfileID' => Yii::t('app', 'Profile ID'),
            'FrontColorID' => Yii::t('app', 'Front Color ID'),
            'BackColorID' => Yii::t('app', 'Back Color ID'),
            'WoodID' => Yii::t('app', 'Wood ID'),
            'TypeID' => Yii::t('app', 'Type ID'),
            'VariantID' => Yii::t('app', 'Variant ID'),
            'Width' => Yii::t('app', 'Width'),
            'Height' => Yii::t('app', 'Height'),
            'TopDimentions' => Yii::t('app', 'Top Dimentions'),
            'RightDimensions' => Yii::t('app', 'Right Dimensions'),
            'BottomDimensions' => Yii::t('app', 'Bottom Dimensions'),
            'LeftDimensions' => Yii::t('app', 'Left Dimensions'),
            'Price' => Yii::t('app', 'Price'),
            'Quantity' => Yii::t('app', 'Quantity'),
            'Status' => Yii::t('app', 'Status'),
            'ProductCategoryID' => Yii::t('app', 'Category'),
            'sortParam' =>Yii::t('app', 'Sort Parametri'),
            'ParentID' =>Yii::t('app', 'Parinte'),
            'SizeID' =>Yii::t('app', 'Marime'),
            'OpenDirection' =>Yii::t('app', 'Directia Deschiderii'),
            'HandleID' =>Yii::t('app', 'Maner'),
            'Visibility'=>Yii::t('app', 'Vizibilitate'),

        ];
    }

    public function getLang()
    {
        return $this->hasOne(ProductLang::className(), ['ProductID' => 'ID'])->where(['ProductLang.LangID' => Yii::$app->language]);
    }

    public function getParentMainImage(){
        return  $this->hasOne(ProductImages::className(), ['ProductID' => 'ParentID'])->where(['IsMain'=>1]);
    }
    public function getParentImages(){

        return  $this->hasMany(ProductImages::className(), ['ProductID' => 'ParentID']);
    }
    public function getLangs()
    {
        $langs = $this->hasMany(ProductLang::className(), ['ProductID' => 'ID'])->indexBy('LangID')->all();
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $LangID){
            $result[$LangID]= isset($langs[$LangID]) ? $langs[$LangID] : new ProductLang([
                'LangID' => $LangID,
            ]);
        }
        return $result;
    }
    public function getImages(){

        return  $this->hasMany(ProductImages::className(), ['ProductID' => 'ID']);
    }
    public function getMainImage(){
        return  $this->hasOne(ProductImages::className(), ['ProductID' => 'ID'])->where(['IsMain'=>1]);
    }

    public function beforeSave($insert)
    {
        parent::beforeSave($insert); // TODO: Change the autogenerated stub

        return true;
    }
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        $langModels = $this->langs;
        foreach ($langModels as $langModel){

            $langModel->ProductID = $this->ID;
        }

        if(Model::loadMultiple($langModels,Yii::$app->request->post())){

            foreach ($langModels as $langModel){

                $langModel->save();
            }

            DoorsColorsRel::deleteAll(['ProductID' => $this->ID]);

            if ($this->DoorsColorsIDs) {

                foreach ($this->DoorsColorsIDs as $colorID) {
                    $model = new DoorsColorsRel();
                    $model->ProductID = $this->ID;
                    $model->DoorsColorID = $colorID;
                    $model->save();
                }
            }

            DoorsSizeRel::deleteAll(['ProductID' => $this->ID]);

            if ($this->DoorsSizesIDs) {

                foreach ($this->DoorsSizesIDs as $sizeID) {
                    $model = new DoorsSizeRel();
                    $model->ProductID = $this->ID;
                    $model->SizeID = $sizeID;
                    $model->save();
                }
            }

            DoorsHandlesRel::deleteAll(['ProductID' => $this->ID]);

            if ($this->DoorsHandlesIDs) {

                foreach ($this->DoorsHandlesIDs as $handleID) {
                    $model = new DoorsHandlesRel();
                    $model->ProductID = $this->ID;
                    $model->DoorsHandleID = $handleID;
                    $model->save();
                }
            }
        }

    }
    public function getCategory(){
        return  $this->hasOne(ProductCategory::className(), ['ID' => 'ProductCategoryID'])->with('lang');
    }

    public static function getSortParams($addEmpty = null){
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $list = [
            self::Sort_Desc_Price => Yii::t('app', 'Descendent dupa Pret'),
            self::Sort_Asc_Price => Yii::t('app', 'Ascendent dupa Pret'),
        ];
        return $return + $list;
    }
    public static function getDoorOpenDir($addEmpty = null){
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $list = [
            self::OpenDirLeft => Yii::t('app', 'Stinga'),
            self::OpenDirRight => Yii::t('app', 'Dreapta'),
        ];
        return $return + $list;
    }
    public function getFrontColor(){
        return $this->hasOne(Color::className(),['ID'=>'FrontColorID'])->with('lang');
    }

    public function getBackColor(){
        return $this->hasOne(Color::className(),['ID'=>'BackColorID'])->with('lang');
    }
    public function getDecoration(){
        return $this->hasOne(Decoration::className(),['ID'=>'DecorationID'])->with('lang');
    }
    public function getMaterial(){
        return $this->hasOne(Material::className(),['ID'=>'MaterialID'])->with('lang');
    }
    public function getRecomandations(){
        return self::find()->where(['ProductCategoryID' => $this->ProductCategoryID,'Visibility'=>1])->andWhere(['<>','ID', $this->ID])->limit(2)->all();
    }

    public function getDoorsColors()
    {
        return $this->hasMany(DoorsColorsRel::className(), ['ProductID' => 'ID'])->with('doorsColor','doorsColor.lang');
    }

    public function getDoorsColorsIDs()
    {
        return ArrayHelper::map($this->doorsColors, 'DoorsColorID', 'DoorsColorID');
    }
    public function getDoorsSizes()
    {
        return $this->hasMany(DoorsSizeRel::className(), ['ProductID' => 'ID'])->with('size');
    }

    public function getDoorsSizesIDs()
    {
        return ArrayHelper::map($this->doorsSizes, 'SizeID', 'SizeID');
    }
    public function getDoorsHandles()
    {
        return $this->hasMany(DoorsHandlesRel::className(), ['ProductID' => 'ID'])->with('doorsHandle');
    }

    public function getDoorsHandlesIDs()
    {
        return ArrayHelper::map($this->doorsHandles, 'DoorsHandleID', 'DoorsHandleID');
    }
    public function getDoorsColorsWithImages()
    {
        return ArrayHelper::map($this->doorsColors, 'DoorsColorID', 'doorsColor.Image');
    }
    public function getDoorSize(){
        return $this->hasOne(DoorsSizes::className(),['ID'=>'SizeID']);
    }
    public function getDoorColor(){
        return $this->hasOne(DoorsColors::className(),['ID'=>'FrontColorID'])->with('lang');
    }
    public function getDoorHandle(){
        return $this->hasOne(DoorsHandles::className(),['ID'=>'HandleID'])->with('lang');
    }
    public function getTotalPrice()
    {
        $product = self::find()->where(['ID'=>$this->ID])->with(['doorColor','doorSize','doorHandle'])->one();
        $price = $product->Price;

        if ($product->doorColor) {
            $price += $product->doorColor->Price;
        }
        if ($product->doorHandle) {

            $price += $product->doorHandle->Price;
        }
        if ($product->doorSize) {

            $price += $product->doorSize->Price;
        }


        return $price;
    }
    public function getProductInfo(){
        $cat = $this->category;
        if ($cat->Type == ProductCategory::TypeConfigured){
             $infos =  Product::find()->where(['ID' => $this->ID])->with(['doorColor','doorHandle','doorSize','material'])->one();
            return $infos;
        }else{
            return '';
        }
    }
}
