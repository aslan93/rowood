<?php

namespace app\modules\Product\models;

use Yii;

/**
 * This is the model class for table "ProductLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property string $Title
 * @property string $ShortDescription
 * @property string $Description
 * @property integer $ProductID
 *
 * @property Product $product
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'ProductID'], 'required'],
            [['ShortDescription', 'Description'], 'string'],
            [['ProductID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'ShortDescription' => 'Short Description',
            'Description' => 'Description',
            'ProductID' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }
}
