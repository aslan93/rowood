<?php

namespace app\modules\Product\controllers;

use app\modules\DoorsHandles\models\DoorsHandles;
use app\modules\DoorsSizes\models\DoorsSizes;
use app\modules\Ecommerce\controllers\EcommerceController;
use app\modules\Product\models\ProductImages;
use Yii;
use app\modules\Product\models\Product;
use app\modules\Product\models\ProductSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use app\modules\DoorsColors\models\DoorsColors;
use yii\helpers\Json;


/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends EcommerceController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $images = UploadedFile::getInstancesByName('Images');
        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/product/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/product/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/product/$thumb"));

                    if ($model->mainImage) {
                        $imageModel = new ProductImages([
                            'ProductID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 0,
                        ]);
                    }else{
                        $imageModel = new ProductImages([
                            'ProductID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 1,
                        ]);
                    }
                    $imageModel->save();
                }
            }
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        ProductImages::findOne($key)->delete();
        return '{}';
    }
    public static function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = ProductImages::findOne($key);

        ProductImages::updateAll(['IsMain'=> 0],['ProductID'=>$imgModel->ProductID]);

        $imgModel->IsMain = 1;
        $rs = $imgModel->save();

        return "$rs";
    }

    public function actionDoorsColors(){

        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];

            if(!$id[0]){ $id[0] = -1;}
            $out = DoorsColors::getListOfImages(false,$id[0]);
            $result =[];
            foreach ($out as $key => $value){
                $result[] = ['id'=> $key,'name'=> $value];
            }
            echo Json::encode(['output'=>$result, 'selected'=>'']);


        }else{
            echo '{}';
        }
    }
    public function actionDoorsSizes(){

        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];

            if(!$id[0]){ $id[0] = -1;}
            $out = DoorsSizes::getList(false,$id[0]);
            $result =[];
            foreach ($out as $key => $value){
                $result[] = ['id'=> $key,'name'=> $value];
            }
            echo Json::encode(['output'=>$result, 'selected'=>'']);


        }else{
            echo '{}';
        }
    }


    public function actionDoorsHandles(){

        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];

            if(!$id[0]){ $id[0] = -1;}
            $out = DoorsHandles::getListOfImages(false,$id[0]);
            $result =[];
            foreach ($out as $key => $value){
                $result[] = ['id'=> $key,'name'=> $value];
            }
            echo Json::encode(['output'=>$result, 'selected'=>'']);


        }else{
            echo '{}';
        }
    }
}
