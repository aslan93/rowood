<?php

namespace app\modules\Product;

use app\modules\Admin\Admin;

/**
 * product module definition class
 */
class Product extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Product\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
