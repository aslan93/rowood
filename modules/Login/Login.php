<?php

namespace app\modules\Login;

/**
 * login module definition class
 */
class Login extends \app\components\Module\FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Login\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
