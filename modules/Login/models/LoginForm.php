<?php

namespace app\modules\Login\models;

use Yii;
use yii\base\Model;
use app\modules\User\models\User;
use app\modules\User\models\UserStatus;

class LoginForm extends Model
{

    public $Email;
    public $Password;
    public $RememberMe = false;
    
    private $_user = false;

    public function rules()
    {
        return [
            [['Email', 'Password'], 'required'],
            ['RememberMe', 'boolean'],
            ['Password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->Password))
            {
                $this->addError($attribute, Yii::t('app', 'Incorrect email or password'));
            }
            elseif ($user->Status === UserStatus::Inactive)
            {
                $this->addError($attribute, Yii::t('app', 'User is inactive'));
            }
        }
    }

    public function login()
    {
        if ($this->validate())
        {
            return Yii::$app->user->login($this->getUser(), $this->RememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    public function getUser()
    {
        if ($this->_user === false)
        {
            $this->_user = User::findByUsername($this->Email);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'Email'      => 'Email',
            'Password'   => Yii::t('app', 'Parola'),
            'RememberMe' => 'Tine-ma minte',
        ];
    }

}
