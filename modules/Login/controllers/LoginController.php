<?php

namespace app\modules\Login\controllers;

use Yii;
use app\components\Controller\FrontController;
use app\modules\Login\models\LoginForm;
use yii\helpers\Url;

class LoginController extends FrontController
{

    public function actionIndex()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login())
        {

            if(Yii::$app->user->isGuest)
            {
                Yii::$app->user->loginUrl = ['site/login', 'return' => Yii::$app->request->url];
                return $this->redirect(Yii::$app->user->loginUrl);
            }
            
            if (Yii::$app->user->identity->Type == 'Admin')
            {
                return $this->redirect(Url::to('/admin/ecommerce/product/'));
            }

            $return_url = Yii::$app->request->get('return');

            if(!empty($return_url))
            {
                return $this->redirect($return_url);
            }
            else
            {
                return $this->goBack();
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
