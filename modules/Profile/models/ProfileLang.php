<?php

namespace app\modules\Profile\models;

use Yii;

/**
 * This is the model class for table "ProfileLang".
 *
 * @property integer $ID
 * @property integer $ProfileID
 * @property integer $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Profile $profile
 */
class ProfileLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProfileLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProfileID', 'LangID', 'Title', 'Text'], 'required'],
            [['ProfileID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ProfileID'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['ProfileID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ProfileID' => Yii::t('app', 'Profile ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['ID' => 'ProfileID']);
    }
}
