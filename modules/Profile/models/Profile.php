<?php

namespace app\modules\Profile\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use app\modules\Material\models\Material;
use app\modules\Profile\models\ProfileLang;

/**
 * This is the model class for table "Profile".
 *
 * @property integer $ID
 * @property integer $MaterialID
 * @property integer $Image
 * @property string $Percent
 * @property integer $MaxGlassCount
 * @property integer $Default
 *
 * @property Material $material
 * @property ProfileLang $lang
 * @property ProfileLang[] $langs
 */
class Profile extends \yii\db\ActiveRecord
{
    
    public function init()
    {
        parent::init();
        
        $this->MaxGlassCount = 2;
    }

        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MaterialID', 'Image', 'Percent', 'MaxGlassCount'], 'required'],
            [['Default'], 'default', 'value' => 0],
            [['MaterialID', 'MaxGlassCount', 'Default'], 'integer'],
            [['Percent'], 'number'],
            [['Image'], 'string', 'max' => 255],
            [['MaxGlassCount'], 'default', 'value' => 2],
            [['MaterialID'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['MaterialID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MaterialID' => Yii::t('app', 'Material'),
            'Image' => Yii::t('app', 'Image'),
            'Percent' => Yii::t('app', 'Percent'),
            'MaxGlassCount' => Yii::t('app', 'Max Glass Count'),
            'Default' => Yii::t('app', 'Use as default for material'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['ID' => 'MaterialID']);
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/profile/' . $this->Image);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ProfileLang::className(), ['ProfileID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ProfileLang::className(), ['ProfileID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ProfileLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], "ID <> $this->ID AND MaterialID = $this->MaterialID");
        }
        else
        {
            $hasDefault = self::find()->where(['Default' => 1])->andWhere(['<>', 'ID', $this->ID])->andWhere(['MaterialID' => $this->MaterialID])->count();
            
            if (!$hasDefault)
            {
                $this->addError('Default', Yii::t('app', 'Although one profile should be installed as default for material'));
                return false;
            }
        }
        
        return true;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ProfileID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    
}
