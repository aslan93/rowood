<?php

namespace app\modules\InsectScreen\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Cloth".
 *
 * @property integer $ID
 * @property string $Price
 * @property string $Image
 *
 * @property GlassLang $lang
 * @property GlassLang[] $langs
 */
class Cloth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Cloth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'Image'], 'required'],
            [['Price'], 'number'],
            [['Default'], 'default', 'value' => 0],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Default' => Yii::t('app', 'Default'),
            'Price' => Yii::t('app', 'Price'),
            'Image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ClothLang::className(), ['ClothID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ClothLang::className(), ['ClothID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ClothLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/cloth/' . $this->Image);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ClothID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
