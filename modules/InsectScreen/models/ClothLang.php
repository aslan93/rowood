<?php

namespace app\modules\InsectScreen\models;

use Yii;

/**
 * This is the model class for table "ClothLang".
 *
 * @property integer $ID
 * @property integer $ClothID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Cloth $glass
 */
class ClothLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ClothLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ClothID', 'Title', 'LangID'], 'required'],
            [['ClothID'], 'integer'],
            [['Text'], 'string'],
            [['Title'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['ClothID'], 'exist', 'skipOnError' => true, 'targetClass' => Cloth::className(), 'targetAttribute' => ['ClothID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ClothID' => Yii::t('app', 'Cloth ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCloth()
    {
        return $this->hasOne(Cloth::className(), ['ID' => 'ClothID']);
    }
}
