<?php

namespace app\modules\InsectScreen\models;

use Yii;
use app\modules\InsectScreen\models\InsectScreenColor;

/**
 * This is the model class for table "ColorLang".
 *
 * @property integer $ID
 * @property integer $InsectScreenColorID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Color $color
 */
class InsectScreenColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'InsectScreenColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InsectScreenColorID', 'LangID', 'Title'], 'required'],
            [['InsectScreenColorID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['InsectScreenColorID'], 'exist', 'skipOnError' => true, 'targetClass' => InsectScreenColor::className(), 'targetAttribute' => ['InsectScreenColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ColorID' => Yii::t('app', 'Color ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }
    
}
