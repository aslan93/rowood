<?php

namespace app\modules\InsectScreen\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\InsectScreen\models\InsectScreenColorLang;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Color".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Percent
 * @property string $Ral
 *
 * @property ColorLang $lang
 * @property ColorLang[] $langs
 * @property ColorMaterial[] $materials
 * @property ColorProfile[] $profiles
 * @property ColorWood[] $woods
 * @property string $imagePath
 */
class InsectScreenColor extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'InsectScreenColor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image',], 'required'],
            [['Price'], 'number'],
            [['Position'], 'default', 'value' => 0],
            [['Position'], 'safe'],
            [['Image', 'Ral'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
            'Ral' => Yii::t('app', 'Ral'),
            'Position' => Yii::t('app', 'Position'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(InsectScreenColorLang::className(), ['InsectScreenColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(InsectScreenColorLang::className(), ['InsectScreenColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new InsectScreenColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/color/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->InsectScreenColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->with('lang');
        $result = ArrayHelper::map($query->all(), 'ID' ,'lang.Title');
        return $return + $result;
    }

    public static function getImageList($addEmpty = false)
    {
     $return = $addEmpty ? ['' => $addEmpty] : [];
     $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image','lang.Title');
        return $return + $result;
    }
    
}
