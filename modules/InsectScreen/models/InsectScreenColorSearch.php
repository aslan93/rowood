<?php

namespace app\modules\InsectScreen\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\InsectScreen\models\InsectScreenColor;

/**
 * ColorSearch represents the model behind the search form about `app\modules\InsectScreen\models\InsectScreenColor`.
 */
class InsectScreenColorSearch extends InsectScreenColor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Image', 'Ral'], 'safe'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InsectScreenColor::find()->with(['lang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
        ]);

        $query->andFilterWhere(['like', 'Image', $this->Image])
            ->andFilterWhere(['like', 'Ral', $this->Ral]);

        return $dataProvider;
    }
}
