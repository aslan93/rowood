<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Decoration\models\Decoration */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Decoration',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Decorations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="decoration-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
