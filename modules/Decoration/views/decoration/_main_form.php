<?php

    /* @var $form yii\widgets\ActiveForm */

    use yii\bootstrap\Html;
    use kartik\file\FileInput;
    use app\modules\Material\models\Material;
    
    $initialPreview = [];
    if ($model->imagePath)
    {
        $initialPreview[] = Html::img($model->imagePath, ['width' => 160]);
    }

?>

<br />
<div class="row">
    <div class="col-md-6">
        <label>Image</label>
        <?= FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
    </div>
</div>
<br />