<?php

namespace app\modules\Decoration\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Decoration".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property DecorationLang[] $decorationLangs
 */
class Decoration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Decoration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'Price'], 'required'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/decoration/' . $this->Image);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(DecorationLang::className(), ['DecorationID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(DecorationLang::className(), ['DecorationID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new DecorationLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->DecorationID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }

    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->with('lang');
        $result = ArrayHelper::map($query->all(),'ID','lang.Title');
        return $return + $result;
    }

    public static function getImageList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image','lang.Title');
        return $return + $result;
    }
}
