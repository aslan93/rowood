<?php

namespace app\modules\Decoration\models;

use Yii;

/**
 * This is the model class for table "DecorationLang".
 *
 * @property integer $ID
 * @property integer $DecorationID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Decoration $decoration
 */
class DecorationLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DecorationLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DecorationID', 'LangID', 'Title'], 'required'],
            [['DecorationID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['DecorationID'], 'exist', 'skipOnError' => true, 'targetClass' => Decoration::className(), 'targetAttribute' => ['DecorationID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'DecorationID' => Yii::t('app', 'Decoration ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDecoration()
    {
        return $this->hasOne(Decoration::className(), ['ID' => 'DecorationID']);
    }
}
