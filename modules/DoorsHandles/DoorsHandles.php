<?php

namespace app\modules\DoorsHandles;

/**
 * DoorsHandles module definition class
 */
class DoorsHandles extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\DoorsHandles\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
