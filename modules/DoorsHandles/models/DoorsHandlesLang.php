<?php

namespace app\modules\DoorsHandles\models;

use Yii;

/**
 * This is the model class for table "DoorsHandlesLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property integer $DoorsHandleID
 *
 * @property DoorsHandles $doorsHandle
 */
class DoorsHandlesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsHandlesLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Title', 'Text', 'DoorsHandleID'], 'required'],
            [['Text'], 'string'],
            [['DoorsHandleID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['DoorsHandleID'], 'exist', 'skipOnError' => true, 'targetClass' => DoorsHandles::className(), 'targetAttribute' => ['DoorsHandleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Text' => 'Text',
            'DoorsHandleID' => 'Doors Handle ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsHandle()
    {
        return $this->hasOne(DoorsHandles::className(), ['ID' => 'DoorsHandleID']);
    }
}
