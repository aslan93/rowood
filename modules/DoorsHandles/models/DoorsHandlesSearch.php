<?php

namespace app\modules\DoorsHandles\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\DoorsHandles\models\DoorsHandles;

/**
 * DoorsHandlesSearch represents the model behind the search form about `app\modules\DoorsHandles\models\DoorsHandles`.
 */
class DoorsHandlesSearch extends DoorsHandles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Price'], 'integer'],
            [['Type', 'Image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DoorsHandles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
        ]);

        $query->andFilterWhere(['like', 'Type', $this->Type])
            ->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
