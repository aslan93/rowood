<?php

namespace app\modules\DoorsHandles\models;

use Yii;
use app\modules\Product\models\Product;

/**
 * This is the model class for table "DoorsHandlesRel".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property integer $DoorsHandleID
 *
 * @property DoorsHandles $doorsHandle
 * @property Product $product
 */
class DoorsHandlesRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsHandlesRel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'DoorsHandleID'], 'integer'],
            [['DoorsHandleID'], 'exist', 'skipOnError' => true, 'targetClass' => DoorsHandles::className(), 'targetAttribute' => ['DoorsHandleID' => 'ID']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'DoorsHandleID' => 'Doors Handle ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsHandle()
    {
        return $this->hasOne(DoorsHandles::className(), ['ID' => 'DoorsHandleID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }
}
