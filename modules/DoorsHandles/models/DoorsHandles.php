<?php

namespace app\modules\DoorsHandles\models;

use Yii;
use yii\base\Model;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * This is the model class for table "DoorsHandles".
 *
 * @property integer $ID
 * @property integer $Price
 * @property string $Type
 * @property string $Image
 *
 * @property DoorsHandlesLang[] $doorsHandlesLangs
 * @property DoorsHandlesRel[] $doorsHandlesRels
 */
class DoorsHandles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsHandles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price'], 'required'],
            [['Price'], 'integer'],
            [['Type', 'Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Price' => 'Price',
            'Type' => 'Type',
            'Image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsHandlesLangs()
    {
        return $this->hasMany(DoorsHandlesLang::className(), ['DoorsHandleID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsHandlesRels()
    {
        return $this->hasMany(DoorsHandlesRel::className(), ['DoorsHandleID' => 'ID']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->DoorsHandleID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }

    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    public static function getImageList($addEmpty = false,$id = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $mods = DoorsHandlesRel::find()->where(['ProductID' => $id])->with('doorsHandle')->all();
        $modelss = [];
        foreach ($mods as $key => $mod){
            $modelss[$key] = $mod->doorsHandle;
        }

        $result2 = [];
        foreach ($modelss as $key => $mod){

            $result2[$mod->lang->Title] =[$mod->ID => '<div class="handle-item" data-attribute="'.$mod->Price.'"><img class="image" src="'.Url::to('@web/uploads/doors-handles/'.$mod->Image).'"/>'.'<br>+ '.$mod->Price .' €  </div>',
            ];

        }return $return + $result2;
    }
    public static function getListOfImages($addEmpty=false,$id=false){
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->andWhere(['ID'=>$id])->all(), 'ID', 'Image');
        return $return + $result;
    }
    public function getLang()
    {
        return $this->hasOne(DoorsHandlesLang::className(), ['DoorsHandleID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(DoorsHandlesLang::className(), ['DoorsHandleID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new DoorsHandlesLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/doors-handles/' . $this->Image);
    }
}
