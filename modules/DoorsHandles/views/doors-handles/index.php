<?php

use yii\helpers\Html;

use yii\widgets\Pjax;
use app\components\GridView\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\DoorsHandles\models\DoorsHandlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Doors Handles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doors-handles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Doors Handles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function($model)
                {
                    return Html::img($model->imagePath, ['height' => 80]);
                },
                'filter' => false,
            ],
            [
                'attribute' => 'lang.Title',
                'filter' => false,
            ],
            'Price',

            ['class' => 'app\components\GridView\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
