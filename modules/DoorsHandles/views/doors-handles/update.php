<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\DoorsHandles\models\DoorsHandles */

$this->title = 'Update Doors Handles: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Doors Handles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="doors-handles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
