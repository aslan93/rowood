<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\DoorsHandles\models\DoorsHandles */

$this->title = 'Create Doors Handles';
$this->params['breadcrumbs'][] = ['label' => 'Doors Handles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doors-handles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
