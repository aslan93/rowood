<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Safety\models\SafetySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Safeties');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="safety-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Safety'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::img($model->imagePath, ['height' => 80]);
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'lang.Title',
                    'filter' => false,
                    'value' => function($model)
                    {
                        return $model->lang->Title . ' (' . $model->lang->Subtitle . ')';
                    },
                ],
                'Price',

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
