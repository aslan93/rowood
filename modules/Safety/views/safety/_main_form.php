<?php

    use yii\bootstrap\Html;
    use kartik\file\FileInput;
    
    /* @var $this yii\web\View */
    /* @var $model app\modules\Glass\models\Glass */
    /* @var $form yii\widgets\ActiveForm */
    
    $initialPreview = [];
    if ($model->imagePath)
    {
        $initialPreview[] = Html::img($model->imagePath, ['width' => 160]);
    }

?>

<br />
<div class="row">
    <div class="col-md-6">
        <label>Image</label>
        <?= FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'Price')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<br />