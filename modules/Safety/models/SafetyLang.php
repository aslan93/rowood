<?php

namespace app\modules\Safety\models;

use Yii;

/**
 * This is the model class for table "SafetyLang".
 *
 * @property integer $ID
 * @property integer $SafetyID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Safety $safety
 */
class SafetyLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SafetyLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SafetyID', 'LangID', 'Title', 'Subtitle', 'Text'], 'required'],
            [['SafetyID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'Subtitle'], 'string', 'max' => 255],
            [['SafetyID'], 'exist', 'skipOnError' => true, 'targetClass' => Safety::className(), 'targetAttribute' => ['SafetyID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SafetyID' => Yii::t('app', 'Safety ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Subtitle' => Yii::t('app', 'Subtitle'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSafety()
    {
        return $this->hasOne(Safety::className(), ['ID' => 'SafetyID']);
    }
}
