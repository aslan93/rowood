<?php

namespace app\modules\Safety\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Safety\models\Safety;

/**
 * SafetySearch represents the model behind the search form about `app\modules\Safety\models\Safety`.
 */
class SafetySearch extends Safety
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Image'], 'safe'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Safety::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
        ]);

        $query->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
