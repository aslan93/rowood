<?php

use kartik\popover\PopoverX;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\Pjax;
use yii\helpers\Url;

$bundle = RowoodAssets::register($this);

Pjax::begin([
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'id'                 => 'cart-info-pjax',
    'timeout'            => 5000,
]);
PopoverX::begin([
    'header'       => 'Cosul cu produse',
    'id'           => 'cart-popover',
    'size'         => PopoverX::SIZE_MEDIUM,
    'placement'    => PopoverX::ALIGN_AUTO_RIGHT,
    'footer'       => '<span class="total">Total: ' . $total . ' € ' . '</span>' . Html::a('Vezi cos', '/cart/', ['class' => 'btn btn-primary']),
    'closeButton'  => ['style' => 'display:inline'],
    'toggleButton' => [
        'label' => '',
        'class' => 'fa fa-shopping-cart ',
        'id'    => 'btn-popover',
        'tag'   => 'i',
        'style' => 'color: #e3210d;
                    font-size: 16px;
                    margin-left: -60px;
                    padding: 17px 0px;'
    ],
]);
?>
<div class="copover">
    <style>
        .copover div{
            padding-left: 0!important;
            padding-right: 0!important;
        }
        .copover .price{
            color: #e3210d;
        }
        .total{
            margin-right: 20px;
        }
        .copover .item-title{
            padding-top: 10px;
        }
    </style>

    <?php
    foreach ($products as $product)
    {
        ?>
        <div class="col-sm-12" style="">
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <a href="<?=Url::to(['/shop-page/shop-page/product/','id'=>$product['id']])?>">
                        <?= Html::img($product['image'], ['width' => '50px']) ?>
                    </a>
                </div>
                <div class="col-sm-9 item-title">
                        <?= $product['title'] ?> x <?= $product['Quantity'] ?>
                    <div class="col-sm-12 price">
                    <?= $product['subtotal'] ?> €
                    </div>
                </div>
            </div>

        </div>
        <?php
    }
    ?>
</div>
<?php
PopoverX::end();

$this->registerJs('
                        $("#cart-info-pjax").on("pjax:end",function(){
                            
                            $("#btn-popover").popoverButton({
                                target: "#cart-popover",
                            });
                        })
                    ');

Pjax::end();
?>

