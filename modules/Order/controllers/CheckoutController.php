<?php

namespace app\modules\Order\controllers;


use app\modules\Order\models\Order;
use app\modules\Order\models\OrderTransportation;
use app\modules\User\models\UserInfo;
use Yii;
use app\components\Controller\FrontController;
use app\modules\User\models\User;
use app\components\Controller\Controller;
use app\modules\Login\models\LoginForm;
use yii\helpers\Url;


/**
 * OrderController implements the CRUD actions for Order model.
 */
class CheckoutController extends FrontController
{

    public function actionIndex($orderID)
    {
        if (Yii::$app->user->isGuest)
        {
            return $this->render('index', [
                'orderID' => $orderID,
            ]);
        }else {
//            return $this->render('index', [
//                'orderID' => $orderID,
//            ]);
            Yii::$app->response->redirect('/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID);
            Yii::$app->end();
        }
    }
    public function actionTransportation($orderID)
    {
        $user = User::find()->where(['ID'=>Yii::$app->user->identity->ID])->with('userInfo')->one();
        if(Yii::$app->user->identity->ID && $order = Order::findOne($orderID)) {
        $order->UserID = $user->ID;
        $order->save();
        }

        if ($modelTransportation = OrderTransportation::find()->where(['OrderID'=>$orderID])->one()){

        }else{
            $modelTransportation = new OrderTransportation();
        }

        $modelTransportation->OrderID = $orderID;

        if (Yii::$app->user->isGuest)
        {

            return $this->redirect('/admin/ecommerce/order/checkout/', [
                'orderID' => $orderID,
            ]);
        }else {
            return $this->render('transportation', [
                'model' => $modelTransportation,
                'orderID' => $orderID,
                'user' => $user,
            ]);
        }
    }
    public function actionCheckout($orderID)
    {
        $order = Order::find()->with('orderTransportation')->where(['ID'=>$orderID])->one();


        if (Yii::$app->user->isGuest)
        {
            return $this->redirect('/admin/ecommerce/order/checkout/', [
                'orderID' => $orderID,
            ]);
        }else {
            return $this->render('checkout', [
                'orderID' => $orderID,
                'order' => $order,

            ]);
        }

    }
    public function actionPay($orderID)
    {   $order = Order::find()->with('orderTransportation')->where(['ID'=>$orderID])->one();
        $order->TotalPrice = $order->TotalPrice + $order->orderTransportation->Price;
        $user = User::find()->where(['ID'=>Yii::$app->user->identity->ID])->with('userInfo')->one();
        $transportation = OrderTransportation::find()->where(['OrderID'=>$orderID])->one();
        return $this->render('pay',[
            'orderID' => $orderID,
            'amount' => $order->TotalPrice,
           // 'firstName' => $user->userInfo->FirstName,
           // 'lastName' => $user->userInfo->LastName,
            'email'=> $user->Email,
           // 'phone' => $user->userInfo->Phone,
            'address'=> $transportation->Adresa,
            'returnUrl' =>'/admin/ecommerce/order/checkout/order',
        ]);

//        Yii::$app->response->redirect(Url::to(['/admin/ecommerce/payment/pay/mobil-pay/',
//            'orderID' => $orderID,
//            'amount' => $order->TotalPrice,
//            'firstName' => $user->userInfo->FirstName,
//            'lastName' => $user->userInfo->LastName,
//            'email'=> $user->Email,
//            'phone' => $user->userInfo->Phone,
//            'address'=> $transportation->Adresa,
//            'returnUrl' =>'/admin/ecommerce/order/checkout/order',
//
//        ]));
//        Yii::$app->end();

    }
    public function actionOrder($orderID)
    {   $order = Order::find()->where(['ID'=>$orderID])->with('orderProducts.product.doorColor','orderProducts.product.doorSize','orderProducts.product.doorHandle')->one();

        if (Yii::$app->user->isGuest)
        {
            return $this->redirect('/admin/ecommerce/order/checkout/', [
                'orderID' => $orderID,
            ]);
        }else {
            return $this->render('order', [
                'orderID' => $orderID,
                'order' => $order,
            ]);
        }
    }

}
