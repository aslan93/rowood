<?php

namespace app\modules\Order\controllers;

use app\modules\Discount\models\Discount;
use app\modules\Order\models\OrderTransportation;
use app\modules\Product\models\Product;
use Yii;
use app\modules\Order\models\Order;
use app\modules\Order\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\Admin\controllers\AdminController;
use app\modules\Order\models\OrderProducts;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   $model = new Order();
        $total = 0;
        if (isset($_SESSION['cart']))
        {
            if ($model->save()){
                foreach ($_SESSION['cart'] as $key => $item){
                    $product = Product::findOne($item['ProductID']);
                    $modelProducts = new OrderProducts();
                    $modelProducts->OrderID = $model->ID;
                    $modelProducts->ProductID = $item['ProductID'];
                    $modelProducts->Quantity = $item['Quantity'];
                    $total += $item['Quantity'] * ($product->Status == 'Configurated' ? $product->Price : $product->totalPrice);
                    if ($modelProducts->save()) {

                    }
                }
            }
        }
        $discount = Discount::find()->where(['<=', 'Price', $total] )->orderBy('Price DESC')->limit(1)->one();
        $discountTotal = $total * ($discount->Percent/100);
        $discountTotal = $total - $discountTotal;
        $model->TotalPrice = $discountTotal;
        $model->save();

        Yii::$app->response->redirect('/checkout/?orderID='.$model->ID);
        Yii::$app->end();
    }
    
    public function actionAddTransportation($orderID){
        if ($model = OrderTransportation::find()->where(['OrderID'=>$orderID])->one()){
        }else{
            $model = new OrderTransportation();
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->Livrare == 1){
                $model->save();
                Yii::$app->response->redirect('/check/?orderID='.$orderID);
                Yii::$app->end();
            }else{
                $model->Price = 0;
                $model->save();
                Yii::$app->response->redirect('/check/?orderID='.$orderID);
                Yii::$app->end();
            }

        } else {
            Yii::$app->response->redirect('/transportation/?orderID='.$orderID);
            Yii::$app->end();
        }
    }
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
