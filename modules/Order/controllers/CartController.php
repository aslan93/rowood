<?php

namespace app\modules\Order\controllers;

use app\modules\Discount\models\Discount;
use app\modules\DoorsSizes\models\DoorsSizes;
use app\modules\Order\models\OrderProducts;
use app\modules\Product\models\Product;
use app\modules\Product\models\ProductLang;
use Yii;
use app\modules\Order\models\Order;
use app\modules\Order\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Controller\FrontController;


/**
 * OrderController implements the CRUD actions for Order model.
 */
class CartController extends FrontController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPdf(){
        Yii::$app->response->format = 'pdf';

        // Rotate the page
        Yii::$container->set(Yii::$app->response->formatters['pdf']['class'], [
            'format' => [216, 356], // Legal page size in mm
            'orientation' => 'Landscape', // This value will be used when 'format' is an array only. Skipped when 'format' is empty or is a string
            'beforeRender' => function($mpdf, $data) {},
        ]);

        //$this->layout = '//print';
        return $this->render('view', []);
    }
    public function actionView($print = false)
    {   $discounts = Discount::find()->all();

        $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [] ;
        $products = [];
        $total = 0;
        foreach ($cart as $key => $item){
            $product = Product::find()->where(['ID'=>$key])->with(['lang'])->one();
            
            $subtotal = $item['Quantity'] * ($product->Status == 'Configurated' ? $product->Price : $product->totalPrice);
            $total += $subtotal;

            $products[] = [
                'Quantity' => $item['Quantity'],
                'title' => !empty($item['name']) ? $item['name'] : $product->lang->Title,
                'image' => $product->ParentID>0 ? $product->parentMainImage->imagePath : (($product->Status == 'Configurated') ? $product->mainImage->typeImagePath : $product->mainImage->imagePath),
                'price' => $product->Status == 'Configurated' ? $product->Price : $product->totalPrice,
                'id' => $key,
                'subtotal' => $subtotal,
                'status' => $product->Status,
                'productInfo' => $product->Status == 'Configurated' ? $product->ConfiguratorInfo : $product->productInfo,
            ];
        }
        $discountTotal = 0;
        $currPercent = 0;
        foreach ($discounts as $discount){

            if ($discount->Price <= $total){

                $discountTotal = $total * ($discount->Percent/100);
                $currPercent = $discount->Percent;
            }
        }
        if ($print == true) {
             Yii::$app->response->format = 'pdf';
                $this->layout = '//print';
            return $this->renderPartial('viewPdf',[
                'products' => $products,
                'total' => $total,
                'discountTotal' => $discountTotal,
                'discounts' => $discounts,
                'percent' => $currPercent,
            ]);
        }else{
            if (count($products)>0) {
                return $this->render('view', [
                    'products' => $products,
                    'total' => $total,
                    'discountTotal' => $discountTotal,
                    'discounts' => $discounts,
                    'percent' => $currPercent,
                ]);
            }else{
                return $this->render('no-products');
            }
        }
    }

    public function actionAddToCart($id, $quantity = 1, $return = true, $isConfigurable = false)
    {
        if (!isset($_SESSION['cart']))
        {
            $_SESSION['cart'] = [];
        }
        
        $product = Product::findOne($id);
        
        if ($isConfigurable)
        {
            $product->Status = 'Configurated';
            $product->save();
        }
        
        $price = $product->totalPrice;
        
        if (isset($_SESSION['cart'][$id]))
        {
            $_SESSION['cart'][$id]['Quantity'] = $quantity;
        }
        else
        {
            $_SESSION['cart'][$id] = [
                'ProductID' => $id,
                'price' => $price,
                'name' => $isConfigurable ? 'Fereastră configurată' : '',
                'Quantity' => $quantity,
            ];
        }

        if ($return)
        {
            return '<div class="title">Adaugat in cos!</div>';
        }else{
            return '<div class="title">eroare</div>';
         }
    }
    public function actionSetProduct($id,$quantity = 1){

        $product = Product::find()->where(['ID'=>$id])->with('lang')->one();
        $model = new Product(
            $product->getAttributes()
        );

        $model->ID = null;
        if (!isset($product->ParentID)){
            $model->ParentID = $product->ID;
        }else{
            $model->ParentID = $product->ParentID;
        }

        $model->Visibility = 0;
        $model->FrontColorID = Yii::$app->request->get('Product')['FrontColorID'];
        $model->HandleID = Yii::$app->request->get('Product')['HandleID'];

        $model->SizeID = Yii::$app->request->get('Product')['SizeID'];
        $model->OpenDirection = Yii::$app->request->get('Product')['OpenDirection'];

        if ($model->save()) {

            foreach ($product->langs as $langModel){
                $newmodel = new ProductLang(
                    $langModel->getAttributes()
                );
                $newmodel->ID = null;
                $newmodel->ProductID = $model->ID;
                $newmodel->save();
            }
            return $this->actionAddToCart($model->ID,$quantity);
        } else {

            return '<div class="title">Eroare!</div>';
        }

    }

    public function actionDeleteItem($id,$quantity = 1,$return = true)
    {

        if (isset($_SESSION['cart'][$id]))
        {
            unset($_SESSION['cart'][$id]);
            return 'success';
        }else{
            return false;
        }


    }
    
    public function actionDedit($epid)
    {
        $this->actionDeleteItem($epid, 1, true);
        return $this->redirect(\yii\helpers\Url::to(['/calculator', 'pid' => $epid]));
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
