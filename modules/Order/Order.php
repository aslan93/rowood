<?php

namespace app\modules\Order;

use app\modules\Admin\Admin;
/**
 * Order module definition class
 */
class Order extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Order\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
