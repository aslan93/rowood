<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\modules\Order\controllers\QHelper;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\web\View;

$bundle = RowoodAssets::register($this);
?>

<div class="cart-view">

    <style>
        .cart-item-labels .centered-text{
            /*text-align: center;*/
            display: block;
            font-size: 16px;
            font-weight: 600;
        }
        .cart-item-info .info{
            /*text-align: center;*/
            display: block;
            padding-top: 15px;

        }
        .cart-item-info .image{
            display: block;
            padding: 5px;
        }
        .cart-item-info .product-title{
            font-weight: 600;
            font-size: 18px;
        }
        .cart-view .container .row{
            border: 1px solid rgba(51, 51, 51, 0.32);
            border-top: none;
        }
        .cart-item-labels{
            border-top: 1px solid rgba(51, 51, 51, 0.32)!important;
            background: rgba(51, 51, 51, 0.32);
        }

        .cart-view {
            margin-bottom: 50px;
        }
        .cart-view a{
            text-decoration: none;
            color: black;
        }
        .final-price{
            font-size: 16px;
            font-weight: 600;
            padding: 10px;
        }
        .final-price .sum{
            font-size: 18px;
            font-weight: 600;
            color: #e3210d;
        }
        .checkout-button{
            padding-top: 15px;
            margin-top: 10px;
            display: inline-block;
            width: 150px;
            height: 60px;
            background-color: #fff;
            border: 2px solid #e3210d;
            font-family: "FuturaPT-Demi" ,sans-serif;
            font-size: 20px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
        }
        .checkout-button:hover{
            border: 2px solid #e3210d;
            background-color: #e3210d;
            color: #ffffff;
        }
    </style>

    <div class="container">
        <div class="col-sm-12">
            <div class="row cart-item-labels">
                <div class="col-sm-1">
                    <span class="centered-text">Nr</span>
                </div>
                <div class="col-sm-1">
                    <span class="centered-text">Imagine</span>
                </div>
                <div class="col-sm-2">
                    <span class="centered-text">Denumire</span>
                </div>
                <div class="col-sm-1">
                    <span class="centered-text">Cantitate</span>
                </div>
                <div class="col-sm-5">
                    <span class="centered-text">Detalii</span>
                </div>
                <div class="col-sm-2">
                    <span class="centered-text">Pret</span>
                </div>
            </div>
            <?php Pjax::begin([
                    'id' => 'p1',
            ])?>
            <?php foreach ($products as $index => $product) { ?>


                <?=Html::beginForm('/admin/ecommerce/order/cart/add-to-cart/','get',['class' => 'quantity-form','id'=>'cart-item'.$product['id']])?>

                <?=Html::hiddenInput('return',true)?>
                <?=Html::hiddenInput('id',$product['id'])?>
            <div class="row cart-item-info">
                    <div class="col-sm-1 info ">
                        <?=$index+1?><br><input type="button" onclick="deleteThis(this.form)" value="X" style="position: absolute; font-weight: 600; color: red; margin-top: 10px;margin-left: 50px;">
                    </div>
                    <div class="col-sm-1 image">
                        <?php if ($product['status'] == 'Configurated') { ?>
                        <?=Html::img($product['image'],['width'=> '100'])?>
                        <a data-pjax="0" style="margin: 20px;color: #397aa3;font-weight: 900;" class="text-center btn btn-link" href="<?= Url::to(['dedit', 'epid' => $product['id']]) ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                        <?php } else { ?>
                        <a href="<?=Url::to(['/shop-page/shop-page/product/','id'=>$product['id']])?>">
                            <?=Html::img($product['image'],['width'=> '100'])?>
                        </a>
                        <?php } ?>
                    </div>
                    <div class="col-sm-2 info product-title">
                        <?php
                        if($product['status'] == 'Configurated') {
                         $link = '#';
                        }else{
                          $link = Url::to(['/shop-page/shop-page/product/','id'=>$product['id']]);
                        }
                        ?>
                        <a href="<?=$link?>">
                        <?=$product['title']?>
                        </a>
                    </div>
                    <div class="col-sm-1 info">
                        <?=Html::dropDownList('quantity',$product['Quantity'],QHelper::getQuantity(),[
                            'onchange' => "submitForm(this.form)"
                        ])?>
                    </div>
                    <div class="col-sm-5 info">

                        <?php if ($product['status'] == 'Configurated') { ?>
                        <?php if (!empty($product['productInfo'])) { ?>
                        <?php $data = unserialize($product['productInfo']);
                              unset($data['Dimensions']);
                              unset($data['Prices']);
                              unset($data['WindowProfileQuantity']);
                              unset($data['ProfileQuantity1']);
                              unset($data['ProfileQuantity']);
                              unset($data['GlassQuantity']);
                        ?>
                        <?php foreach ($data as $key => $val) { ?>
                        <?php if (empty($val)) continue; ?>
                        <div style="margin: 3px;"><b><?= Yii::t('app', $key) ?>:</b> <?= $val ?></div>
                        <?php } ?>
                        <?php } ?>
                        <?php } else { ?>
                        <div style="margin: 3px;"><b>Culoare:</b> <?=$product['productInfo']->doorColor->lang->Title?>   <span class="label label-default">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                        <div style="margin: 3px;"><b>Dimensiuni:</b> <?=$product['productInfo']->doorSize->WidthHeight?>   <span class="label label-default">+  <?=$product['productInfo']->doorSize->Price?> &euro;</span></div>
                        <div style="margin: 3px;"><b>Maner:</b> <?=$product['productInfo']->doorHandle->lang->Title?>   <span class="label label-default">+  <?=$product['productInfo']->doorHandle->Price?> &euro;</span></div>
                        <?php
                        if (isset($product['productInfo']->material)) {
                            ?>
                            <div style="margin: 3px;"><b>Material:</b> <?= $product['productInfo']->material->lang->Title ?> <span
                                        class="label label-default">+ <?= $product['productInfo']->material->Price ?>
                                    &euro;</span></div>
                            <?php
                        }
                        ?>
                        <?php } ?>

                    </div>

                    <div class="col-sm-2 info">
                        <span>Unitate: </span><?=$product['price']?> &euro;<br>
                        <span>Total: </span><b><?=$product['subtotal']?> &euro; </b><br>
                    </div>
            </div>
                <?=Html::endForm()?>

            <?php
            }
            ?>

            <div class="row">
                <div class="col-sm-3">

                </div>
                <div class="col-sm-3">

                </div>
                <div class="col-sm-3"><b>Reducere la cantitate</b>
                    <?php
                    foreach ($discounts as $discount){
                        ?>
                        <div><b><?=$discount->Percent?>%</b> &nbsp; <span><?=$discount->Price?> &euro; </span></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-sm-3">
                    <div class="pull-right">

                        <div class="final-price">Pret total: <span class="sum"><?=$total?> &euro;</span></div>
                        <div class="final-price">Reducere: <span class="sum"><?=$percent?> % = <?=$discountTotal?> &euro;</span></div>
                        <div class="final-price">Pret cu reducere: <span class="sum"><?=$total-$discountTotal?> &euro;</span></div>


                    </div>
                </div>

            </div>
            <?php
            Pjax::end();
            ?>
        </div>
    </div>

    <div class="container ">
        <div class="pull-right">
            <a href="<?= Url::to(['/admin/ecommerce/order/order/create/']) ?>"
               class="btn-primary checkout-button" data-pjax="false">
                ACHITARE
            </a>
        </div>
    </div>
</div>

<section class="cart-wrap">
    <div class="container">
        <div class="title">
            Cos
        </div>
        <form action="#" method="post">
            <div class="pages-cart-wrap">
                <div class="cart-page-step active">
                    <span class="step-page">
                        1
                    </span>
                    <span class="name-page">
                        Cos
                    </span>
                </div>
                <div class="cart-page-step">
                     <span class="step-page">
                        2
                    </span>
                    <span class="name-page">
                        Datele personale
                    </span>
                </div>
                <div class="cart-page-step">
                     <span class="step-page">
                         3
                     </span>
                     <span class="name-page">
                         Transportare
                     </span>
                </div>
                <div class="cart-page-step">
                     <span class="step-page">
                        4
                     </span>
                     <span class="name-page">
                        Plata
                     </span>
                </div>
                <div class="cart-page-step">
                     <span class="step-page">
                        5
                     </span>
                     <span class="name-page">
                        Comanda
                     </span>
                </div>
            </div>
            <div class="cart-wrap-item">
                <table>
                    <thead>
                        <tr>
                            <th>
                                Nr
                            </th>
                            <th>
                                Imagine
                            </th>
                            <th>
                                Denumire
                            </th>
                            <th>
                                Cantitate
                            </th>
                            <th>
                                Detalii
                            </th>
                            <th>
                                Pret
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="fixed-h">
                                    1
                                </div>
                            </td>
                            <td>
                                <div class="fixed-h">
                                      <span class="img-wrap">
                                        <img src="<?= $bundle->baseUrl ?>/images/glass-item1.png" alt="">
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span class="product-name">
                                    <span class="fixed-h">
                                        Gelsenkirchen
                                    </span>
                                </span>
                            </td>
                            <td>
                                <div class="quantity-box">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="quantity1">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>

                                        <input type="number" class="form-control input-number" name="quantity1" value="1">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity1">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="product-details">
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Culoare:
                                        </span>
                                        <span class="light">
                                            Castan
                                        </span>
                                        <div class="label">
                                            +200€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Dimensiuni:
                                        </span>
                                        <span class="light">
                                            1000x2000mm
                                        </span>
                                        <div class="label">
                                            +0€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Maner:
                                        </span>
                                        <span class="light">
                                            Aluminiu 203
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="show-more-details">
                                        <span class="default-text text">Mai multe</span>
                                        <span class="active-text text hide">
                                            Ascundeti
                                        </span>
                                        <span class="fa fa-caret-down"></span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="prices">
                                    <div>
                                        <span class="light">
                                            Unitate
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                    <div>
                                        <span class="bold">
                                            Total
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="fixed-h">
                                    2
                                </div>
                            </td>
                            <td>
                                <div class="fixed-h">
                                      <span class="img-wrap">
                                        <img src="<?= $bundle->baseUrl ?>/images/glass-item1.png" alt="">
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span class="product-name">
                                    <span class="fixed-h">
                                        Gelsenkirchen
                                    </span>
                                </span>
                            </td>
                            <td>
                                <div class="quantity-box">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="quantity2">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>

                                        <input type="number" class="form-control input-number" name="quantity2" value="1">                                    <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity2">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="product-details">
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Culoare:
                                        </span>
                                        <span class="light">
                                            Castan
                                        </span>
                                        <div class="label">
                                            +200€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Dimensiuni:
                                        </span>
                                        <span class="light">
                                            1000x2000mm
                                        </span>
                                        <div class="label">
                                            +0€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Maner:
                                        </span>
                                        <span class="light">
                                            Aluminiu 203
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="show-more-details">
                                        <span class="default-text text">Mai multe</span>
                                        <span class="active-text text hide">
                                            Ascundeti
                                        </span>
                                        <span class="fa fa-caret-down"></span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="prices">
                                    <div>
                                        <span class="light">
                                            Unitate
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                    <div>
                                        <span class="bold">
                                            Total
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="fixed-h">
                                    3
                                </div>
                            </td>
                            <td>
                                <div class="fixed-h">
                                      <span class="img-wrap">
                                        <img src="<?= $bundle->baseUrl ?>/images/glass-item1.png" alt="">
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span class="product-name">
                                    <span class="fixed-h">
                                        Gelsenkirchen
                                    </span>
                                </span>
                            </td>
                            <td>
                                <div class="quantity-box">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="quantity3">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>

                                        <input type="number" class="form-control input-number" name="quantity3" value="1">                                    <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity3">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="product-details">
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Culoare:
                                        </span>
                                        <span class="light">
                                            Castan
                                        </span>
                                        <div class="label">
                                            +200€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Dimensiuni:
                                        </span>
                                        <span class="light">
                                            1000x2000mm
                                        </span>
                                        <div class="label">
                                            +0€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Maner:
                                        </span>
                                        <span class="light">
                                            Aluminiu 203
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="show-more-details">
                                        <span class="default-text text">Mai multe</span>
                                        <span class="active-text text hide">
                                            Ascundeti
                                        </span>
                                        <span class="fa fa-caret-down"></span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="prices">
                                    <div>
                                        <span class="light">
                                            Unitate
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                    <div>
                                        <span class="bold">
                                            Total
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="fixed-h">
                                    4
                                </div>
                            </td>
                            <td>
                                <div class="fixed-h">
                                      <span class="img-wrap">
                                        <img src="<?= $bundle->baseUrl ?>/images/glass-item1.png" alt="">
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span class="product-name">
                                    <span class="fixed-h">
                                        Gelsenkirchen
                                    </span>
                                </span>
                            </td>
                            <td>
                                <div class="quantity-box">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="quantity4">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>

                                        <input type="number" class="form-control input-number" name="quantity4" value="1">                                    <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity4">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="product-details">
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Culoare:
                                        </span>
                                        <span class="light">
                                            Castan
                                        </span>
                                        <div class="label">
                                            +200€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Dimensiuni:
                                        </span>
                                        <span class="light">
                                            1000x2000mm
                                        </span>
                                        <div class="label">
                                            +0€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                            Maner:
                                        </span>
                                        <span class="light">
                                            Aluminiu 203
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="product-details-item">
                                        <span class="bold">
                                           Material:
                                        </span>
                                        <span class="light">
                                            Lemn
                                        </span>
                                        <div class="label">
                                            +20€
                                        </div>
                                    </div>
                                    <div class="show-more-details">
                                        <span class="default-text text">Mai multe</span>
                                        <span class="active-text text hide">
                                            Ascundeti
                                        </span>
                                        <span class="fa fa-caret-down"></span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="prices">
                                    <div>
                                        <span class="light">
                                            Unitate
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                    <div>
                                        <span class="bold">
                                            Total
                                        </span>
                                        <span class="price-red">
                                            1100€
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="finally-info">
                    <div class="row">
                        <div class="col-md-9 col-sm-6 col-xs-6">
                            <span class="text">
                                Reducere la cantitate
                            </span>
                            <span>
                                <span class="label">
                                    4%
                                </span>
                                <span class="text">
                                    10000 €
                                </span>
                            </span>
                            <span>
                                <span class="label">
                                    5%
                                </span>
                                <span class="text">
                                    15000 €
                                </span>
                            </span>
                            <span>
                                <span class="label">
                                    6%
                                </span>
                                <span class="text">
                                    20000 €
                                </span>
                            </span>
                            <span>
                                <span class="label">
                                    6%
                                </span>
                                <span class="text">
                                    25000 €
                                </span>
                            </span>
                            <span>
                                <span class="label">
                                    8%
                                </span>
                                <span class="text">
                                    30000 €
                                </span>
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="prices">
                                <div>
                                    <span class="light">
                                        Pret total:
                                    </span>
                                    <span class="price-red">
                                        1610 €
                                    </span>
                                </div>
                                <div>
                                    <span class="light">
                                        Reducere:
                                    </span>
                                        <span class="price-red">
                                        0 % = 0 €
                                    </span>
                                </div>
                                <div>
                                    <span class="light">
                                        Pret cu reducere:
                                    </span>
                                        <span class="price-red">
                                        1610 €
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="submit-form-details cart-submit text-right">
                    <button>
                        ACHITARE
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php $this->registerJs(
        "   function submitForm(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: form.attr('action'),
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});
             
                    },
                    error: function () {}
                });

            }
            
            function deleteThis(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: '/admin/ecommerce/order/cart/delete-item/',
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});   
                    },
                    error: function () {}
                });

            }
            "
        ,View::POS_HEAD);
?>
