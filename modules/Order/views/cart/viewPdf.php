<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\modules\Order\controllers\QHelper;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\web\View;

?>

<div class="cart-view">

    <style>
        * {
            font-size: 10px !important;
        }
        table{
            width: 100%;
            font-family: 'Open Sans';

        }
        table th{
            padding: 10px 0;
        }
        table td{
            text-align: center;
        }
        table .change-width{
            width: 40%;
            text-align: left;
        }
        table td{
            padding: 10px;
        }
        table th.change-width{
            text-align: center;
        }
        .change-width>div>span{
            font-size: 10px;
            font-weight: 700;
        }
    </style>
    <table style="border-collapse: collapse;" border="1" >
        <thead>
        <tr>
            <th>
                Nr
            </th>
            <th>
                Dnumire
            </th>
            <th>
                Cantitate
            </th>
            <th class="change-width">
                Detalii
            </th>
            <th>
                Pret
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products as $index => $product) { ?>
            <tr>
                <td>
                    <?=$index+1?>
                </td>
                <td>
                    <span style="font-weight: 600; font-size: 14px"><?=$product['title']?></span>
                </td>
                <td>
                    <?=$product['Quantity']?>
                </td>
                <td class="change-width">
                    <?php if ($product['status'] == 'Configurated') { ?>
                    <?php if (!empty($product['productInfo'])) { ?>
                    <?php $data = unserialize($product['productInfo']);?>
                    <?php foreach ($data as $key => $val) { ?>
                    <?php if (empty($val)) continue; ?>
                    <div style="margin: 3px;"><b><?= Yii::t('app', $key) ?>:</b> <?= $val ?></div>
                    <?php } ?>
                    <?php } ?>
                    <?php } else { ?>
                    <div style="margin: 3px;"><b>Culoare:</b> <?=$product['productInfo']->doorColor->lang->Title?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px; margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                    <div style="margin: 3px;"><b>Dimensiuni:</b> <?=$product['productInfo']->doorSize->WidthHeight?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                    <div style="margin: 3px;"><b>Maner:</b> <?=$product['productInfo']->doorHandle->lang->Title?>   <span style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+  <?=$product['productInfo']->doorColor->Price?> &euro;</span></div>
                    <?php
                    if (isset($product['productInfo']->material)) {
                        ?>
                        <div style="margin: 3px;"><b>Material:</b> <?= $product['productInfo']->material->lang->Title ?> <span
                                    style="background-color: #8b9bb2!important; color: #fff; padding: 1px;margin: 1px;">+ <?= $product['productInfo']->doorColor->Price ?>
                                &euro;</span></div>
                        <?php
                    }
                    ?>
                    <?php } ?>
                </td>
                <td>
                    Unitate: <?=$product['price']?> &euro; <br/>
                    Total: <span style="font-weight: 600; font-size: 14px"><?=$product['subtotal']?> &euro;</span>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                Reduceri la cantitate <br />
                <?php
                foreach ($discounts as $discount){
                    ?>
                    <span style="font-weight: 600; font-size: 14px;"><?=$discount->Percent?>%</span> <?=$discount->Price?> &euro; <br />
                    <?php
                }
                ?>
            </td>
            <td colspan="2">

                Pret total: <span style="font-weight: 900; font-size: 20px;"><?=$total?> &euro;</span><br>
                Reducere: <span style="font-weight: 900; font-size: 20px;"><?=$percent?> % = <?=$discountTotal?> &euro;</span><br>
                Pret cu reducere: <span style="font-weight: 900; font-size: 20px;"><?=$total-$discountTotal?> &euro;</span><br>
            </td>
        </tr>
        </tbody>
    </table>

</div>

