<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\modules\Order\controllers\QHelper;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\web\View;

$bundle = RowoodAssets::register($this);
?>

<div class="cart-view">

    <div class="container ">
        <div class="text-center" style="padding: 100px 0; text-transform: uppercase">
            <h2><?=Yii::t('app','Nu aveti produse in cos')?></h2>
        </div>
    </div>
</div>

