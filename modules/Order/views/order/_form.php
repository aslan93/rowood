<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Order\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TotalPrice')->textInput() ?>

    <?= $form->field($model, 'OrderDate')->textInput() ?>

    <?= $form->field($model, 'PayToken')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserID')->textInput() ?>

    <div class="row">
        <div class="title"><h2>Products</h2></div>
        <br>
        <div class="row">
            <?php
            foreach($model->orderProducts as $product) {
                $type = \app\modules\Type\models\Type::findOne($product->product->TypeID);
                $variant = \app\modules\Type\models\Variant::findOne($product->product->VariantID);
                $rightDimensions = unserialize($product->product->RightDimensions);
                $bottomDimensions = unserialize($product->product->BottomDimensions);
                ?>

                <div class="col-md-12">
                    <div class="row"><hr>
                        <div class="title"><?=$product->product->lang->Title?></div>
                        <div class="image col-md-6">
                           <div class="panel-content-padding">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="calculator-img">
                                            <?= Html::img($variant->imagePath,['width'=>300]) ?>
                                        </div>
                                        <br>
                                        <?php if ($type->BottomDimensions) { ?>
                                            <?php for ($i = 0; $i < $type->BottomDimensions; $i++) { ?>
                                                <div style="display: inline-block; width: <?= round(100/$type->BottomDimensions) - 2 ?>%;" class="size-glass mt10" >
                                                    <label>
                                                        <input  type="text" placeholder="00" required="required" value="<?= $bottomDimensions[$i] ?>" name="BottomDimensions[]" style="width: 100px">
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="size-glass">
                                            <?php if ($type->RightDimensions) { ?>
                                                <?php for ($i = 0; $i < $type->RightDimensions; $i++) { ?>
                                                    <div class="top-glass mt30">
                                                        <label>
                                                            <input type="text" placeholder="00" required="required" value="<?= $rightDimensions[$i] ?>" name="LeftDimensions[]" style="width: 100px;  margin-top: 10px; margin-bottom: 30px;">
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="price"><h2>Total: <?=$product->product->Price?> Euro</h2> </div>
                        </div>

                        <div class="col-md-3">
                            <div ><h4>Optiuni selectate:</h4></div>
                            <br>
                            <div class="info">
                                <?php
                                if ($product->product->Status == 'Configurated') {
                                    $pr = [];
                                    $pr['productInfo'] = $product->product->ConfiguratorInfo;
                                  if (!empty($pr['productInfo'])) { ?>
                                        <?php $data = unserialize($pr['productInfo']);?>
                                      <?php
                                      $profileQuant = \yii\helpers\ArrayHelper::getValue($data, 'ProfileQuantity');
                                      $glassQuant = \yii\helpers\ArrayHelper::getValue($data, 'GlassQuantity');
                                      ?>
                                        <?php foreach ($data as $key => $val) { ?>
                                            <?php if (empty($val)) continue; ?>
                                            <div style="margin: 3px;"><b><?= Yii::t('app', $key) ?>:</b> <?= $val ?></div>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } else {
                                    $pr = [];
                                    $pr['productInfo'] = $product->product; ?>
                                    <div style="margin: 3px;"><b>Culoare:</b> <?=$pr['productInfo']->doorColor->lang->Title?>   <span class="label label-default">+  <?=$pr['productInfo']->doorColor->Price?> &euro;</span></div>
                                    <div style="margin: 3px;"><b>Dimensiuni:</b> <?=$pr['productInfo']->doorSize->WidthHeight?>   <span class="label label-default">+  <?=$pr['productInfo']->doorSize->Price?> &euro;</span></div>
                                    <div style="margin: 3px;"><b>Maner:</b> <?=$pr['productInfo']->doorHandle->lang->Title?>   <span class="label label-default">+  <?=$pr['productInfo']->doorHandle->Price?> &euro;</span></div>
                                    <?php
                                    if (isset($pr['productInfo']->material)) {
                                        ?>
                                        <div style="margin: 3px;"><b>Material:</b> <?= $pr['productInfo']->material->lang->Title ?> <span
                                                    class="label label-default">+ <?= $pr['productInfo']->material->Price ?>
                                                &euro;</span></div>
                                        <?php
                                    }
                                    ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div ><h4>Calcule de materiale:</h4></div>
                            <br>
                           <div> <b><?=Yii::t('app','Profil:').' </b> ' . $profileQuant?></div>
                            <div><b><?=Yii::t('app','Sticla:').' </b> ' . $glassQuant?></div>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
