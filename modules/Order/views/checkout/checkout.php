<?php
use app\views\themes\rowood\assets\RowoodAssets;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\modules\Payment\Mobilpay\Mobilpay;
$bundle = RowoodAssets::register($this);
?>

<style>
    .card-row{
        margin: 40px 200px;
        padding: 20px 50px;
        border: 1px solid #333;
        border-radius:10px;
        min-height: 350px;
    }
    .method-title{
        color: #e3210d;
        font-size: 16px;
        font-weight: 800;
    }
    .box-title{
        text-align: center;
        font-size: 24px;
        font-weight: 800;
        color: #e3210d;
        margin-bottom:10px;
        display: block;
    }
    .pay-method{
        margin-top: 40px;
        padding: 10px;

    }
</style>
<div class="container">
    <div id="checkout-steps">
        <a href="<?=Url::to('/admin/ecommerce/order/cart/view/?orderID='.$orderID)?>">
        <div class="checkout-block-step  previous" >
            <div class="checkout-step-circle "><i class="fa fa-check"></i>
            </div>Cos</div></a>
        <a href="<?=Url::to('/admin/ecommerce/order/checkout/?orderID='.$orderID)?>">
        <div class="checkout-block-step previous ">
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>
            Datele personale</div></a>
        <a href="<?=Url::to('/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID)?>">
        <div class="checkout-block-step previous ">
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>
            Transportare</div></a>
        <div class="checkout-block-step active ">
            <div class="checkout-step-circle active ">4</div>
            Plata</div>
        <div class="checkout-block-step  ">
            <div class="checkout-step-circle ">5</div>
            Comanda</div>
    </div>
    <div class="clearfix"></div>
    <div class="row ">

        <div class="col-sm-12">
            <div class="card-row">
                <h3 class="box-title">Plata</h3>
                <div class="curency">
                    <span class="sum">Suma cumparaturii: <?=$order->TotalPrice?> € </span><br>
                    <span class="transport-sum">Pretul transportarii: <?php if($order->orderTransportation){ echo  $order->orderTransportation->Price;}else{echo 0;}?> € </span><br>
                    <span class="total-sum">Suma Totala: <b><?php if($order->orderTransportation){ echo  $order->TotalPrice + $order->orderTransportation->Price;}else{echo $order->TotalPrice;}?> € </b></span><br>
                </div>

                <div class="pay-method">
                    <div class="col-sm-6 ">
                        <span class="col-sm-12 method-title">
                        Achitare cu cardul (Mobilpay)
                        </span>

                    </div>
                    <div class="col-sm-6 ">
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['/admin/ecommerce/order/checkout/pay/','orderID'=>$orderID]),
                            'id' => 'payment-form',
                        ]); ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Achita'), ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="pay-method">
                    <div class="col-sm-6 ">
                        <span class="col-sm-12 method-title">
                        Descarca factura
                        </span>

                    </div>
                    <div class="col-sm-6 ">
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['/admin/ecommerce/order/cart/view/','print'=>true]),
                            'id' => 'payment-form',
                        ]); ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Descarca'), ['class' => 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>