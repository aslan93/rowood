<?php
use app\views\themes\rowood\assets\RowoodAssets;

$bundle = RowoodAssets::register($this);
?>

<style>
    .order-box{
        margin: 40px 200px;
        padding: 20px 50px;
        border: 1px solid #333;
        border-radius:10px;
    }
    .box-title{
        font-size: 24px;
        font-weight: 800;
        color: #e3210d;
        margin-bottom:10px;
        display: block;
    }
</style>

<div class="container">
    <div id="checkout-steps"><div class="checkout-block-step  previous">
            <div class="checkout-step-circle "><i class="fa fa-check"></i>
            </div>Cos</div>
        <div class="checkout-block-step previous ">
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>
            Datele personale</div>
        <div class="checkout-block-step previous ">
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>
            Transportare</div>
        <div class="checkout-block-step previous ">
            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>
            Plata</div>
        <div class="checkout-block-step  active ">
            <div class="checkout-step-circle active ">5</div>
            Comanda</div>
    </div>
    <div class="clearfix"></div>
    <div class="row ">

        <div class="col-sm-12">
            <div class="order-box">
                <?php
                echo $this->render('_order_render',[
                        'order' => $order,
                ]);
                ?>
            </div>

        </div>



    </div>
</div>