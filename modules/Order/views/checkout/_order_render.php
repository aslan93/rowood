<?php
use app\views\themes\rowood\assets\RowoodAssets;

$bundle = RowoodAssets::register($this);
?>
<div><h3>Factura de plata</h3>
    <div>
        <?php
    foreach ($order->orderProducts as $index =>$product) {
        ?>
        <div class="product">

           <b> <?=$index+1?>. <?=$product->product->lang->Title?> x<?=$product->Quantity?><br></b>
            <div class="col-sm-12">
            <?php
            if ($product->product->doorColor){?>
                 Culoare: <span><?=$product->product->doorColor->lang->Title?></span> +<?=$product->product->doorColor->Price?> €
                <br>
            <?php
            }
            ?>
            <?php
            if ($product->product->doorHandle){?>
                Maner: <span><?=$product->product->doorHandle->lang->Title?></span> +<?=$product->product->doorHandle->Price?> €
                <br>
                <?php
            }
            ?>
            <?php
            if ($product->product->doorSize){?>
                 Dimensiunea: <span><?=$product->product->doorSize->WidthHeight?></span> +<?=$product->product->doorSize->Price?> €
                <br>
                <?php
            }
            ?>
             Pret: <?=$product->product->totalPrice?> €<br>
             Subtotal: <b><?=$product->product->totalPrice*$product->Quantity?> €</b><br>
            </div>
        </div>

        <?php
    }
        ?>

    </div>
    <br><br>
    <div>Data: <?=$order->OrderDate?></div>
    <div><b>Pret Total: <?=$order->TotalPrice?> euro</b></div>
    <div>Status: <?=$order->Status?></div>

</div>

