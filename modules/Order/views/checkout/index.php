<?php
use app\views\themes\rowood\assets\RowoodAssets;
use app\modules\User\widgets\Login\Login;
use app\modules\User\widgets\Registration\Registration;
use yii\helpers\Url;

$bundle = RowoodAssets::register($this);
?>

<!--<style>-->
<!---->
<!--    .login-box,.register-box{-->
<!--        margin-top: 20px;-->
<!--        margin-bottom: 20px;-->
<!--        padding: 60px;-->
<!--        border: 1px solid #333;-->
<!--        border-radius:10px;-->
<!--    }-->
<!--    .box-title{-->
<!--        font-size: 24px;-->
<!--        font-weight: 800;-->
<!--        color: #e3210d;-->
<!--        margin-bottom:10px;-->
<!--        display: block;-->
<!--    }-->
<!---->
<!---->
<!--</style>-->
<!---->
<!--<div class="container">-->
<!--    <div id="checkout-steps">-->
<!--        <a href="--><?//=Url::to('/admin/ecommerce/order/cart/view/')?><!--">-->
<!--        <div class="checkout-block-step  previous" >-->
<!--            <div class="checkout-step-circle "><i class="fa fa-check"></i></div>Cos</div></a>-->
<!--        <div class="checkout-block-step active ">-->
<!--            <div class="checkout-step-circle active">2</div>Datele personale</div>-->
<!--        <div class="checkout-block-step  ">-->
<!--            <div class="checkout-step-circle ">3</div>Transportare</div>-->
<!--        <div class="checkout-block-step  ">-->
<!--            <div class="checkout-step-circle ">4</div>Plata</div>-->
<!--        <div class="checkout-block-step  ">-->
<!--            <div class="checkout-step-circle ">5</div>Comanda</div>-->
<!--    </div>-->
<!---->
<!--    <div class="clearfix"></div>-->
<!---->
<!--    <div class="row">-->
<!--        <div class="col-md-6">-->
<!---->
<!--            <div class="login-box">-->
<!--                <span class="box-title">Logare</span><br>-->
<!--                --><?//=Login::widget(['redirect' => '/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID])?>
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="col-md-6">-->
<!---->
<!--            <div class="register-box">-->
<!--                <span class="box-title">Înregistrare</span><br>-->
<!--                --><?//=Registration::widget(['redirect' => '/admin/ecommerce/order/checkout/transportation/?orderID='.$orderID])?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<section class="cart-wrap">
    <div class="container">
        <div class="title">
            Cos
        </div>
        <div class="pages-cart-wrap">
            <div class="cart-page-step">
                <span class="step-page">
                    1
                </span>
                <span class="name-page">
                    Cos
                </span>
            </div>
            <div class="cart-page-step active">
                 <span class="step-page">
                    2
                </span>
                <span class="name-page">
                    Datele personale
                </span>
            </div>
            <div class="cart-page-step">
                 <span class="step-page">
                     3
                 </span>
                <span class="name-page">
                     Transportare
                 </span>
            </div>
            <div class="cart-page-step">
                 <span class="step-page">
                    4
                 </span>
                <span class="name-page">
                    Plata
                 </span>
            </div>
            <div class="cart-page-step">
                 <span class="step-page">
                    5
                 </span>
                <span class="name-page">
                    Comanda
                 </span>
            </div>
        </div>
        <div class="cart-wrap-item">
            <div class="row">
                <div class="col-md-4">
                    <div class="login">
                        <div class="head-box">
                            <div class="title">
                                Logare
                            </div>
                        </div>
                        <div class="content" data-mh="3000">
                            <div>
                                <form action="#" method="post">
                                    <div>
                                        <label>
                                            <input type="text" placeholder="Email">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="text" placeholder="Parola">
                                        </label>
                                    </div>
                                    <div>
                                        <input type="checkbox" id="confirm-terms">
                                        <label class="checkbox" for="confirm-terms">
                                            Tine-ma minte
                                        </label>
                                    </div>
                                    <div class="submit-form-details">
                                        <button type="submit">
                                            Logare
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="registration">
                        <div class="head-box">
                            <div class="title">
                                Inregistrare
                            </div>
                        </div>
                        <div class="content" data-mh="3000">
                            <form action="#" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Nume">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Prenume">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Sex">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Telefon">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Email">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Parola">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="submit-form-details">
                                    <button type="submit">
                                        Logare
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cart-wrap">
    <div class="container">
        <div class="title">
            Cos
        </div>
        <div class="pages-cart-wrap">
            <div class="cart-page-step">
            <span class="step-page">
                1
            </span>
                <span class="name-page">
                Cos
            </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                2
            </span>
                <span class="name-page">
                Datele personale
            </span>
            </div>
            <div class="cart-page-step active">
             <span class="step-page">
                 3
             </span>
                <span class="name-page">
                 Transportare
             </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                4
             </span>
                <span class="name-page">
                Plata
             </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                5
             </span>
                <span class="name-page">
                Comanda
             </span>
            </div>
        </div>
        <div class="delivery-wrap">
            <div class="cart-wrap-item">
                <div class="head-box">
                    <div class="title">
                        Transportare
                    </div>
                </div>
                <div class="content">
                    <div class="delivery">
                        <div>
                            <input type="checkbox" id="submit-terms">
                            <label class="checkbox" for="submit-terms">
                                Aveti nevoie de livrare ?
                            </label>
                        </div>
                        <div>
                        <span class="bold">
                            Pret:
                        </span>
                            <span class="price-red">
                            200 Lei
                        </span>
                        </div>
                    </div>
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <label>
                                        <input type="text" placeholder="Companie">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Index postal">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div>
                                            <label>
                                                <input type="text" placeholder="Localitate">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label>
                                        <input type="text" placeholder="Maxim Colesnic">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label>
                                        <select class="style-select">
                                            <option value="Tara">
                                                Tara
                                            </option>
                                            <option value="Moldova">
                                                Moldova
                                            </option>
                                            <option value="Romania">
                                                Romania
                                            </option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label>
                                        <input type="text" placeholder="Adresa">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="submit-form-details">
                            <button type="submit">
                                Logare
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="cart-wrap">
    <div class="container">
        <div class="title">
            Cos
        </div>
        <div class="pages-cart-wrap">
            <div class="cart-page-step">
            <span class="step-page">
                1
            </span>
                <span class="name-page">
                Cos
            </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                2
            </span>
                <span class="name-page">
                Datele personale
            </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                 3
             </span>
                <span class="name-page">
                 Transportare
             </span>
            </div>
            <div class="cart-page-step active">
             <span class="step-page">
                4
             </span>
                <span class="name-page">
                Plata
             </span>
            </div>
            <div class="cart-page-step">
             <span class="step-page">
                5
             </span>
                <span class="name-page">
                Comanda
             </span>
            </div>
        </div>
        <div class="pay">
            <div class="cart-wrap-item">
                <div class="head-box">
                    <div class="title">
                        Plata
                    </div>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="prices">
                                <div>
                                    <span class="light">
                                        Suma cumparaturii:
                                    </span>
                                    <span class="price-red">
                                        1610 €
                                    </span>
                                </div>
                                <div>
                                    <span class="light">
                                        Pretul transportarii:
                                    </span>
                                    <span class="price-red">
                                       0 €
                                    </span>
                                </div>
                                <div>
                                    <span class="light">
                                        Suma Totala:
                                    </span>
                                    <span class="price-red">
                                       1610 €
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="submit-form-details">
                                <button type="submit">
                                    Achitare cu cardul (Mobilpay)
                                </button>
                                <button type="submit">
                                    Descarca factura
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="cart-wrap">
    <div class="container">
        <div class="title">
            Cos
        </div>
        <div class="finish-pay">
            <div class="head-box">
                <div class="title">
                    MULTUMIM
                </div>
            </div>
            <div class="content">
                <p>
                    <span class="bold">
                        Multumim pentru comanda Dumneavoastra!
                    </span>
                </p>
                <p>
                    Comanda a fost creata.In curind o sa fiti contactat de managerul nostru.
                </p>
                <p>
                    <span class="bold">
                        ID comdanda D-stra #36
                    </span>
                </p>
            </div>
        </div>
    </div>
</section>

