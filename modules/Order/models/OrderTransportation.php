<?php

namespace app\modules\Order\models;

use Yii;

/**
 * This is the model class for table "OrderTransportation".
 *
 * @property integer $ID
 * @property integer $OrderID
 * @property integer $Livrare
 * @property string $Companie
 * @property string $NumePrenume
 * @property string $Adresa
 * @property string $IndexPostal
 * @property string $Localitate
 * @property string $Tara
 *
 * @property Order $order
 */
class OrderTransportation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderTransportation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderID', 'Livrare'], 'required'],
            [['OrderID', 'Livrare','Price'], 'integer'],
            [['Companie', 'NumePrenume', 'Adresa', 'Localitate', 'Tara'], 'string', 'max' => 255],
            [['IndexPostal'], 'string', 'max' => 8],
            [['OrderID'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['OrderID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrderID' => 'Order ID',
            'Livrare' => 'Livrare',
            'Companie' => 'Companie',
            'NumePrenume' => 'Nume Prenume',
            'Adresa' => 'Adresa',
            'IndexPostal' => 'Index Postal',
            'Localitate' => 'Localitate',
            'Tara' => 'Tara',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['ID' => 'OrderID']);
    }
}
