<?php

namespace app\modules\Order\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Order\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\modules\Order\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TotalPrice', 'UserID'], 'integer'],
            [['OrderDate', 'PayToken', 'Status', 'PayType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'TotalPrice' => $this->TotalPrice,
            'OrderDate' => $this->OrderDate,
            'UserID' => $this->UserID,
        ]);

        $query->andFilterWhere(['like', 'PayToken', $this->PayToken])
            ->andFilterWhere(['like', 'Status', $this->Status])
            ->andFilterWhere(['like', 'PayType', $this->PayType]);

        return $dataProvider;
    }
}
