<?php

namespace app\modules\Order\models;

use Yii;
use app\modules\Product\models\Product;

/**
 * This is the model class for table "OrderProducts".
 *
 * @property integer $ID
 * @property integer $OrderID
 * @property integer $ProductID
 *
 * @property Order $order
 * @property Product $product
 */
class OrderProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderID', 'ProductID'], 'required'],
            [['OrderID', 'ProductID','Quantity'], 'integer'],
            [['OrderID'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['OrderID' => 'ID']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrderID' => 'Order ID',
            'ProductID' => 'Product ID',
            'Quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['ID' => 'OrderID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID'])->with(['lang']);
    }
}
