<?php

namespace app\modules\Order\models;

use Yii;
use app\modules\User\models\User;

/**
 * This is the model class for table "Order".
 *
 * @property integer $ID
 * @property integer $TotalPrice
 * @property string $OrderDate
 * @property string $PayToken
 * @property string $Status
 * @property string $PayType
 * @property integer $UserID
 *
 * @property User $user
 * @property OrderProducts[] $orderProducts
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'Order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [[ 'UserID'], 'integer'],
            [['OrderDate'], 'safe'],
            [['TotalPrice'],'number'],
            [['PayToken', 'Status', 'PayType'], 'string', 'max' => 255],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TotalPrice' => 'Total Price',
            'OrderDate' => 'Order Date',
            'PayToken' => 'Pay Token',
            'Status' => 'Status',
            'PayType' => 'Pay Type',
            'UserID' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProducts::className(), ['OrderID' => 'ID']);
    }
    public function getOrderTransportation()
    {
        return $this->hasOne(OrderTransportation::className(), ['OrderID' => 'ID']);
    }


}
