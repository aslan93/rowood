<?php

namespace app\modules\Color\models;

use Yii;
use app\modules\Color\models\Color;
use app\modules\Material\models\Material;

/**
 * This is the model class for table "ColorMaterial".
 *
 * @property integer $ID
 * @property integer $ColorID
 * @property integer $MaterialID
 *
 * @property Material $material
 * @property Color $color
 */
class ColorMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ColorMaterial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ColorID', 'MaterialID'], 'required'],
            [['ColorID', 'MaterialID'], 'integer'],
            [['MaterialID'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['MaterialID' => 'ID']],
            [['ColorID'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['ColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ColorID' => Yii::t('app', 'Color ID'),
            'MaterialID' => Yii::t('app', 'Material ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['ID' => 'MaterialID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['ID' => 'ColorID']);
    }
}
