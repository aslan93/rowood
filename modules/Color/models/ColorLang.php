<?php

namespace app\modules\Color\models;

use Yii;
use app\modules\Color\models\Color;

/**
 * This is the model class for table "ColorLang".
 *
 * @property integer $ID
 * @property integer $ColorID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Color $color
 */
class ColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ColorID', 'LangID', 'Title', 'Text'], 'required'],
            [['ColorID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ColorID'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['ColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ColorID' => Yii::t('app', 'Color ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['ID' => 'ColorID']);
    }
}
