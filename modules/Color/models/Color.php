<?php

namespace app\modules\Color\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\Color\models\ColorLang;
use app\modules\Color\models\ColorMaterial;
use app\modules\Color\models\ColorProfile;
use app\modules\Color\models\ColorWood;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Color".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Percent
 * @property string $Ral
 *
 * @property ColorLang $lang
 * @property ColorLang[] $langs
 * @property ColorMaterial[] $materials
 * @property ColorProfile[] $profiles
 * @property ColorWood[] $woods
 * @property string $imagePath
 */
class Color extends \yii\db\ActiveRecord
{
    
    public $MaterialIDs = [];
    public $ProfileIDs = [];
    public $WoodIDs = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Color';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'MaterialIDs', 'ProfileIDs'], 'required'],
            [['Front', 'Back'], 'integer'],
            [['Percent'], 'number'],
            [['Position'], 'default', 'value' => 999999],
            [['Default'], 'default', 'value' => 0],
            [['Position'], 'integer'],
            [['WoodIDs'], 'safe'],
            [['Image', 'Ral'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Percent' => Yii::t('app', 'Percent'),
            'Ral' => Yii::t('app', 'Ral'),
            'Front' => Yii::t('app', 'Front'),
            'Back' => Yii::t('app', 'Back'),
            'Position' => Yii::t('app', 'Position'),
            'Default' => Yii::t('app', 'Default for all colors'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ColorLang::className(), ['ColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(ColorLang::className(), ['ColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new ColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(ColorMaterial::className(), ['ColorID' => 'ID'])->with(['material', 'material.lang']);
    }
    
    public function getMaterialsIDs()
    {
        return ArrayHelper::map($this->materials, 'MaterialID', 'MaterialID');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(ColorProfile::className(), ['ColorID' => 'ID'])->with(['profile', 'profile.lang']);
    }
    
    
    public function getProfilesIDs()
    {
        return ArrayHelper::map($this->profiles, 'ProfileID', 'ProfileID');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWoods()
    {
        return $this->hasMany(ColorWood::className(), ['ColorID' => 'ID'])->with(['wood', 'wood.lang']);
    }
    
    public function getWoodsIDs()
    {
        return ArrayHelper::map($this->woods, 'WoodID', 'WoodID');
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/color/' . $this->Image);
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }
        else
        {
            $hasDefault = self::find()->where(['Default' => 1])->andWhere(['<>', 'ID', $this->ID])->count();
            
            if (!$hasDefault)
            {
                $this->addError('Default', Yii::t('app', 'Although one material should be installed as default'));
                return false;
            }
        }
        
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->ColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
            
            ColorMaterial::deleteAll(['ColorID' => $this->ID]);
            foreach ($this->MaterialIDs as $materialID)
            {
                $model = new ColorMaterial();
                $model->ColorID = $this->ID;
                $model->MaterialID = $materialID;
                $model->save();
            }
            
            ColorProfile::deleteAll(['ColorID' => $this->ID]);
            foreach ($this->ProfileIDs as $profileID)
            {
                $model = new ColorProfile();
                $model->ColorID = $this->ID;
                $model->ProfileID = $profileID;
                $model->save();
            }
            
            ColorWood::deleteAll(['ColorID' => $this->ID]);
            foreach ((array)$this->WoodIDs as $woodID)
            {
                $model = new ColorWood();
                $model->ColorID = $this->ID;
                $model->WoodID = $woodID;
                $model->save();
            }
        }
    }
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->with('lang');
        $result = ArrayHelper::map($query->all(), 'ID' ,'lang.Title');
        return $return + $result;
    }

    public static function getImageList($addEmpty = false)
    {
     $return = $addEmpty ? ['' => $addEmpty] : [];
     $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image','lang.Title');
        return $return + $result;
    }
    
}
