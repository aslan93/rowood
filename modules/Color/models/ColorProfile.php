<?php

namespace app\modules\Color\models;

use Yii;
use app\modules\Profile\models\Profile;

/**
 * This is the model class for table "ColorProfile".
 *
 * @property integer $ID
 * @property integer $ColorID
 * @property integer $ProfileID
 *
 * @property Profile $profile
 * @property Color $color
 */
class ColorProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ColorProfile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ColorID', 'ProfileID'], 'required'],
            [['ColorID', 'ProfileID'], 'integer'],
            [['ProfileID'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['ProfileID' => 'ID']],
            [['ColorID'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['ColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ColorID' => Yii::t('app', 'Color ID'),
            'ProfileID' => Yii::t('app', 'Profile ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['ID' => 'ProfileID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['ID' => 'ColorID']);
    }
}
