<?php

namespace app\modules\Color\models;

use Yii;
use app\modules\Color\models\Color;
use app\modules\Wood\models\Wood;

/**
 * This is the model class for table "ColorWood".
 *
 * @property integer $ID
 * @property integer $ColorID
 * @property integer $WoodID
 *
 * @property Color $color
 * @property Wood $wood
 */
class ColorWood extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ColorWood';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ColorID', 'WoodID'], 'required'],
            [['ColorID', 'WoodID'], 'integer'],
            [['ColorID'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['ColorID' => 'ID']],
            [['WoodID'], 'exist', 'skipOnError' => true, 'targetClass' => Wood::className(), 'targetAttribute' => ['WoodID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ColorID' => Yii::t('app', 'Color ID'),
            'WoodID' => Yii::t('app', 'Wood ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['ID' => 'ColorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWood()
    {
        return $this->hasOne(Wood::className(), ['ID' => 'WoodID']);
    }
}
