<?php

    use yii\bootstrap\Html;
    use kartik\file\FileInput;
    use app\modules\Material\models\Material;
    use app\modules\Profile\models\Profile;
    use app\modules\Wood\models\Wood;
    use kartik\select2\Select2;
    
    $initialPreview = [];
    if ($model->imagePath)
    {
        $initialPreview[] = Html::img($model->imagePath, ['width' => 160]);
    }

?>

<br />
<div>
    <?= $form->field($model, 'MaterialIDs[]')->widget(Select2::className(), [
        'data' => Material::getList(),
        'options' => [
            'multiple' => true,
            'value' => $model->materialsIDs,
        ]
    ]) ?>
    
    <?= $form->field($model, 'ProfileIDs[]')->widget(Select2::className(), [
        'data' => Profile::getList(),
        'options' => [
            'multiple' => true,
            'value' => $model->profilesIDs,
        ]
    ]) ?>
    
    <?= $form->field($model, 'WoodIDs[]')->widget(Select2::className(), [
        'data' => Wood::getList(),
        'options' => [
            'multiple' => true,
            'value' => $model->woodsIDs,
        ]
    ]) ?>
</div>
<div class="row">
    <div class="col-md-6">
        <label>Image</label>
        <?= FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        
        <?= $form->field($model, 'Percent')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>

        <?= $form->field($model, 'Ral')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'Position')->textInput(['type' => 'number', 'min' => 0, 'step' => 1]) ?>
        
        <?= $form->field($model, 'Front')->checkbox() ?>
        
        <?= $form->field($model, 'Back')->checkbox() ?>
        
        <?= $form->field($model, 'Default')->checkbox() ?>
        
    </div>
</div>
<br />