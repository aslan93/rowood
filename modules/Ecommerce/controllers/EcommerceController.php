<?php

namespace app\modules\Ecommerce\controllers;

use app\components\Controller\Controller;
use app\modules\Admin\controllers\AdminController;

/**
 * Default controller for the `Ecommerce` module
 */
class EcommerceController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */


    public function actionIndex()
    {
        return $this->redirect(['/product']);
    }
}
