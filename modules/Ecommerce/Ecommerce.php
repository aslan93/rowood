<?php

namespace app\modules\Ecommerce;


use app\components\Module\Module;
use app\modules\Admin\Admin;

/**
 * Ecommerce module definition class
 */
class Ecommerce extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Ecommerce\controllers';

    /**
     * @inheritdoc
     */

}
