<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\SeoPost\models\SeoPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-post-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
    $items[] = [
    'label' => strtoupper($langID),
    'content' => $this->render('_desc_form',[
    'form' => $form,
    'langModel' => $langModel,
    'model' => $model,
    ]),
    ];
    }
    echo '<br>';
    echo Tabs::widget([
    'items' => $items,
    ]);
    ?>
    <?= $form->field($model, 'CreatedAt')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
