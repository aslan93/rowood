<?php

namespace app\modules\SeoPost;

use \app\modules\Admin\Admin;
/**
 * SeoPost module definition class
 */
class SeoPost extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\SeoPost\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
