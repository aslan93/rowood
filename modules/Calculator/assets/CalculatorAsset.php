<?php

namespace app\modules\Calculator\assets;

use yii\web\View;
use yii\web\AssetBundle;

class CalculatorAsset extends AssetBundle
{
    
    public function init()
    {
        $this->jsOptions['position'] = View::POS_BEGIN;
        parent::init();
    }
    
    public $sourcePath = '@app/modules/Calculator/assets/files';
    
    public $css = [
        'css/calculator.css',
    ];
    
    public $js = [
        'js/calculator.js',
    ];
    
    public $depends = [
        'app\views\themes\rowood\assets\RowoodAssets',
    ];
    
}
