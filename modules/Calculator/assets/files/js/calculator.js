var Calculator = {

    productID: null,

    setProductID: function (productID) {
        this.productID = productID;
    },

    setMaterial: function (id) {
        this.getProfiles(id);
        Calculator.getSizes(false);
    },

    getProfiles: function (materialID) {
        $.get('/calculator/ajax/get-profiles/', {productID: this.productID, materialID: materialID}, function (json) {
            $('#profiles-wrap').html(json.profilesContent);
            $('#woods-wrap').html(json.woodsContent);
            //$('#front-colors-wrap').html(json.frontColorsContent);
            //$('#back-colors-wrap').html(json.backColorsContent);
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setProfile: function (profileID) {
        $.get('/calculator/ajax/set-profiles/', {productID: this.productID, profileID: profileID}, function (json) {
            Calculator.getColors(json.product.MaterialID, json.product.ProfileID, json.product.WoodID);
            Calculator.getSizes(false);
            Calculator.getGlasses(profileID);
        }, 'json');
    },

    setWood: function (woodID) {
        $.get('/calculator/ajax/set-wood/', {productID: this.productID, woodID: woodID}, function (json) {
            Calculator.getColors(json.product.MaterialID, json.product.ProfileID, json.product.WoodID);
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    getColors: function (materialID = false, profileID = false, woodID = false) {
        $.get('/calculator/ajax/get-colors/', {
            productID: this.productID,
            materialID: materialID,
            profileID: profileID,
            woodID: woodID
        }, function (json) {
            $('#front-colors-wrap').html(json.frontColorsContent);
            setTimeout(function () {
                $('[data-mh="1"]').matchHeight();
            }, 500);
        }, 'json');
    },

    setColor: function (colorID, side) {
        $.get('/calculator/ajax/set-color/', {
            productID: this.productID,
            colorID: colorID,
            side: side
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setType: function (typeID, elem, saveSizes = 0) {
        $.get('/calculator/ajax/set-type/', {
            productID: this.productID,
            typeID: typeID,
            saveSizes: saveSizes
        }, function (json) {
            $('.form-panel-content').find('.variants-wrap').empty();
            $(elem).closest('.form-panel-content').find('.variants-wrap').html(json.variantsContent);
            Calculator.getSizes(false);
        }, 'json');
    },

    setVariant: function (variantID, saveSizes = 0) {
        $.get('/calculator/ajax/set-variant/', {
            productID: this.productID,
            variantID: variantID,
            saveSizes: saveSizes
        }, function (json) {
            Calculator.getSizes(false);
        }, 'json');
    },
//added by Denis
    getSizes: function (bindInputs = true, reset=false) {
        $.get('/calculator/ajax/get-sizes/', {productID: this.productID, reset: reset}, function (json) {
            $('#sizes-wrap').html(json.sizesContent);

            if (bindInputs) {
                if ($('input[name="FullHeight"]').val() == 0) {
                    $('input[name="FullHeight"]').val('').focus();
                }

                if ($('input[name="FullWidth"]').val() == 0) {
                    $('input[name="FullWidth"]').val('').focus();
                }
            }
            if (reset == true) {
                Calculator.recalculateAll();
            }
        }, 'json');
    },

    getDimensions: function () {
        $.get('/calculator/ajax/get-dimensions/', {productID: this.productID}, function (json) {
            $('#sizes-wrap').html(json.sizesContent);
        }, 'json');
    },
//*** added by Denis
    setDimensions: function (reset = false) {
        var width = parseInt($('input[name="FullWidth"]').val());
        var minWidth = parseInt($('input[name="FullWidth"]').attr('data-min'));
        var maxWidth = parseInt($('input[name="FullWidth"]').attr('data-max'));
        var height = parseInt($('input[name="FullHeight"]').val());
        var minHeight = parseInt($('input[name="FullHeight"]').attr('data-min'));
        var maxHeight = parseInt($('input[name="FullHeight"]').attr('data-max'));

        // $('input[name="FullWidth"]').attr('style', '');
        // $('input[name="FullHeight"]').attr('style', '');
        //
        var isValid = true;

        if (isNaN(width) || width < minWidth || width > maxWidth) {
            $('input[name="FullWidth"]').attr('style', 'border: 1px solid red!important');
            $('header').append('<style type="text/css">.submit-form-details{display: none!important;}</style>');
            isValid = false;
        } else {
            $('input[name="FullWidth"]').attr('style', 'border: 1px solid green');
            $('header').append('<style type="text/css">input[name="FullWidth"]{border:1px solid green!important;}</style>');
        }

        if (isNaN(height) || height < minHeight || height > maxHeight) {
            $('input[name="FullHeight"]').attr('style', 'border: 1px solid red!important');
            $('header').append('<style type="text/css">.submit-form-details{display: none!important;}</style>');
            isValid = false;
        } else {
            $('input[name="FullHeight"]').attr('style', 'border: 1px solid green');
            $('header').append('<style type="text/css"> input[name="FullHeight"]{border:1px solid green!important;}</style>');
        }

        if (isValid) {
            $.get('/calculator/ajax/set-dimensions/', {
                productID: this.productID,
                width: width,
                height: height
            }, function (json) {

                Calculator.recalculatePrice(reset, reset);

                $('#windowsill-min').text(Math.round(width - (width * 0.2)));
                $('#windowsill-max').text(Math.round(width + (width * 0.2)));
                $('header').append('<style type="text/css">.submit-form-details{display: block!important;}</style>');

            }, 'json');
        }
    },
    //added by Denis
    setCustomDimensions: function (position, obj, type) {
        var width = false;
        var height = false;
        var fullWidth = parseInt($('input[name="FullWidth"]').val());
        var fullHeight = parseInt($('input[name="FullHeight"]').val());

        if (type === 'height') {
            height = $(obj).val();
        } else {
            width = $(obj).val();
        }

        $.get('/calculator/ajax/set-custom-dimensions/', {
            productID: this.productID,
            width: width,
            height: height,
            position: position,
            fullWidth: fullWidth,
            fullHeight: fullHeight
        })
            .done(function (json) {
                Calculator.getDimensions();
                Calculator.recalculateAll();
            });

    },

    recalculateAll: function () {
        var pid = this.productID;
        $.get('/calculator/ajax/get-recalcules/', {productID: this.productID}).done(function (json) {
            // $('#product-info-wrap').html(json.productInfoContent);

            $.get('/calculator/ajax/get-price/', {productID: pid}).done(function (json) {
                $('#product-info-wrap').html(json.productInfoContent);
            }, 'json');

        }, 'json');
    },
    //*** added by Denis
    recalculatePrice: function (bindInputs = true, reset=false) {
        $.get('/calculator/ajax/get-price/', {productID: this.productID}).done(function (json) {
            $('#product-info-wrap').html(json.productInfoContent);
            Calculator.getSizes(bindInputs, reset);
        }, 'json');
    },

    nextStep: function () {
        $('#calculator-step-1').fadeOut(400, function () {
            $('#calculator-step-2').fadeIn(400);
        });
        $.pjax.reload({
            container: '#step2-pjax',
            push: false,
            replace: false,
            timeout: 50000,
            data: {productID: Calculator.productID}
        });
        $("html, body").animate({scrollTop: 0}, 500);
    },

    prevStep: function () {
        $('#calculator-step-2').fadeOut(400, function () {
            $('#calculator-step-1').fadeIn(400);
        });
    },

    getGlasses: function (profileID) {
        $.get('/calculator/ajax/get-glasses/', {productID: this.productID, profileID: profileID}, function (json) {
            $('#glasses-wrap').html(json.glassesContent);
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setGlass: function (glassID) {
        $.get('/calculator/ajax/set-glass/', {productID: this.productID, glassID: glassID}, function (json) {
            Calculator.recalculatePrice(false);
            $.pjax.reload({
                container: '#termoedge-pjax',
                push: false,
                replace: false,
                timeout: 50000,
                data: {productID: Calculator.productID}
            });
        }, 'json');
    },

    setTermoEdge: function (termoEdge) {
        if (termoEdge == 1) {
            $('.termo-edge-variants').show();
        }
        else {
            $('.termo-edge-variants').hide();
        }

        $.get('/calculator/ajax/set-termo-edge/', {productID: this.productID, termoEdge: termoEdge}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setTermoEdgeID: function (termoEdgeID) {
        $.get('/calculator/ajax/set-termo-edge-id/', {
            productID: this.productID,
            termoEdgeID: termoEdgeID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSoundProofing: function (soundProofing) {
        if (soundProofing == 1) {
            $('.sound-proofing-variants').show();
        }
        else {
            $('.sound-proofing-variants').hide();
        }

        $.get('/calculator/ajax/set-sound-proofing/', {
            productID: this.productID,
            soundProofing: soundProofing
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSoundProofingID: function (soundProofingID) {
        $.get('/calculator/ajax/set-sound-proofing-id/', {
            productID: this.productID,
            soundProofingID: soundProofingID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSafety: function (safety) {
        if (safety == 1) {
            $('.safety-variants').show();
        }
        else {
            $('.safety-variants').hide();
        }

        $.get('/calculator/ajax/set-safety/', {productID: this.productID, safety: safety}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSafetyID: function (safetyID) {
        $.get('/calculator/ajax/set-safety-id/', {productID: this.productID, safetyID: safetyID}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setDecoration: function (decoration) {
        if (decoration == 1) {
            $('.decoration-variants').show();
        }
        else {
            $('.decoration-variants').hide();
        }

        $.get('/calculator/ajax/set-decoration/', {productID: this.productID, decoration: decoration}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setDecorationID: function (decorationID) {
        $.get('/calculator/ajax/set-decoration-id/', {
            productID: this.productID,
            decorationID: decorationID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setMontageHoles: function (montageHoles) {
        $.get('/calculator/ajax/set-montage-holes/', {
            productID: this.productID,
            montageHoles: montageHoles
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setWindowsill: function (windowsill) {
        if (windowsill == 1) {
            $.get('/calculator/ajax/set-windowsill/', {
                productID: this.productID,
                windowsill: windowsill
            }, function (json) {
                $('.windowsill-options').html(json.windowsillInfo).show();
                Calculator.recalculatePrice(false);
            }, 'json');
        }
        else {
            $('.windowsill-options').hide();
            $.get('/calculator/ajax/set-windowsill/', {
                productID: this.productID,
                windowsill: windowsill
            }, function (json) {
                Calculator.recalculatePrice(false);
            }, 'json');
        }
    },

    setWindowsillLength: function (length) {
        var min = parseInt($('#windowsill-min').text());
        var max = parseInt($('#windowsill-max').text());

        $('input[name="WindowsillLength"]').attr('style', '');

        if (length < min || length > max) {
            $('input[name="WindowsillLength"]').attr('style', 'border: 1px solid red');
        }
        else {
            $.get('/calculator/ajax/set-windowsill-length/', {
                productID: this.productID,
                length: length
            }, function (json) {
                Calculator.recalculatePrice(false);
                $.pjax.reload({
                    container: '#windowsill-widths-pjax',
                    push: false,
                    replace: false,
                    timeout: 50000,
                    data: {productID: Calculator.productID}
                });
                setTimeout(function () {
                    $.pjax.reload({
                        container: '#connection-profiles-pjax',
                        push: false,
                        replace: false,
                        timeout: 50000,
                        data: {productID: Calculator.productID}
                    });
                }, 1000);
            }, 'json');
        }
    },

    setWindowsillColor: function (colorID) {
        $.get('/calculator/ajax/set-windowsill-color/', {productID: this.productID, colorID: colorID}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setWindowsillWidth: function (width) {
        $.get('/calculator/ajax/set-windowsill-width/', {productID: this.productID, width: width}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setEndCorner: function (endCorner) {
        if (endCorner == 1) {
            $('.end-corners-variants').show();
        }
        else {
            $('.end-corners-variants').hide();
        }

        $.get('/calculator/ajax/set-end-corner/', {productID: this.productID, endCorner: endCorner}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setEndCornerID: function (endCornerID) {
        $.get('/calculator/ajax/set-end-corner-id/', {
            productID: this.productID,
            endCornerID: endCornerID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setHConnector: function (HConnector) {
        $.get('/calculator/ajax/set-h-connector/', {
            productID: this.productID,
            HConnector: HConnector
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setScrew: function (screw) {
        if (screw == 1) {
            $('.screw-variants').show();
        }
        else {
            $('.screw-variants').hide();
        }

        $.get('/calculator/ajax/set-screw/', {productID: this.productID, screw: screw}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setScrewID: function (screwID) {
        $.get('/calculator/ajax/set-screw-id/', {productID: this.productID, screwID: screwID}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setFittingID: function (fittingID) {
        $.get('/calculator/ajax/set-fitting-id/', {productID: this.productID, fittingID: fittingID}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setAnschraubdichtung: function (anschraubdichtung) {
        if (anschraubdichtung == 1) {
            $('.anschraubdichtung-variants').show();
        }
        else {
            $('.anschraubdichtung-variants').hide();
        }

        $.get('/calculator/ajax/set-anschraubdichtung/', {
            productID: this.productID,
            anschraubdichtung: anschraubdichtung
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setAnschraubdichtungID: function (anschraubdichtungID) {
        $.get('/calculator/ajax/set-anschraubdichtung-id/', {
            productID: this.productID,
            anschraubdichtungID: anschraubdichtungID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSoundLine: function (soundLine) {
        $.get('/calculator/ajax/set-sound-line/', {productID: this.productID, soundLine: soundLine}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setInsectScreen: function (insectScreen) {
        if (insectScreen == 1) {
            $('.insect-screens-variants').show();
        }
        else {
            $('.insect-screens-variants').hide();
        }

        $.get('/calculator/ajax/set-insect-screen/', {
            productID: this.productID,
            insectScreen: insectScreen
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setInsectScreenID: function (insectScreenID) {
        $.get('/calculator/ajax/set-insect-screen-id/', {
            productID: this.productID,
            insectScreenID: insectScreenID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setClothID: function (clothID) {
        $.get('/calculator/ajax/set-cloth-id/', {productID: this.productID, clothID: clothID}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setFrameExtension: function (part, size, addPrice = 0) {
        $.get('/calculator/ajax/set-frame-extension/', {
            productID: this.productID,
            part: part,
            size: size,
            addPrice: addPrice
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    addToCart: function () {
        $.get('/admin/ecommerce/order/cart/add-to-cart/', {
            id: this.productID,
            quantity: 1,
            return: false,
            isConfigurable: true
        }, function () {
            window.location.href = '/admin/ecommerce/order/cart/view/';
        });
    },

    setSprossen: function (sprossen) {
        if (sprossen == 1) {
            $('.sprossen-variants').show();
        }
        else {
            $('div[sprossen-id]').fadeOut();
            $('.sprossen-variants').hide();
        }

        $.get('/calculator/ajax/set-sprossen/', {productID: this.productID, sprossen: sprossen}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSprossenID: function (sprossenID) {
        $('div[sprossen-id]').hide();
        $('div[sprossen-id=' + sprossenID + ']').show();
        $.get('/calculator/ajax/set-sprossen-id/', {
            productID: this.productID,
            sprossenID: sprossenID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSprossen: function (sprossen) {
        if (sprossen == 1) {
            $('.sprossen-variants').show();
            $('.sprossen-variants label:first').click();
            $('.sprossen-variants input[name="SprossenWidth"]:visible:first').closest('label').click();
        }
        else {
            $('div[sprossen-id]').fadeOut();
            $('.sprossen-variants').hide();
        }

        $.get('/calculator/ajax/set-sprossen/', {productID: this.productID, sprossen: sprossen}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSprossenID: function (sprossenID) {
        $('div[sprossen-id]').hide();
        $('div[sprossen-id=' + sprossenID + ']').show();
        $.get('/calculator/ajax/set-sprossen-id/', {
            productID: this.productID,
            sprossenID: sprossenID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setSprossenWidth: function (sprossenWidth) {
        $.get('/calculator/ajax/set-sprossen-width/', {
            productID: this.productID,
            sprossenWidth: sprossenWidth
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setHandle: function (handle) {
        if (handle == 1) {
            $('.handle-variants').show();
        }
        else {
            $('.handle-variants').hide();
        }

        $.get('/calculator/ajax/set-handle/', {productID: this.productID, handle: handle}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setHandleID: function (handleID, handlePrice) {
        $.get('/calculator/ajax/set-handle-id/', {
            productID: this.productID,
            handleID: handleID,
            handlePrice: handlePrice
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setAirRegulator: function (airRegulator) {
        $.get('/calculator/ajax/set-air-regulator/', {
            productID: this.productID,
            airRegulator: airRegulator
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setConnectionProfile: function (connectionProfile) {
        if (connectionProfile == 1) {
            $('.connection-profile-variants').show();
        }
        else {
            $('.connection-profile-variants').hide();
        }

        $.get('/calculator/ajax/set-connection-profile/', {
            productID: this.productID,
            connectionProfile: connectionProfile
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setConnectionProfileID: function (connectionProfileID, addPrice = 0) {
        $.get('/calculator/ajax/set-connection-profile-id/', {
            productID: this.productID,
            connectionProfileID: connectionProfileID,
            addPrice: addPrice
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setJalousies: function (jalousies) {
        if (jalousies == 1) {
            $('.jalousies-variants').show();
        }
        else {
            $('.jalousies-variants').hide();
        }

        $.get('/calculator/ajax/set-jalousies/', {productID: this.productID, jalousies: jalousies}, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setJalousieID: function (jalousieID, type, hasOptions = 0) {
        $.get('/calculator/ajax/set-jalousie-id/', {
            productID: this.productID,
            jalousieID: jalousieID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');

        $('[data-jalousie-id]').not($('[data-jalousie-id=' + jalousieID + ']').show()).hide();

        var jalousieSizeInputs = $('input[name="JalousieFullWidth"], input[name="JalousieFullHeight"]');
        if (type == 'Internal') {
            jalousieSizeInputs.attr('disabled', 'disabled');
            $('.jalousie-size-info').hide();
        }
        else {
            jalousieSizeInputs.removeAttr('disabled');
            $('.jalousie-size-info').show();
        }

        if (hasOptions == 1) {
            $('.jalousie-options').show();
        }
        else {
            $('.jalousie-options').hide();
        }
    },

    setJalousieModelID: function (jalousieModelID, addPrice = 0) {
        $.get('/calculator/ajax/set-jalousie-model-id/', {
            productID: this.productID,
            jalousieModelID: jalousieModelID,
            addPrice: addPrice
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
        $('[data-jalousie-model-id]').not($('[data-jalousie-model-id=' + jalousieModelID + ']').show()).hide();
    },

    setJalousieModelSizeID: function (jalousieModelSizeID) {
        $.get('/calculator/ajax/set-jalousie-model-size-id/', {
            productID: this.productID,
            jalousieModelSizeID: jalousieModelSizeID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setJalousieModelColorID: function (jalousieModelColorID) {
        $.get('/calculator/ajax/set-jalousie-model-color-id/', {
            productID: this.productID,
            jalousieModelColorID: jalousieModelColorID
        }, function (json) {
            Calculator.recalculatePrice(false);
        }, 'json');
    },

    setJalousieDimensions: function () {
        var width = parseInt($('input[name="JalousieFullWidth"]').val());
        var minWidth = parseInt($('input[name="JalousieFullWidth"]').attr('data-min'));
        var maxWidth = parseInt($('input[name="JalousieFullWidth"]').attr('data-max'));
        var height = parseInt($('input[name="JalousieFullHeight"]').val());
        var minHeight = parseInt($('input[name="JalousieFullHeight"]').attr('data-min'));
        var maxHeight = parseInt($('input[name="JalousieFullHeight"]').attr('data-max'));

        $('input[name="JalousieFullWidth"]').attr('style', '');
        $('input[name="JalousieFullHeight"]').attr('style', '');

        var isValid = true;

        if (isNaN(width) || width < minWidth || width > maxWidth) {
            $('input[name="JalousieFullWidth"]').attr('style', 'border: 1px solid red');
            isValid = false;
        }

        if (isNaN(height) || height < minHeight || height > maxHeight) {
            $('input[name="JalousieFullHeight"]').attr('style', 'border: 1px solid red');
            isValid = false;
        }

        if (isValid) {
            $.get('/calculator/ajax/set-jalousie-dimensions/', {
                productID: this.productID,
                width: width,
                height: height
            }, function (json) {
                Calculator.recalculatePrice();
            }, 'json');
        }
    },

    calculateSprosses: function () {
        if ($('input[name="SprossenWidth"]:visible:checked').length == 0) {
            alert('Selectati marimea sprosuri');
            return;
        }

        var noEmpty = false;
        $('input[name^="SprossenCount"]').each(function () {
            if ($(this).val() > 0) {
                noEmpty = true;
            }
        });

        if (!noEmpty) {
            alert('Indicati cantitatea sprosuri');
            return;
        }

        $.post('/calculator/ajax/set-sprossen-count/', {
            productID: this.productID,
            data: $('#sprossen-count-form').serialize()
        }, function (json) {
            Calculator.recalculatePrice();
            $.pjax.reload({
                container: '#sprossen-pjax',
                push: false,
                replace: false,
                timeout: 50000,
                data: {productID: Calculator.productID}
            });
        }, 'json');
    },

    setOpeningMechanismID: function (OpeningMechanismID) {
        $('div[data-mechanism-id]').not($('div[data-mechanism-id=' + OpeningMechanismID + ']').show()).hide();
        $.get('/calculator/ajax/set-opening-mechanism-id/', {
            productID: this.productID,
            OpeningMechanismID: OpeningMechanismID
        }, function (json) {
            Calculator.recalculatePrice();
        }, 'json');
    },

    setOpeningMechanismOptionID: function (OpeningMechanismOptionID) {
        $.get('/calculator/ajax/set-opening-mechanism-option-id/', {
            productID: this.productID,
            OpeningMechanismOptionID: OpeningMechanismOptionID
        }, function (json) {
            Calculator.recalculatePrice();
        }, 'json');
    },

    setOpeningMechanismPosition: function (position) {
        $.get('/calculator/ajax/set-opening-mechanism-position/', {
            productID: this.productID,
            position: position
        }, function (json) {
            Calculator.recalculatePrice();
        }, 'json');
    },
    setRollColorID: function (rollColorID) {
        $.get('/calculator/ajax/set-roll-color-id/', {
            productID: this.productID,
            rollColorID: rollColorID
        }, function (json) {
            Calculator.recalculatePrice();
        }, 'json');
    },
    drawSprossens: function (elementID, lv, lh) {
        var c = document.getElementById(elementID);
        var ctx = [];

        for (var i = 0; i < lv; i++) {
            var width = c.width;
            var height = c.height;
            var step = (width / (lv + 1));
            var x = step * (i + 1);
            ctx[i] = c.getContext("2d");
            ctx[i].beginPath();
            ctx[i].moveTo(x, 0);
            ctx[i].lineTo(x, height);
            ctx[i].lineWidth=2;
            ctx[i].stroke();
        }

        var htx = [];
        for (var i = 0; i < lh; i++) {
            var width = c.width;
            var height = c.height;
            var step = (height / (lh + 1));
            var y = step * (i + 1);
            htx[i] = c.getContext("2d");
            htx[i].beginPath();
            htx[i].moveTo(0, y);
            htx[i].lineTo(width, y);
            htx[i].lineWidth=2;
            htx[i].stroke();
        }

    },

    validateInput: function (ob) {
        // $('input[type=number]').each(function(){

        var width = parseInt($(ob).val());
        var maxWidth = parseInt($(ob).attr('data-max'));

        if (isNaN(width) || width > maxWidth) {
            $(ob).attr('style', 'border: 1px solid red!important');
            $(ob).val(maxWidth);
        } else {
            $(ob).attr('style', 'border: 1px solid green');
        }
    }


};


    
