<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'GAURI PENTRU MONTARE') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Gauri pentru montare
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setMontageHoles(this.value)" <?= !$product->MontageHoles ? 'checked' : '' ?> style="display: block" type="radio" name="MontageHoles" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setMontageHoles(this.value)" <?= $product->MontageHoles ? 'checked' : '' ?> style="display: block" type="radio" name="MontageHoles" value="1"> 
                Da <span class="label label-default">+ 15.04 &euro;</span>
            </label>
        </div>
    </div>
</div>