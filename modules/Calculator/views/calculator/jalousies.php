<?php

    use yii\bootstrap\Html;
    use app\modules\Calculator\components\CalculatorHelper;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'JALOUSIES') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div style="padding-bottom: 20px;" class="form-panel-content">
    <h3>
        Jalousies
    </h3>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setJalousies(this.value)" <?= !$product->Jalousies ? 'checked' : '' ?> style="display: block" type="radio" name="Jalousies" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setJalousies(this.value)" <?= $product->Jalousies ? 'checked' : '' ?> style="display: block" type="radio" name="Jalousies" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content jalousies-variants" <?= !$product->Jalousies ? 'style="display:none;"' : '' ?>>
    <h3>Tipuri</h3>
    <div>
        <div class="row">
            <?php foreach ($jalousies as $jalousie) { ?>
            <div>
                <label>
                    <input onchange="Calculator.setJalousieID(<?= $jalousie->ID ?>, '<?= $jalousie->Type ?>', <?= $jalousie->HasOptions ?>)" <?= $product->JalousieID == $jalousie->ID ? 'checked' : '' ?> style="display: inline-block" type="radio" name="JalousieID" value="<?= $jalousie->ID ?>">  
                    <?= $jalousie->lang->Title ?>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
    <div>
        <h3>Modeluri</h3>
        <?php foreach ($jalousies as $jalousie) { ?>
        <div class="row" data-jalousie-id="<?= $jalousie->ID ?>">
            <?php foreach ($jalousie->models as $model) { ?>
            <?php if ($jalousie->HasOptions) { ?>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <input <?= $product->JalousieModelID == $model->ID ? 'checked' : '' ?> type="radio" name="JalousieModelID" value="<?= $model->ID ?>" id="model-<?= $model->ID ?>">
                <label onclick="Calculator.setJalousieModelID(<?= $model->ID ?>, <?= number_format($model->Price * $product->Width * $product->Height / 1000000, 2) ?>)" class="panel-input profil" for="model-<?= $model->ID ?>">
                    <div class="img">
                        <?= Html::img($model->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $model->lang->Title ?>
                    </div>
                    
                    <div class="text-center add-price-label">
                    <?php if (isset($pJalousies[$model->ID]) && $pJalousies[$model->ID] > 0) { ?>
                        <span class="label label-default">+ <?= number_format($pJalousies[$model->ID], 2) ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } else { ?>
            <div class="col-md-12">
                <label>
                    <input onchange="Calculator.setJalousieModelID(<?= $model->ID ?>)" <?= $product->JalousieModelID == $model->ID ? 'checked' : '' ?> style="display: inline-block" type="radio" name="JalousieModelID" value="<?= $model->ID ?>">  
                    <?= $model->lang->Title ?> <span class="label label-default">+ <?= round($pJalousies[$model->ID], 2) ?> &euro;</span>
                </label>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <div>
        <h3>Jalousies total size</h3>
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon">Lățimea totală:</span>
                    <input onchange="Calculator.setJalousieDimensions()" type="text" class="form-control" placeholder="00" data-min="<?= round($product->Width - ($product->Width * 0.25)) ?>" data-max="<?= round($product->Width + ($product->Width * 0.25)) ?>" name="JalousieFullWidth" value="<?= $product->Width ?>" required="required">
                    <span class="input-group-addon">mm</span>
                </div>
                <div class="jalousie-size-info">Gama admisa: <?= round($product->Width - ($product->Width * 0.25)) ?> mm - <?= round($product->Width + ($product->Width * 0.25)) ?> mm</div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon">Inalțimea totală:</span>
                    <input onchange="Calculator.setJalousieDimensions()" type="text" class="form-control" placeholder="00" data-min="<?= round($product->Height - ($product->Height * 0.25)) ?>" data-max="<?= round($product->Height + ($product->Height * 0.25)) ?>" name="JalousieFullHeight" value="<?= $product->Height ?>" required="required">
                    <span class="input-group-addon">mm</span>
                </div>
                <div class="jalousie-size-info">Gama admisa: <?= round($product->Height - ($product->Height * 0.25)) ?> mm - <?= round($product->Height + ($product->Height * 0.25)) ?> mm</div>
            </div>
        </div>
    </div>
    <div class="jalousie-options" style="display: <?= !empty($jalousies[$product->JalousieID]) && $jalousies[$product->JalousieID]->HasOptions == 1 ? 'block': 'none' ?>;">
        <div>
            <h3>Jalousies sizes</h3>
            <?php foreach ($jalousies as $jalousie) { ?>
            <?php foreach ($jalousie->models as $model) { ?>
            <?php foreach ($model->sizes as $size) { ?>
            <div <?= $product->JalousieModelID == $model->ID ? 'style="display:block;"' : '' ?> data-jalousie-model-id="<?= $model->ID ?>">
                <label>
                    <input onchange="Calculator.setJalousieModelSizeID(<?= $size->ID ?>)" <?= $product->JalousieModelSizeID == $size->ID ? 'checked' : '' ?> style="display: inline-block" type="radio" name="JalousieModelSizeID" value="<?= $size->ID ?>">  
                    <?= $size->lang->Title ?>
                </label>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </div>
        <div>
            <h3>Jalousies colors</h3>
            <?php foreach ($jalousies as $jalousie) { ?>
            <?php foreach ($jalousie->models as $model) { ?>
            <div class="row" <?= $product->JalousieModelID == $model->ID ? 'style="display:block;"' : '' ?> data-jalousie-model-id="<?= $model->ID ?>">
            <?php foreach ($model->colors as $color) { ?>
            <div class="col-md-2 col-sm-4 col-xs-6" data-mh="126">
                <input <?= $product->JalousieModelColorID == $color->ID ? 'checked' : '' ?> type="radio" name="JalousieModelColorID" value="<?= $model->ID ?>" id="model-color-<?= $color->ID ?>">
                <label onclick="Calculator.setJalousieModelColorID(<?= $color->ID ?>)" class="panel-input profil" for="model-color-<?= $color->ID ?>" data-mh="127">
                    <div class="img">
                        <?= Html::img($color->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $color->lang->Title ?>
                    </div>
                    <?php
                    if ($color->Price > 0) {
                        ?>
                        <div class="text-center add-price-label">
                            <span class="label label-default">+<?= CalculatorHelper::calculateJalousieModelColorPrice($product, $color) ?></span>
                        </div>
                        <?php
                    }
                        ?>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div>
            <h3>Opening mechanism</h3>
            <div>
                <?php foreach ($openingMechanisms as $m) { ?>
                <div>
                    <label>
                        <input style="display: inline-block;" onchange="Calculator.setOpeningMechanismID(<?= $m->ID ?>)" <?= $product->OpeningMechanismID == $m->ID ? 'checked' : '' ?> type="radio" name="OpeningMechanismID" value="<?= $m->ID ?>" /> <?= $m->lang->Title ?>
                    </label>
                </div>
                <?php } ?>
            </div>
            <div id="mechanism-options-wrap">
                <?php foreach ($openingMechanisms as $m) { ?>
                <div data-mechanism-id="<?= $m->ID ?>">
                <h4><?= $m->lang->OptionsName ?></h4>
                <?php foreach ($m->mechanismOptions as $o) { ?>
                <div>
                    <label>
                        <input style="display: inline-block;" onchange="Calculator.setOpeningMechanismOptionID(<?= $o->ID ?>)" <?= $product->OpeningMechanismOptionID == $o->ID ? 'checked' : '' ?> type="radio" name="OpeningMechanismOptionID" value="<?= $o->ID ?>" /> <?= $o->lang->Title ?>
                        <?php if ($o->Price > 0) { ?>
                        <span class="label label-default">+ <?= round($o->Price, 2) ?> &euro;</span>
                        <?php } ?>
                    </label>
                </div>
                <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <div>
            <h3>Opening mechanism position</h3>
            <div>
                <div>
                    <label>
                        <input style="display: inline-block;" onchange="Calculator.setOpeningMechanismPosition(this.value)" <?= $product->OpeningMechanismPosition == 'Left' ? 'checked' : '' ?> type="radio" name="OpeningMechanismPosition" value="Left" /> Left
                    </label>
                </div>
                <div>
                    <label>
                        <input style="display: inline-block;" onchange="Calculator.setOpeningMechanismPosition(this.value)" <?= $product->OpeningMechanismPosition == 'Right' ? 'checked' : '' ?> type="radio" name="OpeningMechanismPosition" value="Right" /> Right
                    </label>
                </div>
            </div>
        </div>
        <div>
            <h3>Roll colors</h3>
            <div>
                <div class="row">
                    <?php foreach ($rollColors as $color) { ?>
                    <div class="col-md-2 col-sm-4 col-sm-6" data-mh="124">
                        <input <?= $product->RollColorID == $color->ID ? 'checked' : '' ?> type="radio" name="RollColorID" value="<?= $model->ID ?>" id="roll-color-<?= $color->ID ?>">
                        <label onclick="Calculator.setRollColorID(<?= $color->ID ?>)" class="panel-input profil" for="roll-color-<?= $color->ID ?>" data-mh="125">
                            <div class="img">
                                <?= Html::img($color->imagePath, ['class' => 'img-responsive']) ?>
                            </div>
                            <div class="material">
                                <?= $color->lang->Title ?>
                            </div>
                            <?php
                            if ($color->Price > 0) {
                                ?>
                                <div class="text-center add-price-label">
                                    <span class="label label-default">+<?= CalculatorHelper::calculateRolesColorPrice($product,$color) ?></span>
                                </div>
                                <?php
                            }
                            ?>
                            <button type="button" class="btn-primary select-or-selected">
                                <span class="hidden-after-select">
                                    selectare
                                </span>
                            </button>
                        </label>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    *[data-jalousie-id] {
        display: none;
    }
    *[data-mechanism-id] {
        display: none;
    }
    *[data-jalousie-model-id] {
        display: none;
    }
</style>

<?php if ($product->Jalousies && $product->JalousieID) { ?>
<?php $this->registerJs("
    $('[data-jalousie-id=" . $product->JalousieID . "]').show();
") ?>
<?php } else { ?>
<?php $this->registerJs("
    $('[data-jalousie-id]:first').show();
    $('[data-jalousie-model-id]:first').show();
") ?>
<?php } ?>

<?php if ($product->Jalousies && $product->OpeningMechanismID) { ?>
<?php $this->registerJs("
    $('[data-mechanism-id=" . $product->OpeningMechanismID . "]').show();
") ?>
<?php } else { ?>
<?php $this->registerJs("
    $('[data-mechanism-id]:first').show();
") ?>
<?php } ?>

