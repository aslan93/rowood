<?php

    use yii\bootstrap\Html;

?>
<div class="form-panel">
    <div class="form-panel">
        <div class="calculator">
            <div class="row">
                <div class="col-md-9 col-md-12">
                    <div class="row">
                        <div class="col-md-6 remove-padding-right">
                            <div class="top-panel">
                                <div class="title-panel">
                                    DIMENSIUNEA
                                </div>
                                <div class="info">
                                    Vă rugăm să introduceți dimensiunea dorită incl. Toate atașamentele în milimetri
                                </div>
                            </div>
                            <div class="panel-content-padding">
                                <div class="input-group">
                                    <span class="input-group-addon">Lățimea totală:</span>
                                    <input onchange="Calculator.setDimensions(true)" type="text" class="form-control" placeholder="00" data-min="<?php if(isset($variant->MinWidth)){echo $variant->MinWidth;}  ?>" data-max="<?php if(isset($variant->MaxWidth)){echo $variant->MaxWidth;}  ?>" name="FullWidth" value="<?= $product->Width ?>" required="required">
                                    <span class="input-group-addon">mm</span>
                                </div>
                                <div class="input-group-text">
                                    Gama admisă: <?= isset($variant)?$variant->MinWidth :''?> mm - <?= isset($variant)? $variant->MaxWidth:'' ?> mm
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">Înălțime totală:</span>
                                    <input onchange="Calculator.setDimensions(true)" type="text" class="form-control" placeholder="00" data-min="<?= isset($variant)?$variant->MinHeight:'' ?>" data-max="<?= isset($variant)?$variant->MaxHeight:'' ?>" name="FullHeight" value="<?= isset($variant)?$product->Height:'' ?>" required="required">
                                    <span class="input-group-addon">mm</span>
                                </div>
                                <div class="input-group-text">
                                    Gama admisă: <?= isset($variant)?$variant->MinHeight:'' ?> mm - <?= isset($variant)?$variant->MaxHeight :''?> mm
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 remove-padding">
                            <?= $this->render('part-sizes', [
                                'type' => isset($type)?$type:'',
                                'variant' => isset($variant)?$variant:'',
                                'product' => $product,
                                'rightDimensions' => isset($rightDimensions)?$rightDimensions:'',
                                'bottomDimensions' => isset($bottomDimensions)?$bottomDimensions:'',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div id="product-info-wrap" class="col-md-3 remove-padding-left">
                    <?= $this->render('product-info', [
                        'product' => $product,
                        'material' => isset($material)?$material:'',
                        'profile' => isset($profile)?$profile:'',
                        'wood' => isset($wood)?$wood:'',
                        'frontColor' => isset($frontColor)?$frontColor:'',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>