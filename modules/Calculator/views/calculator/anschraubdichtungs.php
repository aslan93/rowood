<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'Anschraubdichtungs') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați anschraubdichtungs.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Anschraubdichtung
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setAnschraubdichtung(this.value)" <?= !$product->Anschraubdichtung ? 'checked' : '' ?> style="display: block" type="radio" name="Anschraubdichtung" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setAnschraubdichtung(this.value)" <?= $product->Anschraubdichtung ? 'checked' : '' ?> style="display: block" type="radio" name="Anschraubdichtung" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content anschraubdichtung-variants" <?= !$product->TermoEdge ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($anschraubdichtungs as $anschraubdichtung) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="103">
                <input <?= $product->AnschraubdichtungID == $anschraubdichtung->ID ? 'checked' : '' ?> type="radio" name="AnschraubdichtungID" value="<?= $anschraubdichtung->ID ?>" id="anschraubdichtung-<?= $anschraubdichtung->ID ?>">
                <label onclick="Calculator.setAnschraubdichtungID(<?= $anschraubdichtung->ID ?>)" class="panel-input profil" for="anschraubdichtung-<?= $anschraubdichtung->ID ?>" data-mh="106">
                    <div class="material">
                        <?= $anschraubdichtung->lang->Title ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>