<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'ECRAN DE INSECTE') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div style="padding-bottom: 20px;" class="form-panel-content">
    <div>
        Ecran de insecte
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setInsectScreen(this.value)" <?= !$product->InsectScreen ? 'checked' : '' ?> style="display: block" type="radio" name="InsectScreen" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setInsectScreen(this.value)" <?= $product->InsectScreen ? 'checked' : '' ?> style="display: block" type="radio" name="InsectScreen" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content insect-screens-variants" <?= !$product->InsectScreen ? 'style="display:none;"' : '' ?>>
    <h3>Culoarea</h3>
    <div>
        <div class="row">
            <?php foreach ($insectScreens as $insectScreen) { ?>
            <div class="col-md-2 col-sm-6 col-xs-6" data-mh="122">
                <input <?= $product->InsectScreenID == $insectScreen->ID ? 'checked' : '' ?> type="radio" name="InsectScreenID" value="<?= $insectScreen->ID ?>" id="insect-screen-<?= $insectScreen->ID ?>">
                <label onclick="Calculator.setInsectScreenID(<?= $insectScreen->ID ?>)" class="panel-input profil" for="insect-screen-<?= $insectScreen->ID ?>" data-mh="123">
                    <div class="img">
                        <?= Html::img($insectScreen->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $insectScreen->lang->Title ?>
                    </div>
                    <div class="material">
                        <?= $insectScreen->Ral ?>
                    </div>
                    <div class="material">
                        <?php if ($insectScreen->Price > 0) { ?>
                        <span class="label label-default">+ <?= \app\modules\Calculator\components\CalculatorHelper::calculateInsectScreenColorPrice($product,$insectScreen) ?> €</span>
                        <?php } ?>
                    </div>

                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
    <h3>Pânză</h3>
    <div>
        <div class="row">
            <?php foreach ($cloths as $cloth) { ?>
            <div class="col-md-2 col-sm-6 col-xs-6" data-mh="120">
                <input <?= $product->ClothID == $cloth->ID ? 'checked' : '' ?> type="radio" name="ClothID" value="<?= $cloth->ID ?>" id="cloth-<?= $cloth->ID ?>">
                <label onclick="Calculator.setClothID(<?= $cloth->ID ?>)" class="panel-input profil" for="cloth-<?= $cloth->ID ?>" data-mh="121">
                    <div class="img">
                        <?= Html::img($cloth->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $cloth->lang->Title ?>
                    </div>
                    <?php if ($cloth->Price > 0) { ?>
                    <span class="label label-default">+ <?= \app\modules\Calculator\components\CalculatorHelper::calculateInsectScreenClothPrice($product,$cloth)?> €</span>
                    <?php } ?>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>