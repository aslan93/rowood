<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'END CORNERS') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'End corners') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setEndCorner(this.value)" <?= !$product->EndCorner ? 'checked' : '' ?> style="display: block" type="radio" name="EndCorner" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setEndCorner(this.value)" <?= $product->EndCorner ? 'checked' : '' ?> style="display: block" type="radio" name="EndCorner" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>

<div style="padding-bottom: 0px; <?= !$product->EndCorner ? 'display:none;' : '' ?>" class="form-panel-content end-corners-variants">
    <div class="row">   
        <?php foreach ($endCorners as $corner) { ?>
        <div class="col-md-2 col-sm-4 col-xs-6" data-mh="111">
            <input <?= $product->EndCornerID == $corner->ID ? 'checked' : '' ?> type="radio" name="culoare" value="<?= $corner->ID ?>"  id="corner-<?= $corner->ID ?>">
            <label onclick="Calculator.setEndCornerID(<?= $corner->ID ?>)" class="panel-input color" for="corner-<?= $corner->ID ?>" data-mh="112">
                <div class="img">
                    <?= Html::img($corner->imagePath, ['class' => 'img-responsive']) ?>
                </div>
                <div class="material">
                    <?= $corner->lang->Title ?>
                </div>
                <div class="text-center add-price-label">
                <?php if (isset($pEndCorners[$corner->ID])) { ?>
                    <span class="label label-default">+ <?= $pEndCorners[$corner->ID] ?> &euro;</span>
                <?php } ?>
                </div>
                <button type="button" class="btn-primary select-or-selected">
                    <span class="hidden-after-select">
                        selectare
                    </span>
                </button>
            </label>
        </div>
        <?php } ?>
    </div>
</div>
<br />