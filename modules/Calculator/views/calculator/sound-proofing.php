<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'IZOLATIE FONICA') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați ferestre.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Izolatia fonica
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSoundProofing(this.value)" <?= !$product->SoundProofing ? 'checked' : '' ?> style="display: block" type="radio" name="SoundProofing" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSoundProofing(this.value)" <?= $product->SoundProofing ? 'checked' : '' ?> style="display: block" type="radio" name="SoundProofing" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content sound-proofing-variants" <?= !$product->SoundProofing ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($soundProofings as $soundProofing) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="136">
                <input <?= $product->SoundProofingID == $soundProofing->ID ? 'checked' : '' ?> type="radio" name="SoundProofingID" value="<?= $soundProofing->ID ?>" id="sound-proofing-<?= $soundProofing->ID ?>">
                <label onclick="Calculator.setSoundProofingID(<?= $soundProofing->ID ?>)" class="panel-input profil" for="sound-proofing-<?= $soundProofing->ID ?>" data-mh="137">
                    <div class="img">
                        <?= Html::img($soundProofing->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $soundProofing->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if (isset($pSoundProofings[$soundProofing->ID]) && $pSoundProofings[$soundProofing->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pSoundProofings[$soundProofing->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>