<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'STICLA DE SIGURANTA') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați sticla.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Sticla de siguranta
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSafety(this.value)" <?= !$product->Safety ? 'checked' : '' ?> style="display: block" type="radio" name="Safety" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSafety(this.value)" <?= $product->Safety ? 'checked' : '' ?> style="display: block" type="radio" name="Safety" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content safety-variants" <?= !$product->Safety ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($safeties as $safety) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="130">
                <input <?= $product->SafetyID == $safety->ID ? 'checked' : '' ?> type="radio" name="SafetyID" value="<?= $safety->ID ?>" id="safety-<?= $safety->ID ?>">
                <label onclick="Calculator.setSafetyID(<?= $safety->ID ?>)" class="panel-input profil" for="safety-<?= $safety->ID ?>" data-mh="131">
                    <div class="img">
                        <?= Html::img($safety->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $safety->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if (isset($pSafeties[$safety->ID]) && $pSafeties[$safety->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pSafeties[$safety->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>