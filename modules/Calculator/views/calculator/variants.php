<?php

    use yii\bootstrap\Html;

?>

<div class="row">
    <?php foreach ($variants as $variant) { ?>
    <div class="col-md-2 col-sm-4 col-xs-6" data-mh="142">
        <input type="radio" name="Variant" value="<?= $variant->ID ?>" <?= $product->VariantID == $variant->ID ? 'checked' : '' ?> id="variant-<?= $variant->ID ?>">
        <label onclick="Calculator.setVariant(<?= $variant->ID ?>)" class="panel-input type-glass" for="variant-<?= $variant->ID ?>" data-mh="143">
            <div class="img">
                <?= Html::img($variant->imagePath, ['class' => 'img-responsive']) ?>
            </div>
            <button type="button" class="btn-primary select-or-selected">
                <span class="hidden-after-select">
                    selectare
                </span>
            </button>
        </label>
    </div>
    <?php } ?>
</div>