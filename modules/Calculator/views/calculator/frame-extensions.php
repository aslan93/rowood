<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'EXTINDEREA CADRULUI') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați marimea.
    </div>
</div>
<div class="form-panel-content">
    <div class="row">
        <?php foreach ($frameExtensions as $part => $sizes) { ?>
        <div class="col-md-3">
            <div><b><?= $part ?></b></div>
            <div>
                <?= Html::radioList('FrameExtension' . $part, '', $sizes, [
                    'item' => function ($index, $label, $name, $checked, $value) use ($product, $part) {
                        $dimension = in_array($part, ['Right', 'Left']) ? 'Height' : 'Width';
                        $label = $value > 0 ? "$label <span class=\"label label-default\">+ " . number_format(($product->$dimension * $value / 1000000 * 530), 2) . " €</span>" : '0';
                        return '<div class="radio">
                                    <label>' . Html::radio($name, $product->{'FrameExtension' . $part} > 0, [
                                        'value' => $value,
                                        'style' => 'display:block',
                                        'onchange' => 'Calculator.setFrameExtension("' . $part . '", ' . $value . ', ' . number_format(($product->$dimension * $value / 1000000 * 530), 2) . ')'
                                    ]) . $label . '</label>
                                </div>';
                    },
                    'tag' => 'div',
                ]) ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>