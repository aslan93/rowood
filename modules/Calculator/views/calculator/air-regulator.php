<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'AIR REGULATOR') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'Air regulator') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setAirRegulator(this.value)" <?= !$product->AirRegulator ? 'checked' : '' ?> style="display: block" type="radio" name="AirRegulator" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setAirRegulator(this.value)" <?= $product->AirRegulator ? 'checked' : '' ?> style="display: block" type="radio" name="AirRegulator" value="1">
                Da <span class="label label-default">+ 12.60 €</span>
            </label>
        </div>
    </div>
</div>
<br />