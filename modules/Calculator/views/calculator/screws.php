<?php

    use yii\bootstrap\Html;

?>

<br />
<div class="form-panel-header">
    <div class="title-panel">
        SCREWS
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'Screws') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setScrew(this.value)" <?= !$product->Screw ? 'checked' : '' ?> style="display: block" type="radio" name="Screw" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setScrew(this.value)" <?= $product->Screw ? 'checked' : '' ?> style="display: block" type="radio" name="Screw" value="1">
                Da
            </label>
        </div>
    </div>
</div>

<div <?= $product->Screw ? 'style="display: block"' : 'style="display: none"' ?> class="form-panel-content screw-variants">
    <div class="row">
        <?php foreach ($screws as $screw) { ?>
        <div class="col-md-2 col-sm-4 col-xs-6" data-mh="134">
            <input <?= $product->ScrewID == $screw->ID ? 'checked' : '' ?> type="radio" name="SkrewID" value="<?= $screw->ID ?>"  id="screw-<?= $screw->ID ?>">
            <label onclick="Calculator.setScrewID(<?= $screw->ID ?>, 'front')" class="panel-input color" for="screw-<?= $screw->ID ?>" data-mh="135">
                <div class="img">
                    <?= Html::img($screw->imagePath, ['class' => 'img-responsive']) ?>
                </div>
                <div class="material">
                    <?= $screw->lang->Title ?>
                </div>
                <div class="text-center add-price-label">
                <?php if ($screwsCount > 0) { ?>
                    <span class="label label-default">+ <?= round($screw->Price * $screwsCount,2) ?> &euro;</span>
                <?php } ?>
                </div>
                <button type="button" class="btn-primary select-or-selected">
                    <span class="hidden-after-select">
                        selectare
                    </span>
                </button>
            </label>
        </div>
        <?php } ?>
    </div>
</div>