<?php

    use yii\bootstrap\Html;

?>

    <div class="form-panel-header">
        <div class="title-panel">
            TIPUL DE FEREASTRĂ
        </div>
        <div class="info">
            Vă rugăm să selectați tipul dorit de fereastră.
        </div>
    </div>
    <div id="categories-types-wrap">
        <?php foreach ($categories as $category) { ?>
       <div style="display: <?=$category->ID==5?'none':'block'?>">
        <div class="category-name">
            <span><?= $category->lang->Title?></span>
        </div>
        <div style="padding: 20px 0;" class="form-panel-content">
            <div class="row">
                <?php foreach ($category->types as $type) { ?>
                <div class="col-md-2 col-sm-4 col-xs-6" data-mh="107">
                    <input type="radio" name="Type" value="<?= $type->ID ?>" <?= $product->TypeID == $type->ID ? 'checked' : '' ?>  id="type-<?= $type->ID ?>">
                    <label onclick="Calculator.setType(<?= $type->ID ?>, this)" class="panel-input type-glass" for="type-<?= $type->ID ?>" data-mh="108">
                        <div class="img">
                            <?= Html::img($type->imagePath, ['class' => 'img-responsive']) ?>
                        </div>
                        <button type="button" class="btn-primary select-or-selected">
                            <span class="hidden-after-select">
                                selectare
                            </span>
                        </button>
                    </label>
                </div>
                <?php } ?>
            </div>
            <div class="variants-wrap">
                
            </div>
          </div>
       </div>
        <?php } ?>
    </div>

    <?php if ($product->TypeID) { ?>
    <script>
        setTimeout(function(){
            Calculator.setType(<?= $product->TypeID ?>, $('label[for="type-<?= $product->TypeID ?>"]'), 1);
        }, 2000);
    </script>
    <?php } ?>