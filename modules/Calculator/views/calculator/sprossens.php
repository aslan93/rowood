<?php

    use yii\bootstrap\Html;
    use yii\widgets\Pjax;
    $countParts = isset($variants[$product->VariantID]) ? $variants[$product->VariantID]->MaxNumber : 0;
    
    $sprossenCount = unserialize($product->SprossenCount);

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'SPROSSENS') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'Sprossen') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSprossen(this.value)" <?= !$product->Sprossen ? 'checked' : '' ?> style="display: block" type="radio" name="Sprossen" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setSprossen(this.value)" <?= $product->Sprossen ? 'checked' : '' ?> style="display: block" type="radio" name="Sprossen" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>

<div style="padding-bottom: 0px; <?= !$product->Sprossen ? 'display:none;' : '' ?>" class="form-panel-content sprossen-variants">
    <div class="row">
        <?php foreach ($sprossens as $sprossen) { ?>
        <div class="col-md-3 col-sm-4 col-xs-6" data-mh="138">
            <input <?= $product->SprossenID == $sprossen->ID ? 'checked' : '' ?> type="radio" name="SprossenID" value="<?= $sprossen->ID ?>"  id="sprossen-<?= $sprossen->ID ?>">
            <label onclick="Calculator.setSprossenID(<?= $sprossen->ID ?>)" class="panel-input color" for="sprossen-<?= $sprossen->ID ?>" data-mh="139">
                <div class="img">
                    <?= Html::img($sprossen->imagePath, ['class' => 'img-responsive']) ?>
                </div>
                <!--div class="material">
                    <?= isset($sprossen->lang->Title)?$sprossen->lang->Title:'' ?>
                </div-->
                <button type="button" class="btn-primary select-or-selected">
                    <span class="hidden-after-select">
                        selectare
                    </span>
                </button>
            </label>
        </div>
        <?php } ?>
    </div>
    <hr />
    <div>
        <h3>Sprossen width</h3>
        <?php foreach ($sprossens as $sprossen) { ?>
        <div <?= $sprossen->ID == $product->SprossenID ? '' : 'style="display:none;"' ?> sprossen-id="<?= $sprossen->ID ?>">
            <?php foreach ($sprossen->sprossenWidths as $width) { ?>
            <div>
                <label>
                    <input onchange="Calculator.setSprossenWidth(<?= $width->ID ?>)" <?= $product->SprossenWidth == $width->ID ? 'checked' : '' ?> style="display: inline-block" type="radio" name="SprossenWidth" value="<?= $width->Width ?>">
                    <?= $width->Width ?> mm 
                    <?php if (isset($pSprossenWidth[$width->ID])) { ?>
                    <span class="label label-default">+ <?= number_format($pSprossenWidth[$width->ID], 2) ?> €</span>
                    <?php } ?>
                </label>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <hr />
    <div>
        <h3>Sprossen position</h3>
        <div class="row">
            <div class="col-md-6">
                <form id="sprossen-count-form">
                    <?php for ($i = 1; $i <= $countParts; $i++) { ?>
                    <h5>Position <?= $i ?></h5>
                    <div style="margin-bottom: 5px;" class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">vertical</span>
                                <input type="number" onchange="Calculator.validateInput(this)" min="0" data-max=4 name="SprossenCount[<?= $i ?>][W]" class="form-control" placeholder="Sprossen count" value="<?= isset($sprossenCount['SprossenCount'][$i]['W']) ? $sprossenCount['SprossenCount'][$i]['W'] : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">horizontal</span>
                                <input type="number" onchange="Calculator.validateInput(this)" min="0" data-max=4 name="SprossenCount[<?= $i ?>][H]" class="form-control" placeholder="Sprossen count" value="<?= isset($sprossenCount['SprossenCount'][$i]['H']) ? $sprossenCount['SprossenCount'][$i]['H'] : '' ?>">
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </form>
            </div>
            <div class="col-md-6">
                <div style="text-align: center">
                    <?= $this->render('canvas', [
                        'sprossens' => $sprossens,
                        'sprossenCount'=> $sprossenCount,
                        'countParts'=>$countParts,
                        'product' => $product,
                    ]) ?>
                </div>
                <br><br>
                <div style="text-align: center">
                <button onclick="Calculator.calculateSprosses()" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Calculeaza prețul sprosuri</button>
                </div>
            </div>
        </div>
    </div>
</div>
<br>