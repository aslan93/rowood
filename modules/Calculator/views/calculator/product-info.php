<?php

    

?>

<div class="title-panel red-bg">
    CONFIGURAȚIA ALEASĂ
</div>
<div class="sum-calculator">
    <div class="price">
        PRET
        <span class="big-text">
            <?= $product->Price ?> &euro;
        </span>
    </div>
    <div class="impozit">
        <span>
            incl. 19% TVA. excl. de transport maritim
        </span>
    </div>
    <div class="what-choose">
        <div>
            <span class="material">
                Material:
            </span>
            <span>
                <?= empty($material) ? '-' : $material->lang->Title ?>
            </span>
        </div>
        <div>
            <span class="specii">
                PROFIL:
            </span>
            <span>
                <?= empty($profile) ? '-' : $profile->lang->Title ?>
            </span>
        </div>
        <div>
            <span class="culoare">
                CULOARE:
            </span>
            <span>
                <?= empty($frontColor) ? '-' : $frontColor->lang->Title ?>
            </span>
        </div>
    </div>
    <div class="submit-form-details" style="display: <?=$product->Width && $product->Height?'block':'none'?>">
        <button onclick="Calculator.nextStep()" type="submit">
            URMATORUL PAS
        </button>
    </div>
</div>