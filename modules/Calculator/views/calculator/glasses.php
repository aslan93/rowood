<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'GLASSES') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați ferestre.
    </div>
</div>
<div class="form-panel-content">
    <div>
        <div class="row">
            <?php
            if(isset($product)){
            ?>
            <?php foreach ($glasses as $glass) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="116">
                <input <?= $product->GlassID == $glass->ID ? 'checked' : '' ?> type="radio" name="Glass" value="<?= $glass->ID ?>" id="glass-<?= $glass->ID ?>">
                <label onclick="Calculator.setGlass(<?= $glass->ID ?>)" class="panel-input profil" for="glass-<?= $glass->ID ?>" data-mh="117">
                    <div class="img">
                        <?= Html::img($glass->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $glass->lang->Title ?>
                    </div>
                    <div>
                        <?= $glass->lang->Text ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if (isset($pGlasses[$glass->ID]) && $pGlasses[$glass->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pGlasses[$glass->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php }
            } ?>
        </div>
    </div>
</div>