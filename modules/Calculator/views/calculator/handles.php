<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'MANIERE') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>
<div class="form-panel-content">
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setHandle(this.value)" <?= !$product->Handle ? 'checked' : '' ?> style="display: block" type="radio" name="Handle" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setHandle(this.value)" <?= $product->Handle ? 'checked' : '' ?> style="display: block" type="radio" name="Handle" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content handle-variants" <?= !$product->Handle ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($handles as $handle) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="118">
                <input <?= $product->HandleID == $handle->ID ? 'checked' : '' ?> type="radio" name="HandleID" value="<?= $handle->ID ?>" id="handle-<?= $handle->ID ?>">
                <label onclick="Calculator.setHandleID(<?= $handle->ID ?>, <?= (float)isset($pHandles[$handle->ID])?$pHandles[$handle->ID]:'' ?>)" class="panel-input profil" for="handle-<?= $handle->ID ?>">
                    <div class="img">
                        <?= Html::img($handle->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $handle->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if (isset($pHandles[$handle->ID]) && $pHandles[$handle->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pHandles[$handle->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>