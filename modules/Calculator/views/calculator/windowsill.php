<?php

    use yii\widgets\Pjax;

    $minLength = round($product->Width - ($product->Width * 0.2));
    $maxLength = round($product->Width + ($product->Width * 0.2));

    
?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'PERVAZ EXTERIOR') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        Pervaz exterior
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setWindowsill(this.value)" <?= !$product->Windowsill ? 'checked' : '' ?> style="display: block" type="radio" name="Windowsill" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setWindowsill(this.value)" <?= $product->Windowsill ? 'checked' : '' ?> style="display: block" type="radio" name="Windowsill" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>

<div class="windowsill-options" <?= !$product->Windowsill ? 'style="display:none;"' : '' ?>>
    
    <div class="form-panel-content">

        <div class="windowsill-length">
            <div style="max-width: 300px;" class="input-group">
                <span class="input-group-addon">Lățimea totală:</span>
                <input onchange="Calculator.setWindowsillLength(this.value)" type="text" class="form-control" placeholder="00" data-min="<?= $minLength ?>" data-max="<?= $maxLength ?>" name="WindowsillLength" value="<?= $product->WindowsillLength ?>" required="required">
                <span class="input-group-addon">mm</span>
            </div>
            <div class="text-muted">Gama admisă: <span id="windowsill-min"><?= $minLength ?></span> mm - <span id="windowsill-max"><?= $maxLength ?></span> mm</div>
        </div>

    </div>
    
    <?= $this->render('windowsill-colors', [
        'windowsillColors' => $windowsillColors,
        'product' => $product,
    ]); ?>
    
    <?= $this->render('windowsill-widths', [
        'windowsillWidths' => $windowsillWidths,
        'pWindowsillWidths' => $pWindowsillWidths,
        'product' => $product,
    ]); ?>
    
    <?= $this->render('end-corners', [
        'endCorners' => $endCorners,
        'pEndCorners' => $pEndCorners,
        'product' => $product,
    ]); ?>
    
    <?= $this->render('h-connector', [
        'product' => $product,
    ]); ?>
    
    <?= $this->render('screws', [
        'screws' => $screws,
        'screwsCount' => $screwsCount,
        'product' => $product,
    ]); ?>
    
    <!-- Anschraubdichtungs -->
    <div id="anschraubdichtungs-wrap">
        <?= $this->render('anschraubdichtungs', [
            'anschraubdichtungs' => $anschraubdichtungs,
            'product' => $product,
        ]) ?>
    </div>
    <!-- Anschraubdichtungs -->

    <!-- Sound line -->
    <div id="sound-line-wrap">
        <?= $this->render('sound-line', [
            'product' => $product,
        ]) ?>
    </div>
    <!-- Sound line -->

    <!-- Connection profiles -->
    <?php Pjax::begin([
        'id' => 'connection-profiles-pjax',
        'timeout' => 50000,
    ]) ?>
    <div id="connection-profiles-wrap">
        <?= $this->render('connection-profiles', [
            'connectionProfiles' => $connectionProfiles,
            'pConnectionProfiles' => $pConnectionProfiles,
            'product' => $product,
        ]) ?>
    </div>
    <?php Pjax::end() ?>
    <!-- Connection profiles -->
    
</div>
<br />