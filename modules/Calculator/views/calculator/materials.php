<?php

    use yii\bootstrap\Html;
    use app\components\PriceConverter\PriceConverter;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'MATERIAL') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați între ferestre din plastic, metal și plastic, lemn sau aluminiu.
    </div>
</div>
<div class="form-panel-content">
    <div>
        <div class="row">
            <?php foreach ($materials as $material) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="128">
                <input type="radio" name="Material" value="<?= $material->ID ?>" id="material-<?= $material->ID ?>">
                <label data-id="<?= $material->ID ?>" onclick="Calculator.setMaterial(<?= $material->ID ?>)" class="panel-input" for="material-<?= $material->ID ?>" data-mh="129">
                    <div class="img">
                        <?= Html::img($material->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $material->lang->Title ?>
                    </div>
                    <button type="button" class="btn-primary price">
                        de la <?= PriceConverter::convert($material->StartPrice, 0) ?>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php if ($product->MaterialID) { ?>
<script>
    $('label[data-id=<?= $product->MaterialID ?>]').click();
</script>
<?php } ?>
