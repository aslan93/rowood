<?php

    use yii\web\View;
    use yii\widgets\Pjax;
    use app\modules\Calculator\assets\CalculatorAsset;
    
    CalculatorAsset::register($this);
    $this->title = 'Configurator';
?>

<?php if ($product) { ?>
<script>
    Calculator.setProductID(<?= $product->ID ?>);
</script>
<?php } ?>
<div style="min-height: 267px;" class="wrap">
    <div class="container">
        <div id="calculator-wrap">
            <div id="calculator-step-1">

                <!-- Materials -->
                <div id="materials-wrap">
                    <?= $this->render('materials', [
                        'materials' => $materials,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Materials -->

                <!-- Profiles -->
                <div id="profiles-wrap">
                    <?= $this->render('profiles', [
                        'profiles' => $profiles,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Profiles -->
                <!-- Woods -->
                <div id="woods-wrap">
                    <?php if ($woods) { ?>
                        <?= $this->render('woods', [
                            'woods' => $woods,
                            'product' => $product,
                        ]) ?>
                    <?php } ?>
                </div>
                <!-- Woods -->

                <!-- Front colors -->
                <div id="front-colors-wrap">
                    <?php if ($frontColors) { ?>
                        <?= $this->render('front-colors', [
                            'frontColors' => $frontColors,
                            'product' => $product,
                        ]) ?>
                    <?php } ?>
                </div>
                <!-- Front colors -->

                <!-- Back colors -->
                <div id="back-colors-wrap">
                    <?= $this->render('back-colors', [
                        'backColors' => $backColors,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Back colors -->

                <!-- Categories -->
                <div id="categories-wrap">
                    <?= $this->render('categories', [
                        'categories' => $categories,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Categories -->

                <!-- Sizes -->
                <div id="sizes-wrap">
                    <?= $this->render('sizes', [
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Sizes -->

            </div>
            <div id="calculator-step-2" style="display: none;">

                <div>
                    <button class="btn btn-primary" onclick="Calculator.prevStep()"><i class="fa fa-chevron-left"></i> Inapoi</button>
                </div>
                <br />
                
                <?php Pjax::begin(['id' => 'step2-pjax', 'timeout' => 50000]) ?>
                
                <!-- Glasses -->
                <div id="glasses-wrap">
                    <?= $this->render('glasses', [
                        'glasses' => $glasses,
                        'pGlasses' => $pGlasses,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Glasses -->

                <!-- Termo edges -->
                <?php Pjax::begin(['id' => 'termoedge-pjax', 'timeout' => 50000]) ?>
                <div id="termo-edges-wrap">
                    <?= $this->render('termo-edges', [
                        'termoEdges' => $termoEdges,
                        'pTermoEdges' => $pTermoEdges,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Termo edges -->

                <!-- Sound proofing -->
                <div id="sound-proofing-wrap">
                    <?= $this->render('sound-proofing', [
                        'soundProofings' => $soundProofings,
                        'pSoundProofings' => $pSoundProofings,
                        'product' => $product,
                    ]) ?>
                </div>
                <?php Pjax::end() ?>
                <!-- Sound proofing -->

                <!-- Safeties -->
                <div id="safeties-wrap">
                    <?= $this->render('safeties', [
                        'safeties' => $safeties,
                        'pSafeties' => $pSafeties,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Safeties -->

                <!-- Sprossen -->
                <?php Pjax::begin(['id' => 'sprossen-pjax', 'timeout' => 50000]) ?>
                <div id="sprossens-wrap">
                    <?= $this->render('sprossens', [
                        'sprossens' => $sprossens,
                        'variants' => $variants,
                        'pSprossenWidth' => $pSprossenWidth,
                        'product' => $product,
                    ]) ?>
                </div>
                <?php Pjax::end() ?>
                <!-- Sprossen -->

                <!-- Decorations -->
                <div id="decorations-wrap">
                    <?= $this->render('decorations', [
                        'decorations' => $decorations,
                        'pDecorations' => $pDecorations,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Decorations -->

                <!-- Fittings -->
                <?php Pjax::begin(['id' => 'fittings-pjax']) ?>
                <div id="fittings-wrap">
                    <?= $this->render('fittings', [
                        'fittings' => $fittings,
                        'pFittings' => $pFittings,
                        'product' => $product,
                    ]) ?>
                </div>
                <?php Pjax::end() ?>
                <!-- Fittings -->
                
                <!-- Montage holes -->
                <div id="montage-holes-wrap">
                    <?= $this->render('montage-holes', [
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Montage holes -->

                <!-- Air regulator -->
<!--                <div id="montage-holes-wrap">-->
<!--                    --><?//= $this->render('air-regulator', [
//                        'product' => $product,
//                    ]) ?>
<!--                </div>-->
                <!-- Air regulator -->

                <!-- Handles -->
                <div id="handles-wrap">
                    <?= $this->render('handles', [
                        'handles' => $handles,
                        'pHandles' => $pHandles,
                        'product' => $product,
                    ]) ?>
                </div>
                <!-- Handles -->

                <!-- Windowsill -->
                <?php Pjax::begin(['id' => 'windowsill-pjax', 'timeout' => 50000]) ?>
                <div id="windowsill-wrap">
                    <?= $this->render('windowsill', [
                        'windowsillColors' => $windowsillColors,
                        'windowsillWidths' => $windowsillWidths,
                        'pWindowsillWidths' => $pWindowsillWidths,
                        'endCorners' => $endCorners,
                        'pEndCorners' => $pEndCorners,
                        'screws' => $screws,
                        'screwsCount' => $screwsCount,
                        'connectionProfiles' => $connectionProfiles,
                        'pConnectionProfiles' => $pConnectionProfiles,
                        'anschraubdichtungs' => $anschraubdichtungs,
                        'product' => $product,
                    ]) ?>
                </div>
                <?php Pjax::end() ?>
                <!-- Windowsill -->
                
                <!-- Jalousies -->
                <div id="jalousies-wrap">
                    <?= $this->render('jalousies', [
                        'product' => $product,
                        'jalousies' => $jalousies,
                        'pJalousies' => $pJalousies,
                        'openingMechanisms' => $openingMechanisms,
                        'rollColors' => $rollColors,
                        'endScheneColors' => $endScheneColors,
                    ]) ?>
                </div>
                <!-- Jalousies -->
                
                <!-- Insect creen -->
                <div id="insect-screen-wrap">
                    <?= $this->render('insect-screen', [
                        'product' => $product,
                        'insectScreens' => $insectScreens,
                        'cloths' => $cloths,
                    ]) ?>
                </div>
                <!-- Insect creen -->
                
                <!-- Insect creen -->
                <div id="frame-extensions-wrap">
                    <?= $this->render('frame-extensions', [
                        'product' => $product,
                        'frameExtensions' => $frameExtensions,
                    ]) ?>
                </div>
                <!-- Insect creen -->
                
                <?php Pjax::end() ?>
                
                <br />
                
                <div>
                    <button onclick="Calculator.addToCart()" class="btn btn-lg btn-danger"><i class="fa fa-shopping-cart"></i> Adauga in cos</button>
                </div>

            </div>
        </div>
    </div>
</div>

