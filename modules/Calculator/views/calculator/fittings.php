<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'FITTING') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați variant.
    </div>
</div>

<div class="form-panel-content fitting-variants">
    <div>
        <div class="row">
            <?php foreach ($fittings as $fitting) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="113">
                <input <?= $product->FittingID == $fitting->ID ? 'checked' : '' ?> type="radio" name="FittingID" value="<?= $fitting->ID ?>" id="fitting-<?= $fitting->ID ?>">
                <label onclick="Calculator.setFittingID(<?= $fitting->ID ?>)" class="panel-input profil" for="fitting-<?= $fitting->ID ?>" data-mh="114">
                    <div class="img">
                        <?= Html::img($fitting->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $fitting->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if ($pFittings[$fitting->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pFittings[$fitting->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>