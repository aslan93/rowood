<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'H-CONNECTOR') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați optiunea.
    </div>
</div>

<div style="padding-bottom: 0px;" class="form-panel-content">
    <div>
        <?= Yii::t('app', 'H-connector') ?>
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setHConnector(this.value)" <?= !$product->HConnector ? 'checked' : '' ?> style="display: block" type="radio" name="HConnector" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setHConnector(this.value)" <?= $product->HConnector ? 'checked' : '' ?> style="display: block" type="radio" name="HConnector" value="1">
                Da
            </label>
        </div>
    </div>
</div>