<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'PROFILE') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați între ferestre din plastic, metal și plastic, lemn sau aluminiu.
    </div>
</div>
<div class="form-panel-content">
    <div>
        <div class="row">
            <?php foreach ($profiles as $profile) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="132">
                <input <?= $product->ProfileID == $profile->ID ? 'checked' : '' ?> type="radio" name="Profile" value="<?= $profile->ID ?>" id="profile-<?= $profile->ID ?>">
                <label onclick="Calculator.setProfile(<?= $profile->ID ?>)" class="panel-input profil" for="profile-<?= $profile->ID ?>" data-mh="133">
                    <div class="img">
                        <?= Html::img($profile->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $profile->lang->Title ?>
                    </div>
                    <div class="title-label">
                        <?= $profile->lang->Text ?>
                    </div>
                    <div class="add-price-label">
                    <?php if ($profile->Percent > 0) { ?>
                        <span class="label label-default">+ <?= round($profile->Percent) ?> %</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>