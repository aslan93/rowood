<?php

    use yii\bootstrap\Html;
    use app\modules\Material\models\Material;
    
    $material = Material::findOne($product->MaterialID);

?>


<?php if ($material->isMultiMaterial) { ?>

    <div class="form-panel-header">
        <div class="title-panel">
            CULOARE
        </div>
        <div class="info">
            Vă rugăm să selectați culoarea dorită.
        </div>
    </div>
    <div class="form-panel-content">
        <div class="row">

            <?php foreach ($backColors as $color) { ?>
            <div class="col-md-2 col-sm-4 col-sm-6" data-mh="104">
                <input <?= $product->BackColorID == $color->ID ? 'checked' : '' ?> type="radio" name="culoare-back" value="<?= $color->ID ?>"  id="color-back-<?= $color->ID ?>">
                <label onclick="Calculator.setColor(<?= $color->ID ?>, 'back')" class="panel-input color" for="color-back-<?= $color->ID ?>" data-mh="105">
                    <div class="img">
                        <?= Html::img($color->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $color->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if ($color->Percent > 0) { ?>
                        <span class="label label-default">+ <?= round($color->Percent) ?> %</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>

<?php } ?>
