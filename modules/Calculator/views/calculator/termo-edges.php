<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'TERMO EDGE') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați ferestre.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Sigiliu de margine separată termic (muchie cald)
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setTermoEdge(this.value)" <?= !$product->TermoEdge ? 'checked' : '' ?> style="display: block" type="radio" name="TermoEdge" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setTermoEdge(this.value)" <?= $product->TermoEdge ? 'checked' : '' ?> style="display: block" type="radio" name="TermoEdge" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content termo-edge-variants" <?= !$product->TermoEdge ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($termoEdges as $termoEdge) { ?>
            <div class="col-md-3 col-sm-6 col-xs-6" data-mh="140">
                <input <?= $product->TermoEdgeID == $termoEdge->ID ? 'checked' : '' ?> type="radio" name="TermoEdgeID" value="<?= $termoEdge->ID ?>" id="termo-edge-<?= $termoEdge->ID ?>">
                <label onclick="Calculator.setTermoEdgeID(<?= $termoEdge->ID ?>)" class="panel-input profil" for="termo-edge-<?= $termoEdge->ID ?>" data-mh="141">
                    <div class="img">
                        <?= Html::img($termoEdge->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $termoEdge->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if (isset($pTermoEdges[$termoEdge->ID]) && $pTermoEdges[$termoEdge->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pTermoEdges[$termoEdge->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>