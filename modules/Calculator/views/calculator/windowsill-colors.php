<?php

    use yii\bootstrap\Html;

?>


<div class="form-panel-header">
    <div class="title-panel">
        CULOARE
    </div>
    <div class="info">
        Vă rugăm să selectați culoarea dorită.
    </div>
</div>
<div class="form-panel-content">
    <div class="row">
        <?php foreach ($windowsillColors as $color) { ?>
        <div class="col-md-2 col-sm-4 col-xs-6" data-mh="144">
            <input <?= $product->WindowsillColorID == $color->ID ? 'checked' : '' ?> type="radio" name="WindowsillColorID" value="<?= $color->ID ?>"  id="windowsill-color-<?= $color->ID ?>">
            <label onclick="Calculator.setWindowsillColor(<?= $color->ID ?>, 'front')" class="panel-input color" for="windowsill-color-<?= $color->ID ?>" data-mh="145">
                <div class="img">
                    <?= Html::img($color->imagePath, ['class' => 'img-responsive']) ?>
                </div>
                <div class="material">
                    <?= $color->lang->Title ?>
                </div>
                <button type="button" class="btn-primary select-or-selected">
                    <span class="hidden-after-select">
                        selectare
                    </span>
                </button>
            </label>
        </div>
        <?php } ?>
    </div>
</div>