<?php

    use yii\bootstrap\Html;

?>


<div class="form-panel-header">
    <div class="title-panel">
        CULOARE
    </div>
    <div class="info">
        Vă rugăm să selectați culoarea dorită.
    </div>
</div>
<div class="form-panel-content">
    <div class="row">


        <?php foreach ($frontColors as $color) { ?>
        <div class="col-md-2 col-sm-4 col-xs-6" data-mh="214">
            <input <?= $product->FrontColorID == $color->ID ? 'checked' : '' ?> type="radio" name="culoare" value="<?= $color->ID ?>"  id="color-<?= $color->ID ?>">
            <label onclick="Calculator.setColor(<?= $color->ID ?>, 'front')" class="panel-input color" for="color-<?= $color->ID ?>" data-mh="215">
                <div class="img">
                    <?= Html::img($color->imagePath, ['class' => 'img-responsive']) ?>
                </div>
                <div class="material" data-mh="217">
                    <?= $color->lang->Title ?>
                </div>
                <div class="text-center add-price-label">
                <?php if ($color->Percent > 0) { ?>
                    <span class="label label-default">+ <?= round($color->Percent) ?> %</span>
                <?php } ?>
                </div>
                <button type="button" class="btn-primary select-or-selected">
                    <span class="hidden-after-select">
                        selectare
                    </span>
                </button>
            </label>
        </div>
        <?php } ?>
    </div>
</div>