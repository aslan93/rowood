<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'WOOD') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați între ferestre din plastic, metal și plastic, lemn sau aluminiu.
    </div>
</div>
<div class="form-panel-content">
    <?php foreach ($woods as $wood) { ?>
    <div>
        <label onclick="Calculator.setWood(<?= $wood->ID ?>)">
            <input <?= $product->WoodID == $wood->ID ? 'checked' : '' ?> style="display: inline-block; position: relative; top: 2px;" type="radio" name="Wood" value="<?= $wood->ID ?>" /> <?= $wood->lang->Title ?> 
            <?php if ($wood->Percent > 0) { ?>
            <code>+<?= $wood->Percent ?>%</code>
            <?php } ?>
        </label>
    </div>
    <?php } ?>
</div>