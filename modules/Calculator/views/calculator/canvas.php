<?php

    ?>
    <div class="canv-wrapper"
         style="border: 2px solid black; padding: 5px; display: inline-block; background-color: #f7f3fb;">
        <?php

        $dimensions = unserialize($product->ConfiguratorInfo)['Dimensions'];
if (isset($dimensions)) {
    foreach ($dimensions as $key => $value) {
        $height = 250;
        $width = 120;

//  2 parts
        if ($countParts == 2) {
            $height = 150;
            $width = 120;


            if ($dimensions[$key]['height'] < $dimensions[$key + 1]['height'] || $dimensions[$key]['height'] < $dimensions[$key - 1]['height']) {
                $height = 70;
            }

            if ($dimensions[$key]['height'] == $dimensions[$key + 1]['height'] || $dimensions[$key]['height'] == $dimensions[$key - 1]['height']) {
                $height = 120;
                $width = 120;
            }

            if ($value['open'] == '0W' || $value['open'] == '1W') {
                $newline = true;
            } else {
                $height = 250;
                $width = 120;
            }
        }

//  3 parts
        if ($countParts == 3) {
            $height = 180;
            $width = 100;
            $type = 'simple';
            foreach ($dimensions as $dimension) {
                if ($dimension['open'] == '1W' || $dimension['open'] == '0W') {
                    $type = 'special';
                }
            }
            if ($type == 'special') {
                if ($value['open'] == '0W' || $value['open'] == '1W') {
                    $newline = true;
                    $width = 215;
                    $height = 70;
                    if ($key == 2) {
                        echo '<br>';
                    }
                } else {
                    $newline = false;
                }
            } else {
                $height = 250;
                $width = 120;
            }
        }


// 4 parts
        if ($countParts == 4) {
            $width = 120;
            $height = 180;
            $type = 'simple';
            foreach ($dimensions as $dimension) {
                if ($dimension['open'] == '1W' || $dimension['open'] == '0W') {
                    $type = 'special';
                }
            }

            if ($type == 'simple') {
                if ($dimensions[0]['height'] > $dimensions[2]['height']) {
                    if ($key >= 2) {
                        $height = 70;
                    }

                    if ($key == 2) {
                        echo '<br>';
                    }
                } elseif ($dimensions[0]['height'] < $dimensions[2]['height']) {

                    if ($key < 2) {
                        $height = 70;
                    }

                    if ($key == 2) {
                        echo '<br>';
                    }

                } else {
                    if ($key == 2) {
                        echo '<br>';
                    }
                }
            } else {
                if ($value['open'] == '0W' || $value['open'] == '1W') {
                    $width = 330;
                    $height = 70;
                    if ($key == 3) {
                        echo '<br>';
                    }
                    $newline = $key == 0 ? true : false;
                } else {
                    $newline = false;
                    $height = 200;
                    $width = 100;
                }
            }
        }


//   6 parts
        if ($countParts == 6) {

            $height = 200;

            if ($dimensions[0]['height'] > $dimensions[3]['height']) {
                if ($key >= 3) {
                    $height = 70;
                }

                if ($key == 3) {
                    echo '<br>';
                }
            } elseif ($dimensions[0]['height'] < $dimensions[3]['height']) {

                if ($key < 3) {
                    $height = 70;
                }

                if ($key == 3) {
                    echo '<br>';
                }

            } else {
                if ($key == 3) {
                    echo '<br>';
                }
            }
        }
//   draw windows
        ?>

        <canvas id="canvas-<?= $key ?>" width="<?= $width ?>" height="<?= $height ?>"
                style="border:1px solid #333; background-color: #dde5f3; margin: 5px;">
            Your browser does not support the HTML5 canvas tag.
        </canvas>
    <?= $newline == true ? '<br>' : '' ?>

        <script>
            var elementID = "canvas-<?=$key?>";
            var lv = <?=$sprossenCount['SprossenCount'][$key + 1]['W'] > 0 ? $sprossenCount['SprossenCount'][$key + 1]['W'] : 0?>;
            var lh = <?=$sprossenCount['SprossenCount'][$key + 1]['H'] > 0 ? $sprossenCount['SprossenCount'][$key + 1]['H'] : 0?>;
            Calculator.drawSprossens(elementID, lv, lh);
        </script>
        <?php
    }
}
        ?>

    </div>

