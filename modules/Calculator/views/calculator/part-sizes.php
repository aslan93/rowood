<?php

    use yii\bootstrap\Html;
?>

<div class="top-panel">
    <div class="title-panel">
        CLASIFICAREA
    </div>
    <div class="info">
        Vă rugăm să luați dispunerea dorită a ferestrei înainte
    </div>
</div>
<div class="panel-content-padding">
    <div class="row">
        <div class="col-md-8">
            <div class="calculator-img">
                <?php
                if (isset($variant->imagePath)) {
                    ?>
                    <?= Html::img($variant->imagePath) ?>
                    <?php
                }elseif(isset($type->imagePath)){
                    ?>
                    <?= Html::img($type->imagePath) ?>
                    <?php
                }
                ?>
            </div>
            <?php if (isset($type->BottomDimensions)) { ?>
            <?php for ($i = 0; $i < $type->BottomDimensions; $i++) { ?>
            <div style="display: inline-block; width: <?= round(100/$type->BottomDimensions) - 2 ?>%;" class="size-glass mt10">
                <label>
                    <input onchange="Calculator.setCustomDimensions(<?=$i?>,this,'width')" <?= $i >  $type->BottomDimensions - 2  && $i>1? 'readonly' : '' ?> type="text" placeholder="00" required="required" value="<?= $bottomDimensions[$i] ?>" name="BottomDimensions[<?=$i?>]">
                </label>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <div class="size-glass">
                <?php if (isset($type->RightDimensions)) { ?>
                <?php for ($i = 0; $i < $type->RightDimensions; $i++) { ?>
                <div class="top-glass mt30">
                    <label>
                        <input onchange="Calculator.setCustomDimensions(<?=$i?>,this,'height')" <?= $i >  $type->RightDimensions - 2  && $i>1? 'readonly' : '' ?> type="text" placeholder="00" required="required" value="<?= $rightDimensions[$i] ?>" name="LeftDimensions[<?=$i?>]">
                    </label>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>