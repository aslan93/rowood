<?php

    use yii\bootstrap\Html;

?>

<div class="form-panel-header">
    <div class="title-panel">
        <?= Yii::t('app', 'STICLA DECORATIVA') ?>
    </div>
    <div class="info">
        Vă rugăm să selectați sticla.
    </div>
</div>
<div class="form-panel-content">
    <div>
        Sticla decorativa
    </div>
    <div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setDecoration(this.value)" <?= !$product->Decoration ? 'checked' : '' ?> style="display: block" type="radio" name="Decoration" value="0"> 
                Nu
            </label>
        </div>
        <div class="radio">
            <label>
                <input onchange="Calculator.setDecoration(this.value)" <?= $product->Decoration ? 'checked' : '' ?> style="display: block" type="radio" name="Decoration" value="1"> 
                Da
            </label>
        </div>
    </div>
</div>
<div class="form-panel-content decoration-variants" <?= !$product->Decoration ? 'style="display:none;"' : '' ?>>
    <div>
        <div class="row">
            <?php foreach ($decorations as $decoration) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6" data-mh="109">
                <input <?= $product->DecorationID == $decoration->ID ? 'checked' : '' ?> type="radio" name="DecorationID" value="<?= $decoration->ID ?>" id="decoration-<?= $decoration->ID ?>">
                <label onclick="Calculator.setDecorationID(<?= $decoration->ID ?>)" class="panel-input profil" for="decoration-<?= $decoration->ID ?>">
                    <div class="img">
                        <?= Html::img($decoration->imagePath, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="material">
                        <?= $decoration->lang->Title ?>
                    </div>
                    <div class="text-center add-price-label">
                    <?php if ($pDecorations[$decoration->ID] > 0) { ?>
                        <span class="label label-default">+ <?= $pDecorations[$decoration->ID] ?> &euro;</span>
                    <?php } ?>
                    </div>
                    <button type="button" class="btn-primary select-or-selected">
                        <span class="hidden-after-select">
                            selectare
                        </span>
                    </button>
                </label>
            </div>
            <?php } ?>
        </div>
    </div>
</div>