<?php

    use yii\widgets\Pjax;

?>

<div class="form-panel-header">
    <div class="title-panel">
        PERVAZ VOLUM
    </div>
    <div class="info">
        Vă rugăm să selectați volum.
    </div>
</div>

<?php Pjax::begin([
    'id' => 'windowsill-widths-pjax',
    'timeout' => 5000,
]) ?>
<div class="form-panel-content">
    <ul style="columns: 4; list-style-type: none;">
        <?php foreach ($windowsillWidths as $width) { ?>
        <li>
            <label onclick="Calculator.setWindowsillWidth(<?= $width->Width ?>)">
                <input <?= $width->Width == $product->WindowsillWidth ? 'checked="checked"' : '' ?> style="display: inline-block; position: relative; top: 2px;" type="radio" value="<?= $width->Width ?>" name="WindowsillWidth" /> <?= $width->Width ?> mm 
                <?php if (isset($pWindowsillWidths[$width->ID])) { ?>
                <span class="label label-default">+ <?= $pWindowsillWidths[$width->ID] ?> &euro;</span>
                <?php } ?>
            </label>
        </li>
        <?php } ?>
    </ul>
</div>
<?php Pjax::end() ?>