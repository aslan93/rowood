<?php

namespace app\modules\Calculator\controllers;

use app\modules\Sprossen\models\SprossenMaterial;
use Yii;
use app\components\Controller\FrontController;
use app\modules\Material\models\Material;
use yii\caching\TagDependency;
use app\modules\Profile\models\Profile;
use app\modules\Product\models\Product;
use app\modules\Type\models\Variant;
use app\modules\Wood\models\Wood;
use app\modules\Color\models\Color;
use app\modules\Category\models\Category;
use app\modules\Type\models\Type;
use app\modules\Glass\models\Glass;
use app\modules\TermoEdge\models\TermoEdge;
use app\modules\Soundproofing\models\Soundproofing;
use app\modules\Safety\models\Safety;
use app\modules\Decoration\models\Decoration;
use app\modules\Windowsill\models\WindowsillColor;
use app\modules\Windowsill\models\WindowsillWidth;
use app\modules\Windowsill\models\EndCorner;
use app\modules\Windowsill\models\Screw;
use app\modules\Fitting\models\Fitting;
use app\modules\Windowsill\models\Anschraubdichtung;
use app\modules\InsectScreen\models\InsectScreenColor;
use app\modules\InsectScreen\models\Cloth;
use app\modules\FrameExtension\models\FrameExtension;
use app\modules\Sprossen\models\Sprossen;
use app\modules\Handle\models\Handle;
use app\modules\Windowsill\models\ConnectionProfile;
use app\modules\Jalousie\models\Jalousie;
use app\modules\Calculator\components\CalculatorHelper;
use app\modules\Jalousie\models\OpeningMechanism;
use app\modules\Jalousie\models\RollColor;
use app\modules\Jalousie\models\EndscheneColor;

class CalculatorController extends FrontController
{
    
    public $ProfileWidth = 110;

    public function actionIndex($pid = null)
    {
        if (Yii::$app->request->isAjax)
        {
            $pjaxID = Yii::$app->request->get('productID');
            $product = Product::findOne($pjaxID);
        }
        else
        {
            if ($pid)
            {
                $product = Product::findOne($pid);
//                $product->ID = null;
//                $product->isNewRecord = true;
//                $product->Status = Product::StatusConfigurable;
//                $product->save(false);
            }
            else
            {
                $product = new Product();
                $product->MaterialID = 5;
                $product->ProfileID = 3;
                $product->FrontColorID = 4;
                $product->GlassID = 3;
                $product->FittingID = 1;
                $product->Status = Product::StatusConfigurable;
                $product->ConfiguratorInfo = serialize([]);
                $product->save(false);
            }
        }
        
        // Materials
        $materials = Material::getDb()->cache(function ($db) {
            return Material::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Material::className()]));

        // Profiles
        $selectedMaterial = empty($product->MaterialID) ? reset($materials) : $materials[$product->MaterialID];
        $profiles = Profile::getDb()->cache(function ($db) use ($selectedMaterial) {
            return Profile::find()->with('lang')->where(['MaterialID' => $selectedMaterial->ID])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Profile::className()]));
        
        $selectedProfile = empty($product->ProfileID) ? reset($profiles) : $profiles[$product->ProfileID];
        
        // Woods
        $woods = null;
        $selectedWood = null;
        if ($selectedMaterial->Wood)
        {
            $woods = Wood::getDb()->cache(function ($db) use ($selectedMaterial) {
                return Wood::find()->with('lang')->indexBy('ID')->all();
            }, 3600, new TagDependency(['tags' => Wood::className()]));
            $selectedWood = empty($product->WoodID) ? reset($woods) : $woods[$product->WoodID];
        }
        
        $variants = Variant::getDb()->cache(function ($db) {
            return Variant::find()->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Variant::className()]));
        
        // collors
        $frontColors = Color::getDb()->cache(function ($db) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Front' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        
        $defaultProfile = reset($profiles);
        
        foreach ($frontColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $selectedMaterial->ID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $selectedProfile->ID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            if (isset($woodID))
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($frontColors[$key]);
                    continue;
                }
            }
        }
        
        $backColors = Color::getDb()->cache(function ($db) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Back' => 1])
                    ->orderBy('Position')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        foreach ($backColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $selectedMaterial->ID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $selectedProfile->ID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($backColors[$key]);
                    continue;
                }
            }
        }
        
        $materialsCount = $selectedMaterial->Plastic + $selectedMaterial->Aluminum + $selectedMaterial->Wood;
        
        // Categories and types
        $categories = Category::getDb()->cache(function ($db) {
            return Category::find()->with(['lang', 'types'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => [Category::className(), Type::className()]]));
        
        // Glasses
        $glasses = Glass::getDb()->cache(function ($db) use ($selectedProfile) {
            return Glass::find()->with(['lang'])->where(['<=', 'GlassCount', $selectedProfile->MaxGlassCount])->orderBy('Default DESC')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Glass::className()]));
        
        $termoEdges = TermoEdge::getDb()->cache(function ($db) {
            return TermoEdge::find()->with(['lang'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => TermoEdge::className()]));
        
        $selectedTermoEdge = null;
        if ($product->TermoEdge)
        {
            $selectedTermoEdge = $termoEdges[$product->TermoEdgeID];
        }
        
        // sound proofing
        $soundProofings = Soundproofing::getDb()->cache(function ($db) {
            return Soundproofing::find()->with(['lang'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Soundproofing::className()]));
        
        // safety
        $safeties = Safety::getDb()->cache(function ($db) {
            return Safety::find()->with(['lang'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Safety::className()]));
        
        // safety
        $decorations = Decoration::getDb()->cache(function ($db) {
            return Decoration::find()->with(['lang'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Decoration::className()]));
        
        // Windowsill //
        $windowsillColors = WindowsillColor::getDb()->cache(function ($db) {
            return WindowsillColor::find()->with(['lang'])->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => WindowsillColor::className()]));
        
        $windowsillWidths = WindowsillWidth::getDb()->cache(function ($db) {
            return WindowsillWidth::find()->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => WindowsillWidth::className()]));
        
        $endCorners = EndCorner::getDb()->cache(function ($db) {
            return EndCorner::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => EndCorner::className()]));
        
        $screws = Screw::getDb()->cache(function ($db) {
            return Screw::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Screw::className()]));
        
        $fittings = Fitting::getDb()->cache(function ($db) use ($product) {
            return Fitting::find()->with('lang')
                ->where(['<=', 'MinLength', $product->Height])
                ->where(['<=', 'MinLength', $product->Width])
                ->orderBy('Default DESC')
                ->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Fitting::className()]));
        
        $anschraubdichtungs = Anschraubdichtung::getDb()->cache(function ($db) {
            return Anschraubdichtung::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Anschraubdichtung::className()]));
        
        $insectScreens = InsectScreenColor::getDb()->cache(function ($db) {
            return InsectScreenColor::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => InsectScreenColor::className()]));
        
        $cloths = Cloth::getDb()->cache(function ($db) {
            return Cloth::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Cloth::className()]));
        
        $frameExtensionModels = FrameExtension::getDb()->cache(function ($db) {
            return FrameExtension::find()->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => FrameExtension::className()]));
        
        $frameExtensions = [
            'Left' => [
                0 => '0',
            ],
            'Right' => [
                0 => '0',
            ],
            'Top' => [
                0 => '0',
            ],
            'Bottom' => [
                0 => '0',
            ],
        ];
        foreach ($frameExtensionModels as $model)
        {
            if ($model->Left)
            {
                $frameExtensions['Left'][$model->Size] = $model->Size . ' mm';
            }
            if ($model->Rigth)
            {
                $frameExtensions['Right'][$model->Size] = $model->Size . ' mm';
            }
            if ($model->Top)
            {
                $frameExtensions['Top'][$model->Size] = $model->Size . ' mm';
            }
            if ($model->Bottom)
            {
                $frameExtensions['Bottom'][$model->Size] = $model->Size . ' mm';
            }
        }
        
        $sprossens = SprossenMaterial::getDb()->cache(function ($db) use ($product) {
            return SprossenMaterial::find()->with(['sprossen', 'sprossen.sprossenWidths'])->where(['MaterialID'=>$product->MaterialID])->all();
        }, 3600, new TagDependency(['tags' => Sprossen::className()]));

        $handles = Handle::getDb()->cache(function ($db) {
            return Handle::find()->with(['lang'])->all();
        }, 3600, new TagDependency(['tags' => Handle::className()]));
        
        $connectionProfiles = ConnectionProfile::getDb()->cache(function ($db) {
            return ConnectionProfile::find()->with(['lang'])->all();
        }, 3600, new TagDependency(['tags' => ConnectionProfile::className()]));
        
        $jalousies = Jalousie::getDb()->cache(function ($db) {
            return Jalousie::find()->with(['lang', 'models.lang', 'models.colors.lang', 'models.sizes.lang'])->all();
        }, 3600, new TagDependency(['tags' => Jalousie::className()]));
        
        $openingMechanisms = OpeningMechanism::getDb()->cache(function ($db) {
            return OpeningMechanism::find()->with(['lang', 'mechanismOptions.lang'])->all();
        }, 3600, new TagDependency(['tags' => OpeningMechanism::className()]));
        
        
        $rollColors = RollColor::getDb()->cache(function ($db) use ($product) {
            return RollColor::find()->with(['lang'])->andFilterWhere(['ID' => $product->RollColorID])->all();
        }, 3600, new TagDependency(['tags' => RollColor::className()]));
        
        $endScheneColors = EndscheneColor::getDb()->cache(function ($db) use ($product) {
            return EndscheneColor::find()->with(['lang'])->andFilterWhere(['ID' => $product->EndScheneColorID])->all();
        }, 3600, new TagDependency(['tags' => RollColor::className()]));
        
        
        
        $screwsCount = 0;
        $pSprossenWidth = [];
        $actualSprossens = [];
        // additional price
        if ($product->Width > 0 && $product->Height > 0)
        {
            // glasses
            $pGlasses = [];
            foreach ($glasses as $glass)
            {
                $pPrice = CalculatorHelper::calculateGlassPrice($product, $glass, $this->ProfileWidth);
                
                $defaultPrice = 0;
                if ($glass->Default)
                {
                    $defaultPrice = $pPrice;
                }
                
                $pGlasses[$glass->ID] = number_format($pPrice - $defaultPrice, 2);
            }
            
            // termoedge
            $pTermoEdges = [];
            if ($product->GlassID)
            {
                foreach ($termoEdges as $termoEdge)
                {
                    $pPrice = CalculatorHelper::calculateTermoEdgePrice($product, $glasses[$product->GlassID], $termoEdge, $this->ProfileWidth);
                    $pTermoEdges[$termoEdge->ID] = number_format($pPrice, 2);
                }
            }
            
            // sound proffing
            $pSoundProofings = [];
            foreach ($soundProofings as $soundProofing)
            {
                $pPrice = CalculatorHelper::calculateSoundprofingPrice($product, $glasses[$product->GlassID], $soundProofing, $this->ProfileWidth);
                
                $defaultPrice = 0;
                if ($soundProofing->Default)
                {
                    $defaultPrice = $pPrice;
                }
                
                $pSoundProofings[$soundProofing->ID] = number_format($pPrice - $defaultPrice, 2);
            }
            
            // safeties
            $pSafeties = [];
            foreach ($safeties as $safety)
            {
                $pPrice = CalculatorHelper::calculateSafetyPrice($product, $glasses[$product->GlassID], $safety, $this->ProfileWidth);
                
                $pSafeties[$safety->ID] = number_format($pPrice, 2);
            }
            
            // decoration
            $pDecorations = [];
            foreach ($decorations as $decoration)
            {
                $pPrice = unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $decoration->Price;
                $pDecorations[$decoration->ID] = number_format($pPrice, 2);
            }
            
            // fitting
            $pFittings = [];
            foreach ($fittings as $fitting)
            {
                $pPrice = $variants[$product->VariantID]->Number * $fitting->Price;
                
                $defaultPrice = 0;
                if ($fitting->Default)
                {
                    $defaultPrice = $pPrice;
                }
                
                $pFittings[$fitting->ID] = number_format($pPrice - $defaultPrice, 2);
            }
            
            // handles
            $pHandles = [];
            foreach ($handles as $handle)
            {
                $pPrice = $variants[$product->VariantID]->Number * $handle->Price;
                $pHandles[$handle->ID] = number_format($pPrice, 2);
            }
            
            $pWindowsillWidths = [];
            if ($product->Windowsill && $product->WindowsillLength)
            {
                foreach ($windowsillWidths as $width)
                {
                    $pWindowsillWidths[$width->ID] = number_format(($product->WindowsillLength * $width->Width) / 1000000 * 35.6, 2);
                }
            }
            
            $pEndCorners = [];
            foreach ($endCorners as $corner)
            {
                $pEndCorners[$corner->ID] = number_format($corner->Price * 2, 2);
            }
            
            $pConnectionProfiles = [];
            if ($product->WindowsillLength)
            {
                foreach ($connectionProfiles as $connectionProfile)
                {
                    $pConnectionProfiles[$connectionProfile->ID] = number_format($connectionProfile->Price * $product->WindowsillLength / 1000, 2);
                }
            }
            
            $screwsCount = ceil($product->Width / 300);
            foreach ($sprossens as $sprossen)
            {
                $actualSprossens[] = $sprossen->sprossen;
            }

            if ($product->Sprossen && $product->SprossenID && $product->SprossenCount)
            {
                $config = unserialize($product->ConfiguratorInfo);
                $sprossenCount = unserialize($product->SprossenCount);
                foreach ($sprossens as $sprossen)
                {
                    foreach ($sprossen->sprossen->sprossenWidths as $sWidth)
                    {
                        if (isset($sprossenCount['SprossenCount']))
                        {
                            foreach ($sprossenCount['SprossenCount'] as $key => $part)
                            {
                                $pSprossenWidth[$sWidth->ID] += (( ($part['W'] * $config['Dimensions'][$key-1]['width'])/1000) + ( ($part['H'] * $config['Dimensions'][$key-1]['height'])/1000))*$sWidth->Price;
                            }
                        }

                    }

                }
            }


            $pJalousies = [];

            foreach ($jalousies as $jalousie)
            {
                foreach ($jalousie->models as $model){
                    $pJalousies[$model->ID] = number_format($model->Price * $product->Width * $product->Height / 1000000, 2);
                }
            }
        }
        
        return $this->render('index', [
            'materials' => $materials,
            'variants' => $variants,
            'selectedMaterial' => $selectedMaterial,
            'profiles' => $profiles,
            'selectedProfile' => $selectedProfile,
            'woods' => $woods,
            'selectedWood' => $selectedWood,
            'selectedFrontColorID' => isset($selectedFrontColorID) ? $selectedFrontColorID:'',
            'frontColors' => $frontColors,
            'selectedBackColorID' => isset($selectedBackColorID) ? $selectedBackColorID:'',
            'backColors' => $backColors,
            'materialsCount' => $materialsCount,
            'categories' => $categories,
            'product' => $product,
            'glasses' => $glasses,
            'pGlasses' => isset($pGlasses) ? $pGlasses:'',
            'termoEdges' => $termoEdges,
            'pTermoEdges' => isset($pTermoEdges) ? $pTermoEdges:'',
            'soundProofings' => $soundProofings,
            'pSoundProofings' => isset($pSoundProofings) ? $pSoundProofings:'',
            'safeties' => $safeties,
            'pSafeties' => isset($pSafeties) ? $pSafeties:'',
            'decorations' => $decorations,
            'pDecorations' => isset($pDecorations) ? $pDecorations:'',
            'windowsillColors' => $windowsillColors,
            'windowsillWidths' => $windowsillWidths,
            'pWindowsillWidths' => isset($pWindowsillWidths) ? $pWindowsillWidths:'',
            'endCorners' => $endCorners,
            'pEndCorners' => isset($pEndCorners) ? $pEndCorners:'',
            'screws' => $screws,
            'screwsCount' => $screwsCount,
            'connectionProfiles' => $connectionProfiles,
            'pConnectionProfiles' => isset($pConnectionProfiles) ? $pConnectionProfiles:'',
            'fittings' => $fittings,
            'pFittings' => isset($pFittings) ? $pFittings:'',
            'handles' => $handles,
            'pHandles' => isset($pHandles) ? $pHandles:'',
            'anschraubdichtungs' => $anschraubdichtungs,
            'insectScreens' => $insectScreens,
            'cloths' => $cloths,
            'frameExtensions' => $frameExtensions,
            'sprossens' => $actualSprossens,
            'pSprossenWidth' => $pSprossenWidth,
            'connectionProfiles' => $connectionProfiles,
            'jalousies' => $jalousies,
            'pJalousies' => isset($pJalousies) ? $pJalousies:'',
            'openingMechanisms' => $openingMechanisms,
            'rollColors' => $rollColors,
            'endScheneColors' => $endScheneColors,
        ]);
    }

}
