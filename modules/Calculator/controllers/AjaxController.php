<?php

namespace app\modules\Calculator\controllers;

use app\modules\Fitting\models\Fitting;
use app\modules\Handle\models\Handle;
use app\modules\InsectScreen\models\Cloth;
use app\modules\InsectScreen\models\InsectScreenColor;
use app\modules\Jalousie\models\MechanismOption;
use app\modules\Jalousie\models\Model;
use app\modules\Jalousie\models\ModelColor;
use app\modules\Jalousie\models\OpeningMechanism;
use app\modules\Jalousie\models\RollColor;
use app\modules\Product\models\ProductImages;
use app\modules\Sprossen\models\SprossenWidth;
use app\modules\Windowsill\models\ConnectionProfile;
use app\modules\Windowsill\models\EndCorner;
use app\modules\Windowsill\models\Screw;
use Yii;
use yii\web\Response;
use yii\caching\TagDependency;
use app\modules\Profile\models\Profile;
use app\components\Controller\FrontController;
use app\components\PriceConverter\PriceConverter;
use app\modules\Product\models\Product;
use app\modules\Material\models\Material;
use app\modules\Wood\models\Wood;
use app\modules\Color\models\Color;
use app\modules\Type\models\Type;
use app\modules\Type\models\Variant;
use app\modules\Glass\models\Glass;
use app\modules\TermoEdge\models\TermoEdge;
use app\modules\Opening\models\Opening;
use app\modules\Soundproofing\models\Soundproofing;
use app\modules\Safety\models\Safety;
use app\modules\Decoration\models\Decoration;
use app\modules\Calculator\components\CalculatorHelper;
use app\modules\Windowsill\models\WindowsillColor;

class AjaxController extends FrontController
{
    
    public $ProfileWidth = 110;
    
    public function init()
    {
        parent::init();
        
        if (!Yii::$app->request->isAjax)
        {
            exit();
        }
        
        Yii::$container->set('yii\widgets\Pjax', [
            'timeout' => 5000,
        ]);
        
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionGetProfiles($productID, $materialID)
    {
        $product = $this->updateProduct($productID, 'MaterialID', $materialID);
        
        $material = Material::getDb()->cache(function ($db) use ($materialID) {
            return Material::find()->with('lang')->where(['ID' => $materialID])->one();
        }, 3600, new TagDependency(['tags' => Material::className()]));
        
        $profiles = Profile::getDb()->cache(function ($db) use ($materialID) {
            return Profile::find()->with('lang')->where(['MaterialID' => $materialID])->all();
        }, 3600, new TagDependency(['tags' => Profile::className()]));
        
        $woods = Wood::getDb()->cache(function ($db) {
            return Wood::find()->with('lang')->indexBy('ID')->all();
        }, 3600, new TagDependency(['tags' => Wood::className()]));
        
        $frontColors = Color::getDb()->cache(function ($db) use ($product, $materialID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Front' => 1])
                    ->orderBy('Position')
                    ->indexBy('ID')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
         
        $defaultProfile = reset($profiles);
        
        foreach ($frontColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == !empty($defaultProfile) ? $defaultProfile->ID : -1)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
            }
            
            if (isset($woodID))
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($frontColors[$key]);
                }
            }
        }
        
        $backColors = Color::getDb()->cache(function ($db) use ($product, $materialID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Back' => 1])
                    ->orderBy('Position')
                    ->indexBy('ID')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        foreach ($backColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == !empty($defaultProfile) ? $defaultProfile->ID : -1)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
                continue;
            }
            
            if (isset($woodID))
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($backColors[$key]);
                    continue;
                }
            }
        }

        return [
            'profilesContent' => $this->renderPartial('../calculator/profiles', [
                'profiles' => $profiles,
                'product' => $product,
            ]),
            'woodsContent' => $material->Wood ? $this->renderPartial('../calculator/woods', [
                'woods' => $woods,
                'product' => $product,
            ]) : null,
            'frontColorsContent' => $this->renderPartial('../calculator/front-colors', [
                'frontColors' => $frontColors,
                'product' => $product,
            ]),
            'backColorsContent' => $this->renderPartial('../calculator/back-colors', [
                'backColors' => $backColors,
                'product' => $product,
            ]),
            'total' => PriceConverter::convert(0),
        ];
    }
    
    public function actionSetProfiles($productID, $profileID)
    {
        $product = $this->updateProduct($productID, 'ProfileID', $profileID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWood($productID, $woodID)
    {
        $product = $this->updateProduct($productID, 'WoodID', $woodID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionGetColors($productID, $materialID, $profileID, $woodID = null)
    {
        $product = $this->updateProduct($productID, 'WoodID', $woodID);
        
        $frontColors = Color::getDb()->cache(function ($db) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Front' => 1])
                    ->orderBy('Position')
                    ->indexBy('ID')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
                    
        foreach ($frontColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $profileID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($frontColors[$key]);
                continue;
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($frontColors[$key]);
                    continue;
                }
            }
        }
        
        $backColors = Color::getDb()->cache(function ($db) use ($product, $materialID, $profileID, $woodID) {
            return Color::find()
                    ->with('lang')
                    ->joinWith(['materials', 'profiles', 'woods'])
                    ->where(['Back' => 1])
                    ->orderBy('Position')
                    ->indexBy('ID')
                    ->all();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        
        foreach ($backColors as $key => $color)
        {
            $res = false;
            foreach ($color->materials as $cm)
            {
                if ($cm->MaterialID == $materialID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
            }
            
            $res = false;
            foreach ($color->profiles as $cp)
            {
                if ($cp->ProfileID == $profileID)
                {
                    $res = true;
                }
            }
            
            if (!$res)
            {
                unset($backColors[$key]);
            }
            
            if ($woodID)
            {
                $res = false;
                foreach ($color->woods as $cw)
                {
                    if ($cw->WoodID == $woodID)
                    {
                        $res = true;
                    }
                }
                
                if (!$res)
                {
                    unset($backColors[$key]);
                }
            }
        }

        return [
            'frontColorsContent' => $this->renderPartial('../calculator/front-colors', [
                'frontColors' => $frontColors,
                'product' => $product,
            ]),
            'backColorsContent' => $this->renderPartial('../calculator/back-colors', [
                'backColors' => $backColors,
                'product' => $product,
            ]),
            'total' => PriceConverter::convert(0),
        ];
    }
    
    public function actionSetColor($productID, $colorID, $side)
    {
        $attribute = $side == 'front' ? 'FrontColorID' : 'BackColorID';
        
        $product = $this->updateProduct($productID, $attribute, $colorID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetType($productID, $typeID, $saveSizes = 0)
    {
        $product = $this->updateProduct($productID, 'TypeID', $typeID);
        
        if (!$saveSizes)
        {
            Product::updateAll(['Width' => null, 'Height' => null], ['ID' => $productID]);
        }
        
        $variants = Variant::find()->where(['TypeID' => $product->TypeID])->orderBy('Position')->all();
        
        return [
            'variantsContent' => $this->renderPartial('../calculator/variants', [
                'variants' => $variants,
                'product' => $product,
            ]),
            'product' => $product,
        ];
    }
    
    public function actionSetVariant($productID, $variantID, $saveSizes = 0)
    {
        $product = $this->updateProduct($productID, 'VariantID', $variantID);
        $variant = Variant::findOne($variantID);
        $imageModel = New ProductImages([
            'ProductID' => $productID,
            'Image' => $variant->Image,
            'IsMain'=> 1,
        ]);
        $imageModel->save();
        
        if (!$saveSizes)
        {
            Product::updateAll(['Width' => null, 'Height' => null, 'Price' => null], ['ID' => $productID]);
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionGetSizes($productID,$reset = false)
    {
        $product = Product::findOne($productID);
        $material = Material::findOne($product->MaterialID);
        $profile = Profile::findOne($product->ProfileID);
        $frontColor = Color::findOne($product->FrontColorID);
        $type = Type::findOne($product->TypeID);
        $variant = Variant::findOne($product->VariantID);
        $rightDimensions = [];
        $stack = 0;
        if (isset($type)) {
            for ($i = 0; $i < $type->RightDimensions; $i++) {
                if ($i == $type->RightDimensions - 1) {
                    $rightDimensions[] = round($product->Height - $stack);
                } else {
                    $res = round($product->Height / $type->RightDimensions);
                    $rightDimensions[] = $res;
                    $stack += $res;
                }
            }
        }

        if ($reset=='true') {
            $product = $this->updateProduct($productID, 'RightDimensions', serialize($rightDimensions));
        }else{
            $rightDimensions = unserialize($product->RightDimensions);
        }

        $bottomDimensions = [];
        $stack = 0;
        if (isset($type)) {
            for ($i = 0; $i < $type->BottomDimensions; $i++) {
                if ($i == $type->BottomDimensions - 1) {
                    $bottomDimensions[$i] = round($product->Width - $stack);
                } else {
                    $res = round($product->Width / $type->BottomDimensions);
                    $bottomDimensions[$i] = $res;
                    $stack += $res;
                }
            }
        }

        if ($reset=='true') {
            $product = $this->updateProduct($productID, 'BottomDimensions', serialize($bottomDimensions));
        }else{
            $bottomDimensions = unserialize($product->BottomDimensions);
        }

        return [
            'sizesContent' => $this->renderPartial('../calculator/sizes', [
                'product' => $product,
                'type' => $type,
                'variant' => $variant,
                'material' => $material,
                'profile' => $profile,
                'frontColor' => $frontColor,
                'rightDimensions' => $rightDimensions,
                'bottomDimensions' => $bottomDimensions,
            ]),
            'product' => $product,
        ];
    }

// added by Denis
    public function actionGetDimensions($productID)
    {
        $product = Product::findOne($productID);
        $material = Material::findOne($product->MaterialID);
        $profile = Profile::findOne($product->ProfileID);
        $frontColor = Color::findOne($product->FrontColorID);
        $type = Type::findOne($product->TypeID);
        $variant = Variant::findOne($product->VariantID);

        $rightDimensions = unserialize($product->RightDimensions);
        $bottomDimensions = unserialize($product->BottomDimensions);

        return [
            'sizesContent' => $this->renderPartial('../calculator/sizes', [
                'product' => $product,
                'type' => $type,
                'variant' => $variant,
                'material' => $material,
                'profile' => $profile,
                'frontColor' => $frontColor,
                'rightDimensions' => $rightDimensions,
                'bottomDimensions' => $bottomDimensions,
            ]),
            'product' => $product,
        ];
    }
//added by Denis
    public function actionGetRecalcules($productID)
    {
        $product = Product::findOne($productID);
        $variant = Variant::findOne($product->VariantID);
        $sections = (int)$variant->MaxNumber;
        $height = $product->Height;
        $width = $product->Width;
        $bottomDimensions = unserialize($product->BottomDimensions);
        $rightDimensions = unserialize($product->RightDimensions);
        $positions = explode(",",$variant->OpeningPositions);
        $dimensions = [];

        if ($sections == '6') {
            $nivels = count($rightDimensions);
            $bottomNR = count($bottomDimensions);
            $nivel = 1;
            for ($n=0;$n<=$nivels-1;$n++){

                if ($nivel==1) {
                    $c1 = 0;
                }elseif($nivel == 2){
                    $c1 = $bottomNR;
                }else{
                    $c1 = $bottomNR*2;
                }
                    for ($c = 0; $c <= $bottomNR - 1; $c++) {

                        $dimensions[$c+$c1]['open'] = $positions[$c+$c1];
                        $dimensions[$c+$c1]['height'] = $rightDimensions[$n];
                        $dimensions[$c+$c1]['width'] = $bottomDimensions[$c];
                    }

                $nivel =$nivel+1;
            }
        }

        if($sections == '1'){
            $dimensions[0]['open'] = $positions[0];
            $dimensions[0]['height'] = $rightDimensions[0];
            $dimensions[0]['width'] = $bottomDimensions[0];
        }

        if($sections == '2'){
            $type = '';
            if (count($rightDimensions) == 1){
                $type = 'vertical';
            }else{
                $type = 'horizontal';
            }

            if ($type == 'vertical'){
                foreach ($positions as $key => $value){
                    $dimensions[$key]['open'] = $value;
                    $dimensions[$key]['height'] = $rightDimensions[0];
                    $dimensions[$key]['width'] = $bottomDimensions[$key];
                }
            }else{
                foreach ($positions as $key => $value){
                    $dimensions[$key]['open'] = $value;
                    $dimensions[$key]['height'] = $rightDimensions[$key];
                    $dimensions[$key]['width'] = $bottomDimensions[0];
                }
            }
        }

        if($sections == '3'){
//    verify type
            $type = '';
            if (count($rightDimensions) == 1 && count($bottomDimensions)== 3){
                $type = 'vertical';
            }elseif(count($bottomDimensions)== 1 && count($rightDimensions) == 3){
                $type = 'horizontal';
            }else{
                $type = 'special';
            }
//   *** Verify type

            if ($type == 'vertical'){
                foreach ($positions as $key => $value){
                    $dimensions[$key]['open'] = $value;
                    $dimensions[$key]['height'] = $rightDimensions[0];
                    $dimensions[$key]['width'] = $bottomDimensions[$key];
                }
            }elseif($type == 'horizontal'){
                foreach ($positions as $key => $value){
                    $dimensions[$key]['open'] = $value;
                    $dimensions[$key]['height'] = $rightDimensions[$key];
                    $dimensions[$key]['width'] = $bottomDimensions[0];
                }
            }else{

                $fullHeight = 999;
                $fullWidth = 999;

                foreach ($positions as $key => $value){
                    if($value == '1H' || $value == '0H'){
                        $fullHeight = $key;
                    }

                    if ($value == '0W' || $value == '1W'){
                        $fullWidth = $key;
                    }
                }

                if ($fullWidth == count($rightDimensions)){
                    $fullWidthElement = 'last';

                }elseif($fullWidth == 0){
                    $fullWidthElement = 'first';
                }else{
                    $fullWidthElement='none';
                }

                if ($fullHeight == count($bottomDimensions)){
                    $fullHeightElement = 'last';

                }elseif($fullHeight == 0){
                    $fullHeightElement = 'first';
                }else{
                    $fullHeightElement='none';
                }

                foreach ($positions as $key => $value){
                    if ($key == $fullWidth){
                        $index = $fullWidthElement =='last'? 1:0;
                        $dimensions[$key]['open'] = $value;
                        $dimensions[$key]['height'] = $fullWidthElement=='last' ? $rightDimensions[1]:$rightDimensions[0];
                        $dimensions[$key]['width'] = $width;
                    }elseif ($key == $fullHeight){
                        $dimensions[$key]['open'] = $value;
                        $dimensions[$key]['height'] = $height;
                        $dimensions[$key]['width'] = $fullHeightElement=='last' ? $bottomDimensions[1]:$bottomDimensions[0];
                    }else{
                        if($fullWidthElement != 'none') {
                            $index = $fullWidthElement =='last'? 0:1;
                            $dimensions[$key]['open'] = $value;
                            $dimensions[$key]['height'] = $fullWidthElement == 'last' ? $rightDimensions[0] : $rightDimensions[1];
                            $dimensions[$key]['width'] = $fullWidthElement == 'last' ? $bottomDimensions[$key] : $bottomDimensions[$key-1];
                        }elseif ($fullHeightElement != 'none'){
                            $dimensions[$key]['open'] = $value;
                            $dimensions[$key]['height'] = $fullHeightElement == 'last' ? $rightDimensions[$key] : $rightDimensions[$key-1];
                            $dimensions[$key]['width'] = $fullHeightElement == 'last' ? $bottomDimensions[0] : $bottomDimensions[1];
                        }
                    }
                }
            }
        }

        if ($sections == '4'){
//    verify type
            if (count($rightDimensions) == 2 && count($bottomDimensions)== 2){
                $type = 'simple';
            }else{
                $type = 'special';
            }
//   *** Verify type
            if ($type == 'simple'){
                foreach ($positions as $key => $value){
                    if ($key==0 || $key==1) {
                        $index = 0;
                    }else{
                        $index = 1;
                    }
                        $dimensions[$key]['open'] = $value;
                        $dimensions[$key]['height'] = $rightDimensions[$index];
                        $dimensions[$key]['width'] = $index==0?$bottomDimensions[$key]:$bottomDimensions[$key-2];

                }
            }else{

                $fullHeight = 999;
                $fullWidth = 999;

                foreach ($positions as $key => $value){
                    if($value == '1H' || $value == '0H'){
                        $fullHeight = $key;
                    }

                    if ($value == '0W' || $value == '1W'){
                        $fullWidth = $key;
                    }
                }

                if ($fullWidth == count($rightDimensions)+1){
                    $fullWidthElement = 'last';

                }elseif($fullWidth == 0){
                    $fullWidthElement = 'first';
                }else{
                    $fullWidthElement='none';
                }

                if ($fullHeight == count($bottomDimensions)){
                    $fullHeightElement = 'last';

                }elseif($fullHeight == 0){
                    $fullHeightElement = 'first';
                }else{
                    $fullHeightElement='none';
                }

                foreach ($positions as $key => $value){
                    if ($key == $fullWidth){
                        $index = $fullWidthElement =='last'? 1:0;
                        $dimensions[$key]['open'] = $value;
                        $dimensions[$key]['height'] = $fullWidthElement=='last' ? $rightDimensions[1]:$rightDimensions[0];
                        $dimensions[$key]['width'] = $width;
                    }elseif ($key == $fullHeight){

                        $dimensions[$key]['open'] = $value;
                        $dimensions[$key]['height'] = $height;
                        $dimensions[$key]['width'] = $fullHeightElement=='last' ? $bottomDimensions[0]:$bottomDimensions[1];
                    }else{
                        if($fullWidthElement != 'none') {
                            $index = $fullWidthElement =='last'? 0:1;
                            $dimensions[$key]['open'] = $value;
                            $dimensions[$key]['height'] = $fullWidthElement == 'last' ? $rightDimensions[0] : $rightDimensions[1];
                            $dimensions[$key]['width'] = $fullWidthElement == 'last' ? $bottomDimensions[$key] : $bottomDimensions[$key-1];
                        }elseif ($fullHeightElement != 'none'){
                            $dimensions[$key]['open'] = $value;
                            $dimensions[$key]['height'] = $fullHeightElement == 'last' ? $rightDimensions[$key] : $rightDimensions[$key-1];
                            $dimensions[$key]['width'] = $fullHeightElement == 'last' ? $bottomDimensions[0] : $bottomDimensions[1];
                        }
                    }
                }
            }
        }

        $configInfo = unserialize($product->ConfiguratorInfo);
// set open mechanism
        foreach ($dimensions as $key => $dimension){
            if ($dimension['open']=='1'|| $dimension['open']=='1H'|| $dimension['open']=='1W'){

                if ($dimension['height'] < 400){
                    $dimensions[$key]['mechanism'] = 1;
                }

                if ($dimension['height'] < 400){
                    $dimensions[$key]['mechanism'] = 1;
                }
            }
        }
// ** open mechanism

        $configInfo['Dimensions']= $dimensions;
        $configInfo = serialize($configInfo);
        $product->ConfiguratorInfo =$configInfo;
        $saved = $product->save(false);
        return $saved;
    }

    public function actionSetCustomDimensions($productID, $width = false, $height = false,$position,$fullWidth,$fullHeight)
    {
        $product = Product::findOne($productID);
        if ($width > 0) {
            $width = (int)$width;
            $wth = unserialize($product->BottomDimensions);
            $count = count($wth);
            if ($count == 2){
                foreach($wth as $key => $value){
                    if ($key == $position){

                        $wth[$key] = $width;

                    }else{

                        $wth[$key] = $fullWidth - $width;

                    }
                }
            }elseif ($count >= 3){

                foreach($wth as $key => $value){

                    if ($key == $position){

                        $wth[$key] = $width;

                    }elseif($key == $position + 1){
                        $totalw = 0;
                        foreach ($wth as $k => $v){
                            if ($k != $key) {
                                $totalw += (int)$v;
                            }
                        }
                        $wth[$key] = $fullWidth - $totalw;
                    }
                }
            }

            $wth = serialize($wth);
            $product->BottomDimensions = $wth;
        }

        if ($height > 0) {
            $height = (int)$height;
            $hth = unserialize($product->RightDimensions);
            $count = count($hth);
            if ($count == 2){
                foreach($hth as $key => $value){
                    if ($key == $position){

                        $hth[$key] = $height;

                    }else{

                        $hth[$key] = $fullHeight - $height;

                    }
                }
            }elseif ($count >= 3){

                foreach($hth as $key => $value){

                    if ($key == $position){

                        $hth[$key] = $height;

                    }elseif($key == $position + 1){
                        $totalw = 0;
                        foreach ($hth as $k => $v){
                            if ($k != $key) {
                                $totalw += (int)$v;
                            }
                        }
                        $hth[$key] = $fullHeight - $totalw;
                    }
                }
            }

            $hth = serialize($hth);
            $product->RightDimensions = $hth;
        }

        $save = $product->save(false);
        return $save;
    }

//    *** added by Denis

    public function actionSetDimensions($productID, $width, $height)
    {
        $width = (int)$width;
        $height = (int)$height;
        $product = $this->updateProduct($productID, 'Width', $width);
        $product = $this->updateProduct($productID, 'Height', $height);

        return [
            'product' => $product,
        ];
    }

    public function actionGetPrice($productID)
    {
        $product = Product::findOne($productID);
        $material = Material::findOne($product->MaterialID);

//  added by Denis

        $type = Type::findOne($product->TypeID);
        $profileQuant = 0;
        if (isset($type)) {
            $TopProfileNumber = $type->BottomDimensions + 1;
            $RightProfileNumber = $type->RightDimensions + 1;
            // calculate profile quant
            $profileQuant = CalculatorHelper::calculateProfileQuantity($product,$TopProfileNumber,$RightProfileNumber);
        }
        $glassQuant = CalculatorHelper::calculateGlassQuantity($product);


//** added by Denis

        // init price
        $price = $material->Price;
        
        if (!$product->Width || !$product->Height)
        {
            return [
                'price' => 0,
            ];
        }
        
        // price with dimensions
        $priceWithDimensions = CalculatorHelper::calculateMaterialPrice($price, $profileQuant);
        $totalPrice = $priceWithDimensions;

        // profile
        $profile = Profile::findOne($product->ProfileID);
        if ($profile->Percent > 0)
        {
            $totalPrice += round($totalPrice * ($profile->Percent/100),2);
        }

        // wood
        if ($material->Wood && $product->WoodID)
        {
            $wood = Wood::findOne($product->WoodID);

            if ($wood->Percent > 0)
            {
                $totalPrice += round($priceWithDimensions * $wood->Percent / 100, 2);
            }
        }

        // front color
        $frontColor = Color::findOne($product->FrontColorID);
        if ($frontColor->Percent > 0)
        {
            $totalPrice += round($priceWithDimensions * $frontColor->Percent / 100, 2);
        }

        // back color
        if ($material->isMultiMaterial && $product->BackColorID)
        {
//            $backColor = Color::findOne($product->BackColorID);
//            if ($backColor->Percent > 0)
//            {
//                $totalPrice += round($priceWithDimensions * $backColor->Percent / 100, 2);
//            }
        }

        // mecanisme deschidere
        $variant = Variant::findOne($product->VariantID);
        if ($variant->Number > 0)
        {   $config = unserialize($product->ConfiguratorInfo);
            $sections = $config['Dimensions'];
            $mechanismesPrice = 0;
            if (isset($sections)){
                foreach ($sections as $key => $section){
                    if ($section['open'] == '1'||$section['open'] == '1H'||$section['open'] == '1W') {
                        $opening = Opening::find()->where(['<', 'From', (int)$section['height']])->andWhere(['>=', 'To', (int)$section['height']])->one();
                        $sections[$key]['mechanism'] = round($opening->Price,2);
                        $sections[$key]['mechID'] = $opening->ID;
                        $totalPrice += round($opening->Price,2);
                        $mechanismesPrice += round($opening->Price,2);
                    }else{
                        unset($sections[$key]['mechanism']);
                    }
                }
                $config['Dimensions'] = $sections;
                $config['MecanismeDeschidere'] = 'Standard' .' '.CalculatorHelper::addPriceInfo($mechanismesPrice);
                $product = $this->updateProduct($product->ID,'ConfiguratorInfo',serialize($config));
            }
        }

        // handle
        if ($product->HandleID && $variant->Number>0)
        {
            $handle = Handle::findOne($product->HandleID);
            $totalPrice += round($variant->Number * $handle->Price,2);
        }

        // fitting
        if ($product->FittingID && $variant->Number > 0)
        {   $fitting = Fitting::findOne($product->FittingID);
            $totalPrice += round($variant->Number * $fitting->Price,2);
        }

        // glass
        if ($product->GlassID)
        {
            $glass = Glass::findOne($product->GlassID);
            $totalPrice += round($glassQuant * $glass->Price,2);
        }

        // termo edge
        if ($product->GlassID && $product->TermoEdge && $product->TermoEdgeID)
        {
            $termoEdge = TermoEdge::findOne($product->TermoEdgeID);
            $totalPrice += round($glassQuant * $termoEdge->Price, 2);
        }

        // sound proofing
        if ($product->SoundProofing && $product->SoundProofingID)
        {
            $soundProofing = Soundproofing::findOne($product->SoundProofingID);
            $totalPrice += round( $glassQuant * $soundProofing->Price, 2);
        }

        // safety
        if ($product->Safety && $product->SafetyID)
        {
            $safety = Safety::findOne($product->SafetyID);
            $totalPrice += round($glassQuant * $safety->Price, 2);
        }

        // decoration
        if ($product->DecorationID > 0)
        {
            $decoration = Decoration::findOne($product->DecorationID);

            $totalPrice += round($glassQuant * $decoration->Price, 2);
        }

        if ($product->MontageHoles)
        {
            $totalPrice += 15.04;
        }

// windowsill
        if ($product->Windowsill && $product->WindowsillLength && $product->WindowsillWidth)
        {
            $totalPrice += round(($product->WindowsillLength * $product->WindowsillWidth) / 1000000 * 35.6,2);
        }
        if ($product->Windowsill && $product->WindowsillLength && $product->WindowsillWidth && $product->EndCornerID)
        {   $endCorners = EndCorner::findOne($product->EndCornerID);
            $totalPrice += round($endCorners->Price * 2,2);
        }

        if ($product->Windowsill && $product->Screw && $product->ScrewID)
        {
            $screw = Screw::findOne($product->ScrewID);
            $totalPrice += round($screw->Price * ceil($product->Width / 300),2);
        }

        if ($product->Windowsill && $product->WindowsillLength && $product->WindowsillWidth && $product->ConnectionProfileID)
        {
            $connectionProfile = ConnectionProfile::findOne($product->ConnectionProfileID);
            $totalPrice +=  round($connectionProfile->Price * $product->WindowsillLength / 1000, 2);
        }
// frame extensions
        if ($product->FrameExtensionLeft || $product->FrameExtensionRight || $product->FrameExtensionTop || $product->FrameExtensionBottom){
               $config = unserialize($product->ConfiguratorInfo);
               $totalPrice += isset($config['Prices']['FrameExtensionLeftPrice'])?$config['Prices']['FrameExtensionLeftPrice']:0;
               $totalPrice += isset($config['Prices']['FrameExtensionRightPrice'])?$config['Prices']['FrameExtensionRightPrice']:0;
               $totalPrice += isset($config['Prices']['FrameExtensionTopPrice'])?$config['Prices']['FrameExtensionTopPrice']:0;
               $totalPrice += isset($config['Prices']['FrameExtensionBottomPrice'])?$config['Prices']['FrameExtensionBottomPrice']:0;
        }
// sprossens

        if ($product->SprossenWidth)
        {
            $sprossenWidth = 0;
            $sprossenPrice = 0;
            $config = unserialize($product->ConfiguratorInfo);
            $sprossenCount = unserialize($product->SprossenCount);
            $sprossen = SprossenWidth::findOne($product->SprossenWidth);

            if (isset($sprossenCount['SprossenCount']))
            {
                foreach ($sprossenCount['SprossenCount'] as $key => $part)
                {
                    $sprossenWidth += (( ($part['W'] * $config['Dimensions'][$key-1]['width'])/1000) + ( ($part['H'] * $config['Dimensions'][$key-1]['height'])/1000));
                    $sprossenPrice += round((( ($part['W'] * $config['Dimensions'][$key-1]['width'])/1000) + ( ($part['H'] * $config['Dimensions'][$key-1]['height'])/1000)) * $sprossen->Price,2);
                }
                $config['SprossenWidth'] = 'Width: '.$sprossen->Width .'mm, Length: '.$sprossenWidth .'m '.CalculatorHelper::addPriceInfo($sprossenPrice) ;
                $this->updateProduct($product->ID,'ConfiguratorInfo',serialize($config));
                $totalPrice += $sprossenPrice;
            }
        }

        if ($product->Anschraubdichtung)
        {
            $config['Anschraubdichtung'] = 'Da: '.CalculatorHelper::addPriceInfo(43.72) ;
            $this->updateProduct($product->ID,'ConfiguratorInfo',serialize($config));
            $totalPrice += 43.72;
        }

//    added by Denis
        if($product->InsectScreenID){
            $insectScreen = InsectScreenColor::findOne($product->InsectScreenID);
            $totalPrice += round(CalculatorHelper::calculateInsectScreenColorPrice($product,$insectScreen),2);
        }

        if($product->ClothID){
            $cloth = Cloth::findOne($product->ClothID);
            $totalPrice += round(CalculatorHelper::calculateInsectScreenClothPrice($product,$cloth),2);
        }

// jalousies
        if ($product->JalousieModelID && $product->Jalousies == 1){
            $model = Model::findOne($product->JalousieModelID);
            $modelPrice = number_format($model->Price * $product->Width * $product->Height / 1000000, 2);
            $totalPrice += $modelPrice;
        }

        if($product->Jalousies == 1 && $product->JalousieModelColorID){
            $jalousieModelColor = ModelColor::findOne($product->JalousieModelColorID);
            $totalPrice += CalculatorHelper::calculateJalousieModelColorPrice($product,$jalousieModelColor);
        }

        if($product->OpeningMechanismOptionID && $product->OpeningMechanismID){
            $mechOption = MechanismOption::findOne($product->OpeningMechanismOptionID);
            $totalPrice += $mechOption->Price;
        }

        if($product->RollColorID && isset($product->OpeningMechanismID) && $product->Jalousies == 1 ){
            $rollColorID = RollColor::findOne($product->RollColorID);
            $totalPrice += CalculatorHelper::calculateRolesColorPrice($product,$rollColorID);
        }

//  +TVA
//        $totalPrice += round($totalPrice * 19 / 100, 2);
        $totalPrice = round($totalPrice,2);
        $product = $this->updateProduct($productID, 'Quantity', 1);
        $savedproduct = $this->updateProduct($productID, 'Price', $totalPrice);
        CalculatorHelper::initNewProduct($product->ID, $product->MaterialID, $product->ProfileID, $product->FrontColorID, $product->WoodID, $product->BackColorID);

        return [
            'productInfoContent' => $this->renderPartial('../calculator/product-info', [
                'product' => $savedproduct,
                'material' => $material,
                'profile' => isset($profile)?$profile:0,
                'wood' => isset($wood)?$wood:'',
                'frontColor' => isset($frontColor)?$frontColor:0,
                'glassQuant'=>$glassQuant,
                'profileQuant' => $profileQuant,
                'sections' => isset($sections)?$sections:false,
            ]),
            'product' => $product,
        ];
    }
    
    public function actionGetGlasses($profileID)
    {
        $profile = Profile::findOne($profileID);
        
        $glasses = Glass::find()->with(['lang'])->where(['<=', 'GlassCount', $profile->MaxGlassCount])->indexBy('ID')->all();
        
        return [
            'glassesContent' => $this->renderPartial('../calculator/glasses', [
                'glasses' => $glasses
            ]),
        ];
    }

    public function actionSetGlass($productID, $glassID)
    {
        $product = $this->updateProduct($productID, 'GlassID', $glassID);
        
        $base = Glass::findOne(['Default' => 1]);
        $new = Glass::findOne($glassID);
        $data = unserialize($product->ConfiguratorInfo);
        $data['GlassID'] = $new->lang->Title . ' ' . CalculatorHelper::addPriceInfo((CalculatorHelper::calculateGlassPrice($product, $new, 110)));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetTermoEdge($productID, $termoEdge)
    {
        $termoEdge = (int)$termoEdge;
        
        $product = $this->updateProduct($productID, 'TermoEdge', $termoEdge);
        
        if ($termoEdge == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['TermoEdgeID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetTermoEdgeId($productID, $termoEdgeID)
    {
        $product = $this->updateProduct($productID, 'TermoEdgeID', $termoEdgeID);
        
        $termoEdge = TermoEdge::find()->joinWith('lang')->where(['TermoEdge.ID' => $termoEdgeID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['TermoEdgeID'] = $termoEdge->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateTermoEdgePrice($product, $glass, $termoEdge, 80));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundProofing($productID, $soundProofing)
    {
        $soundProofing = (int)$soundProofing;
        
        $product = $this->updateProduct($productID, 'SoundProofing', $soundProofing);
        
        if ($soundProofing == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['SoundProofingID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundProofingId($productID, $soundProofingID)
    {
        $product = $this->updateProduct($productID, 'SoundProofingID', $soundProofingID);
        $soundProofing = Soundproofing::find()->joinWith('lang')->where(['Soundproofing.ID' => $soundProofingID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['SoundProofingID'] = $soundProofing->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateSoundprofingPrice($product, $glass, $soundProofing, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSafety($productID, $safety)
    {
        $safety = (int)$safety;
        
        $product = $this->updateProduct($productID, 'Safety', $safety);
        
        if ($safety == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['SafetyID']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSafetyId($productID, $safetyID)
    {
        $product = $this->updateProduct($productID, 'SafetyID', $safetyID);
        
        $safety = Safety::find()->joinWith('lang')->where(['Safety.ID' => $safetyID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['SafetyID'] = $safety->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateSafetyPrice($product, $glass, $safety, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetDecoration($productID, $decoration)
    {
        $decoration = (int)$decoration;
        
        $product = $this->updateProduct($productID, 'Decoration', $decoration);
        
        if ($decoration == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['Decoration']);
            unset($data['DecorationID']);
            $product = $this->updateProduct($productID, 'DecorationID', 0);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetDecorationId($productID, $decorationID)
    {
        $product = $this->updateProduct($productID, 'DecorationID', $decorationID);
        $decoration = Decoration::find()->joinWith('lang')->where(['Decoration.ID' => $decorationID])->one();
        $glass = Glass::find()->where(['ID' => $product->GlassID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        $data['DecorationID'] = $decoration->lang->Title . ' ' . CalculatorHelper::addPriceInfo(CalculatorHelper::calculateDecorationPrice($product, $glass, $decoration, 110));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetMontageHoles($productID, $montageHoles)
    {
        $montageHoles = (int)$montageHoles;
        
        $product = $this->updateProduct($productID, 'MontageHoles', $montageHoles);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($montageHoles == 0)
        {
            unset($data['MontageHoles']);
        }
        else
        {
            $data['MontageHoles'] = 'Da <span class="label label-default">+ 15.04 €</span>';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsill($productID, $windowsill)
    {
        $windowsill = (int)$windowsill;
        
        $product = $this->updateProduct($productID, 'Windowsill', $windowsill);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($windowsill == 0)
        {
            unset($data['Windowsill']);
        }
        else
        {
            $data['Windowsill'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillLength($productID, $length)
    {
        $length = (int)$length;
        
        $product = $this->updateProduct($productID, 'WindowsillLength', $length);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['WindowsillLength'] = $length;
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillColor($productID, $colorID)
    {
        $colorID = (int)$colorID;
        
        $product = $this->updateProduct($productID, 'WindowsillColorID', $colorID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $color = WindowsillColor::find()->joinWith('lang')->where(['WindowsillColor.ID' => $colorID])->one();
        $data['WindowsillColorID'] = $color->lang->Title;
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetWindowsillWidth($productID, $width)
    {
        $product = $this->updateProduct($productID, 'WindowsillWidth', $width);
        
        $data = unserialize($product->ConfiguratorInfo);
        $data['WindowsillWidth'] = $product->WindowsillWidth . ' mm <span class="label label-default">+ ' . number_format(($product->WindowsillLength * $width) / 1000000 * 35.6, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetEndCorner($productID, $endCorner)
    {
        $endCorner = (int)$endCorner;
        
        $product = $this->updateProduct($productID, 'EndCorner', $endCorner);
        
        if ($endCorner == 0)
        {
            $data = unserialize($product->ConfiguratorInfo);
            unset($data['EndCorner']);
            $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        }
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetEndCornerId($productID, $endCornerID)
    {
        $endCornerID = (int)$endCornerID;
        
        $product = $this->updateProduct($productID, 'EndCornerID', $endCornerID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $endCorner = \app\modules\Windowsill\models\EndCorner::find()->joinWith('lang')->where(['EndCorner.ID' => $endCornerID])->one();
        $data['EndCornerID'] = $endCorner->lang->Title . ' <span class="label label-default">+ ' . number_format($endCorner->Price * 2, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHConnector($productID, $HConnector)
    {
        $HConnector = (int)$HConnector;
        
        $product = $this->updateProduct($productID, 'HConnector', $HConnector);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($HConnector == 0)
        {
            unset($data['HConnector']);
        }
        else
        {
            $data['HConnector'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetScrew($productID, $screw)
    {
        $screw = (int)$screw;
        $product = $this->updateProduct($productID, 'Screw', $screw);
        
        $data = unserialize($product->ConfiguratorInfo);
        
        if ($screw == 0)
        {
            unset($data['Screw']);
            unset($data['ScrewID']);
        }
        else
        {
            $data['Screw'] = 'Da';
        }
        
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetScrewId($productID, $screwID)
    {
        $screwID = (int)$screwID;
        
        $product = $this->updateProduct($productID, 'ScrewID', $screwID);
        $data = unserialize($product->ConfiguratorInfo);
        $screw = \app\modules\Windowsill\models\Screw::find()->joinWith('lang')->where(['Screw.ID' => $screwID])->one();
        $data['ScrewID'] = $screw->lang->Title . ' '.CalculatorHelper::addPriceInfo(round($screw->Price * ceil($product->Width/300),2));
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));

        return [
            'product' => $product,
        ];
    }
    
    public function actionSetFittingId($productID, $fittingID)
    {
        $fittingID = (int)$fittingID;
        $product = $this->updateProduct($productID, 'FittingID', $fittingID);
        $data = unserialize($product->ConfiguratorInfo);
        $fitting = \app\modules\Fitting\models\Fitting::find()->joinWith('lang')->where(['Fitting.ID' => $fittingID])->one();
        $variant = Variant::find()->where(['ID' => $product->VariantID])->one();
        $data['FittingID'] = $fitting->lang->Title . ' <span class="label label-default">+ ' . number_format($fitting->Price * $variant->Number, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAnschraubdichtung($productID, $anschraubdichtung)
    {
        $anschraubdichtung = (int)$anschraubdichtung;
        $product = $this->updateProduct($productID, 'Anschraubdichtung', $anschraubdichtung);
        $data = unserialize($product->ConfiguratorInfo);
        if ($anschraubdichtung == 0)
        {
            unset($data['Anschraubdichtung']);
        }
        else
        {
            $data['Anschraubdichtung'] = 'Da';
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAnschraubdichtungId($productID, $anschraubdichtungID)
    {
        $product = $this->updateProduct($productID, 'AnschraubdichtungID', $anschraubdichtungID);
        $data = unserialize($product->ConfiguratorInfo);
        $anschraubdichtung = \app\modules\Windowsill\models\Anschraubdichtung::find()->joinWith('lang')->where(['Anschraubdichtung.ID' => $anschraubdichtungID])->one();
        $data['AnschraubdichtungID'] = $anschraubdichtung->lang->Title . ' <span class="label label-default">+ ' . number_format($product->WindowsillWidth * 17.05 / 1000, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSoundLine($productID, $soundLine)
    {
        $soundLine = (int)$soundLine;
        
        $product = $this->updateProduct($productID, 'SoundLine', $soundLine);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($soundLine)
        {
            $data['SoundLine'] = 'Da <span class="label label-default">+ ' . number_format($product->WindowsillWidth * 15.05 / 1000, 2) . ' €</span>';
        }
        else
        {
            unset($data['SoundLine']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetInsectScreen($productID, $insectScreen)
    {
        $insectScreen = (int)$insectScreen;
        
        $product = $this->updateProduct($productID, 'InsectScreen', $insectScreen);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($insectScreen)
        {
            $data['InsectScreen'] = 'Da';
        }
        else
        {
            unset($data['InsectScreen']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetInsectScreenId($productID, $insectScreenID)
    {
        $product = $this->updateProduct($productID, 'InsectScreenID', $insectScreenID);
        
        $data = unserialize($product->ConfiguratorInfo);
        $insectScreen = \app\modules\InsectScreen\models\InsectScreenColor::find()->joinWith('lang')->where(['InsectScreenColor.ID' => $insectScreenID])->one();
        $data['InsectScreenID'] = $insectScreen->lang->Title . ' <span class="label label-default">+ ' . CalculatorHelper::calculateInsectScreenColorPrice($product,$insectScreen) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetClothId($productID, $clothID)
    {
        $product = $this->updateProduct($productID, 'ClothID', $clothID);
        $data = unserialize($product->ConfiguratorInfo);
        $cloth = \app\modules\InsectScreen\models\Cloth::find()->joinWith('lang')->where(['Cloth.ID' => $clothID])->one();
        $data['ClothID'] = $cloth->lang->Title . ' <span class="label label-default">+ ' .CalculatorHelper::calculateInsectScreenClothPrice($product,$cloth). ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetFrameExtension($productID, $part, $size, $addPrice = 0)
    {
        $size = (int)$size;
        
        switch ($part)
        {
            case 'Left':
                $attribute = 'FrameExtensionLeft';
                break;
            case 'Right':
                $attribute = 'FrameExtensionRight';
                break;
            case 'Top':
                $attribute = 'FrameExtensionTop';
                break;
            case 'Bottom':
                $attribute = 'FrameExtensionBottom';
                break;
        }
        
        $product = $this->updateProduct($productID, $attribute, $size);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($addPrice > 0)
        {   $data['Prices'][$attribute.'Price'] = $addPrice;
            $data[$attribute] = $size . ' mm <span class="label label-default">+ ' . $addPrice . ' €</span>';
        }
        else
        {
            unset($data[$attribute]);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossen($productID, $sprossen)
    {
        $sprossen = (int)$sprossen;
        
        $product = $this->updateProduct($productID, 'Sprossen', $sprossen);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($sprossen)
        {
            $data['Sprossen'] = 'Da';
        }
        else
        {
            unset($data['Sprossen']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenId($productID, $sprossenID)
    {
        $product = $this->updateProduct($productID, 'SprossenID', $sprossenID);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenWidth($productID, $sprossenWidth)
    {
        $product = $this->updateProduct($productID, 'SprossenWidth', $sprossenWidth);
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHandle($productID, $handle)
    {
        $handle = (int)$handle;
        
        $product = $this->updateProduct($productID, 'Handle', $handle);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($handle)
        {
            $data['Handle'] = 'Da';
        }
        else
        {
            unset($data['Handle']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetHandleId($productID, $handleID,$handlePrice)
    {
        $product = $this->updateProduct($productID, 'HandleID', $handleID);
        $handle = \app\modules\Handle\models\Handle::find()->with('lang')->where(['ID' => $handleID])->one();
        $data = unserialize($product->ConfiguratorInfo);
        if ($handle)
        {
            $data['HandleID'] = $handle->lang->Title . ' <span class="label label-default">+ ' . number_format($handlePrice, 2) . ' €</span>';
        }
        else
        {
            unset($data['Handle']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetAirRegulator($productID, $airRegulator)
    {
        $airRegulator = (int)$airRegulator;
        
        $product = $this->updateProduct($productID, 'AirRegulator', $airRegulator);
        $variant = Variant::findOne($product->VariantID);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($airRegulator)
        {
            $data['AirRegulator'] = 'Da';// <span class="label label-default">+ ' . number_format(14.20 * $variant->Number, 2) . ' €</span>';
        }
        else
        {
            $data['AirRegulator'] = 'Nu';
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetConnectionProfile($productID, $connectionProfile)
    {
        $connectionProfile = (int)$connectionProfile;
        $product = $this->updateProduct($productID, 'ConnectionProfile', $connectionProfile);
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($connectionProfile)
        {
            $data['ConnectionProfile'] = 'Da ';
        }
        else
        {
            $data['ConnectionProfile'] = 'Nu';
            unset($data['ConnectionProfileID']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetConnectionProfileId($productID, $connectionProfileID, $addPrice = 0)
    {
        $product = $this->updateProduct($productID, 'ConnectionProfileID', $connectionProfileID);
        
        $connectionProfile = \app\modules\Windowsill\models\ConnectionProfile::find()->with('lang')->where(['ID' => $connectionProfileID])->one();
        
        $data = unserialize($product->ConfiguratorInfo);
        if ($addPrice > 0)
        {
            $data['ConnectionProfileID'] = $connectionProfile->lang->Title . ' '.CalculatorHelper::addPriceInfo( number_format($connectionProfile->Price * ($product->WindowsillLength / 1000), 2));
        }
        else
        {
            unset($data['ConnectionProfileID']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousies($productID, $jalousies)
    {
        $jalousies = (int)$jalousies;
        $product = $this->updateProduct($productID, 'Jalousies', $jalousies);
        $data = unserialize($product->ConfiguratorInfo);
        if (isset($jalousies) && $jalousies == 1)
        {
            $data['Jalousies'] = 'Da';
        }
        else
        {
            $data['Jalousies'] = 'Nu';
            unset($data['JalousieID']);
            unset($data['JalousieModelID']);
        }
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieId($productID, $jalousieID)
    {
        $product = $this->updateProduct($productID, 'JalousieID', $jalousieID);
        $data = unserialize($product->ConfiguratorInfo);
        $jalousie = \app\modules\Jalousie\models\Jalousie::find()->with('lang')->where(['Jalousie.ID' => $jalousieID])->one();
        $data['JalousieID'] = $jalousie->lang->Title;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelId($productID, $jalousieModelID, $addPrice = 0)
    {
        $product = $this->updateProduct($productID, 'JalousieModelID', $jalousieModelID);
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModel = \app\modules\Jalousie\models\Model::find()->with('lang')->where(['Model.ID' => $jalousieModelID])->one();
        $data['JalousieModelID'] = $jalousieModel->lang->Title . ' <span class="label label-default">+ ' . number_format($jalousieModel->Price * $product->Width * $product->Height / 1000000, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelSizeId($productID, $jalousieModelSizeID)
    {
        $product = $this->updateProduct($productID, 'JalousieModelSizeID', $jalousieModelSizeID);
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModelSize = \app\modules\Jalousie\models\ModelSize::find()->with('lang')->where(['ModelSize.ID' => $jalousieModelSizeID])->one();
        $data['JalousieModelSizeID'] = $jalousieModelSize->lang->Title;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieModelColorId($productID, $jalousieModelColorID)
    {
        $product = $this->updateProduct($productID, 'JalousieModelColorID', $jalousieModelColorID);
        $data = unserialize($product->ConfiguratorInfo);
        $jalousieModelColor = \app\modules\Jalousie\models\ModelColor::find()->with('lang')->where(['ModelColor.ID' => $jalousieModelColorID])->one();
        $data['JalousieModelColorID'] = $jalousieModelColor->lang->Title  . ' <span class="label label-default">+ ' .CalculatorHelper::calculateJalousieModelColorPrice($product,$jalousieModelColor).' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetJalousieDimensions($productID, $width, $height)
    {
        $width = (int)$width;
        $height = (int)$height;
        
        $product = $this->updateProduct($productID, 'JalousieWidth', $width);
        $product = $this->updateProduct($productID, 'JalousieHeight', $height);
        $data = unserialize($product->ConfiguratorInfo);
        $data['JalousieWidth'] = $width . ' mm';
        $data['JalousieHeight'] = $height . ' mm';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetSprossenCount()
    {
        $productID = \Yii::$app->request->post('productID');
        $data = \Yii::$app->request->post('data');
        parse_str($data, $data);
        $product = $this->updateProduct($productID, 'SprossenCount', serialize($data));
        return [
            'product' => $product,
            'data' => $data,
        ];
    }
    
    public function actionSetOpeningMechanismId($productID, $OpeningMechanismID)
    {
        $product = $this->updateProduct($productID, 'OpeningMechanismID', $OpeningMechanismID);
        $data = unserialize($product->ConfiguratorInfo);
        $OpeningMechanism = \app\modules\Jalousie\models\OpeningMechanism::find()->with('lang')->where(['OpeningMechanism.ID' => $OpeningMechanismID])->one();
        $data['OpeningMechanismID'] = $OpeningMechanism->lang->Title;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        return [
            'product' => $product,
        ];
    }
    
    public function actionSetOpeningMechanismOptionId($productID, $OpeningMechanismOptionID)
    {
        $product = $this->updateProduct($productID, 'OpeningMechanismOptionID', $OpeningMechanismOptionID);
        $data = unserialize($product->ConfiguratorInfo);
        $OpeningMechanismOption = \app\modules\Jalousie\models\MechanismOption::find()->with('lang')->where(['MechanismOption.ID' => $OpeningMechanismOptionID])->one();
        $data['OpeningMechanismOptionID'] = $OpeningMechanismOption->lang->Title   . ' <span class="label label-default">+ ' . number_format($OpeningMechanismOption->Price, 2) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));
        
        return [
            'product' => $product,
        ];
    }

    public function actionSetOpeningMechanismPosition($productID,$position){

        $product = $this->updateProduct($productID, 'OpeningMechanismPosition', $position);
        $data = unserialize($product->ConfiguratorInfo);
        $data['OpeningMechanismPosition'] = $position;
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));

        return[
            'product' => $product,
        ];
    }

    public function actionSetRollColorId($productID, $rollColorID)
    {
        $product = $this->updateProduct($productID, 'RollColorID', $rollColorID);
        $data = unserialize($product->ConfiguratorInfo);
        $rollColor = RollColor::find()->with('lang')->where(['ID' => $rollColorID])->one();
        $data['RollColorID'] = $rollColor->lang->Title   . ' <span class="label label-default">+ ' .CalculatorHelper::calculateRolesColorPrice($product,$rollColor) . ' €</span>';
        $product = $this->updateProduct($productID, 'ConfiguratorInfo', serialize($data));

        return [
            'product' => $product,
        ];
    }
    protected function updateProduct($productID, $attribute, $value)
    {
        $product = Product::findOne($productID);
        $product->$attribute = $value;
        $product->save(false);
        return $product;
    }


}
