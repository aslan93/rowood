<?php

namespace app\modules\Calculator\components;

use app\modules\Glass\models\Glass;
use app\modules\Product\models\Product;
use app\modules\Material\models\Material;
use app\modules\Product\models\ProductImages;
use app\modules\Profile\models\Profile;
use app\modules\Color\models\Color;
use app\modules\Type\models\Type;
use app\modules\Wood\models\Wood;
use yii\caching\TagDependency;

class CalculatorHelper
{
    
    public static function initNewProduct($productID, $materialID, $profileID, $frontColorID, $woodID = null, $backColorID = null)
    {
        $product = Product::findOne($productID);
        $configInfo = unserialize($product->ConfiguratorInfo);
        $configInfo = (array)$configInfo;
        if ($product->GlassID){
            $glass = Glass::find()->where(['ID'=>$product->GlassID])->with('lang')->one();
            $configInfo['GlassID'] = $glass->lang->Title . ' ' . self::addPriceInfo($configInfo['GlassQuantity'] * $glass->Price);
        }
        $material = Material::getDb()->cache(function ($db) use ($materialID) {
            return Material::find()->with('lang')->where(['ID' => $materialID])->one();
        }, 3600, new TagDependency(['tags' => Material::className()]));
        
        $materialPrice = self::calculateMaterialPrice($material->Price, isset($configInfo['ProfileQuantity'])?$configInfo['ProfileQuantity']:0);
        $configInfo['MaterialID'] = $material->lang->Title . ' ' . self::addPriceInfo($materialPrice);
        $profile = Profile::getDb()->cache(function ($db) use ($profileID) {
            return Profile::find()->with('lang')->where(['ID' => $profileID])->one();
        }, 3600, new TagDependency(['tags' => Profile::className()]));
        $configInfo['ProfileID'] = $profile->lang->Title . ' ' . self::addPriceInfo(self::calculateProfilePrice($materialPrice, $profile->Percent));
        
        $frontColor = Color::getDb()->cache(function ($db) use ($frontColorID) {
            return Color::find()->with('lang')->where(['ID' => $frontColorID])->one();
        }, 3600, new TagDependency(['tags' => Color::className()]));
        $configInfo['FrontColorID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateColorPrice($materialPrice, $frontColor->Percent));
        
        if ($material->isMultiMaterial && $backColorID)
        {
            $backColor = Color::getDb()->cache(function ($db) use ($backColorID) {
                return Color::find()->with('lang')->where(['ID' => $backColorID])->one();
            }, 3600, new TagDependency(['tags' => Color::className()]));
            $configInfo['BackColorID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateColorPrice($materialPrice, $backColor->Percent));
        }
        else
        {
            unset($configInfo['BackColorID']);
        }
        
        if ($material->Wood == 1 && $woodID)
        {
            $wood = Wood::getDb()->cache(function ($db) use ($woodID) {
                return Wood::find()->with('lang')->where(['ID' => $woodID])->one();
            }, 3600, new TagDependency(['tags' => Wood::className()]));
            $configInfo['WoodID'] = $material->lang->Title . ' ' . self::addPriceInfo(self::calculateWoodPrice($materialPrice, $wood->Percent));
        }
        else
        {
            unset($configInfo['WoodID']);
        }

        $configInfo['Width'] = $product->Width . ' mm';
        $configInfo['Height'] = $product->Height . ' mm';
        $product->ConfiguratorInfo = serialize($configInfo);
        $product->save(false);
    }
    
    public static function calculateMaterialPrice($materialPrice, $profileQuant)
    {
        return round($materialPrice * $profileQuant,2);
    }
    
    public static function calculateProfilePrice($materialPriceWithDimentions, $profilePercent)
    {
        return round($materialPriceWithDimentions * $profilePercent / 100, 2);
    }
    
    public static function calculateColorPrice($materialPriceWithDimentions, $colorPercent)
    {
        return round($materialPriceWithDimentions * $colorPercent / 100, 2);
    }
    
    public static function calculateWoodPrice($materialPriceWithDimentions, $woodPercent)
    {
        return round($materialPriceWithDimentions * $woodPercent / 100, 2);
    }
    
    public static function calculateGlassPrice($product, $glass, $profileWidth)
    {
        return round(unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $glass->Price, 2);
//          return unserialize($product->ConfiguratorInfo)['GlassQuantity']*$glass->Price;
    }
//     added by Denis
    public static function calculateGlassQuantity($product)
    {   $configInfo = unserialize($product->ConfiguratorInfo);
        if (isset($configInfo['Dimensions'])) {
            $windowsSections = $configInfo['Dimensions'];
        }
        $parts = [];
        if (isset($windowsSections)) {
            foreach ($windowsSections as $k => $section) {
                $parts[$k] = $section;
                $part = $section;
                if ($part['open'] == '1' || $part['open'] == '1W' || $part['open'] == '1H') {
                    $profile = 150;
                } else {
                    $profile = 80;
                }
                $parts[$k]['glassQuant'] = (($part['height'] - $profile) * ($part['width'] - $profile)) / 1000000;
            }
        }

        $m2 = 0;
        foreach ($parts as $part){
            $m2 += $part['glassQuant'];
        }
        $configInfo['GlassQuantity'] = round($m2,2);
        $configInfo['Dimensions'] = $parts;
        $product->ConfiguratorInfo = serialize($configInfo);
        $product->save(false);
        return round($m2,2);
    }
    public static function calculateProfileQuantity($product,$TopProfileNumber = 2,$RightProfileNumber = 2)
    {   $config = unserialize($product->ConfiguratorInfo);
        $windowProfile = 0;
        $profileVCut = 0;
        $profileHCut = 0;

        if (isset($config['Dimensions'])) {
            foreach ($config['Dimensions'] as $key => $dimension) {
                if ($dimension['open'] == '1' || $dimension['open'] == '1W' || $dimension['open'] == '1H') {
                    $windowProfile += ((($dimension['width'] - 80) + ($dimension['height'] - 80)) / 1000) * 2;
                }
//  profile for cut
                if ($dimension['open'] == '1W' || $dimension['open'] == '0W') {
                    $cutProfiles = $TopProfileNumber - 2;
                    $profileHCut += $dimension['height'] * $cutProfiles;
                }

                if ($dimension['open'] == '1H' || $dimension['open'] == '0H') {
                    $cutProfiles = $RightProfileNumber - 2;
                    $profileVCut += $dimension['width'] * $cutProfiles;
                }
            }
        }
        $width = ($product->Width * $RightProfileNumber) - $profileHCut;
        $height = ($product->Height * $TopProfileNumber) - $profileVCut;
        $ml = round(($width + $height)/1000,2);
        $config['ProfileQuantity']=round($windowProfile+$ml,2);
        $config['ProfileQuantity1']=$ml;
        $config['WindowProfileQuantity']=round($windowProfile,2);
        $product->ConfiguratorInfo = serialize($config);
        $product->save(false);
        return $config['ProfileQuantity'];
    }
//***  added by Denis
    public static function calculateTermoEdgePrice($product, $glass, $termoEdge, $profileWidth)
    {   //modified by Denis
        return round(unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $termoEdge->Price, 2);
    }
    
    public static function calculateSoundprofingPrice($product, $glass, $soundProofing, $profileWidth)
    {
        return round(unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $soundProofing->Price, 2);
    }

    public static function calculateSafetyPrice($product, $glass, $safety, $profileWidth)
    {
        return round(unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $safety->Price, 2);
    }
    
    public static function calculateDecorationPrice($product, $glass, $decoration, $profileWidth)
    {
        return round(unserialize($product->ConfiguratorInfo)['GlassQuantity'] * $decoration->Price, 2);
    }

    public static function calculateInsectScreenColorPrice($product,$insectScreen)
    {   $windowsSections = unserialize($product->ConfiguratorInfo)['Dimensions'];
        $parts = [];
        $perim = 0;
        if (isset($windowsSections)) {
            foreach ($windowsSections as $k => $section) {
                $parts[$k] = $section;
                $part = $section;
                if ($part['open'] == '1' || $part['open'] == '1W' || $part['open'] == '1H') {
                    $perim += (($part['height']+$part['width'])*2)/1000;
                } else {

                }

            }
        }
        return round( $perim * $insectScreen->Price, 2);
    }

    public static function calculateInsectScreenClothPrice($product,$cloth)
    {   $windowsSections = unserialize($product->ConfiguratorInfo)['Dimensions'];
        $parts = [];
        $perim = 0;
        if (isset($windowsSections)) {
            foreach ($windowsSections as $k => $section) {
                $parts[$k] = $section;
                $part = $section;
                if ($part['open'] == '1' || $part['open'] == '1W' || $part['open'] == '1H') {
                    $perim += ($part['height']*$part['width'])/1000000;
                } else {

                }
            }
        }
        return round( $perim * $cloth->Price, 2);
    }

    public static function calculateJalousieModelColorPrice($product,$jalousieColor)
    {
        return number_format($jalousieColor->Price * (($product->Width * $product->Height) / 1000000), 2);
    }

    public static function calculateRolesColorPrice($product,$roleColor)
    {
        return number_format($roleColor->Price * ($product->Width / 1000), 2);
    }
    
    public static function addPriceInfo($price)
    {
        return $price > 0 ? '<span class="label label-default">+ ' . number_format($price, 2) . ' &euro;</span>' : '';
    }
    
}
