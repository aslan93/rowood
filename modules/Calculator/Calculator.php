<?php

namespace app\modules\Calculator;

/**
 * calculator module definition class
 */
class Calculator extends \app\components\Module\FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Calculator\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
