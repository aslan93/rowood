<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Glass\models\Glass */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
        <?= Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('app', 'Date generale'),
                    'content' => $this->render('_main_form', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                    'active' => true,
                ],
                [
                    'label' => Yii::t('app', 'Descriere'),
                    'content' => $this->render('_languages_form', [
                        'form' => $form,
                        'model' => $model,
                    ]),
                ]
            ]
        ]); ?>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

