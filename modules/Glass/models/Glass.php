<?php

namespace app\modules\Glass\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Glass".
 *
 * @property integer $ID
 * @property integer $GlassCount
 * @property string $Price
 * @property string $Image
 *
 * @property GlassLang $lang
 * @property GlassLang[] $langs
 */
class Glass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Glass';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['GlassCount', 'Price', 'Image'], 'required'],
            [['GlassCount'], 'integer'],
            [['Price'], 'number'],
            [['Default'], 'default', 'value' => 0],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Default' => Yii::t('app', 'Default'),
            'GlassCount' => Yii::t('app', 'Glass Count'),
            'Price' => Yii::t('app', 'Price'),
            'Image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(GlassLang::className(), ['GlassID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(GlassLang::className(), ['GlassID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new GlassLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/glass/' . $this->Image);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->GlassID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
