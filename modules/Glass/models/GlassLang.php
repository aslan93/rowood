<?php

namespace app\modules\Glass\models;

use Yii;

/**
 * This is the model class for table "GlassLang".
 *
 * @property integer $ID
 * @property integer $GlassID
 * @property string $LangID
 * @property string $Title
 * @property string $Subtitle
 * @property string $Text
 *
 * @property Glass $glass
 */
class GlassLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GlassLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['GlassID', 'Title', 'Subtitle', 'LangID'], 'required'],
            [['GlassID'], 'integer'],
            [['Text'], 'string'],
            [['Title', 'Subtitle'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['GlassID'], 'exist', 'skipOnError' => true, 'targetClass' => Glass::className(), 'targetAttribute' => ['GlassID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'GlassID' => Yii::t('app', 'Glass ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Subtitle' => Yii::t('app', 'Subtitle'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGlass()
    {
        return $this->hasOne(Glass::className(), ['ID' => 'GlassID']);
    }
}
