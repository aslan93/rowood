<?php

namespace app\modules\Glass\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Glass\models\Glass;

/**
 * GlassSearch represents the model behind the search form about `app\modules\Glass\models\Glass`.
 */
class GlassSearch extends Glass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'GlassCount'], 'integer'],
            [['Price'], 'number'],
            [['Image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Glass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'GlassCount' => $this->GlassCount,
            'Price' => $this->Price,
        ]);

        $query->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
