<?php

namespace app\modules\Payment;

use app\components\Module\FrontModule;
/**
 * Payment module definition class
 */
class Payment extends FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Payment\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
