<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\TermoEdge\models\TermoEdge */

$this->title = Yii::t('app', 'Create Termo Edge');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Termo Edges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="termo-edge-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
