<?php

namespace app\modules\TermoEdge;

/**
 * termo-edge module definition class
 */
class TermoEdge extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\TermoEdge\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
