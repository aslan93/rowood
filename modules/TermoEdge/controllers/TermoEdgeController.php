<?php

namespace app\modules\TermoEdge\controllers;

use Yii;
use app\modules\TermoEdge\models\TermoEdge;
use app\modules\TermoEdge\models\TermoEdgeSearch;
use app\modules\Admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TermoEdgeController implements the CRUD actions for TermoEdge model.
 */
class TermoEdgeController extends AdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TermoEdge models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TermoEdgeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TermoEdge model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TermoEdge model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TermoEdge();

        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);
            
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TermoEdge model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);
            
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TermoEdge model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TermoEdge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TermoEdge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TermoEdge::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function saveImage($model)
    {
        $uploaded = UploadedFile::getInstanceByName('Image');
        
        if ($uploaded)
        {
            $fileName = md5(microtime(true)) . '.' . $uploaded->extension;
            if ($uploaded->saveAs(Yii::getAlias('@webroot/uploads/profile/' . $fileName)))
            {
                return $fileName;
            }
        }
        
        return $model->Image;
    }

}
