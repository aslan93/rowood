<?php

namespace app\modules\TermoEdge\models;

use Yii;
use yii\caching\TagDependency;
use yii\base\Model;
use yii\helpers\Url;

/**
 * This is the model class for table "TermoEdge".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 * @property string $Default
 *
 * @property TermoEdgeLang[] $termoEdgeLangs
 */
class TermoEdge extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TermoEdge';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'Price'], 'required'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
            'Default' => Yii::t('app', 'Default'),
        ];
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/profile/' . $this->Image);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(TermoEdgeLang::className(), ['TermoEdgeID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(TermoEdgeLang::className(), ['TermoEdgeID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new TermoEdgeLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->TermoEdgeID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
}
