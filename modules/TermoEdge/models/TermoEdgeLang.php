<?php

namespace app\modules\TermoEdge\models;

use Yii;

/**
 * This is the model class for table "TermoEdgeLang".
 *
 * @property integer $ID
 * @property integer $TermoEdgeID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property TermoEdge $termoEdge
 */
class TermoEdgeLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TermoEdgeLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TermoEdgeID', 'LangID', 'Title', 'Text'], 'required'],
            [['TermoEdgeID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['TermoEdgeID'], 'exist', 'skipOnError' => true, 'targetClass' => TermoEdge::className(), 'targetAttribute' => ['TermoEdgeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TermoEdgeID' => Yii::t('app', 'Termo Edge ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTermoEdge()
    {
        return $this->hasOne(TermoEdge::className(), ['ID' => 'TermoEdgeID']);
    }
}
