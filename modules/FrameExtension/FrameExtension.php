<?php

namespace app\modules\FrameExtension;

/**
 * frame-extension module definition class
 */
class FrameExtension extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\FrameExtension\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
