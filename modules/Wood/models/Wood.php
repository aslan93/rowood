<?php

namespace app\modules\Wood\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use app\modules\Wood\models\WoodLang;
use app\modules\Color\models\ColorWood;

/**
 * This is the model class for table "Wood".
 *
 * @property integer $ID
 * @property string $Percent
 *
 * @property ColorWood[] $colorWoods
 * @property WoodLang $lang
 * @property WoodLang[] $langs
 */
class Wood extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Wood';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Percent'], 'number'],
            [['Default', 'Percent'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Percent' => Yii::t('app', 'Percent'),
            'Default' => Yii::t('app', 'Default wood'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColorWoods()
    {
        return $this->hasMany(ColorWood::className(), ['WoodID' => 'ID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(WoodLang::className(), ['WoodID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(WoodLang::className(), ['WoodID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new WoodLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }
        else
        {
            $hasDefault = self::find()->where(['Default' => 1])->andWhere(['<>', 'ID', $this->ID])->count();
            
            if (!$hasDefault)
            {
                $this->addError('Default', Yii::t('app', 'Although one wood type should be installed as default'));
                return false;
            }
        }
        
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->WoodID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    
}
