<?php

namespace app\modules\Wood\models;

use Yii;
use app\modules\Wood\models\Wood;

/**
 * This is the model class for table "WoodLang".
 *
 * @property integer $ID
 * @property integer $WoodID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Wood $wood
 */
class WoodLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WoodLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WoodID', 'LangID', 'Title', 'Text'], 'required'],
            [['WoodID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['WoodID'], 'exist', 'skipOnError' => true, 'targetClass' => Wood::className(), 'targetAttribute' => ['WoodID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'WoodID' => Yii::t('app', 'Wood ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWood()
    {
        return $this->hasOne(Wood::className(), ['ID' => 'WoodID']);
    }
}
