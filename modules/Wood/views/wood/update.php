<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Wood\models\Wood */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Wood',
]) . $model->lang->Title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Woods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="wood-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
