<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Wood\models\Wood */

$this->title = Yii::t('app', 'Create Wood');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Woods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wood-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
