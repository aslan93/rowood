<?php

namespace app\modules\Discount\models;

use Yii;

/**
 * This is the model class for table "Discount".
 *
 * @property integer $ID
 * @property integer $Price
 * @property double $Percent
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'Percent'], 'required'],
            [['Price'], 'integer'],
            [['Percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Price' => 'Price',
            'Percent' => 'Percent',
        ];
    }
}
