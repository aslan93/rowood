<?php

namespace app\modules\Discount\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Discount\models\Discount;

/**
 * DiscountSearch represents the model behind the search form about `app\modules\Discount\models\Discount`.
 */
class DiscountSearch extends Discount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Price'], 'integer'],
            [['Percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
            'Percent' => $this->Percent,
        ]);

        return $dataProvider;
    }
}
