<?php

namespace app\modules\Discount;

/**
 * Discount module definition class
 */
class Discount extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Discount\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
