<?php

namespace app\modules\Sprossen\models;

use Yii;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "SprossenWidth".
 *
 * @property integer $ID
 * @property integer $SprossenID
 * @property integer $Image
 * @property integer $Number
 * @property integer $MinWidth
 * @property integer $MaxWidth
 * @property integer $MinHeight
 * @property integer $MaxHeight
 * @property integer $Position
 *
 * @property Sprossen $type
 */
class SprossenWidth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SprossenWidth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SprossenID'], 'required'],
            [['SprossenID', 'Width'], 'integer', 'min' => 0],
            [['Price'], 'number', 'min' => 0],
            [['SprossenID'], 'exist', 'skipOnError' => true, 'targetClass' => Sprossen::className(), 'targetAttribute' => ['SprossenID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SprossenID' => Yii::t('app', 'Sprossen ID'),
            'Width' => Yii::t('app', 'Width'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprossen()
    {
        return $this->hasOne(Sprossen::className(), ['ID' => 'SprossenID']);
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/variant/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, [self::className(), Sprossen::className()]);
    }
    
}
