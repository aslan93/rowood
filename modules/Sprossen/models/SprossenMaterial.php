<?php

namespace app\modules\Sprossen\models;

use Yii;
use app\modules\Material\models\Material;

/**
 * This is the model class for table "SprossenMaterial".
 *
 * @property integer $ID
 * @property integer $SprossenID
 * @property integer $MaterialID
 *
 * @property Sprossen $sprossen
 * @property Material $material
 */
class SprossenMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SprossenMaterial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['SprossenID', 'MaterialID'], 'required'],
            [['SprossenID', 'MaterialID'], 'integer'],
            [['SprossenID'], 'exist', 'skipOnError' => true, 'targetClass' => Sprossen::className(), 'targetAttribute' => ['SprossenID' => 'ID']],
            [['MaterialID'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['MaterialID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SprossenID' => Yii::t('app', 'Sprossen ID'),
            'MaterialID' => Yii::t('app', 'Material ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprossen()
    {
        return $this->hasOne(Sprossen::className(), ['ID' => 'SprossenID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['ID' => 'MaterialID']);
    }
}
