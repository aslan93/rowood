<?php

namespace app\modules\Sprossen\models;

use Yii;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Sprossen".
 *
 * @property integer $ID
 * @property string $Image
 *

 */
class Sprossen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $MaterialID = false;
    public static function tableName()
    {
        return 'Sprossen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprossenMaterials()
    {
        return $this->hasOne(SprossenMaterial::className(), ['SprossenID' => 'ID'])->with('material');
    }
    
    public function getSprossenWidths()
    {
        return $this->hasMany(SprossenWidth::className(), ['SprossenID' => 'ID']);
    }
    
    public function getLang()
    {
        //return $this->hasMany(SprossenLang::className(), ['SprossenID' => 'ID']);
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/sprossen/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
    }
    
}
