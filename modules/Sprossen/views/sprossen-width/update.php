<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Variant */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sprossen',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprossen'), 'url' => ['sprossen/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprossen'), 'url' => ['sprossen/update', 'id' => Yii::$app->request->get('sprossenID'), '#' => 'variants']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="variant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
