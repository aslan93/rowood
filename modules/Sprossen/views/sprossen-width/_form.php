<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$initialPreview = [];
if ($model->imagePath)
{
    $initialPreview[] = Html::img($model->imagePath, ['width' => 80]);
}

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Variant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="variant-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= Html::activeHiddenInput($model, 'SprossenID', [
                'value' => $model->SprossenID ? $model->SprossenID : Yii::$app->request->get('sprossenID'),
            ]) ?>

            <?= $form->field($model, 'Width')->textInput() ?>
            
            <?= $form->field($model, 'Price')->textInput() ?>
        </div>
        <div class="col-md-6">

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
