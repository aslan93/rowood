<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Variant */

$this->title = Yii::t('app', 'Create Width');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprossens'), 'url' => ['sprossen/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sprossen'), 'url' => ['sprossen/update', 'id' => Yii::$app->request->get('sprossenID'), '#' => 'sprossen-widths']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="variant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sprossenID' => $sprossenID,
    ]) ?>

</div>
