<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Type\models\VariantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<br />
<div class="sprossen-width-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'Width',
                'Price',

                [
                    'class' => 'app\components\GridView\ActionColumn',
                    'header' => Html::a(Yii::t('app', 'Create Width'), ['sprossen-width/create', 'sprossenID' => $model->ID], ['class' => 'btn btn-success', 'data-pjax' => 0]),
                    'buttons' => [
                        'update' => function($url, $model, $key)
                        {
                            return Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['sprossen-width/update', 'id' => $model->ID, 'sprossenID' => $model->sprossen->ID], ['data-pjax' => 0]);
                        },
                        'delete' => function($url, $model, $key)
                        {
                            return Html::a('<i class="glyphicon glyphicon-trash text-danger"></i>', ['sprossen-width/delete', 'id' => $model->ID, 'sprossenID' => $model->sprossen->ID], ['data-pjax' => 0, 'onclick' => 'return confirm("Sterge?");']);
                        },
                    ]
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
