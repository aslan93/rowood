<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Type\models\TypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sprossens');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sprossen'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'value' => function($model)
                    {
                        return Html::img($model->imagePath, ['height' => 50]);
                    },
                    'format' => 'raw',
                    'filter' => false,
                ],
                [
                    'label' => 'Sizes',
                    'value' => function($model)
                    {
                        return implode(', ', ArrayHelper::map($model->sprossenWidths, 'ID', 'Width'));
                    },
                ],

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
