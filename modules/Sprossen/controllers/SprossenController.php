<?php

namespace app\modules\Sprossen\controllers;

use app\modules\Sprossen\models\SprossenMaterial;
use Yii;
use app\modules\Sprossen\models\Sprossen;
use app\modules\Sprossen\models\SprossenSearch;
use app\modules\Admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\modules\Sprossen\models\SprossenWidthSearch;

/**
 * SprossenController implements the CRUD actions for Sprossen model.
 */
class SprossenController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sprossen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SprossenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sprossen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sprossen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sprossen();
        $materialSprossen = new SprossenMaterial();
        if (Yii::$app->request->isPost)
        {
            
            $model->Image = $this->saveImage($model);
            
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                if ($_POST['Sprossen']['MaterialID']) {
                    $materialSprossen->SprossenID = $model->ID;
                    $materialSprossen->MaterialID = $_POST['Sprossen']['MaterialID'];
                    $materialSprossen->save(false);
                }
                return $this->redirect(['update', 'id' => $model->ID]);
            }


        }
        
        return $this->render('create', [
            'model' => $model,
            'materialSprossen'=>$materialSprossen,
        ]);
    }

    /**
     * Updates an existing Sprossen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $materialSprossen = $model->sprossenMaterials?$model->sprossenMaterials:new SprossenMaterial();
        $model->MaterialID = $materialSprossen->MaterialID;
        $searchModel = new SprossenWidthSearch([
            'SprossenID' => $model->ID,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($_POST['Sprossen']['MaterialID']) {
            $materialSprossen->SprossenID = $model->ID;
            $materialSprossen->MaterialID = $_POST['Sprossen']['MaterialID'];
            $materialSprossen->save(false);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'materialSprossen'=>$materialSprossen
        ]);
    }

    /**
     * Deletes an existing Sprossen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sprossen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sprossen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sprossen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function saveImage($model)
    {
        $uploaded = UploadedFile::getInstanceByName('Image');
        
        if ($uploaded)
        {
            $fileName = md5(microtime(true)) . '.' . $uploaded->extension;
            if ($uploaded->saveAs(Yii::getAlias('@webroot/uploads/sprossen/' . $fileName)))
            {
                return $fileName;
            }
        }
        
        return $model->Image;
    }
    
}
