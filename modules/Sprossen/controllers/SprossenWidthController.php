<?php

namespace app\modules\Sprossen\controllers;

use Yii;
use app\modules\Sprossen\models\SprossenWidth;
use app\modules\Sprossen\models\SprossenWidthSearch;
use app\modules\Admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * SprossenWidthController implements the CRUD actions for SprossenWidth model.
 */
class SprossenWidthController extends AdminController
{

    /**
     * Lists all SprossenWidth models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SprossenWidthSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SprossenWidth model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SprossenWidth model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($sprossenID)
    {
        $model = new SprossenWidth();
        
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID, 'sprossenID' => $sprossenID,  '#' => 'sprossen-widths']);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'sprossenID' => $sprossenID,
        ]);
    }

    /**
     * Updates an existing SprossenWidth model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $sprossenID)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['sprossen/update', 'id' => $model->sprossen->ID, '#' => 'variants']);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'sprossenID' => $sprossenID,
        ]);
    }

    /**
     * Deletes an existing SprossenWidth model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['type/update', 'id' => Yii::$app->request->get('sprossenID'), '#' => 'sprossen-widths']);
    }

    /**
     * Finds the SprossenWidth model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SprossenWidth the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SprossenWidth::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
