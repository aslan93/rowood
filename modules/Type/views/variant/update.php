<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Variant */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Variant',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Types'), 'url' => ['type/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Type'), 'url' => ['type/update', 'id' => Yii::$app->request->get('typeID'), '#' => 'variants']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="variant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
