<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$initialPreview = [];
if ($model->imagePath)
{
    $initialPreview[] = Html::img($model->imagePath, ['width' => 80]);
}

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Variant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="variant-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= Html::activeHiddenInput($model, 'TypeID', [
                'value' => $model->TypeID ? $model->TypeID : Yii::$app->request->get('typeID'),
            ]) ?>

            <div class="form-group">
                <label>Image</label>
                <?= FileInput::widget([
                    'name' => 'Image',
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'showRemove' => false,
                        'showUpload' => false,
                        'initialPreview' => $initialPreview,
                    ]
                ]) ?>
            </div>
            <?= $form->field($model, 'Number')->textInput(['id'=>'opens-number']) ?>
            
            <?= $form->field($model, 'MaxNumber')->textInput(['id'=>'opens-max-number']) ?>
            
            <?= $form->field($model, 'Position')->textInput() ?>
            <?= $form->field($model, 'OpeningPositions')->textInput() ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'MinWidth')->textInput() ?>

            <?= $form->field($model, 'MaxWidth')->textInput() ?>

            <?= $form->field($model, 'MinHeight')->textInput() ?>

            <?= $form->field($model, 'MaxHeight')->textInput() ?>

            <div class="variants-positions">

            </div>
        </div>
    </div>
    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>