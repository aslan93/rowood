<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\VariantSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="variant-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'TypeID') ?>

    <?= $form->field($model, 'Image') ?>

    <?= $form->field($model, 'Number') ?>

    <?= $form->field($model, 'MinWidth') ?>

    <?php // echo $form->field($model, 'MaxWidth') ?>

    <?php // echo $form->field($model, 'MinHeight') ?>

    <?php // echo $form->field($model, 'MaxHeight') ?>

    <?php // echo $form->field($model, 'Position') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
