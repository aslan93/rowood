<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Type\models\VariantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<br />
<div class="variant-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'value' => function($model)
                    {
                        return Html::tag('div', Html::img($model->imagePath, [
                            'width' => 25,
                        ]), [
                            'class' => 'text-center',
                        ]); 
                    },
                    'format' => 'raw',
                ],
                'Number',
                'MaxNumber',
                'MinWidth',
                'MaxWidth',
                'MinHeight',
                'MaxHeight',
                'Position',
                'OpeningPositions',

                [
                    'class' => 'app\components\GridView\ActionColumn',
                    'header' => Html::a(Yii::t('app', 'Create Variant'), ['variant/create', 'typeID' => $model->ID], ['class' => 'btn btn-success', 'data-pjax' => 0]),
                    'buttons' => [
                        'update' => function($url, $model, $key)
                        {
                            return Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['variant/update', 'id' => $model->ID, 'typeID' => $model->type->ID], ['data-pjax' => 0]);
                        },
                        'delete' => function($url, $model, $key)
                        {
                            return Html::a('<i class="glyphicon glyphicon-trash text-danger"></i>', ['variant/delete', 'id' => $model->ID, 'typeID' => $model->type->ID], ['data-pjax' => 0, 'onclick' => 'return confirm("Sterge?");']);
                        },
                    ]
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
