<?php

    use yii\bootstrap\Html;
    use kartik\file\FileInput;
    use kartik\select2\Select2;
    use app\modules\Category\models\Category;
    
    $initialPreview = [];
    if ($model->imagePath)
    {
        $initialPreview[] = Html::img($model->imagePath, ['width' => 80]);
    }

?>
<br />
<div class="row">
    <div class="col-md-6">

        <?= $form->field($model, 'CategoryID')->widget(Select2::className(), [
            'data' => Category::getList(Yii::t('app', 'Selectati')),
        ]) ?>

        <div class="form-group">
            <label class="control-label">Image</label>
            <?= FileInput::widget([
                'name' => 'Image',
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showRemove' => false,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                ]
            ]) ?>
        </div>

    </div>
    <div class="col-md-6">

        <?= $form->field($model, 'TopDimensions')->textInput() ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'LeftDimensions')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'RightDimensions')->textInput() ?>
            </div>
        </div>

        <?= $form->field($model, 'BottomDimensions')->textInput() ?>

    </div>
</div>