<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\Type\models\Type */
/* @var $form yii\widgets\ActiveForm */
?>

<br />
<div class="type-form">
    
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    
        <?php
            $items = [];
            $items[] = [
                'label' => Yii::t('app', 'Date generale'),
                'content' => $this->render('_main_form', [
                    'form' => $form,
                    'model' => $model,
                ]),
                'active' => true,
            ];

            if (isset($dataProvider))
            {
                $items[] = [
                    'label' => Yii::t('app', 'Variante'),
                    'content' => $this->render('_variants_form', [
                        'form' => $form,
                        'model' => $model,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]),
                ];
            }
        ?>
    
        <?= Tabs::widget([
            'items' => $items,
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs("
    var url = document.location.toString();
    
    if (url.split('#')[1] !== undefined)
    {
        var hash = url.split('#')[1];
        if (hash == 'variants')
        {
            $('.type-form .nav-tabs li').eq(1).find('a').click();
        }
    }

    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    });
") ?>
