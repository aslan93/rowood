<?php

namespace app\modules\Type\controllers;

use Yii;
use app\modules\Type\models\Variant;
use app\modules\Type\models\VariantSearch;
use app\modules\Admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * VariantController implements the CRUD actions for Variant model.
 */
class VariantController extends AdminController
{

    /**
     * Lists all Variant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VariantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Variant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Variant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($typeID)
    {
        $model = new Variant();
        
        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);
            
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID, 'typeID' => $typeID,  '#' => 'variants']);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'typeID' => $typeID,
        ]);
    }

    /**
     * Updates an existing Variant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $typeID)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);
            
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['type/update', 'id' => $model->type->ID, '#' => 'variants']);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'typeID' => $typeID,
        ]);
    }

    /**
     * Deletes an existing Variant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['type/update', 'id' => Yii::$app->request->get('typeID'), '#' => 'variants']);
    }

    /**
     * Finds the Variant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Variant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Variant::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function saveImage($model)
    {
        $uploaded = UploadedFile::getInstanceByName('Image');
        
        if ($uploaded)
        {
            $fileName = md5(microtime(true)) . '.' . $uploaded->extension;
            if ($uploaded->saveAs(Yii::getAlias('@webroot/uploads/variant/' . $fileName)))
            {
                return $fileName;
            }
        }
        
        return $model->Image;
    }

}
