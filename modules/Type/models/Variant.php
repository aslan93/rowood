<?php

namespace app\modules\Type\models;

use Yii;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Variant".
 *
 * @property integer $ID
 * @property integer $TypeID
 * @property integer $Image
 * @property integer $Number
 * @property integer $MaxNumber
 * @property integer $MinWidth
 * @property integer $MaxWidth
 * @property integer $MinHeight
 * @property integer $MaxHeight
 * @property integer $Position
 *
 * @property Type $type
 */
class Variant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TypeID'], 'required'],
            [['OpeningPositions'], 'safe'],
            [['TypeID', 'Number', 'MaxNumber', 'MinWidth', 'MaxWidth', 'MinHeight', 'MaxHeight', 'Position'], 'integer'],
            [['TypeID'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['TypeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TypeID' => Yii::t('app', 'Type ID'),
            'Image' => Yii::t('app', 'Image'),
            'Number' => Yii::t('app', 'Number'),
            'MaxNumber' => Yii::t('app', 'MaxNumber'),
            'MinWidth' => Yii::t('app', 'Min Width'),
            'MaxWidth' => Yii::t('app', 'Max Width'),
            'MinHeight' => Yii::t('app', 'Min Height'),
            'MaxHeight' => Yii::t('app', 'Max Height'),
            'Position' => Yii::t('app', 'Position'),
            'OpeningPositions' => Yii::t('app', 'OpeningPositions'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['ID' => 'TypeID']);
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/variant/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, [self::className(), Type::className()]);
    }
    
}
