<?php

namespace app\modules\Type\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Type\models\Variant;

/**
 * VariantSearch represents the model behind the search form about `app\modules\Type\models\Variant`.
 */
class VariantSearch extends Variant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TypeID', 'Image', 'Number', 'MinWidth', 'MaxWidth', 'MinHeight', 'MaxHeight', 'Position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Variant::find()->with('type')->orderBy('Position');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,

            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'TypeID' => $this->TypeID,
            'Image' => $this->Image,
            'Number' => $this->Number,
            'MinWidth' => $this->MinWidth,
            'MaxWidth' => $this->MaxWidth,
            'MinHeight' => $this->MinHeight,
            'MaxHeight' => $this->MaxHeight,
            'Position' => $this->Position,
        ]);

        return $dataProvider;
    }
}
