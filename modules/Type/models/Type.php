<?php

namespace app\modules\Type\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use app\modules\Category\models\Category;

/**
 * This is the model class for table "Type".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property string $Image
 * @property integer $TopDimensions
 * @property integer $RightDimensions
 * @property integer $BottomDimensions
 * @property integer $LeftDimensions
 *
 * @property Category $category
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CategoryID', 'Image', 'RightDimensions', 'BottomDimensions'], 'required'],
            [['TopDimensions', 'LeftDimensions'], 'default', 'value' => 0],
            [['CategoryID', 'TopDimensions', 'RightDimensions', 'BottomDimensions', 'LeftDimensions'], 'integer'],
            [['Image'], 'string', 'max' => 255],
            [['CategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['CategoryID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'CategoryID' => Yii::t('app', 'Category'),
            'Image' => Yii::t('app', 'Image'),
            'TopDimensions' => Yii::t('app', 'Top Dimensions'),
            'RightDimensions' => Yii::t('app', 'Right Dimensions'),
            'BottomDimensions' => Yii::t('app', 'Bottom Dimensions'),
            'LeftDimensions' => Yii::t('app', 'Left Dimensions'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['ID' => 'CategoryID']);
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/type/' . $this->Image);
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->all(), 'ID', 'ID');
        return $return + $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
    }
    
}
