<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\components\GridView\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Opening\models\OpeningSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Openings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="opening-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Opening'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'ID',
                [
                    'attribute' => 'From',
                    'value' => function($model)
                    {
                        return $model->From . ' mm';
                    },
                ],
                [
                    'attribute' => 'To',
                    'value' => function($model)
                    {
                        return $model->To . ' mm';
                    },
                ],
                'Price',

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
