<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Opening\models\Opening */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opening-form">

    <?php $form = ActiveForm::begin(); ?>
    
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'From')->textInput(['type' => 'number', 'min' => 0, 'step' => 1])->hint('* window min height in mm') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'To')->textInput(['type' => 'number', 'min' => 0, 'step' => 1])->hint('* window max height in mm') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
