<?php

namespace app\modules\Opening\models;

use Yii;

/**
 * This is the model class for table "Opening".
 *
 * @property integer $ID
 * @property integer $From
 * @property integer $To
 * @property string $Price
 */
class Opening extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Opening';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['From', 'To', 'Price'], 'required'],
            [['From', 'To'], 'integer'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'From' => Yii::t('app', 'From'),
            'To' => Yii::t('app', 'To'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }
}
