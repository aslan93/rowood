<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use app\modules\User\models\UserStatus;
use app\modules\User\models\UserType;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\User\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => [
                        'width' => '50px',
                    ],
                ],

                'Email:email',
                [
                    'attribute' => 'Status',
                    'filter' => Html::activeDropDownList($searchModel, 'Status', UserStatus::getList(Yii::t('app', '-')), ['class' => 'form-control'])
                ],
                [
                    'attribute' => 'Type',
                    'filter' => Html::activeDropDownList($searchModel, 'Type', UserType::getList(Yii::t('app', '-')), ['class' => 'form-control'])
                ],

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
