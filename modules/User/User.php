<?php

namespace app\modules\User;

use app\modules\Admin\Admin;

/**
 * user module definition class
 */
class User extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\User\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
