<?php

use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

$bundle = RowoodAssets::register($this);
?>
<div >

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',

    ]); ?>

    <?= $form->field($model, 'Email')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'Password')->passwordInput() ?>

    <?= $form->field($model, 'RememberMe')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Logare'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
