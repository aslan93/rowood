<?php

namespace app\modules\User\widgets\Login;

use Yii;
use app\modules\Login\models\LoginForm;
use app\modules\Product\models\Product;
use yii\base\Widget;


class Login extends Widget
{
    public $redirect = false;
    public $orderID = false;

    public function run()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if ($this->redirect) {
                Yii::$app->response->redirect($this->redirect);
                Yii::$app->end();
            }
        }

        return $this->render('index', [
            'model' => $model,
            'orderID' => $this->orderID,
        ]);
    }
}