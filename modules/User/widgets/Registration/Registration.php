<?php

namespace app\modules\User\widgets\Registration;

use app\modules\User\models\User;
use Yii;
use yii\base\Widget;
use app\modules\User\models\UserInfo;
use app\modules\Login\models\LoginForm;



class Registration extends Widget
{   public $redirect = false;
    public $success = false;
    public $orderID = false;


    public function run()
    {
        $model = new User();
        $modelInfo = new UserInfo();
        $loginModel = new LoginForm();

        $model->Type = 'User';
        $model->Status = 'Active';
        if ($model->load(Yii::$app->request->post())&& $model->save()) {

            $modelInfo->UserID =$model->ID;

            if($modelInfo->load(Yii::$app->request->post())&& $modelInfo->save()){
                $this->success = true;


                $loginModel->Email = $model->Email;
                $loginModel->Password = $model->Password;
                $loginModel->RememberMe = 1;

                if ($loginModel->login()) {

//                    if ($this->redirect) {
//                        Yii::$app->response->redirect($this->redirect);
//                        Yii::$app->end();
//                    }
                }
            }

         }
        return $this->render('index',[
            'model' => $model,
            'modelInfo' => $modelInfo,
            'success' => $this->success,
            'orderID' => $this->orderID,
        ]);

    }
}