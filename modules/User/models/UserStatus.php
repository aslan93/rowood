<?php

namespace app\modules\User\models;

use Yii;
use app\components\Enum\Enum;

class UserStatus extends Enum
{

    const Active = 'Active';
    const Inactive = 'Inactive';

    public static function items()
    {
        return [
            self::Active   => Yii::t('app', 'Active'),
            self::Inactive => Yii::t('app', 'Inactive'),
        ];
    }

}
