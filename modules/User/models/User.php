<?php

namespace app\modules\User\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "User".
 *
 * @property integer $ID
 * @property string $Email
 * @property string $Password
 * @property string $AuthKey
 * @property string $Status
 * @property string $Type
 *
 * @property UserInfo $userInfo
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Email', 'Password'], 'required'],
            [['Email'], 'string', 'max' => 125],
            [['Password', 'Status', 'Type'], 'string', 'max' => 50],
            [['AuthKey'], 'safe'],
            [['Email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID'       => Yii::t('app', 'ID'),
            'Email'    => Yii::t('app', 'Email'),
            'Password' => Yii::t('app', 'Parola'),
            'Status'   => Yii::t('app', 'Status'),
            'Type'     => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfo()
    {
        return $this->hasOne(UserInfo::className(), ['UserID' => 'ID']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::find()->where(['ID' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['AuthKey' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['Email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->ID;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->AuthKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === md5($password);
    }

    public function beforeSave($insert) {
        $pass = md5($this->Password);
        $this->Password = $pass;
        return true;
    }
}
