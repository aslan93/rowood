<?php

namespace app\modules\User\models;

use Yii;
use app\components\Enum\Enum;

class UserType extends Enum
{

    const User = 'User';
    const Admin = 'Admin';

    public static function items()
    {
        return [
            self::User  => Yii::t('app', 'User'),
            self::Admin => Yii::t('app', 'Admin'),
        ];
    }

}
