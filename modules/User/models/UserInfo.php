<?php

namespace app\modules\User\models;

use Yii;

/**
 * This is the model class for table "UserInfo".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property string $FirstName
 * @property string $LastName
 * @property string $Sex
 * @property string $Phone
 *
 * @property User $user
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SexMasculin = 'Masculin';
    const SexFeminin = 'Feminin';

    public static function tableName()
    {
        return 'UserInfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'FirstName', 'LastName', 'Sex', 'Phone'], 'required'],
            [['UserID'], 'integer'],
            [['FirstName', 'LastName', 'Sex'], 'string', 'max' => 255],
            [['Phone'], 'string', 'max' => 25],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'FirstName' => 'Nume',
            'LastName' => 'Prenume',
            'Sex' => 'Sex',
            'Phone' => 'Telefon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }

    public function getSexType(){
        return [

            self::SexMasculin => Yii::t('app', 'Masculin'),
            self::SexFeminin   => Yii::t('app', 'Feminin'),
        ];
    }
}
