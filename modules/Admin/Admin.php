<?php

namespace app\modules\Admin;

use Yii;
use app\components\Module\Module;

class Admin extends Module
{

    public $controllerNamespace = 'app\modules\Admin\controllers';

    public function init()
    {
        $this->layoutPath = Yii::getAlias('@app/views/themes/rowood/layouts');
        
        parent::init();
    }

}
