<?php

namespace app\modules\Admin\controllers;

use Yii;
use app\components\Controller\Controller;

class AdminController extends Controller
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = 'admin';
        
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->Type !== 'Admin')
        {
            return $this->redirect('/');
        }
    }
    
    public function actionIndex()
    {
        return $this->redirect('/admin/dashboard/');
    }
    
}
