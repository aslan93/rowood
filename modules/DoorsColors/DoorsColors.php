<?php

namespace app\modules\DoorsColors;

/**
 * DoorsColors module definition class
 */
class DoorsColors extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\DoorsColors\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
