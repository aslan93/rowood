<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\modules\DoorsColors\models\DoorsColors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doors-colors-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Date generale'),
                'content' => $this->render('_main_form', [
                    'form' => $form,
                    'model' => $model,
                ]),
                'active' => true,
            ],
            [
                'label' => Yii::t('app', 'Descriere'),
                'content' => $this->render('_languages_form', [
                    'form' => $form,
                    'model' => $model,
                ]),
            ]
        ]
    ]); ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
