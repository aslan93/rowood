<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\DoorsColors\models\DoorsColors */

$this->title = 'Update Doors Colors: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Doors Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="doors-colors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
