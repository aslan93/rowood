<?php

namespace app\modules\DoorsColors\models;

use app\modules\DoorsSizes\models\DoorsSizeRel;
use app\modules\Product\models\Product;
use Yii;
use yii\base\Model;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "DoorsColors".
 *
 * @property integer $ID
 * @property string $Image
 * @property integer $Price
 * @property string $Type
 *
 * @property DoorsColorsLang[] $doorsColorsLangs
 * @property DoorsColorsRel[] $doorsColorsRels
 */
class DoorsColors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsColors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['Price'], 'integer'],
            [['Image', 'Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Image' => 'Image',
            'Price' => 'Price',
            'Type' => 'Type',
        ];
    }


    public function getDoorsColorsRels()
    {
        return $this->hasMany(DoorsColorsRel::className(), ['DoorsColorID' => 'ID']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->DoorsColorID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }

    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    public static function getImageList($addEmpty = false,$id = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $mods = DoorsColorsRel::find()->where(['ProductID' => $id])->with('doorsColor')->all();
        $modelss = [];
        foreach ($mods as $key => $mod){
            $modelss[$key] = $mod->doorsColor;
        }

        $result2 = [];
        foreach ($modelss as $key => $mod){

            $result2[$mod->lang->Title] =[$mod->ID => '<div class="color-item" data-attribute="'.$mod->Price.'"><img class="image" src="'.Url::to('@web/uploads/doors-colors/'.$mod->Image).'"/>'.'<br>+ '.$mod->Price .' €  </div>',
            ];

        }return $return + $result2;
    }
    public static function getListOfImages($addEmpty=false,$id=false){
        $return = $addEmpty ? ['' => $addEmpty] : [];
        if (!$id == false){
            $result = ArrayHelper::map(self::find()->with('lang')->andWhere(['ID'=>$id])->all(), 'ID', 'Image');
        }else {

            $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image', 'lang.Title');
        }
        return $return + $result;
    }
    public function getLang()
    {
        return $this->hasOne(DoorsColorsLang::className(), ['DoorsColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(DoorsColorsLang::className(), ['DoorsColorID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new DoorsColorsLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/doors-colors/' . $this->Image);
    }

}
