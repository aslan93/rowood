<?php

namespace app\modules\DoorsColors\models;

use Yii;
use app\modules\Product\models\Product;

/**
 * This is the model class for table "DoorsColorsRel".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property integer $DoorsColorID
 *
 * @property DoorsColors $doorsColor
 * @property Product $product
 */
class DoorsColorsRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsColorsRel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'DoorsColorID'], 'integer'],
            [['DoorsColorID'], 'exist', 'skipOnError' => true, 'targetClass' => DoorsColors::className(), 'targetAttribute' => ['DoorsColorID' => 'ID']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'DoorsColorID' => 'Doors Color ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsColor()
    {
        return $this->hasOne(DoorsColors::className(), ['ID' => 'DoorsColorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }
}
