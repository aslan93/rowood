<?php

namespace app\modules\DoorsColors\models;

use Yii;

/**
 * This is the model class for table "DoorsColorsLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property integer $DoorsColorID
 * @property string $Title
 * @property string $Text
 *
 * @property DoorsColors $doorsColor
 */
class DoorsColorsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsColorsLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'DoorsColorID', 'Title', 'Text'], 'required'],
            [['DoorsColorID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['DoorsColorID'], 'exist', 'skipOnError' => true, 'targetClass' => DoorsColors::className(), 'targetAttribute' => ['DoorsColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'DoorsColorID' => 'Doors Color ID',
            'Title' => 'Title',
            'Text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsColor()
    {
        return $this->hasOne(DoorsColors::className(), ['ID' => 'DoorsColorID']);
    }
}
