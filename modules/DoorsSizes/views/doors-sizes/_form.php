<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\DoorsSizes\models\DoorsSizes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doors-sizes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'WidthHeight')->textInput() ?>

    <?= $form->field($model, 'Type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
