<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\DoorsSizes\models\DoorsSizes */

$this->title = 'Update Doors Sizes: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Doors Sizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="doors-sizes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
