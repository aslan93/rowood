<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\DoorsSizes\models\DoorsSizes */

$this->title = 'Create Doors Sizes';
$this->params['breadcrumbs'][] = ['label' => 'Doors Sizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doors-sizes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
