<?php

namespace app\modules\DoorsSizes\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\Product\models\Product;
/**
 * This is the model class for table "DoorsSizes".
 *
 * @property integer $ID
 * @property integer $Width
 * @property integer $Height
 * @property string $Type
 * @property integer $Price
 *
 * @property DoorsSizeRel[] $doorsSizeRels
 */
class DoorsSizes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsSizes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WidthHeight',  'Price'], 'required'],
            [['Price'], 'integer'],
            [['WidthHeight','Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'WidthHeight' => 'Width x Height',
            'Type' => 'Type',
            'Price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoorsSizeRels()
    {
        return $this->hasMany(DoorsSizeRel::className(), ['SizeID' => 'ID']);
    }

    public static function getList($addEmpty = false,$id = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $mods = self::find()->all();
        if ($id){
            $mods = self::find()->where(['ID' => $id])->all();
        }
        $result2 = [];
        foreach ($mods as $mod){

            $result2[$mod->ID] = $mod->WidthHeight;
       
        }
        return $return + $result2;
    }
    public static function getSpecialList($addEmpty = false,$id = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $mods = DoorsSizeRel::find()->where(['ProductID'=>$id])->with('size')->all();

        $modelss = [];
        foreach ($mods as $key => $mod){
            $modelss[$key] = $mod->size;
        }
        $result2 = [];
        foreach ($modelss as $mod){

            $result2[$mod->ID] = '<div class="size-item" data-attribute ="'.$mod->Price.'"><span>'.$mod->WidthHeight.'</span><span class="plus-price"> + '.$mod->Price .' €  </span></div>';

        }
        return $return + $result2;
    }

}
