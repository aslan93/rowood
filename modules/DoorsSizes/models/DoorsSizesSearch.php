<?php

namespace app\modules\DoorsSizes\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\DoorsSizes\models\DoorsSizes;

/**
 * DoorsSizesSearch represents the model behind the search form about `app\modules\DoorsSizes\models\DoorsSizes`.
 */
class DoorsSizesSearch extends DoorsSizes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'WidthHeight', 'Price'], 'integer'],
            [['Type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DoorsSizes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'WidthHeight' => $this->WidthHeight,
            'Price' => $this->Price,
        ]);

        $query->andFilterWhere(['like', 'Type', $this->Type]);

        return $dataProvider;
    }
}
