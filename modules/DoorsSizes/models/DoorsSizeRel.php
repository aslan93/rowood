<?php

namespace app\modules\DoorsSizes\models;

use Yii;
use app\modules\Product\models\Product;

/**
 * This is the model class for table "DoorsSizeRel".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property integer $SizeID
 *
 * @property Product $product
 * @property DoorsSizes $size
 */
class DoorsSizeRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DoorsSizeRel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'SizeID'], 'integer'],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
            [['SizeID'], 'exist', 'skipOnError' => true, 'targetClass' => DoorsSizes::className(), 'targetAttribute' => ['SizeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'SizeID' => 'Size ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSize()
    {
        return $this->hasOne(DoorsSizes::className(), ['ID' => 'SizeID']);
    }
}
