<?php

namespace app\modules\DoorsSizes;

/**
 * DoorsSizes module definition class
 */
class DoorsSizes extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\DoorsSizes\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
