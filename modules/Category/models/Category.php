<?php

namespace app\modules\Category\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use app\modules\Type\models\Type;

/**
 * This is the model class for table "Category".
 *
 * @property integer $ID
 *
 * @property CategoryLang $lang
 * @property CategoryLang[] $langs
 * @property Type[] $types
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'typesCount' => Yii::t('app', 'Types Count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(CategoryLang::className(), ['CategoryID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(CategoryLang::className(), ['CategoryID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new CategoryLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasMany(Type::className(), ['CategoryID' => 'ID']);
    }
    
    public function getTypesCount()
    {
        return count($this->types);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            // no comments
            
            $langModel->CategoryID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    
}
