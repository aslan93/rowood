<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use app\modules\Category\models\Category;
use kartik\datetime\DateTimePicker;
use app\modules\Post\models\PostLang;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;



/* @var $this yii\web\View */
/* @var $model app\modules\Post\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            ]
    ]); ?>


    <?php
    //************************************
    $items[0] = [
    'label' => 'Descriere',
    'content' => $this->render('_lang_form',[
    'form' => $form,
    'model' => $model,

    ])];
    $items[1] = [
    'label' => 'Imagini',
    'content' => $this->render('_image_form',[
    'form' => $form,
    'model' => $model,


    ])];

    echo '<br>';
    echo Tabs::widget([
    'items' => $items,
    ]);

    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


