<?php

namespace app\modules\Post;

use \app\modules\Admin\Admin;

/**
 * post module definition class
 */
class Post extends Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Post\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
