<?php

namespace app\modules\Post\controllers;

use app\controllers\SiteBackendController;

/**
 * Default controller for the `post` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
