<?php

namespace app\modules\Post\controllers;

use Yii;
use app\modules\Admin\controllers\AdminController;
use app\modules\Post\models\Post;
use app\modules\Post\models\PostSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\Post\models\PostImage;
use yii\web\UploadedFile;
use yii\imagine\Image;


/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            Yii::$app->session->setFlash('succes');
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            Yii::$app->session->setFlash('succes');
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $images = UploadedFile::getInstancesByName('PostImages');

        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/post/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/post/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/post/$thumb"));

                    $imageModel = new PostImage([
                        'PostID' => $model->ID,
                        'Thumb' => $thumb,
                        'Image' => $file,
                        'IsMain' => 1,
                    ]);
                    $imageModel->save();
                }
            }
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        PostImage::findOne($key)->delete();
        return '{}';
    }
    public function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = PostImage::findOne($key);

        PostImage::updateAll(['IsMain'=> 0],['PostID'=>$imgModel->PostID]);

        $imgModel->IsMain = 1;
        $rs = $imgModel->save();

        return "$rs";
    }


}
