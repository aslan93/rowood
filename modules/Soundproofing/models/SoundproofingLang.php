<?php

namespace app\modules\Soundproofing\models;

use Yii;

/**
 * This is the model class for table "SoundproofingLang".
 *
 * @property integer $ID
 * @property integer $SoundproofingID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Soundproofing $soundproofing
 */
class SoundproofingLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SoundproofingLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SoundproofingID', 'LangID', 'Title', 'Text'], 'required'],
            [['SoundproofingID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['SoundproofingID'], 'exist', 'skipOnError' => true, 'targetClass' => Soundproofing::className(), 'targetAttribute' => ['SoundproofingID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SoundproofingID' => Yii::t('app', 'Soundproofing ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoundproofing()
    {
        return $this->hasOne(Soundproofing::className(), ['ID' => 'SoundproofingID']);
    }
}
