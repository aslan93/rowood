<?php

namespace app\modules\Soundproofing;

/**
 * soundproofing module definition class
 */
class Soundproofing extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Soundproofing\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
