<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Soundproofing\models\Soundproofing */

$this->title = Yii::t('app', 'Create Soundproofing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Soundproofings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soundproofing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
