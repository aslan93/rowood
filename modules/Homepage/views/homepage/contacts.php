<?php
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
$this->title = 'Contacte';
?>
<section class="contacts">
    <div class="container">
        <div class="row" id="scrie">
            <div class="col-md-6">
                <div class="title">
                    SCRIETI-NE!
                </div>
                <?php Pjax::begin([
                    'enablePushState' => false,
                    'enableReplaceState'=>false,

                ])?>
                <?php $form = ActiveForm::begin([
                    'action' => ['/admin/feedback/feedback/new-feedback'],
                    'method' => 'post',
                    'id'=> 'feedback',
                    'fieldConfig' =>[
                        'options' => [
                            'tag' => false,
                        ]
                    ],
                    'options' => [

                        'class' => 'form-contacts',
                        'data-pjax' => true,
                    ]
                ]); ?>
                    <div>
                        <label>
                            <?= $form->field($feedback, 'Nume',['template' => "{input}"])->input('text',[
                                'placeholder' => 'Nume',
                                'class' => '',
                                'id' => 'inp1',
                            ])->label(false); ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($feedback, 'Email',['template' => "{input}"])->input('text',[
                                'placeholder' => 'Email',
                                'class' => '',
                                'id' => 'inp3'
                            ])->label(false); ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($feedback, 'Mesaj',['template' => "{input}"])->textarea([
                                'placeholder' => 'Mesaj',
                                'class' => '',
                            ])->label(false); ?>
                        </label>
                    </div>
                    <div class="submit-form-details">
                        <?= Html::submitButton(Yii::t('app', 'Trimite'), ['class' => 'submit-btn btn btn-primary']) ?>

                    </div>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end()?>
            </div>
            <div class="col-md-6">
                <div class="title">
                    INFORMATII
                </div>
                <div class="info">
                    Pellentesque lectus augue, blandit a nibh eu, lacinia dictum arcu. Quisque dapibus
                    nulla id dictum consequat. Sed purus elit, mollis non aliquam at, tincidunt id ipsum.
                    Sed viverra felis neque, et feugiat metus.
                </div>
                <ul>
                    <li>
                        <span class="fa fa-location-arrow"></span>
                        or. Chisinau, str. Stefan cel Mare 127
                    </li>
                    <li>
                        <span class="fa fa-phone"></span>
                        <a href="tel:373 12 345 678">
                            +373 12 345 678
                        </a>
                    </li>
                    <li>
                        <span class="fa fa-envelope"></span>
                        <a href="mailto:mail@rowood.md">
                            mail@rowood.md
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>