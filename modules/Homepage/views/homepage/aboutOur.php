<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;


$bundle = RowoodAssets::register($this);
$this->title = 'Despre Noi';
?>
<section class="about-our">
    <div class="img">
        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/about-our-bg.png" alt="">
    </div>
    <div class="container">
        <div class="info-about-our">
            <div class="title">
                DESPRE NOI
            </div>
            <div class="description">
                <div>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                </div>
                <div>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat
                    mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper
                    suscipit, posuere a, pede.
                </div>
                <div>
                    Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.
                    Aenean dignissim pellentesque felis. Pellentesque fermentum dolor. Aliquam quam lectus,
                    facilisis auctor, ultrices ut, elementum vulputate, nunc.
                </div>
                <div>
                    Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel,
                    velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.
                </div>
                <div>
                    Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra
                    a, ultricies in, diam. Sed arcu. Cras consequat.
                </div>
                <div>
                    Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna
                    eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor,
                    facilisis luctus, metus.
                    Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer
                    ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a,
                    sodales sit amet, nisi.
                </div>
            </div>
            <a href="/shop-page/shop-page/" class="btn-primary buy-now">
                Comanda acum
            </a>
        </div>
    </div>
</section>
