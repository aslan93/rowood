<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
?>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="news-box">
        <div class="img">
            <a href="<?= Url::to(['/homepage/homepage/new', 'id' => $model->ID]) ?>">
                <?= Html::img($model->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
            </a>
        </div>
        <div class="title-news-box">
            <?=$model->lang->Title?>
        </div>
        <span class="date-of-publishing">
                            <?=$model->niceDate?>
                        </span>
        <div class="description">
            <?=$model->shortContent?>
        </div>
        <a  href=" <?= Url::to(['/homepage/homepage/new', 'id' => $model->ID]) ?>" class="btn-primary view-more">
            Vezi detalii
        </a>
    </div>
</div>
