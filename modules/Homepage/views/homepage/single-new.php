<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;


$bundle = RowoodAssets::register($this);
$this->title = $new->lang->Title;
?>
<section class="top-post-page">
    <div class="img-bg">
        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/noutate2.png" alt="">
    </div>
    <div class="container">
        <div class="date-of-publishing-post">
            <div class="title">
                <?=$new->lang->Title?>
            </div>
            <div class="date">
                <?=$new->niceDate?>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="about-post">
                    <div class="description">
                        <div class="small-title">
                            <?=$new->lang->Title?>
                        </div>
                        <div>
                            <?=$new->lang->Content?>
                        </div>
                    </div>
                    <div>
                        <?= Html::img($new->mainImage->imagePath, ['width' => 1000, 'class' => 'img-responsive']) ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="right-sidebar-title text-center">
                    Alte noutati
                </div>
                <div class="row">
                    <?php
                    foreach ($relativeNews as $relativeNew){
                        ?>
                        <div class="col-md-12">
                            <div class="news-box">
                                <div class="img">
                                    <a href="<?= Url::to(['/homepage/homepage/new', 'id' => $relativeNew->ID]) ?>">
                                        <?= Html::img($relativeNew->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                                    </a>
                                </div>
                                <div class="title-news-box">
                                    <?=$relativeNew->lang->Title?>
                                </div>
                                <span class="date-of-publishing">
                                <?=$relativeNew->niceDate?>
                                </span>
                                <div class="description">
                                    <?=$relativeNew->shortContent?>
                                </div>
                                <a  href=" <?= Url::to(['/homepage/homepage/new', 'id' => $relativeNew->ID]) ?>" class="btn-primary view-more">
                                    Vezi detalii
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>

