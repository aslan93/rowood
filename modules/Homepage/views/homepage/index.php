<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;


$bundle = RowoodAssets::register($this);
$this->title = 'Home';
?>
<section class="top-slider">
    <div class="container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                foreach ($products as $product) {


                    ?>
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="slider-img">
                                    <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $product->ID]) ?>">
                                        <?= Html::img($product->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="content-slider">
                                    <div class="title">
                                        <?=$product->lang->Title?>
                                    </div>
                                    <div class="description">
                                        <div>
                                            <?=$product->lang->ShortDescription?>
                                        </div>
                                        <div>
                                            <div class="big-text">
                                                <?=$product->Price?>
                                                <sub>
                                                    €
                                                </sub>
                                            </div>
                                        </div>
                                        <div class="mobile-btn-info">
                                            <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $product->ID]) ?>">
                                                detalii
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="another-info">
                                    <div class="another-info-box">
                                        Praesent placerat risus quis eros.
                                        <span class="depth">
                                        1.5 mm
                                    </span>
                                    </div>
                                    <div class="another-info-box">
                                        Praesent placerat risus quis eros.
                                        <span class="depth">
                                            1.5 mm
                                        </span>
                                    </div>
                                    <div class="another-info-box">
                                        Praesent placerat risus quis eros.
                                        <span class="depth">
                                            1.5 mm
                                        </span>
                                    </div>
                                    <div class="another-info-box">
                                        Praesent placerat risus quis eros.
                                        <span class="depth">
                                            1.5 mm
                                        </span>
                                    </div>
                                    <div class="another-info-box">
                                        Praesent placerat risus quis eros.
                                        <span class="depth">
                                            1.5 mm
                                        </span>
                                    </div>
                                    <a class="more-detailes" href="#">
                                        DETALII
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>

            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
</section>

<section class="product-range">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/gama-produse.png" alt="">
            </div>
            <div class="col-md-4 text-center">
                <div class="middle-box">
                    <div class="title-gama-box">
                        Gamă largă
                        de produse
                    </div>
                    <div class="parent-links">
                        <a href="<?= Url::to(['/calculator/']) ?>" data-mh="101">
                            Configurator ferestre
                        </a>
                        <a href="<?= Url::to(['/shop-page/shop-page/']) ?>" data-mh="101">
                            Soluția
                            gata
                        </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="range-list">
                    <ul>
                        <li>
                                <span>
                                    <img src="<?=$bundle->baseUrl?>/images/chek-list.png" alt="">
                                </span>
                            Aliquam tincidunt mauris
                            eu risus.
                        </li>
                        <li>
                                <span>
                                    <img src="<?=$bundle->baseUrl?>/images/chek-list.png" alt="">
                                </span>
                            Aliquam tincidunt mauris
                            eu risus.
                        </li>
                        <li>
                                <span>
                                    <img src="<?=$bundle->baseUrl?>/images/chek-list.png" alt="">
                                </span>
                            Aliquam tincidunt mauris
                            eu risus.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="advantages">
    <div class="container">
        <div class="title text-center">
            Aliquam tincidunt mauris eu risus. Măăi Măăăi!!
        </div>
        <div class="row">
            <div class="col-md-3">
                <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/company-section.png" alt="">
            </div>
            <div class="col-md-6">
                <div class="year">
                    <div class="big-text">
                        4
                    </div>
                    <div class="small-text">
                        ani
                    </div>
                </div>
                <div class="another-text">
                    Aliquam tincidunt mauris eu risus.
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-3">
                                <span>
                                    <img src="<?=$bundle->baseUrl?>/images/company-icon.png" alt="">
                                </span>
                        </div>
                        <div class="col-md-9">
                            <div class="simple-text">
                                    <span class="red-text">
                                    Integer vitae libero ac risus
                                    egestas placerat.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="benefits">
                    <div class="box">
                        <div class="row">
                            <div class="col-md-4">
                                <img  class="img-responsive" src="<?=$bundle->baseUrl?>/images/company-icon2.png" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="small-title">
                                    Vestibulum
                                </div>
                                <div class="description">
                                    Cras ornare tristique elit.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/company-icon3.png" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="small-title">
                                    Vestibulum
                                </div>
                                <div class="description">
                                    Cras ornare tristique elit.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="row">
                            <div class="col-md-4">
                                <img  class="img-responsive" src="<?=$bundle->baseUrl?>/images/company-icon4.png" alt="">
                            </div>
                            <div class="col-md-8">
                                <div class="small-title">
                                    Vestibulum
                                </div>
                                <div class="description">
                                    Cras ornare tristique elit.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="our-recomandation">
    <div class="container">
        <div class="title">
            Recomandari
        </div>
        <div class="row">
            <?php
            foreach ($products as $product) {
                ?>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="recomandation-box">
                        <div class="img">
                            <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $product->ID]) ?>">
                                <?= Html::img($product->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                            </a>
                        </div>
                        <div class="title-glass pull-left">
                            <?=$product->lang->Title?>
                        </div>
                        <div class="price pull-right">
                            <?=$product->Price?> <sup>€ </sup>
                        </div>
                        <div class="clearfix"></div>
                        <div class="type-products">

                        </div>
                            <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $product->ID]) ?>" class="btn-primary buy-now">
                            Comanda acum
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>


        </div>
    </div>
</section>


<section class="news">
    <div class="container">
        <div class="title">
            Noutati
        </div>
        <div class="row">
            <?php
            foreach ($news as $new) {
                ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="news-box">
                        <div class="img">
                            <a href="<?= Url::to(['/homepage/homepage/new', 'id' => $new->ID]) ?>">
                                <?= Html::img($new->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                            </a>
                        </div>
                        <div class="title-news-box">
                            <?=$new->lang->Title?>
                        </div>
                        <span class="date-of-publishing">
                            <?=$new->niceDate?>
                        </span>
                        <div class="description">
                            <?=$new->shortContent?>
                        </div>
                        <a  href=" <?= Url::to(['/homepage/homepage/new', 'id' => $new->ID]) ?>" class="btn-primary view-more">
                            Vezi detalii
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>



<section class="seo">
    <div class="container">
        <div class="title">
            Ferestre - Lorem ipsum
        </div>
        <div class="seo-description">
            <div class="row">
                <?php
                foreach ($seoText as $seoTxt){
                    ?>

                    <div class="col-md-6">
                        <?= $seoTxt->lang->Content?>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
</section>

