<?php
use yii\widgets\ListView;

$this->title = 'Noutati';
?>

    <section class="name-page">
        <div class="container text-center">
            <div>
                Noutati
            </div>
        </div>
    </section>
    <section class="news white-bg">
        <div class="container">
            <div class="title">
                Noutati
            </div>
            <div class="row">

                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,

                    'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                    'itemView' => function ($model) {
                        return $this->render('_new_view',['model' => $model]);
                    },
                ]);
                ?>

                <div class="clearfix"></div>

            </div>
        </div>
    </section>
