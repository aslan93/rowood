<?php

namespace app\modules\Homepage\controllers;

use app\components\Controller\FrontController;
use app\modules\Post\models\Post;
use app\modules\Product\models\Product;
use app\modules\SeoPost\models\SeoPost;
use app\modules\Feedback\models\Feedback;
use yii\data\ActiveDataProvider;

class HomepageController extends FrontController
{
    
    public function actionIndex()
    { $seoText = SeoPost::find()->limit(2)->all();
      $products = Product::find()->with('lang')->where(['Visibility' => 1])->limit(6)->all();
        $news = Post::find()->with(['lang','mainImage'])->limit(3)->all();
        return $this->render('index',[
            'products' => $products,
            'news' => $news,
            'seoText' => $seoText,
        ]);
    }
    public function actionNew($id)
    {   $relativeNews = Post::find()->with(['lang','mainImage'])->andWhere(['<>','ID', $id])->limit(2)->all();
        $new = Post::find()->with(['lang','mainImage'])->where(['ID'=>$id])->one();
        return $this->render('single-new',[
            'new' => $new,
            'relativeNews' => $relativeNews,
        ]);
    }

    public function actionContacts()
    {   $feedback = new Feedback();
        return $this->render('contacts',[
            'feedback' => $feedback,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('aboutOur',[

        ]);
    }
    public function actionNews()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->with('lang'),
            'pagination' => [
                'pageSize' => 9,
            ],
        ]);
        return $this->render('news',[
            'dataProvider' => $dataProvider,
        ]);

    }
}
