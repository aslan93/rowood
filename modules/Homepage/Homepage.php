<?php

namespace app\modules\Homepage;

/**
 * homepage module definition class
 */
class Homepage extends \app\components\Module\FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Homepage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
