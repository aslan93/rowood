<?php
use app\views\themes\rowood\assets\RowoodAssets;
use yii\helpers\Url;
use yii\bootstrap\Html;

$bundle = RowoodAssets::register($this);
?>

<div class="col-md-4 col-sm-6 col-xs-6">
    <div class="recomandation-box">
        <a href="<?=Url::to(['/shop-page/shop-page/product','id' => $model->ID])?>" data-pjax="false">
            <div class="img img-responsive">
                <?php
                if ($model->mainImage) {
                    ?>
                    <?= Html::img($model->mainImage->imagePath, ['width' => 250 , 'class'=> 'img-responsive']) ?>
                    <?php
                }
                ?>
            </div>
        </a>
        <div class="title-glass pull-left">
            <?=$model->lang->Title?>
        </div>
        <div class="price pull-right">
            <?=$model->Price?> <sup>€ </sup>
        </div>
        <div class="clearfix"></div>
        <div class="type-products">
            <?=$model->category->Type?>
        </div>
        <a href="<?=Url::to(['/shop-page/shop-page/product','id' => $model->ID])?>" class="btn-primary buy-now" data-pjax="false">
            Comanda acum
        </a>
    </div>
</div>
