<?php

use app\views\themes\rowood\assets\RowoodAssets;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use app\modules\ShopPage\widgets\ProductChange\ProductChange;

$bundle = RowoodAssets::register($this);
$this->title = $product->lang->Title;
?>
<section class="name-page">
    <div class="container text-center">
        <div>
            <?=$product->lang->Title?>
        </div>
    </div>
</section>

<section class="produs">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-8">
                        <div class="owl-carousel" data-slider-id="1">
                            <?php
                                foreach ($product->images as $image){
                            ?>
                            <div>
                                <?=Html::img($image->imagePath,['width'=> 300,'class'=>'img-responsive'])?>
                            </div>
                            <?php
                                }
                            ?>


                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="about-products">
                            <div class="recomandation-box">
                                <div class="title-glass">
                                    <?=$product->lang->Title?>
                                </div>
                                <div class="price">
                                    <?=$product->Price ?> <sup>€ </sup>
                                </div>
                            </div>
                            <div class="description">
                                <?=$product->lang->ShortDescription?>
                            </div>
                           <div>
                           </div>
                            <?php Pjax::begin([
                                'id'=> 'cart-pjax',
                                'enablePushState' => false,
                                'enableReplaceState'=>false,
                            ])?>
                            <?php $form = ActiveForm::begin([
                                'action'=> '/admin/ecommerce/order/cart/add-to-cart/?id='.$product->ID,
                                'method' => 'get',
                                'id'=> 'add-to-cart',
                                'fieldConfig' =>[
                                    'options' => [
                                        'tag' => false,
                                    ]
                                ],
                                'options' => [

                                    'class' => 'form-add-to-cart',
                                    'data-pjax' => true,
                                ]

                            ]); ?>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quantity">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>

                                    <?= Html::input('number','quantity',1,['class' => 'form-control input-number'])?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                                <div class="submit-form-details">
                                    <?=Html::SubmitButton('Comanda acum',['class'=>'btn-primary'])?>
                                </div>
                            <?php ActiveForm::end(); ?>
                            <?php Pjax::end()?>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 mt35">
                        <div class="tabs-section">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#informatii" aria-controls="home" role="tab" data-toggle="tab">Informatii</a></li>
                                <li role="presentation"><a href="#detalii" aria-controls="messages" role="tab" data-toggle="tab">Detalii tehnice</a></li>
                           </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="informatii">
                                    <div class="description">
                                        <?=$product->lang->Description?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="detalii">
                                    <div class="row mt30 mb30">
                                        <?php
                                        if ($product->material){
                                        ?>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="bold">Material</div><br>
                                            <div class="img-tabs">
                                                <?=Html::img($product->material->imagePath,['width'=> 150,'class'=>'img-responsive'])?>
                                            </div>
                                            <br>
                                            <div><?=$product->material->lang->Title?></div>
                                        </div>
                                        <?php
                                        }
                                        if ($product->frontColor){
                                        ?>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div>Culoare din fata</div><br>
                                            <div class="img-tabs">
                                                <?=Html::img($product->frontColor->imagePath,['width'=> 150,'class'=>'img-responsive'])?>
                                            </div>
                                            <br>
                                            <div><?=$product->frontColor->lang->Title?></div>
                                        </div>
                                            <?php
                                        }
                                        if ($product->backColor){
                                        ?>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div>Culoare din spate</div><br>
                                            <div class="img-tabs">
                                                <?=Html::img($product->backColor->imagePath,['width'=> 150,'class'=>'img-responsive'])?>
                                            </div>
                                            <br>
                                            <div><?=$product->backColor->lang->Title?></div>
                                        </div>
                                        <?php
                                        }
                                        if ($product->decoration){
                                        ?>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div>Tipul sticlei</div><br>
                                            <div class="img-tabs">
                                                <?=Html::img($product->decoration->imagePath,['width'=> 150,'class'=>'img-responsive'])?>
                                            </div>
                                            <br>
                                            <div><?=$product->decoration->lang->Title?></div>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="right-sidebar-title text-center">
                    Alte produse
                </div>
                <div class="row">
                    <?php
                    foreach ($product->recomandations as $recomandation) {
                            ?>
                            <div class="col-md-12 col-sm-6 col-xs-6">
                            <div class="recomandation-box">
                            <div class="img">
                            <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $recomandation->ID]) ?>">
                        <?= Html::img($recomandation->mainImage->imagePath, ['width' => 350, 'class' => 'img-responsive']) ?>
                        </a>
                        </div>

                        <div class="title-glass pull-left">
                            <?= $recomandation->lang->Title ?>
                        </div>
                        <div class="price pull-right">
                            <?= $recomandation->Price ?> <sup> € </sup>
                        </div>
                        <div class="clearfix"></div>
                        <div class="type-products">
                        </div>
                        <a href="<?= Url::to(['/shop-page/shop-page/product', 'id' => $recomandation->ID]) ?>"
                           class="btn-primary buy-now" data-pjax="false">
                            Comanda acum
                        </a>
                        </div>
                        </div>
                        <?php

                    }
                    $this->registerJs('
                        $("#cart-pjax").on("pjax:end",function(){
                            
                            $.pjax.reload({container:"#cart-info-pjax"});
                        })
                    ');
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
