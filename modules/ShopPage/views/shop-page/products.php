<?php
use yii\widgets\ListView;
use app\modules\ProductCategory\models\ProductCategory;
use yii\widgets\LinkSorter;
use yii\widgets\Pjax;
$this->title = $categoryTitle;
?>
<section class="name-page">
    <div class="container">
        <div>
            <?=$categoryTitle?>
        </div>
    </div>
</section>
<div class="container">
<?php

if($type == ProductCategory::ProductTypeFereastra ){
         echo $this->render('_search_window', ['model' => $searchModel,'cid' => $cid,]);
}elseif($type == ProductCategory::ProductTypeUsa){
        echo $this->render('_search_door', ['model' => $searchModel,'cid' => $cid,]);
    }else {
    echo $this->render('_sorter', ['model' => $searchModel, 'cid' => $cid,]);
}

?>
</div>
<section class="our-recomandation">  <!-- Clasa pentru padding // elementele de jos nu sunt legate de ea -->
    <div class="container">
        <div class="row">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",

                'itemView' => function ($model) use($type) {

                    if($type == ProductCategory::ProductTypeFereastra ){
                        return $this->render('windows/_product_window_item',[
                            'model' => $model,
                        ]);
                    }elseif ($type == ProductCategory::ProductTypeUsa){
                        return $this->render('doors/_product_door_item',[
                            'model' => $model,
                        ]);
                    }else{
                        return $this->render('_product_item',[
                                'model' => $model,
                        ]);
                    }
                },
            ]);
            ?>
        </div>

        <div class="clearfix"></div>
    </div>
</section>
