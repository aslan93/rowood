<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\modules\Product\models\Product;
use yii\widgets\Pjax;
use app\modules\Decoration\models\Decoration;
use app\modules\Material\models\Material;
use app\modules\Color\models\Color;
use yii\web\View;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\ProductCategory\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */

?>

    <?php $form = ActiveForm::begin([
        'action'=> '/shop-page/shop-page/products/?cid='.$cid,
        'method' => 'get',
        'id'=>'sort-form',

    ]); ?>

    <div class="row page-sorter" style="margin-top: 20px; padding-top: 10px;">

        <div class="col-md-4 col-xs-12 col-xs-12">
            <label >Sortare</label>
            <?= $form->field($model, 'sortParam')->dropDownList(Product::getSortParams(),['onchange' => "this.form.submit()",'prompt'=> 'Sortare implicita'])->label(false)?>
        </div>

    </div>


<?php ActiveForm::end(); ?>
