<?php
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\ListView;

$bundle = RowoodAssets::register($this);
$this->title = 'Produse';
?>
<section class="name-page">
    <div class="container">
        <div>
            Produse
        </div>
    </div>
</section>

<section class="our-recomandation">  <!-- Clasa pentru padding // elementele de jos nu sunt legate de ea-->
    <div class="container">
        <div class="row">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,

                'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                'itemView' => function ($model) {
                    return $this->render('_category_item',['model' => $model]);
                },
            ]);
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

