<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\modules\Product\models\Product;
use yii\widgets\Pjax;
use app\modules\Decoration\models\Decoration;
use app\modules\Material\models\Material;
use app\modules\Color\models\Color;
use yii\web\View;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\ProductCategory\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */

$url =  Url::to('@web/uploads/doors-colors/');
$format = <<< SCRIPT
function format(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url' +  state.text
    return '<img class="image" src="' + src + '"/>';
    
}
SCRIPT;
$url2 =  Url::to('@web/uploads/decoration/');
$format2 = <<< SCRIPT
function format2(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url2' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$url3 =  Url::to('@web/uploads/material/');
$format3 = <<< SCRIPT
function format3(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url3' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$escape = new JsExpression("function(m) { return m; }");

$this->registerJs($format, View::POS_HEAD);
$this->registerJs($format2, View::POS_HEAD);
$this->registerJs($format3, View::POS_HEAD);
?>
<style>
    .image{
        width:50%;
        height:50%;
        position: relative;

    }
    .select2-container--krajee .select2-selection--single{
        height: auto;
    }
    .select2-container--krajee .select2-selection--single .select2-selection__arrow{
        border-left: none!important;
    }
</style>


<?php $form = ActiveForm::begin([
    'action'=> '/shop-page/shop-page/products/?cid='.$cid,
    'method' => 'get',
    'id'=>'search-form',

]); ?>

<div class="row" style="margin-top: 20px; padding-top: 10px; ">

    <div class="col-md-2 col-sm-4 col-xs-6">
        <?= $form->field($model, "MaterialID")->widget(Select2::className(),[
            'name' => 'DMaterialID',
            'data' => Material::getImageList(),
            'options' => ['placeholder' => 'Select a material ...'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format3'),
                'templateSelection' => new JsExpression('format3'),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ])->label('Material');//->dropDownList( Material::getList() ,['id'=>'material','prompt'=> 'Select...'])->label('Material'); ?>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
        <?= $form->field($model, "FrontColorID")->widget(Select2::className(),[
            'name' => 'FrontColorID',
            'data' => \app\modules\DoorsColors\models\DoorsColors::getListOfImages(),
            'options' => ['placeholder' => 'Select color ...'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format'),
                'templateSelection' => new JsExpression('format'),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ])->label('Culoarea din fata');//->dropDownList( Color::getList() ,['id'=>'fcolor','prompt'=> 'Select...'])->label('Culoarea din fata'); ?>
    </div>

    <div class="col-md-2 col-sm-4 col-xs-6">
        <?= $form->field($model, "DecorationID")->widget(Select2::className(),[
            'name' => 'DecorationID',
            'data' => Decoration::getImageList(),
            'options' => ['placeholder' => 'Select a decoration ...'],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format2'),
                'templateSelection' => new JsExpression('format2'),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ])->label('Tipul sticlei');//->dropDownList( Decoration::getList() ,['id'=>'decor','prompt'=> 'Select...'])->label('Tipul Sticlei'); ?>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        <label >Sortare</label>
        <?= $form->field($model, 'sortParam')->dropDownList(Product::getSortParams(),['onchange' => "this.form.submit()",'prompt'=> 'Sortare implicita'])->label(false)?>

    </div>
</div>
<div class="row" style="margin-top: 20px; padding-top: 10px;">
    <div class="col-md-12">
        <label ></label>
        <?= Html::submitButton(Yii::t('app', 'Cauta'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
