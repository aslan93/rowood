<?php
use app\views\themes\rowood\assets\RowoodAssets;
use yii\helpers\Url;
use app\modules\ProductCategory\models\ProductCategory;
use yii\bootstrap\Html;

$bundle = RowoodAssets::register($this);
?>
<?php

$link = ['/shop-page/shop-page/config','cid' => $model->ID];
$button = '';
if ($model->Type == ProductCategory::TypeConfigured){
    $button = 'Listati produsele';
    $link = ['/shop-page/shop-page/products','cid' => $model->ID];
}elseif($model->Type == ProductCategory::TypeConfigurable){
    $link = ['/calculator/'];
    $button = 'Configurator';
}else{
    $button = 'Listati produsele';
    $link = ['/shop-page/shop-page/products','cid' => $model->ID];
}
?>
<div class="col-md-4 col-sm-6 col-xs-6">
    <div class="recomandation-box">
        <a href="<?=Url::to($link)?>">
            <div class="img img-responsive">
                <?=Html::img($model->imagePath,['width'=> 250 , 'class'=> 'img-responsive'])?>
            </div>
        </a>
        <div class="title-glass pull-left">
            <?=$model->lang->Title?>
        </div>
        <div class="price pull-right">
             de la  <?php if ($model->minPriceProduct){ echo $model->minPriceProduct;}?> €
        </div>
        <div class="clearfix"></div>
        <div class="type-products">
            <?=$model->Type?>
        </div>

        <a href="<?=Url::to($link)?>" class="btn-primary buy-now">
            <?=$button?>
        </a>
    </div>
</div>
