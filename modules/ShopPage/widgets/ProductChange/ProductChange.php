<?php

namespace app\modules\ShopPage\widgets\ProductChange;

use app\modules\Product\models\Product;
use yii\base\Widget;

class ProductChange extends Widget
{   public $product = false;

    public function run()
   {


       $model = new Product(
           $this->product->getAttributes()
       );
       $model->ID = null;

       return $this->render('index',[
           'product' => $this->product,
           'model' => $model,
       ]);
   }
}