<?php

use yii\bootstrap\Html;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\web\View;

$bundle = RowoodAssets::register($this);

$url =  Url::to('@web/uploads/directions/');
$format = <<< SCRIPT
function format(state) {
if (!state.id) return state.text; // optgroup
src = '$url' +  state.id + '.png'
name = state.text
return '<img class="image" src="' + src + '"/><span>'+name+'</span>';
}
SCRIPT;

$url2 =  Url::to('@web/uploads/doors-handles/');
$format2 = <<< SCRIPT
function format2(state) {
if (!state.id) return state.text;
return state.text;
}
SCRIPT;

$url3 =  Url::to('@web/uploads/doors-colors/');
$format3 = <<< SCRIPT
function format3(state) {
if (!state.id) return state.text; // optgroup

return state.text;
}
SCRIPT;
$format4 = <<< SCRIPT
function format4(state) {
if (!state.id) return state.text; // optgroup
return state.text;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");

$this->registerJs($format, View::POS_HEAD);
$this->registerJs($format2, View::POS_HEAD);
$this->registerJs($format3, View::POS_HEAD);
$this->registerJs($format4, View::POS_HEAD);

?>

<style>
    .image{
        width:50%;
        height:50%;
        position: relative;

    }
    .select2-container--krajee .select2-selection--single{
        height: auto;
    }
    .select2-container--krajee .select2-selection--single .select2-selection__arrow{
        border-left: none!important;
    }
    .plus-price{
        color: #e3210d;
    }
</style>
<div class="col-md-4">
    <div class="about-products">
        <div class="box">
            <div class="title-glass">
                <?=$product->lang->Title?>
            </div>
            <div class="main-price">
                <span data-handle="0" data-color="0" data-size="0"><?=$product->Price ?></span><sup>€ </sup>
            </div>
            <div class="hidden first-price">
                <?=$product->Price ?>
            </div>
        </div>

        <?php Pjax::begin([
            'id'=> 'cart-pjax',
            'enablePushState' => false,
            'enableReplaceState'=>false,
        ])?>
        <?php $form = ActiveForm::begin([
            'action'=> Url::to(['/admin/ecommerce/order/cart/set-product','id' => $product->ID]),
            'method' => 'get',
            'id'=> 'add-to-cart',
            'fieldConfig' =>[
                'options' => [
                    'tag' => false,
                ]
            ],
            'options' => [

                'class' => 'form-add-to-cart',
                'data-pjax' => true,
            ]

        ]); ?>
        <?php
        $pid = 0;
        if ($product->ParentID){
            $pid = $product->ParentID;
        }else{
            $pid = $product->ID;
        }
        ?>

            <div class="col-md-12 col-sm-6 col-xs-6">


                    <?= $form->field($model, "FrontColorID")->widget(Select2::className(),[
                        'name' => 'FrontColorID',
                        'data' => \app\modules\DoorsColors\models\DoorsColors::getImageList(false,$pid),
                        'options' => ['placeholder' => 'Selectati o culoare ...'],
                        'hideSearch' => true,
                        'pluginOptions' => [

                            'templateResult' => new JsExpression('format3'),
                            'templateSelection' => new JsExpression('format3'),
                            'escapeMarkup' => $escape,
                            'allowClear' => true,

                        ],
                        'pluginEvents' => [
                            "change" => "function() { 
                   
                           var newTotalPrice = parseFloat($('.first-price').text());
                           var dataHandle = 0;
                           var dataColor = 0;
                           var dataSize = 0;
                           
                           if(parseFloat($('.main-price>span').attr('data-handle'))){
                               dataHandle = parseFloat($('.main-price>span').attr('data-handle'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-color'))){
                                dataColor = parseFloat($('.main-price>span').attr('data-color'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-size'))){
                                dataSize = parseFloat($('.main-price>span').attr('data-size'));
                           }
                            var itemPrice = 0;
                            if(parseFloat($('.select2-selection__rendered>.color-item').attr('data-attribute'))){
                                itemPrice = parseFloat($('.select2-selection__rendered .color-item').attr('data-attribute'));
                           }
                        
                           $('.main-price>span').attr('data-color',itemPrice);
                           
                           dataColor = parseFloat($('.main-price>span').attr('data-color'));
                           var price = newTotalPrice + dataHandle + dataColor + dataSize;
                            
                           $('.main-price>span').text(price.toFixed(2));
                         }",
                            "select2:unselect" => "function() { 
                            $('.main-price>span').attr('data-color',0);
                            
                        }",


                        ],
                    ])->label('Culoare');//->dropDownList( Material::getList() ,['id'=>'material','prompt'=> 'Select...'])->label('Material'); ?>

            </div>

            <div class="col-md-12 col-sm-6 col-xs-6">

                <?= $form->field($model, "HandleID")->widget(Select2::className(),[
                    'name' => 'HandleID',
                    'data' => \app\modules\DoorsHandles\models\DoorsHandles::getImageList(false,$pid),
                    'options' => ['placeholder' => 'Selectati maner ...'],
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'templateResult' => new JsExpression('format2'),
                        'templateSelection' => new JsExpression('format2'),
                        'escapeMarkup' => $escape,
                        'allowClear' => true,

                    ],
                    'pluginEvents' => [
                        "change" => "function() { 
                           var newTotalPrice = parseFloat($('.first-price').text());
                           var dataHandle = 0;
                           var dataColor = 0;
                           var dataSize = 0;
                           
                           if(parseFloat($('.main-price>span').attr('data-handle'))){
                               dataHandle = parseFloat($('.main-price>span').attr('data-handle'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-color'))){
                                dataColor = parseFloat($('.main-price>span').attr('data-color'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-size'))){
                                dataSize = parseFloat($('.main-price>span').attr('data-size'));
                           }
                            var itemPrice = 0;
                            if(parseFloat($('.select2-selection__rendered>.handle-item').attr('data-attribute'))){
                                itemPrice = parseFloat($('.select2-selection__rendered>.handle-item').attr('data-attribute'));
                           }
                           $('.main-price>span').attr('data-handle',itemPrice);
                           
                           dataHandle = parseFloat($('.main-price>span').attr('data-handle'));
                           var price = newTotalPrice + dataHandle + dataColor + dataSize;
                            
                           $('.main-price>span').text(price.toFixed(2));
                         }",
                        "select2:unselect" => "function() { 
                            $('.main-price>span').attr('data-handle',0);

                        }",
                        ],
                ])->label('Minere');?>

            </div>
        <div class="col-md-12 col-sm-6 col-xs-6">

            <?= $form->field($model, "SizeID")->widget(Select2::className(),[
                'data' => \app\modules\DoorsSizes\models\DoorsSizes::getSpecialList(false,$pid),
                'options' => ['placeholder' => 'Selectati dimensiunile...'],
                'hideSearch' => true,
                'pluginOptions' => [
                    'templateResult' => new JsExpression('format4'),
                    'templateSelection' => new JsExpression('format4'),
                    'escapeMarkup' => $escape,
                    'allowClear' => true,

                ],
                'pluginEvents' => [
                    "change" => "function() { 
                   
                           var newTotalPrice = parseFloat($('.first-price').text());
                           var dataHandle = 0;
                           var dataColor = 0;
                           var dataSize = 0;
                           
                           if(parseFloat($('.main-price>span').attr('data-handle'))){
                               dataHandle = parseFloat($('.main-price>span').attr('data-handle'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-color'))){
                                dataColor = parseFloat($('.main-price>span').attr('data-color'));
                           }
                           if(parseFloat($('.main-price>span').attr('data-size'))){
                                dataSize = parseFloat($('.main-price>span').attr('data-size'));
                           }
                            var itemPrice = 0;
                            if(parseFloat($('.select2-selection__rendered>.size-item').attr('data-attribute'))){
                                itemPrice = parseFloat($('.select2-selection__rendered .size-item').attr('data-attribute'));
                           }
                        
                           $('.main-price>span').attr('data-size',itemPrice);
                           
                           dataSize = parseFloat($('.main-price>span').attr('data-size'));
                           var price = newTotalPrice + dataHandle + dataColor + dataSize;
                            
                           $('.main-price>span').text(price.toFixed(2));
                         }",
                    "select2:unselect" => "function() { 
                            $('.main-price>span').attr('data-size',0);
                            
                        }",

                ],

            ])->label('Dimensiuni');
            ?>
        </div>
        <div class="col-md-12 col-sm-6 col-xs-6">

            <?= $form->field($model, "OpenDirection")->widget(Select2::className(),[
                'data' => \app\modules\Product\models\Product::getDoorOpenDir(),
                'options' => ['placeholder' => 'Select a direction ...'],
                'hideSearch' => true,
                'pluginOptions' => [
                    'templateResult' => new JsExpression('format'),
                    'templateSelection' => new JsExpression('format'),
                    'escapeMarkup' => $escape,
                    'allowClear' => true
                ],

            ])->label('Deschidere');
            ?>
        </div>
        <div class="hidden">
            <?=Select2::widget([
                    'name' => 'hidden',
                    'data' => ['1','2'],
                    'pluginOptions' => [
                            'initSelection' => new JsExpression("function() {
                            $('#product-frontcolorid').trigger('change');
                            $('#product-handleid').trigger('change');
                            $('#product-sizeid').trigger('change');
                            return 1;
                         }"),
                    ]
            ])?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="input-group">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quantity">
                        <span class="glyphicon glyphicon-minus"></span>
                    </button>
                </span>

            <?= Html::input('number','quantity',1,['class' => 'form-control input-number'])?>
            <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quantity">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </span>
        </div>
        <div class="submit-form-details">
            <?=Html::SubmitButton('Comanda acum',['class'=>'btn-primary'])?>
        </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::end()?>

    </div>
</div>
