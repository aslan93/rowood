<?php

namespace app\modules\ShopPage;

use app\components\Module\FrontModule;
/**
 * ShopPage module definition class
 */
class ShopPage extends FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\ShopPage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
