<?php

namespace app\modules\ShopPage\controllers;

use app\modules\Product\models\ProductSearch;
use Yii;
use app\components\Controller\FrontController;
use app\modules\Product\models\WindowsSearch;
use app\modules\Product\models\DoorSearch;
use app\modules\ProductCategory\models\ProductCategory;
use app\modules\Product\models\Product;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `ShopPage` module
 */
class ShopPageController extends FrontController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {   //$category = ProductCategory::find()->all();

        $dataProvider = new ActiveDataProvider([
            'query' => ProductCategory::find(),
            'pagination' => [
                'pageSize' => 9,
            ],
        ]);
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionProducts($cid)
    {   //$products = Product::find()->with('lang')->where(['ProductCategoryID'=>$cid])->all();
        $category = ProductCategory::find()->with('lang')->where(['ID' => $cid])->one();

        $categoryType = '';
        if ($category->ProductType == ProductCategory::ProductTypeFereastra){
            $searchModel = new WindowsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $cid);
            return $this->render('products', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'type' => $category->ProductType,
                'cid' => $cid,
                'categoryTitle' => $category->lang->Title,

            ]);
        }elseif ($category->ProductType == ProductCategory::ProductTypeUsa){
            $searchModel = new DoorSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cid);
            return $this->render('products', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'type' => $category->ProductType,
                'cid' => $cid,
                'categoryTitle' => $category->lang->Title,
            ]);
        }else{
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cid);
//            new ActiveDataProvider([
//            'query' => Product::find()->with('lang')->where(['ProductCategoryID'=>$cid]),
//            'pagination' => [
//                'pageSize' => 9,
//            ],
//        ]);
        return $this->render('products',[
            'dataProvider' => $dataProvider,
            'searchModel'=> $searchModel,
            'type' => $category->ProductType,
            'cid' => $cid,
            'categoryTitle' => $category->lang->Title,
        ]);
        }
    }

    public function actionProduct($id){
        $product = Product::find()->with('lang')->where(['ID'=> $id])->one();
        $type = $product->category->ProductType;
        $images= [];
        if (isset($product->ParentID)){
            $images = $product->parentImages;
        }else{
            $images = $product->images;
        }


        if ($type == ProductCategory::ProductTypeUsa){
            return $this->render('doors/single-product',[
                'product' => $product,
                'type' => $type,
                'images' => $images,
            ]);
        }else{
            return $this->render('single-product',[
                'product' => $product,
                'type' => $type,
            ]);
        }

    }
}
