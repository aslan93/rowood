<?php

namespace app\modules\Fitting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Fitting\models\Fitting;

/**
 * FittingSearch represents the model behind the search form about `app\modules\Fitting\models\Fitting`.
 */
class FittingSearch extends Fitting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Default'], 'integer'],
            [['Image'], 'safe'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fitting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Price' => $this->Price,
            'Default' => $this->Default,
        ]);

        $query->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
