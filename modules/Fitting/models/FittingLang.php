<?php

namespace app\modules\Fitting\models;

use Yii;

/**
 * This is the model class for table "FittingLang".
 *
 * @property integer $ID
 * @property integer $FittingID
 * @property string $LangID
 * @property string $Title
 * @property string $Subtitle
 * @property string $Text
 *
 * @property Fitting $fitting
 */
class FittingLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FittingLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FittingID', 'LangID'], 'required'],
            [['FittingID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'Subtitle'], 'string', 'max' => 255],
            [['FittingID'], 'exist', 'skipOnError' => true, 'targetClass' => Fitting::className(), 'targetAttribute' => ['FittingID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'FittingID' => Yii::t('app', 'Fitting ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Subtitle' => Yii::t('app', 'Subtitle'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFitting()
    {
        return $this->hasOne(Fitting::className(), ['ID' => 'FittingID']);
    }
}
