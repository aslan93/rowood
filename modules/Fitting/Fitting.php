<?php

namespace app\modules\Fitting;

/**
 * fitting module definition class
 */
class Fitting extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Fitting\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
