<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Fitting\models\Fitting */

$this->title = Yii::t('app', 'Create Fitting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fittings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fitting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
