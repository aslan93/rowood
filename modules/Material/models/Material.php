<?php

namespace app\modules\Material\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\modules\Material\models\MaterialLang;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Material".
 *
 * @property integer $ID
 * @property string $Image
 * @property number $StartPrice
 * @property number $Price
 * @property integer $Plastic
 * @property integer $Aluminum
 * @property integer $Wood
 * @property integer $Default
 *
 * @property MaterialLang[] $langs
 * @property bool $isMultiMaterial
 */
class Material extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'StartPrice'], 'required'],
            [['Plastic', 'Aluminum', 'Wood', 'Default'], 'integer'],
            [['StartPrice', 'Price'], 'number', 'min' => 0],
            [['Image'], 'string', 'max' => 255],
            [['Default'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID'         => Yii::t('app', 'ID'),
            'Image'      => Yii::t('app', 'Image'),
            'StartPrice' => Yii::t('app', 'Start Price'),
            'Price'      => Yii::t('app', 'Price m.l.'),
            'Plastic'    => Yii::t('app', 'Plastic'),
            'Aluminum'   => Yii::t('app', 'Aluminum'),
            'Wood'       => Yii::t('app', 'Wood'),
            'Default'       => Yii::t('app', 'Use as default'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(MaterialLang::className(), ['MaterialID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(MaterialLang::className(), ['MaterialID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new MaterialLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }

    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/material/' . $this->Image);
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        if ($this->Default)
        {
            Material::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }
        else
        {
            $hasDefault = Material::find()->where(['Default' => 1])->count();
            
            if (!$hasDefault)
            {
                $this->addError('Default', Yii::t('app', 'Although one material should be installed as default'));
                return false;
            }
        }
        
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->MaterialID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }

    public static function getList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'lang.Title');
        return $return + $result;
    }
    public static function getImageList($addEmpty = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $result = ArrayHelper::map(self::find()->with('lang')->all(), 'ID', 'Image','lang.Title');
        return $return + $result;
    }

    public function getIsMultiMaterial()
    {
        return ($this->Wood + $this->Plastic + $this->Aluminum) > 1;
    }

}
