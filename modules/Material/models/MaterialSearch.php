<?php

namespace app\modules\Material\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Material\models\Material;

/**
 * MaterialSearch represents the model behind the search form about `app\modules\Material\models\Material`.
 */
class MaterialSearch extends Material
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Plastic', 'Aluminum', 'Wood'], 'integer'],
            [['Image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Material::find()->with('lang');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Plastic' => $this->Plastic,
            'Aluminum' => $this->Aluminum,
            'Wood' => $this->Wood,
        ]);

        $query->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
