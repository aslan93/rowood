<?php

namespace app\modules\Material\models;

use Yii;
use app\modules\Material\models\Material;

/**
 * This is the model class for table "MaterialLang".
 *
 * @property integer $ID
 * @property integer $MaterialID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Material $material
 */
class MaterialLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MaterialLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MaterialID', 'LangID', 'Title', 'Text'], 'required'],
            [['MaterialID'], 'integer'],
            [['Text'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['MaterialID'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['MaterialID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MaterialID' => Yii::t('app', 'Material ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['ID' => 'MaterialID']);
    }
}
