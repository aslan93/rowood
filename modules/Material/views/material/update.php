<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Material\models\Material */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Material',
]) . $model->lang->Title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
