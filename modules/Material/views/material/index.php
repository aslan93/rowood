<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Material\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Materials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Material'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::img($model->imagePath, ['height' => 60]);
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'lang.Title'
                ],
                [
                    'attribute' => 'Plastic',
                    'format' => 'boolean',
                ],
                [
                    'attribute' => 'Aluminum',
                    'format' => 'boolean',
                ],
                [
                    'attribute' => 'Wood',
                    'format' => 'boolean',
                ],

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
