<?php

    use yii\bootstrap\Html;
    use kartik\file\FileInput;
    
    $initialPreview = [];
    if ($model->imagePath)
    {
        $initialPreview[] = Html::img($model->imagePath, ['width' => 160]);
    }

?>

<br />
<div class="row">
    <div class="col-md-6">
        <label>Image</label>
        <?= FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ]
        ]) ?>
    </div>
    <div class="col-md-6">
        <label>Materials</label>

        <?= $form->field($model, 'Plastic')->checkbox([
            'class' => 'style-checkbox',
        ]) ?>

        <?= $form->field($model, 'Aluminum')->checkbox() ?>

        <?= $form->field($model, 'Wood')->checkbox() ?>
        
        <?= $form->field($model, 'StartPrice')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
        
        <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
        
        <?= $form->field($model, 'Default')->checkbox() ?>
    </div>
</div>
<br />