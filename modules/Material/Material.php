<?php

namespace app\modules\Material;

/**
 * material module definition class
 */
class Material extends \app\modules\Admin\Admin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Material\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
