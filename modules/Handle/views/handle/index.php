<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Handle\models\HandleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Handles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="glass-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Handle'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>   
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::img($model->imagePath, ['height' => 80]);
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'lang.Title',
                    'filter' => false,
                ],
                'Price',

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
