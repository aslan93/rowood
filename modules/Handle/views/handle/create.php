<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Handle\models\Handle */

$this->title = Yii::t('app', 'Create Handle');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Handlees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="glass-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
