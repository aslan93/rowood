<?php

namespace app\modules\Handle\models;

use Yii;

/**
 * This is the model class for table "HandleLang".
 *
 * @property integer $ID
 * @property integer $HandleID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Handle $glass
 */
class HandleLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HandleLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['HandleID', 'Title', 'LangID'], 'required'],
            [['HandleID'], 'integer'],
            [['Text'], 'string'],
            [['Title'], 'string', 'max' => 255],
            [['LangID'], 'string', 'max' => 2],
            [['HandleID'], 'exist', 'skipOnError' => true, 'targetClass' => Handle::className(), 'targetAttribute' => ['HandleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'HandleID' => Yii::t('app', 'Handle ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHandle()
    {
        return $this->hasOne(Handle::className(), ['ID' => 'HandleID']);
    }
}
