<?php

namespace app\modules\Handle\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Handle".
 *
 * @property integer $ID
 * @property string $Price
 * @property string $Image
 *
 * @property HandleLang $lang
 * @property HandleLang[] $langs
 */
class Handle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Handle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price', 'Image'], 'required'],
            [['Price'], 'number'],
            [['Default'], 'default', 'value' => 0],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Default' => Yii::t('app', 'Default'),
            'Price' => Yii::t('app', 'Price'),
            'Image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(HandleLang::className(), ['HandleID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(HandleLang::className(), ['HandleID' => 'ID'])->indexBy('LangID')->all();

        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new HandleLang([
                'LangID' => $langID,
            ]);
        }

        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/handle/' . $this->Image);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->Default)
        {
            self::updateAll(['Default' => 0], ['<>', 'ID', $this->ID]);
        }

        TagDependency::invalidate(Yii::$app->cache, self::className());

        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->HandleID = $this->ID;
            $langModels[] = $langModel;
        }

        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
