<?php

namespace app\modules\Windowsill\models;

use Yii;
use yii\base\Model;
use yii\caching\TagDependency;

/**
 * This is the model class for table "ConnectionProfile".
 *
 * @property integer $ID
 * @property integer $Price
 *
 * @property AnschraubdichtungLang[] $langs
 */
class Anschraubdichtung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Anschraubdichtung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Price'], 'required'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(AnschraubdichtungLang::className(), ['AnschraubdichtungID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(AnschraubdichtungLang::className(), ['AnschraubdichtungID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new AnschraubdichtungLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->AnschraubdichtungID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
