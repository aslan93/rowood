<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "ConnectionProfileLang".
 *
 * @property integer $ID
 * @property integer $ConnectionProfileID
 * @property string $LangID
 * @property string $Title
 *
 * @property ConnectionProfile $connectionProfile
 */
class AnschraubdichtungLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AnschraubdichtungLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AnschraubdichtungID', 'LangID', 'Title'], 'required'],
            [['AnschraubdichtungID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['AnschraubdichtungID'], 'exist', 'skipOnError' => true, 'targetClass' => Anschraubdichtung::className(), 'targetAttribute' => ['AnschraubdichtungID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'AnschraubdichtungID' => Yii::t('app', 'Anschraubdichtung ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectionProfile()
    {
        return $this->hasOne(Anschraubdichtung::className(), ['ID' => 'AnschraubdichtungID']);
    }
}
