<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "WindowsillWidth".
 *
 * @property integer $ID
 * @property integer $Width
 */
class WindowsillWidth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WindowsillWidth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Width'], 'required'],
            [['Width'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Width' => Yii::t('app', 'Width'),
        ];
    }
    
}
