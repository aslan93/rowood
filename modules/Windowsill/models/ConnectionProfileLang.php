<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "ConnectionProfileLang".
 *
 * @property integer $ID
 * @property integer $ConnectionProfileID
 * @property string $LangID
 * @property string $Title
 *
 * @property ConnectionProfile $connectionProfile
 */
class ConnectionProfileLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConnectionProfileLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ConnectionProfileID', 'LangID', 'Title'], 'required'],
            [['ConnectionProfileID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['ConnectionProfileID'], 'exist', 'skipOnError' => true, 'targetClass' => ConnectionProfile::className(), 'targetAttribute' => ['ConnectionProfileID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ConnectionProfileID' => Yii::t('app', 'Connection Profile ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectionProfile()
    {
        return $this->hasOne(ConnectionProfile::className(), ['ID' => 'ConnectionProfileID']);
    }
}
