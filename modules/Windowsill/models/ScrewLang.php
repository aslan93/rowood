<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "ScrewLang".
 *
 * @property integer $ID
 * @property integer $ScrewID
 * @property string $LangID
 * @property string $Title
 * @property string $Subtitle
 *
 * @property Screw $screw
 */
class ScrewLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ScrewLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ScrewID', 'LangID', 'Title'], 'required'],
            [['ScrewID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'Subtitle'], 'string', 'max' => 255],
            [['ScrewID'], 'exist', 'skipOnError' => true, 'targetClass' => Screw::className(), 'targetAttribute' => ['ScrewID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ScrewID' => Yii::t('app', 'Screw ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Subtitle' => Yii::t('app', 'Subtitle'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScrew()
    {
        return $this->hasOne(Screw::className(), ['ID' => 'ScrewID']);
    }
}
