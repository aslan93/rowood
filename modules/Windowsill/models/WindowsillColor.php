<?php

namespace app\modules\Windowsill\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\caching\TagDependency;

/**
 * This is the model class for table "WindowsillColor".
 *
 * @property integer $ID
 * @property string $Image
 * @property string $Price
 *
 * @property WindowsillColorLang[] $windowsillColorLangs
 */
class WindowsillColor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WindowsillColor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Image', 'Price'], 'required'],
            [['Price'], 'number'],
            [['Image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Image' => Yii::t('app', 'Image'),
            'Price' => Yii::t('app', 'Price'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(WindowsillColorLang::className(), ['WindowsillColorID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLangs()
    {
        $langs = $this->hasMany(WindowsillColorLang::className(), ['WindowsillColorID' => 'ID'])->indexBy('LangID')->all();
        
        $result = [];
        foreach (array_keys(Yii::$app->params['languages']) as $key => $langID)
        {
            $result[$key] = isset($langs[$langID]) ? $langs[$langID] : new WindowsillColorLang([
                'LangID' => $langID,
            ]);
        }
        
        return $result;
    }
    
    public function getImagePath()
    {
        return empty($this->Image) ? '' : Url::to('@web/uploads/windowsill/' . $this->Image);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        TagDependency::invalidate(Yii::$app->cache, self::className());
        
        $langModels = [];
        foreach ($this->langs as $langModel)
        {
            $langModel->WindowsillColorID = $this->ID;
            $langModels[] = $langModel;
        }
        
        if (Model::loadMultiple($langModels, Yii::$app->request->post()) && Model::validateMultiple($langModels))
        {
            foreach ($langModels as $langModel)
            {
                $langModel->save();
            }
        }
    }
    
}
