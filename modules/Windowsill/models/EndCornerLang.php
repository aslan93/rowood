<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "EndCornerLang".
 *
 * @property integer $ID
 * @property integer $EndCornerID
 * @property string $LangID
 * @property string $Title
 * @property string $Material
 * @property string $Size
 *
 * @property EndCorner $endCorner
 */
class EndCornerLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EndCornerLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EndCornerID', 'LangID', 'Title', 'Material', 'Size'], 'required'],
            [['EndCornerID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title', 'Material', 'Size'], 'string', 'max' => 255],
            [['EndCornerID'], 'exist', 'skipOnError' => true, 'targetClass' => EndCorner::className(), 'targetAttribute' => ['EndCornerID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'EndCornerID' => Yii::t('app', 'End Corner ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
            'Material' => Yii::t('app', 'Material'),
            'Size' => Yii::t('app', 'Size'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndCorner()
    {
        return $this->hasOne(EndCorner::className(), ['ID' => 'EndCornerID']);
    }
}
