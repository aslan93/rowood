<?php

namespace app\modules\Windowsill\models;

use Yii;

/**
 * This is the model class for table "WindowsillColorLang".
 *
 * @property integer $ID
 * @property integer $WindowsillColorID
 * @property string $LangID
 * @property string $Title
 *
 * @property WindowsillColor $windowsillColor
 */
class WindowsillColorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WindowsillColorLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['WindowsillColorID', 'LangID', 'Title'], 'required'],
            [['WindowsillColorID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['WindowsillColorID'], 'exist', 'skipOnError' => true, 'targetClass' => WindowsillColor::className(), 'targetAttribute' => ['WindowsillColorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'WindowsillColorID' => Yii::t('app', 'Windowsill Color ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWindowsillColor()
    {
        return $this->hasOne(WindowsillColor::className(), ['ID' => 'WindowsillColorID']);
    }
}
