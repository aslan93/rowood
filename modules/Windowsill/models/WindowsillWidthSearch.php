<?php

namespace app\modules\Windowsill\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;


class WindowsillWidthSearch extends WindowsillWidth
{

    public function rules()
    {
        return [
            ['Width', 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = WindowsillWidth::find();

        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}
