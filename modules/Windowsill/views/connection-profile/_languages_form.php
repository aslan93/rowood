<?php

    use yii\bootstrap\Tabs;
    
    $items = [];
    foreach ($model->langs as $key => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langModel->LangID),
            'content' => $this->render('_language_form', [
                'form' => $form,
                'langModel' => $langModel,
                'key' => $key,
            ]),
            'active' => $key === 0,
        ];
    }

?>

<br />

<?= Tabs::widget([
    'items' => $items,
]) ?>
