<br />

<div class="row">
    <div class="col-md-6">
        
        <?= $form->field($model, 'Price')->textInput(['type' => 'number', 'min' => 0, 'step' => 0.01]) ?>
        
    </div>
</div>
<br />