<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Windowsill\models\WindowsillColor */

$this->title = Yii::t('app', 'Create Windowsill Color');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Windowsill Colors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="windowsill-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
