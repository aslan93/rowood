<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Color\models\ColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pervaz latimi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="row">
        <div class="col-md-6">
            <?php Pjax::begin(['id' => 'add-form-pjax']) ?>
                <?= Html::beginForm('', 'post', ['id' => 'add-form', 'data-pjax' => '']) ?>
                    <div class="row">
                        <div class="col-lg-9">
                            <?= Html::textInput('Width', null, ['required' => true, 'class' => 'form-control']) ?>
                        </div>
                        <div class="col-lg-3">
                            <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-block']) ?>
                        </div>
                    </div>
                <?= Html::endForm() ?>
            <?php Pjax::end() ?>
            <hr />
            <?php Pjax::begin(['id' => 'list-pjax', 'timeout' => 5000]); ?>    
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => [
                                'width' => 50,
                            ]
                        ],
                        'Width',
                        [
                            'class' => 'app\components\GridView\ActionColumn',
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>

<?php $this->registerJs("
    $('#add-form-pjax').on('pjax:success', function(){
        $.pjax.reload({'container': '#list-pjax'});
    });
") ?>