<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Windowsill\models\Screw */

$this->title = Yii::t('app', 'Create Screw');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Screws'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="screw-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
