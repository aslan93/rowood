<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Windowsill\models\EndCorner */

$this->title = Yii::t('app', 'Create End Corner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'End Corners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="end-corner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
