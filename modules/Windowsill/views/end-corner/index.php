<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Windowsill\models\EndCornerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Colors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Color'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(); ?>    
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Image',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::img($model->imagePath, ['height' => 40]);
                    },
                    'filter' => false,
                ],
                'lang.Title',
                'Price',

                ['class' => 'app\components\GridView\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
