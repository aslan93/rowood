<?php

namespace app\modules\Windowsill\controllers;

use Yii;
use app\modules\Windowsill\models\WindowsillWidth;
use app\modules\Admin\controllers\AdminController;
use app\modules\Windowsill\models\WindowsillWidthSearch;


class WindowsillWidthController extends AdminController
{

    public function actionIndex()
    {
        if (Yii::$app->request->isPost)
        {
            $model = new WindowsillWidth();
            $model->Width = Yii::$app->request->post('Width');
            $model->save();
        }
        
        $searchModel = new WindowsillWidthSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDelete($id)
    {
        WindowsillWidth::findOne($id)->delete();
        
        $this->actionIndex();
    }

}
