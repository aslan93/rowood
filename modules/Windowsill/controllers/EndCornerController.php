<?php

namespace app\modules\Windowsill\controllers;

use Yii;
use app\modules\Windowsill\models\EndCorner;
use app\modules\Windowsill\models\EndCornerSearch;
use app\modules\Admin\controllers\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EndCornerController implements the CRUD actions for EndCorner model.
 */
class EndCornerController extends AdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EndCorner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EndCornerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EndCorner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EndCorner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EndCorner();

        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);

            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EndCorner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            $model->Image = $this->saveImage($model);

            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data was saved'));
                return $this->redirect(['update', 'id' => $model->ID]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EndCorner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EndCorner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EndCorner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EndCorner::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function saveImage($model)
    {
        $uploaded = UploadedFile::getInstanceByName('Image');

        if ($uploaded)
        {
            $fileName = md5(microtime(true)) . '.' . $uploaded->extension;
            if ($uploaded->saveAs(Yii::getAlias('@webroot/uploads/windowsill/' . $fileName)))
            {
                return $fileName;
            }
        }

        return $model->Image;
    }

}
