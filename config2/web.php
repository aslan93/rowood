<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ro',
    'bootstrap' => ['log'],
    'defaultRoute' => 'homepage/homepage/index',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Vq2jm4y2bVM-7ahIVF1xuNO0bbgOoikD',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\User\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'forceCopy' => true,
        ], 
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
            'suffix' => '/',
            'languages' => array_keys($params['languages']),
            'enableLanguageDetection' => false,
            'enableDefaultLanguageUrlCode' => false,
        ],
        'view' => [
            'class' => 'yii\web\View',
            'theme' => [
                'basePath' => '@app/views/themes/rowood',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/rowood',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'admin' => [
            'class' => 'app\modules\Admin\Admin',
            'defaultRoute' => 'admin/index',
            'modules' => [
                'ecommerce' => [
                    'class' => 'app\modules\Ecommerce\Ecommerce',
                    'defaultRoute' => 'ecommerce/index',
                    'modules' => [
                        'product-category' => [
                            'class' => 'app\modules\ProductCategory\ProductCategory',
                            'defaultRoute' => 'product-category/index',
                        ],
                        'product' => [
                            'class' => 'app\modules\Product\Product',
                            'defaultRoute' => 'product/index',
                        ],
                        'order' => [
                            'class' => 'app\modules\Order\Order',
                            'defaultRoute' => 'order/index',
                        ],
                        'payment' => [
                            'class' => 'app\modules\Payment\Payment',
                        ],
                    ],
                ],
                'dashboard' => [
                    'class' => 'app\modules\Dashboard\Dashboard',
                    'defaultRoute' => 'dashboard/index',

                ],
                'post' => [
                    'class' => 'app\modules\Post\Post',
                    'defaultRoute' => 'post/index',
                ],
                'seo-post' => [
                    'class' => 'app\modules\SeoPost\SeoPost',
                    'defaultRoute' => 'seo-post/index',
                ],
                'feedback' => [
                    'class' => 'app\modules\Feedback\Feedback',
                    'defaultRoute' => 'feedback/index',
                ],
                'user' => [
                    'class' => 'app\modules\User\User',
                    'defaultRoute' => 'user/index',
                ],
                'doors-handles' => [
                    'class' => 'app\modules\DoorsHandles\DoorsHandles',
                    'defaultRoute' => 'doors-handles/index',
                ],
                'doors-sizes' => [
                    'class' => 'app\modules\DoorsSizes\DoorsSizes',
                    'defaultRoute' => 'doors-sizes/index',
                ],
                'doors-colors' => [
                    'class' => 'app\modules\DoorsColors\DoorsColors',
                    'defaultRoute' => 'doors-colors/index',
                ],
                'material' => [
                    'class' => 'app\modules\Material\Material',
                    'defaultRoute' => 'material/index',
                ],
                'profile' => [
                    'class' => 'app\modules\Profile\Profile',
                    'defaultRoute' => 'profile/index',
                ],
                'color' => [
                    'class' => 'app\modules\Color\Color',
                    'defaultRoute' => 'color/index',
                ],
                'wood' => [
                    'class' => 'app\modules\Wood\Wood',
                    'defaultRoute' => 'wood/index',
                ],
                'category' => [
                    'class' => 'app\modules\Category\Category',
                    'defaultRoute' => 'category/index',
                ],
                'type' => [
                    'class' => 'app\modules\Type\Type',
                    'defaultRoute' => 'type/index',
                ],
                'glass' => [
                    'class' => 'app\modules\Glass\Glass',
                    'defaultRoute' => 'glass/index',
                ],
                'termo-edge' => [
                    'class' => 'app\modules\TermoEdge\TermoEdge',
                    'defaultRoute' => 'termo-edge/index',
                ],
                'soundproofing' => [
                    'class' => 'app\modules\Soundproofing\Soundproofing',
                    'defaultRoute' => 'soundproofing/index',
                ],
                'safety' => [
                    'class' => 'app\modules\Safety\Safety',
                    'defaultRoute' => 'safety/index',
                ],
                'decoration' => [
                    'class' => 'app\modules\Decoration\Decoration',
                    'defaultRoute' => 'decoration/index',
                ],
                'opening' => [
                    'class' => 'app\modules\Opening\Opening',
                    'defaultRoute' => 'opening/index',
                ],
                'windowsill' => [
                    'class' => 'app\modules\Windowsill\Windowsill',
                    'defaultRoute' => 'windowsill-color/index',
                ],
                'fitting' => [
                    'class' => 'app\modules\Fitting\Fitting',
                    'defaultRoute' => 'fitting/index',
                ],
                'insect-screen' => [
                    'class' => 'app\modules\InsectScreen\InsectScreen',
                    'defaultRoute' => 'color/index',
                ],
                'frame-extension' => [
                    'class' => 'app\modules\FrameExtension\FrameExtension',
                    'defaultRoute' => 'frame-extension/index',
                ],
                'handle' => [
                    'class' => 'app\modules\Handle\Handle',
                    'defaultRoute' => 'handle/index',
                ],
                'sprossen' => [
                    'class' => 'app\modules\Sprossen\Sprossen',
                    'defaultRoute' => 'sprossen/index',
                ],
                'jalousie' => [
                    'class' => 'app\modules\Jalousie\Jalousie',
                    'defaultRoute' => 'jalousie/index',
                ],
            ],
        ],
        'login' => [
            'class' => 'app\modules\Login\Login',
            'defaultRoute' => 'login/index',
        ],
        'homepage' => [
            'class' => 'app\modules\Homepage\Homepage',
        ],
        'shop-page' => [
            'class' => 'app\modules\ShopPage\ShopPage',
            'defaultRoute' => 'shop-page/index',
        ],
        'calculator' => [
            'class' => 'app\modules\Calculator\Calculator',
            'defaultRoute' => 'calculator/index',
        ],
        'product' => [
            'class' => 'app\modules\Product\Product',
            'defaultRoute' => 'product/index',
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['77.89.202.10'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
