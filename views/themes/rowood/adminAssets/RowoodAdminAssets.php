<?php

namespace app\views\themes\rowood\adminAssets;

use yii\web\AssetBundle;

class RowoodAdminAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/rowood/adminAssets/files';
    
    public $css = [
        'css/jquery.formstyler.css',
        'css/animate.css',
        'css/main.css',
        '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic-ext,greek-ext,latin-ext',
    ];
    
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyD5pXp7Lol5kF9oPCrWOukTc5k3mNImjwI',
        'js/jquery.mousewheel.js',
        'js/jquery.formstyler.min.js',
        'js/jquery.matchHeight-min.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
    
}
