<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Breadcrumbs;
use kartik\growl\Growl;
use app\views\themes\rowood\assets\RowoodAssets;
use app\modules\Order\widgets\CartInfo\CartInfo;

$bundle = RowoodAssets::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,800,900&amp;subset=latin-ext" rel="stylesheet">
    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/" />
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
    
<?php
    if (Yii::$app->session->getFlash('success'))
    {
        echo Growl::widget([
            'type' => Growl::TYPE_SUCCESS,
            'icon' => 'glyphicon glyphicon-ok-sign',
            'title' => Yii::t('app', 'Success'),
            'showSeparator' => true,
            'body' => Yii::$app->session->getFlash('success')
        ]);
    }
?>
    
    <header>
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-5">
                        <div class="page-nav">
                            <?php echo Nav::widget([
                                'items' => [
                                    ['label' => Yii::t('app', 'Home'), 'url' => ['/']],
                                    ['label' => Yii::t('app', 'Calculator'), 'url' => ['/calculator']],
                                    ['label' => Yii::t('app', 'Contacte'), 'url' => ['/homepage/homepage/contacts/']],
                                    ['label' => Yii::t('app', 'Despre Noi'), 'url' => ['/homepage/homepage/about/']],
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <?=CartInfo::widget()?>
                    </div>
                    <div class="col-md-6 col-sm-5 text-right">
                        <div class="news-and-ofercts-btn">
                            <div class="news-btn">
                                <a href="<?=Url::to(['/homepage/homepage/news/'])?>">
                                    <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 504 504" style="enable-background:new 0 0 504 504;" xml:space="preserve">
                                        <circle style="fill:#FD8469;" cx="252" cy="252" r="252"/>
                                        <rect x="127.484" y="116.409" transform="matrix(-0.9613 -0.2754 0.2754 -0.9613 424.8119 563.6621)" style="fill:#E6E9EE;" width="248.993" height="271.192"/>
                                        <rect x="127.5" y="116.4" style="fill:#FFFFFF;" width="249" height="271.2"/>
                                        <path style="fill:#324A5E;" d="M205.1,186.6h-9.9l-17.9-27.3c-1.1-1.6-1.8-2.8-2.2-3.6H175c0.2,1.5,0.2,3.9,0.2,7v23.9H166v-43.4
                                            h10.5l17.2,26.4c0.8,1.2,1.5,2.4,2.2,3.5h0.1c-0.2-1-0.2-3-0.2-6v-24h9.2v43.5H205.1z"/>
                                        <path style="fill:#324A5E;" d="M241.1,186.6h-26v-43.4h25v8h-15.3v9.7H239v7.9h-14.2v9.9h16.3V186.6z"/>
                                        <path style="fill:#324A5E;" d="M304.9,143.2l-11.5,43.4h-10.8l-7.2-27.9c-0.4-1.5-0.6-3.1-0.7-4.9h-0.1c-0.2,2-0.4,3.6-0.8,4.9
                                            l-7.4,27.9H255l-11.4-43.4h10.7l6.1,28.9c0.3,1.2,0.5,2.9,0.6,5h0.2c0.1-1.6,0.4-3.3,0.9-5.1l7.9-28.8h10.5l7.1,29.2
                                            c0.3,1.1,0.5,2.6,0.7,4.7h0.1c0.1-1.6,0.3-3.2,0.6-4.8l6-29h9.9V143.2z"/>
                                        <path style="fill:#324A5E;" d="M308.4,185v-9.7c1.8,1.5,3.7,2.6,5.7,3.3c2.1,0.7,4.1,1.1,6.2,1.1c1.2,0,2.3-0.1,3.2-0.3
                                            c0.9-0.2,1.7-0.5,2.3-0.9c0.6-0.4,1.1-0.9,1.4-1.4s0.5-1.1,0.5-1.7c0-0.8-0.2-1.6-0.7-2.3s-1.1-1.3-2-1.8c-0.8-0.6-1.8-1.1-3-1.6
                                            s-2.4-1.1-3.7-1.6c-3.4-1.4-5.9-3.1-7.6-5.2c-1.7-2-2.5-4.5-2.5-7.4c0-2.3,0.5-4.2,1.4-5.8s2.1-3,3.7-4s3.4-1.8,5.4-2.3
                                            c2.1-0.5,4.2-0.7,6.5-0.7s4.3,0.1,6,0.4c1.7,0.3,3.4,0.7,4.8,1.3v9.1c-0.7-0.5-1.5-0.9-2.4-1.3c-0.9-0.4-1.7-0.7-2.6-1
                                            s-1.8-0.4-2.7-0.6c-0.9-0.1-1.8-0.2-2.6-0.2c-1.1,0-2.1,0.1-3,0.3s-1.7,0.5-2.3,0.9c-0.6,0.4-1.1,0.8-1.5,1.4
                                            c-0.3,0.5-0.5,1.1-0.5,1.8s0.2,1.4,0.6,2s0.9,1.1,1.6,1.6s1.6,1,2.6,1.5s2.2,1,3.4,1.5c1.7,0.7,3.3,1.5,4.7,2.3
                                            c1.4,0.8,2.6,1.7,3.6,2.8c1,1,1.7,2.2,2.3,3.5c0.5,1.3,0.8,2.9,0.8,4.6c0,2.4-0.5,4.5-1.4,6.1s-2.2,3-3.7,4c-1.6,1-3.4,1.8-5.5,2.2
                                            s-4.3,0.7-6.6,0.7c-2.4,0-4.6-0.2-6.8-0.6C311.8,186.4,309.9,185.8,308.4,185z"/>
                                        <rect x="159.8" y="212.5" style="fill:#324A5E;" width="184.4" height="11.2"/>
                                        <rect x="159.8" y="246.4" style="fill:#324A5E;" width="184.4" height="11.2"/>
                                        <rect x="159.8" y="280.3" style="fill:#324A5E;" width="98.2" height="11.2"/>
                                        <rect x="159.8" y="314.1" style="fill:#324A5E;" width="98.2" height="11.2"/>
                                        <rect x="159.8" y="348" style="fill:#324A5E;" width="98.2" height="11.2"/>
                                        <rect x="274.2" y="280.3" style="fill:#FFD05B;" width="70" height="79"/>
                                    </svg>

                                    <span class="text">
                                        Noutati
                                    </span>
                                </a>
                            </div>
                            <div class="oferts-btn">
                                <a href="#">
                                    <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                       <path style="fill:#FF6464;" d="M256,512C114.844,512,0,397.156,0,256S114.844,0,256,0s256,114.844,256,256S397.156,512,256,512z"/>
                                       <path style="fill:#D2555A;" d="M375.467,426.667c-141.156,0-256-114.844-256-256c0-59.087,20.318-113.41,54.071-156.783
                                       C72.768,48.311,0,143.72,0,256c0,141.156,114.844,256,256,256c82.069,0,155.049-38.974,201.929-99.217
                                       C432.012,421.638,404.342,426.667,375.467,426.667z"/>
                                       <path style="fill:#FFFFFF;" d="M141.241,388.414c-4.518,0-9.038-1.725-12.483-5.173c-6.897-6.892-6.897-18.073,0-24.966
                                           l229.517-229.517c6.888-6.897,18.078-6.897,24.966,0c6.897,6.892,6.897,18.073,0,24.966L153.724,383.241
                                           C150.28,386.689,145.759,388.414,141.241,388.414z"/>
                                       <path style="fill:#FFFFFF;" d="M176.552,238.345c-34.073,0-61.793-27.72-61.793-61.793s27.72-61.793,61.793-61.793
                                           s61.793,27.72,61.793,61.793S210.625,238.345,176.552,238.345z M176.552,150.069c-14.603,0-26.483,11.88-26.483,26.483
                                           s11.88,26.483,26.483,26.483s26.483-11.88,26.483-26.483S191.155,150.069,176.552,150.069z"/>
                                       <path style="fill:#FFFFFF;" d="M335.448,397.241c-34.073,0-61.793-27.72-61.793-61.793s27.72-61.793,61.793-61.793
                                           s61.793,27.72,61.793,61.793S369.522,397.241,335.448,397.241z M335.448,308.966c-14.603,0-26.483,11.88-26.483,26.483
                                           c0,14.603,11.88,26.483,26.483,26.483c14.603,0,26.483-11.88,26.483-26.483C361.931,320.845,350.051,308.966,335.448,308.966z"/>
                                    </svg>
                                    <span class="text">
                                        Oferte
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-header">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="logo">
                        <a href="/">
                            <img src="<?= $bundle->baseUrl ?>/images/logo.png" alt=" alt=">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                    <div class="header-components">
                        <a href="<?=Url::to(['/admin/ecommerce/order/cart/view/'])?>">
                            <span class="fa fa-shopping-basket"></span>
                        </a>
                        <a href="#" class="open-mobile-menu">
                            <span class="fa fa-navicon"></span>
                        </a>
                    </div>
                </div>
            </div>

            <?php echo Nav::widget([
                'items' => [
                    ['label' => Yii::t('app', 'Home'), 'url' => ['/']],
                    ['label' => Yii::t('app', 'Calculator'), 'url' => ['/calculator']],
                    ['label' => Yii::t('app', 'Contacte'), 'url' => ['/homepage/homepage/contacts/']],
                    ['label' => Yii::t('app', 'Despre Noi'), 'url' => ['/homepage/homepage/about/']],
                    ['label' => Yii::t('app', 'Internet Magazin'), 'url' => ['/shop-page/shop-page']],
                    ['label' => Yii::t('app', 'Ferestre'), 'url' => ['/shop-page/shop-page/products/?cid=1/']],
                    ['label' => Yii::t('app', 'Usi din metal'), 'url' => ['/shop-page/shop-page/products/?cid=2/']],
                    ['label' => Yii::t('app', 'Usi din lemn'), 'url' => ['/shop-page/shop-page/products/?cid=4/']],
                    ['label' => Yii::t('app', 'Noutati'), 'url' => ['/homepage/homepage/news/']],
                    ['label' => Yii::t('app', 'Oferte'), 'url' => ['/homepage/#/']],
                ],
            ]); ?>

        </div>
    </header>

    <section class="bottom-header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-12 hidden-xs">
                    <a href="<?=Url::to(['/'])?>">
                        <div class="logo">
                            <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/logo.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="page-navigation">
                        <div class="top-navigaion">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <a href="<?=Url::to('/shop-page/')?>">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                                <!DOCTYPE PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="42.22px" height="42.22px" viewBox="0 0 42.22 42.22" style="enable-background:new 0 0 42.22 42.22;" xml:space="preserve">
                                                    <path d="M33.24,23.641l-1.817-2.151l0.002-0.063l-2.896-0.107l-0.062,1.946l1.705,0.07l0.957,1.149l0.048,4.548v0.979h8.715
                                                        l0.007,0.002l2.054-5.709l0.268-0.664H33.24z M38.53,28.065h-5.416l-0.03-2.476h6.338L38.53,28.065z"/>
                                                    <circle cx="32.647" cy="31.92" r="1.408"/>
                                                    <circle cx="38.359" cy="31.92" r="1.408"/>
                                                    <path d="M33.928,16.846c0.229,0,0.457,0.019,0.684,0.034c0.032-0.226,0.058-0.453,0.069-0.682
                                                        c-0.223-4.134-3.166-7.074-7.297-7.297c-2.424-0.13-4.445,1.107-5.743,2.917c-1.295-1.706-3.311-2.785-5.746-2.917
                                                        c-4.123-0.222-7.09,3.5-7.295,7.297c-0.023,0.488,0.008,0.957,0.084,1.411c-0.258-0.037-0.521-0.062-0.789-0.077
                                                        c-4.457-0.24-7.664,3.784-7.885,7.886c-0.227,4.193,3.32,7.279,7.158,7.813v0.072h19.555c-1.613-1.749-2.607-4.078-2.607-6.646
                                                        C24.116,21.238,28.509,16.846,33.928,16.846z"/>
                                                </svg>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="big-red-text">
                                                    Internet  magazin
                                                </div>
                                                <div class="small-black-text">
                                                    Internet  magazin
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <a href="<?= Url::to(['/calculator']) ?>">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     viewBox="0 0 490.3 490.3" style="enable-background:new 0 0 490.3 490.3;" xml:space="preserve">
                                                    <path d="M303.75,304.1c-4.5,4.5-4.5,11.8,0,16.2l106.3,106.3l9.1-9.1l7.1-7.1l-106.2-106.3
                                                    C315.55,299.6,308.25,299.6,303.75,304.1z"/>
                                                    <path d="M489.95,245.1v-22.9c0-8.5-6.2-15.9-14.6-17.3l-45.6-7.6c-4.5-17.5-11.4-34-20.4-49.3l26.8-37.7c4.9-7,4.1-16.5-1.9-22.5
                                                    l-16.1-16.1l-16.1-16.1c-6-6-15.5-6.8-22.5-1.8l-37.7,26.8c-15.3-9.1-31.8-15.8-49.3-20.4l-7.7-45.6c-1.4-8.5-8.7-14.6-17.3-14.6
                                                    h-22.9h-22.9c-8.2,0.4-15.5,6.6-16.9,14.9l-7.6,45.6c-17.5,4.5-34,11.4-49.3,20.4l-37.7-26.8c-7-4.9-16.5-4.1-22.5,1.9l-16,16.3
                                                    l-16.1,16.1c-6,6-6.8,15.5-1.8,22.5l26.8,37.7c-9.3,15.5-16.3,32.4-20.6,50.5l-45.1,7.7c-8.4,1.4-14.6,8.7-14.6,17.3V247v22.9
                                                    c0,8.5,6.2,15.9,14.6,17.3l46,7.8c4.5,17.5,11.8,34,20.6,49l-27,37.1c-4.9,7-4.1,16.5,1.9,22.5l16.1,16.1l16.1,16.1
                                                    c6,6,15.5,6.8,22.5,1.8l37.9-27c15.3,8.9,31.8,15.6,49.3,20l7.7,45.1c1.5,8.5,8.7,14.6,17.3,14.6h22.9h22.9
                                                    c8.5,0,15.9-6.2,17.3-14.6l7.8-46c4-1,7.9-2.2,11.7-3.5c1-0.3,2.1-0.7,3.1-1.1c3.2-1.1,6.3-2.3,9.4-3.6c0.7-0.3,1.4-0.6,2.1-0.9
                                                    c3.7-1.6,7.3-3.3,10.9-5.1c0.1-0.1,0.2-0.1,0.3-0.2c3.6-1.8,7.2-3.8,10.6-5.8c2.2-1.3,4.4-2.7,6.5-4.1l-58.8-58.8
                                                    c-10.5-10.5-25.1-15.6-40-14.6c-25.2,1.7-51-7-70.2-26.3c-23.3-23.3-31.2-56.3-23.7-86.2c0.5-2.2,3.3-2.9,4.8-1.3l36.7,36.7
                                                    c10.9,10.9,28.9,10.9,39.8-0.1l16.7-16.7c11-11,11-28.9,0.1-39.8l-38.6-38.6c-1.5-1.5-0.9-4.1,1.1-4.8c31.3-10,67-2.7,91.8,22.1
                                                    c20.2,20.2,28.8,47.6,25.9,74c-1.7,15.7,3.4,31.4,14.6,42.6l55.7,55.7c1.8-2.8,3.5-5.6,5.2-8.4c1.9-3.3,3.7-6.6,5.4-10
                                                    c0.5-1,1-2,1.5-3c1.1-2.3,2.1-4.6,3.2-7c0.8-1.8,1.6-3.7,2.3-5.5c0.5-1.2,0.9-2.5,1.4-3.8c1-2.6,1.9-5.2,2.8-7.9l0,0
                                                    c1.1-3.6,2.2-7.1,3.2-10.7l45.6-7.7c8.5-1.4,14.6-8.7,14.6-17.3L489.95,245.1z"/>
                                                </svg>
                                            </div>
                                            <div class="col-md-9 text-right">
                                                <div class="big-red-text">
                                                    Configurator online
                                                </div>
                                                <div class="small-black-text">
                                                    Configurator online
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-navigation">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <a href="<?=Url::to(['/products/?cid=1'])?>">
                                        FERESTRE
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="<?=Url::to(['/products/?cid=2'])?>">
                                        USI DIN METAL
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <a href="<?=Url::to(['/products/?cid=4'])?>">
                                        USI DIN LEMN
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm">
                    <div class="all-contacts">
                        <div class="box">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?= $bundle->baseUrl ?>/images/work-grafic.png" alt="">
                                </div>
                                <div class="col-md-9">
                                    <div class="big-text">
                                        1-23-456-789
                                    </div>
                                    <div class="small-text">
                                        Aliquam tincidunt mauris eu risus.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?= $bundle->baseUrl ?>/images/phone-icon.png" alt="">
                                </div>
                                <div class="col-md-9">
                                    <div class="big-text">
                                        +373-12-345-678
                                    </div>
                                    <div class="small-text">
                                        Lorem ipsum lorem.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box write-now text-center">
                            <a href="<?=Url::to(['/homepage/homepage/contacts/', '#' => 'scrie'])?>">
                                <img src="<?= $bundle->baseUrl ?>/images/write-now.png" alt="">
                                Scrieti-ne!
                            </a>
                        </div>
                        <div class="box socio-link no-border">
                            <ul>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/vk.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/odno.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/twitter.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/facebook.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/instagram.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/youtube.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/in.png" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-bottom"></div>
    </section>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <?= $content ?>


    <footer>
        <div class="footer-nav">
            <div class="container">
                <div class="hide-footer-nav">
                    <a href="#">
                        <span class="fa fa-navicon"></span>
                    </a>
                </div>
                <div class="footer-nav-list">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="logo">
                                <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/logo.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eos et.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eum an.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Feugiat definitiones.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Ne nec.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        An iudico.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Id quas.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Denique molestiae.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Elit philisophia.
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eos et.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eum an.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Feugiat definitiones.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Ne nec.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        An iudico.
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eos et.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eum an.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Feugiat definitiones.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Ne nec.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        An iudico.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Id quas.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Denique molestiae.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Elit philisophia.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        An iudico.
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eos et.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Eum an.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Feugiat definitiones.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        Ne nec.
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-angle-double-right"></i>
                                        An iudico.
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="all-contacts">
                                <div class="box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="<?= $bundle->baseUrl ?>/images/work-grafic2.png" alt="">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="big-text">
                                                1-23-456-789
                                            </div>
                                            <div class="small-text">
                                                Aliquam tincidunt mauris eu risus.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="<?= $bundle->baseUrl ?>/images/phone-icon2.png" alt="">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="big-text">
                                                +373-12-345-678
                                            </div>
                                            <div class="small-text">
                                                Lorem ipsum lorem.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box write-now text-center">
                                    <a href="#">
                                        <img src="<?= $bundle->baseUrl ?>/images/write-now.png" alt="">
                                        Scrieti-ne!
                                    </a>
                                </div>
                                <div class="box socio-link no-border">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/vk.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/odno.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/twitter.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/facebook.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/instagram.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/youtube.png" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= $bundle->baseUrl ?>/images/in.png" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-box">
            <div class="container text-center">
                <div>
                    Copyright © 2017  Creat de <a href="#">Nixap.</a>
                </div>
            </div>
        </div>
    </footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
