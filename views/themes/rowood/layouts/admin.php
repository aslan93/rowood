<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\growl\Growl;

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 , user-scalable=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    
<?php 
    if (Yii::$app->session->getFlash('success'))
    {
        echo Growl::widget([
            'type' => Growl::TYPE_SUCCESS,
            'icon' => 'glyphicon glyphicon-ok-sign',
            'title' => Yii::t('app', 'Success'),
            'showSeparator' => true,
            'body' => Yii::$app->session->getFlash('success')
        ]);
    } 
?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'RoWood',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app', 'Dashboard'), 'url' => ['/admin/dashboard']],
            ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['/admin/feedback/feedback/'],'options'=>['class'=>'feedb']],
            [
                'label' => Yii::t('app', 'Posts'),
                'items' => [
                    ['label' => Yii::t('app', 'News'), 'url' => ['/admin/post/post/']],
                    ['label' => Yii::t('app', 'Seo Posts'), 'url' => ['/admin/seo-post/seo-post/']],
                ]
            ],
            [
                'label' => Yii::t('app', 'Magazin'),
                'items' => [
                    ['label' => Yii::t('app', 'Produse'), 'url' => ['/admin/ecommerce/product']],
                    ['label' => Yii::t('app', 'Categorii de produse'), 'url' => ['/admin/ecommerce/product-category']],
                    ['label' => Yii::t('app', 'Comenzi'), 'url' => ['/admin/ecommerce/order']],
                    ['label' => Yii::t('app', 'Discount'), 'url' => ['/admin/ecommerce/discount']],
                ]
            ],
            [
                'label' => 'Ferestre',
                'items' => [
                    [
                        'label' => Yii::t('app', 'Materiale, profile'),
                        'items' => [
                            ['label' => Yii::t('app', 'Materiale'), 'url' => ['/admin/material']],
                            ['label' => Yii::t('app', 'Profile'), 'url' => ['/admin/profile']],
                            ['label' => Yii::t('app', 'Culori'), 'url' => ['/admin/color']],
                            ['label' => Yii::t('app', 'Lemn'), 'url' => ['/admin/wood']],
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Categorii, tipuri'),
                        'items' => [
                            ['label' => Yii::t('app', 'Categorii'), 'url' => ['/admin/category']],
                            ['label' => Yii::t('app', 'Tipuri'), 'url' => ['/admin/type']],
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Sticlă'),
                        'items' => [
                            ['label' => Yii::t('app', 'Sticlă'), 'url' => ['/admin/glass']],
                            ['label' => Yii::t('app', 'Termo edge'), 'url' => ['/admin/termo-edge']],
                            ['label' => Yii::t('app', 'Izolare fonica'), 'url' => ['/admin/soundproofing']],
                            ['label' => Yii::t('app', 'Sticlă de siguranță'), 'url' => ['/admin/safety']],
                            ['label' => Yii::t('app', 'Sticlă decorativă'), 'url' => ['/admin/decoration']],
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Pervaz'),
                        'items' => [
                            ['label' => Yii::t('app', 'Culori'), 'url' => ['/admin/windowsill/windowsill-color']],
                            ['label' => Yii::t('app', 'Pervaz lățime'), 'url' => ['/admin/windowsill/windowsill-width']],
                            ['label' => Yii::t('app', 'End corners'), 'url' => ['/admin/windowsill/end-corner']],
                            ['label' => Yii::t('app', 'Screws'), 'url' => ['/admin/windowsill/screw']],
                            ['label' => Yii::t('app', 'Connection profiles'), 'url' => ['/admin/windowsill/connection-profile']],
                            ['label' => Yii::t('app', 'Anschraubdichtung'), 'url' => ['/admin/windowsill/anschraubdichtung']],
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Ecran de insecte'),
                        'items' => [
                            ['label' => Yii::t('app', 'Culori'), 'url' => ['/admin/insect-screen/insect-screen-color']],
                            ['label' => Yii::t('app', 'Pânză'), 'url' => ['/admin/insect-screen/cloth']],
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Jalousie'),
                        'items' => [
                            ['label' => Yii::t('app', 'Jalousie'), 'url' => ['/admin/jalousie']],
                            ['label' => Yii::t('app', 'Jalousie models'), 'url' => ['/admin/jalousie/model']],
                            ['label' => Yii::t('app', 'Model sizes'), 'url' => ['/admin/jalousie/model-size']],
                            ['label' => Yii::t('app', 'Opening mechanism'), 'url' => ['/admin/jalousie/opening-mechanism']],
                            ['label' => Yii::t('app', 'Mechanism options'), 'url' => ['/admin/jalousie/mechanism-option']],
                            ['label' => Yii::t('app', 'Rolls materials'), 'url' => ['/admin/jalousie/roll-material']],
                            ['label' => Yii::t('app', 'Rolls colors'), 'url' => ['/admin/jalousie/roll-color']],
                        ],
                    ],
                    [
                        'label' => Yii::t('app', 'Alte'),
                        'items' => [
                            ['label' => Yii::t('app', 'Mânere'), 'url' => ['/admin/handle/handle']],
                            ['label' => Yii::t('app', 'Mecanismul de deschidere'), 'url' => ['/admin/opening']],
                            ['label' => Yii::t('app', 'Fitting'), 'url' => ['/admin/fitting']],
                            ['label' => Yii::t('app', 'Sprosuri'), 'url' => ['/admin/sprossen']],
                            ['label' => Yii::t('app', 'Extinderea cadrului'), 'url' => ['/admin/frame-extension']],
                        ],
                    ],
                ]
            ],
            [
                'label' => 'Usi',
                'items' => [
                    [
                        'label' => Yii::t('app', 'Detalii'),
                        'items' => [
                            ['label' => Yii::t('app', 'Culori'), 'url' => ['/admin/doors-colors']],
                            ['label' => Yii::t('app', 'Minere'), 'url' => ['/admin/doors-handles']],
                            ['label' => Yii::t('app', 'Marimi'), 'url' => ['/admin/doors-sizes']],
                        ]
                    ],

                ]
            ],
            Yii::$app->user->isGuest ? ['label' => 'Login', 'url' => ['/login']] : ['label' => 'Logout', 'url' => ['/login/login/logout']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
    
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; RoWood <?= date('Y') ?></p>
        <p class="pull-right">Created by NIXAP</p>
    </div>
</footer>

<style>
    
    .dropdown-menu {
        height: 70vh;
        overflow-y: scroll;
    }
    
</style>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
