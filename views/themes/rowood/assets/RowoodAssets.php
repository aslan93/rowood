<?php

namespace app\views\themes\rowood\assets;

use yii\web\AssetBundle;

class RowoodAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/rowood/assets/files';
    
    public $css = [
        'js/owl-slider/dist/assets/owl.carousel.css',
        'js/swiper-slider/css/swiper.css',
        'css/select2.min.css',
        'css/main.css',
        
    ];
    
    public $js = [
        'js/owl-slider/dist/owl.carousel.js',
        'js/owl-slider/dist/owl.carousel2.thumbs.js',
        'js/swiper-slider/js/swiper.js',
        'js/jquery.matchHeight-min.js',
        'js/jquery.pixlayout.0.9.7.min.js',
        'js/select2.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
    
}
