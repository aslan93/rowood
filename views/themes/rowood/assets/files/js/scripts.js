


$(document).ready(function(){

    /* EGAL HEIGHT BOX 1*/
    function applyMathcheight1() {

        if ($(window).width() > 768) {
            $(".recomandation-box").matchHeight();
        }
    }

    applyMathcheight1();


    /* EGAL HEIGHT BOX 1*/
    function applyMathcheight2() {

        if ($(window).width() > 768) {
            $(".news-box").matchHeight();
        }
    }

    applyMathcheight2();



    /* EGAL HEIGHT BOX 3*/
    function applyMathcheight3() {

        if ($(window).width() > 768) {
            $(".color").matchHeight();
        }
    }

    applyMathcheight3();


    /* EGAL HEIGHT BOX 4*/
    function applyMathcheight4() {

        if ($(window).width() > 768) {
            $(".type-glass").matchHeight();
        }
    }

    applyMathcheight4();


    /* EGAL HEIGHT BOX 5*/
    function applyMathcheight5() {

        if ($(window).width() > 768) {
            $(".piese").matchHeight();
        }
    }

    applyMathcheight5();








//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/{
    $('.btn-number').click(function(e){
        e.preventDefault();

        var fieldName = $(this).attr('data-field');
        var type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                var minValue = parseInt(input.attr('min'));
                if(!minValue) minValue = 1;
                if(currentVal > minValue) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == minValue) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {
                var maxValue = parseInt(input.attr('max'));
                if(!maxValue) maxValue = 9999999999999;
                if(currentVal < maxValue) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == maxValue) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('max'));
        if(!minValue) minValue = 1;
        if(!maxValue) maxValue = 9999999999999;
        var valueCurrent = parseInt($(this).val());

        var name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });



    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: true,
        items: 1,
        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item'
    });



    $(".btn-primary").click(function(){
        $(this).closest("label").prev('input[type=radio]').prop('checked' , true);
    });


    $('.hide-footer-nav > a').on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("active");
        $(".footer-nav-list").slideToggle();
    });

    $('.hide-header-nav > a').on('click' , function(e){
        e.preventDefault();
        $(this).closest(".hide-header-nav").next(".page-navigation").slideToggle();
    });


    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 5,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });

    $(".open-mobile-menu").click(function(){
       $(this).closest(".mobile-header").find("ul").slideToggle();
    });

    $(window).on('load resize', function () {
        $(".recomandation-box .title-glass").matchHeight();
        $(".material").matchHeight();
        $(".form-panel-content .panel-input").matchHeight();
        $(".panel-input").matchHeight();
        $(".panel-input.profil").matchHeight();
        $(".panel-input.color").matchHeight();
        $("label.panel-input .title").matchHeight();
    });

    $(".panel-input.profil .material").each(function(){
       if($(this).length == 0) {
           $(this).hide();
       }
    });

    $(".cart-wrap-item .product-details .show-more-details").click(function(){
       $(this).toggleClass("open").css({
           "margin-top": "28px"
       });
       $(this).find(".default-text").toggleClass("hide");
       $(this).find(".active-text").toggleClass("hide");
       $(this).closest(".product-details").toggleClass("is-open");
    });

    if($(".login , .register").length > 0){
        $(".cart-wrap-item").css({
           "margin-bottom" : "45px",
        });
    }

    $(".style-select").select2({
        minimumResultsForSearch: -1
    });

    if($("#search-form , #sort-form").length > 0){
        $('.our-recomandation').css({
           "padding" : "30px 0"
        });
    }

});



