<?php

namespace app\components\Module;

use Yii;
use yii\base\Module as YiiModule;

class FrontModule extends YiiModule
{

    public function init()
    {
        parent::init();
        
        $this->layoutPath = Yii::getAlias('@app/views/themes/rowood/layouts');
    }

}
