<?php

namespace app\components\PriceConverter;

class PriceConverter
{
    
    public static function convert($price, $precision = 2)
    {
        return number_format($price, $precision) . ' &euro;';
    }
    
}
