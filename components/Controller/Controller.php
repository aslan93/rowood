<?php

namespace app\components\Controller;

use Yii;
use yii\web\Controller as YiiController;

class Controller extends YiiController
{
    
    public function init()
    {
        parent::init();
        
        Yii::$container->set(\yii\data\Pagination::className(), [
            'defaultPageSize' => 10,
        ]);
        
        Yii::$container->set(\yii\widgets\Pjax::className(), [
            'timeout' => 5000,
        ]);
        
        Yii::$container->set(\yii\grid\ActionColumn::className(), [
            'buttons' => [
                'edit' => function ($url) {
                    return Html::a(
                        '<span class="fa fa-pencil"></span>',
                        $url, 
                        [
                            'title' => 'Update',
                            'data-pjax' => '0',
                        ]
                    );
                },
            ],
        ]);
    }
    
}
