<?php

namespace app\components\Controller;

use Yii;

class FrontController extends Controller
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = 'front';
        
        Yii::$container->set('yii\widgets\Pjax', [
            'timeout' => 5000,
        ]);
    }
    
}