<?php

namespace app\components\GridView;

use kartik\grid\GridView as KartikGridView;

class GridView extends KartikGridView
{
    
    public $tableOptions = ['class' => 'table table-hover table-condensed'];
    
}
