<?php

namespace app\components\GridView;

use kartik\grid\ActionColumn as KartikActionColumn;

class ActionColumn extends KartikActionColumn
{
    
    public $template = '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}';
    
    public $options = [
        'width' => '100px',
        'class' => 'grid-action-column'
    ];
    
}
