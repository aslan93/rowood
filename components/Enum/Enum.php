<?php

namespace app\components\Enum;

abstract class Enum
{

    /**
     * @return array
     */
    public static function items(){}

    public static function getList($addEmpty = false)
    {
        $result = $addEmpty ? ['' => $addEmpty] : [];
        $items = static::items();
        return $result + $items;
    }

}
