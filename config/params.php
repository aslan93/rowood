<?php

return [
    'adminEmail' => 'admin@example.com',
    'languages' => [
        'ro' => [
            'name' => 'ro',
        ],
        'en' => [
            'name' => 'en',
        ],
    ],
    'displayDateFormat' => 'd.m.Y',
    'dateTimeFormatPHP' => 'd F Y ',
    'dateTimeFormatJS' => 'dd.mm.yyyy',
];
